"""
Plot maps of bias, RMS and correlations between salinity  datasets. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import xesmf as xe
import iris
import datetime
from iris.time import PartialDateTime
import cf_units
import glob
import scipy.stats
import os
import sys
import subprocess
# For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

# Get git info for this script.
sys.path.append(os.path.expanduser('~/Documents/code/'))
from python_git_tools import git_rev_info
[txtl, last_hash, rel_path, clean] = \
    git_rev_info(os.path.realpath(__file__))
    
###########################################


###### Load data. #################################################

# Specify file locations.
SMOS_filename = '~/Data/satellite/SMOS/SMOS_1x1deg_timeseries.nc'
SMAP_filename = '~/Data/satellite/SMAP/SMAP_1x1deg_timeseries.nc'
aq_filename = '~/Data/satellite/aquarius/aquarius_1x1deg_timeseries.nc'

plot_dir = os.path.expanduser('~/Documents/plots/AmazonCongo/')

# Specify time period for comparison.
st_time_aq = np.datetime64('2011-09-01')
end_time_aq = np.datetime64('2015-05-31')
st_time_SMAP = np.datetime64('2015-04-01')
end_time_SMAP = np.datetime64('2017-12-31')


##### Read data. ###########################################

print('Reading data...')

ds_SMOS = xr.open_dataset(SMOS_filename)
ds_SMAP = xr.open_dataset(SMAP_filename)
ds_aq = xr.open_dataset(aq_filename)

sss_SMOS = ds_SMOS.SSS.load()
sss_SMAP = ds_SMAP.SSS.load()
sss_aq = ds_aq.SSS.load()



###### Calculate bias and RMS. ###############################################

# Bias (or really difference, as SMOS isn't "truth").
bias_SMAP = sss_SMAP.loc[dict(time=slice(st_time_SMAP, end_time_SMAP))].\
            mean('time')- \
            sss_SMOS.loc[dict(time=slice(st_time_SMAP, end_time_SMAP))].\
            mean('time')
bias_aq = sss_aq.loc[dict(time=slice(st_time_aq, end_time_aq))].\
          mean('time')- \
          sss_SMOS.loc[dict(time=slice(st_time_aq, end_time_aq))].\
          mean('time')

# RMSD (first calculate squared difference time series then take
# the time mean and square root).
SD_SMAP = sss_SMOS.loc[dict(time=slice(st_time_SMAP, end_time_SMAP))]
SD_SMAP.data =sss_SMAP.loc[dict(time=slice(st_time_SMAP,
                                           end_time_SMAP))].data - \
               sss_SMOS.loc[dict(time=slice(st_time_SMAP,
                                            end_time_SMAP))].data
SD_SMAP.data = np.square(SD_SMAP.data)
RMSD_SMAP = SD_SMAP.mean('time')
RMSD_SMAP.data = np.sqrt(RMSD_SMAP.data)
#
SD_aq = sss_SMOS.loc[dict(time=slice(st_time_aq, end_time_aq))]
SD_aq.data = sss_aq.loc[dict(time=slice(st_time_aq, end_time_aq))].data - \
             sss_SMOS.loc[dict(time=slice(st_time_aq, end_time_aq))].data
SD_aq.data = np.square(SD_aq.data)
RMSD_aq = SD_aq.mean('time')
RMSD_aq.data = np.sqrt(RMSD_aq.data)

# De-biased RMSD.
dbSD_SMAP = SD_SMAP
dbSD_SMAP.data = sss_SMAP.loc[dict(time=slice(st_time_SMAP,
                                              end_time_SMAP))].data - \
                 sss_SMOS.loc[dict(time=slice(st_time_SMAP,
                                              end_time_SMAP))].data
dbSD_SMAP = dbSD_SMAP - bias_SMAP
dbSD_SMAP.data = np.square(dbSD_SMAP.data)
dbRMSD_SMAP = dbSD_SMAP.mean('time')
dbRMSD_SMAP.data = np.sqrt(dbRMSD_SMAP.data)
#
dbSD_aq = SD_aq
dbSD_aq.data = sss_aq.loc[dict(time=slice(st_time_aq,
                                          end_time_aq))].data - \
               sss_SMOS.loc[dict(time=slice(st_time_aq,
                                            end_time_aq))].data
dbSD_aq = dbSD_aq - bias_aq
dbSD_aq.data = np.square(dbSD_aq.data)
dbRMSD_aq = dbSD_aq.mean('time')
dbRMSD_aq.data = np.sqrt(dbRMSD_aq.data)


###### Calculate correlations. ###############################################

print('Calculating correlations...')

# Define function.
corr_map = np.vectorize(scipy.stats.pearsonr,signature='(n),(n)->(),()')

# Define dataarrays to hold answer.
r_SMAP = xr.DataArray(np.zeros((len(sss_SMAP.latitude),
                                len(sss_SMAP.longitude))),
                      coords=[('latitude', sss_SMAP.latitude),
                              ('longitude', sss_SMAP.longitude)])
r_aq = xr.DataArray(np.zeros((len(sss_aq.latitude),
                              len(sss_aq.longitude))),
                    coords=[('latitude', sss_aq.latitude),
                            ('longitude', sss_aq.longitude)])

# Do the calculation.
(r_SMAP.data,dummy) = corr_map(
    sss_SMAP.loc[dict(time=slice(st_time_SMAP, end_time_SMAP))]\
    .transpose('latitude','longitude','time').data,
    sss_SMOS.loc[dict(time=slice(st_time_SMAP, end_time_SMAP))]\
    .transpose('latitude','longitude','time').data)

(r_aq.data,dummy) = corr_map(
    sss_aq.loc[dict(time=slice(st_time_aq, end_time_aq))]\
    .transpose('latitude','longitude','time').data,
    sss_SMOS.loc[dict(time=slice(st_time_aq, end_time_aq))]\
    .transpose('latitude','longitude','time').data)


###### Plot  maps. ###############################################

print('Plotting...')

# Define color maps.
cmap_r = plt.get_cmap('RdBu_r')
cmap_r.set_bad(color='lightgrey')
norm_list = [colors.BoundaryNorm(np.arange(-0.5, 0.51, 0.1),
                                 ncolors=cmap_r.N),
             colors.BoundaryNorm(np.arange(0.0, 1.01, 0.1),
                                 ncolors=cmap_r.N),
             colors.BoundaryNorm(np.arange(0.0, 1.01, 0.1),
                                 ncolors=cmap_r.N),
             colors.BoundaryNorm(np.arange(0.0, 1.01, 0.1),
                                 ncolors=cmap_r.N)]

# 2d lat and lon arrays for plotting.
dlatm, dlonm = np.meshgrid(sss_aq.latitude,sss_aq.longitude,indexing='ij')

# Define map projection.
projection = ccrs.PlateCarree()
axes_class = (GeoAxes,
              dict(map_projection=projection))

# Open figure.
fig = plt.figure(figsize=(12, 6))

# Define number of rows and columns in plot.
n_r_ax = 2
n_c_ax = 4

# Define axes of different panels.
axgr = AxesGrid(fig, 111, axes_class=axes_class,
                nrows_ncols=(n_r_ax, n_c_ax),
                axes_pad=(0.25,0.5),
                cbar_location='bottom',
                cbar_mode='edge',
                cbar_pad=0.4,
                cbar_size='3%',
                direction='row',
                label_mode='')  # note the empty label_mode

# Loop over different variables and plot maps.
var_list = [bias_SMAP,RMSD_SMAP,dbRMSD_SMAP,r_SMAP,
            bias_aq,RMSD_aq,dbRMSD_aq,r_aq]
titles = ['Difference\nSMAP - SMOS', 'RMSD\n',
          'de-biased RMSD\n', 'correlation\n',
          'Aquarius - SMOS', '',
          '', '']
max_vals = [ 0.5, 1.0, 1.0,  1.0,  0.5, 1.0, 1.0,  1.0]
min_vals = [-0.5, 0.0, 0.0, -1.0, -0.5, 0.0, 0.0, -1.0]
cbar_labels = ['PSU','PSU','PSU','-']
for i, ax in enumerate(axgr):
    ax.set_extent([-70, 0, -20, 25], projection)
    ax.coastlines()
    ax.add_feature(cfeature.RIVERS,edgecolor='dodgerblue')
    ax.add_feature(cfeature.LAKES,edgecolor='dodgerblue',
                   facecolor='dodgerblue')
    ax.add_feature(cfeature.LAND, facecolor='lightgray')
    ax.set_title(titles[i])
    ax.set_xticks(np.array([-60, -45, -30, -15]), crs=projection)
    ax.set_yticks(np.array([-15, 0, 15]), crs=projection)
    lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                       degree_symbol='')
    lat_formatter = LatitudeFormatter(degree_symbol='')
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    p = ax.pcolormesh(dlonm, dlatm, var_list[i],
                      transform=projection,
                      vmin=min_vals[i], vmax=max_vals[i],
                      cmap=cmap_r, norm=norm_list[i%4])
    if i > (n_c_ax-1):
        cbar = axgr.cbar_axes[i-n_c_ax].colorbar(p)
        cbar.set_label_text(cbar_labels[i-n_c_ax])
        
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)

# Add git info to footer.
anchored_text = AnchoredText(txtl, loc=3,borderpad=0,frameon=False,
                             prop=dict(size=5),
                             bbox_to_anchor=(0., 0.),
                             bbox_transform=fig.transFigure) #ax.transAxes)
axgr[4].add_artist(anchored_text)

# Display the plot
#plt.show()

# Save the figure.
plt.savefig(plot_dir +
            os.path.splitext(os.path.basename(__file__))[0] +
            '.png',
            format='png')
#           format='png', dpi=900
#####
