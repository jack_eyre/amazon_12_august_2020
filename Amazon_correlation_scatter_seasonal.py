"""
Plot correlation coefficient of streamflow with salinity from different combinations of seasonal average data.

Run it from plotenv conda environment.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################

################################################################################
def main():
    
    ##### Read hydrology data. ###########################################
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Fill gaps in streamflow (inserts nan for missing months)
    ro_ob = ro_ob.resample(time='1M').asfreq()
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    
    # Get factor to correct Obidos streamflow to mouth.
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    ob_to_mouth = ds_DT.ratio_m2s[0].data
    
    # Scale up Obidos streamflow to river mouth.
    ro_DT = ro_ob.copy()
    ro_DT = ro_DT*ob_to_mouth
    
    # Get other water budget variables (custom load functions below).
    # All in m3 s-1.
    budget_Amazon = get_budget_vars('Amazon')
    #
    # Add streamflow to dictionary.
    budget_Amazon['streamflow'] = ro_ob
    
    
    ##### Read salinity data. ###########################################
    
    # Define region to average over.
    sal_region = np.array([0.0,3.0,-52.5,-47.5])
    sal_st_time = '2010-02-01'
    sal_end_time = '2017-12-31'
    
    # Open and read file.
    sal_filename_wildcard = os.path.expanduser('~') +\
                            '/Data/satellite/SMOS/' + \
                            'SMOS_L3_DEBIAS_LOCEAN_AD_*' + \
                            '_EASE_09d_25km_v03_monthly.nc'
    ds_sal = xr.open_mfdataset(sal_filename_wildcard, concat_dim='time')
    # Don't get the first (incomplete) month.
    sss = ds_sal.SSS.sel(time=slice(sal_st_time,sal_end_time),
                         lat=slice(sal_region[0],sal_region[1]),
                         lon=slice(sal_region[2],sal_region[3]))\
                         .mean(['lat','lon']).load()
    
    ##### Read P-E data. ###############################################
    
    # Open files.
    pr_ds = xr.open_dataset(os.path.expanduser('~') +\
                            '/Data/satellite/GPCP/' +
                            'GPCP.v2.3.precip.mon.mean.nc')
    evap_ds = xr.open_mfdataset(os.path.expanduser('~') +\
                                '/Data/reanalysis/ERA5/' +
                                'era5_moda_sfc_' + '*.nc')
    
    # Read data (same region and period as salinity).
    # (Have to account for longitude format (0,360) -> (-180,180).)
    pr_local = pr_ds.precip.sel(time=slice(sal_st_time,sal_end_time),
                                lat=slice(sal_region[0],sal_region[1]),
                                lon=slice(sal_region[2]+360.0,
                                          sal_region[3]+360.0)).load()
    evap_local = evap_ds.e.sel(time=slice(sal_st_time,sal_end_time),
                               latitude=slice(sal_region[1],
                                              sal_region[0]),
                               longitude=slice(sal_region[2]+360.0,
                                               sal_region[3]+360.0)).load()
    
    # Just evaporation needs changing.
    # It already has an implicit "per day" so just
    # needs conversion from m to mm.
    # Also needs change of sign (from positive to negative).
    evap_local = evap_local*(-1000.0)
    evap_local.attrs['units'] = 'mm/day'
    
    # Calculate area averaged P-E.
    PmE_local = pr_local.mean(['lat','lon']) - \
                evap_local.mean(['latitude','longitude']).load()
    #
    # Add to data dictionary.
    budget_Amazon['P-E_local'] = PmE_local
    #
    #
    # Print out useful information..
    pr_l = pr_ds.precip.sel(lat=slice(sal_region[0],sal_region[1]),
                            lon=slice(sal_region[2]+360.0,
                                      sal_region[3]+360.0)).\
                                      load().mean(['lat','lon'])
    pr_a = budget_Amazon['pr_GPCP'].copy()
    dis_o = ro_ob.copy()
    if 0:
        print('---------- P-E local ----------')
        print('Mean:')  
        print(PmE_local.mean())
        print('Mean absolute:')
        print(abs(PmE_local).mean())
        print('Max:')   
        print(max(PmE_local))
        print('Min:')   
        print(min(PmE_local))
        print('All:')   
        print(PmE_local)
        print('---------- Amazon P vs local P correlation: ----------')
        print('local precip leads Amazon precip')
        print(corr_stats(pr_l, pr_a))
        print('Amazon precip leads local precip')
        print(corr_stats(pr_a, pr_l))
        print('---------- Discharge vs local P correlation: ----------')
        print('Local precip leads Obidos discharge')
        print(corr_stats(pr_l, dis_o))
        print('Obidos discharge leads local precip')
        print(corr_stats(dis_o, pr_l))
    
    ##### Calculate correlations. ###########################################
    
    print('Processing...')
    
    # Define maximum time lag for correlations.
    max_lag = 1
    
    # Calculate correlations of streamflow estimates with salinity.
    corrs_Amazon = corrs_all(budget_Amazon, sss, max_lag)
    
    # Print out results for combinations that meet a criterion.
    if 0:
        print('Correlations (anomaly, lag-0) larger than Obidos streamflow:')
        for comb in corrs_Amazon['seasonal']:
            if (abs(corrs_Amazon['seasonal'][comb]['anom_lag0']) >=
                abs(corrs_Amazon['seasonal']['Obidos']['anom_lag0'])):
                print(comb + ':     ' +
                      str(corrs_Amazon['seasonal'][comb]['anom_lag0']))
        print('Correlations (absolute, lag-0) larger than Obidos streamflow:')
        for comb in corrs_Amazon['seasonal']:
            if (abs(corrs_Amazon['seasonal'][comb]['abs_lag0']) >=
                abs(corrs_Amazon['seasonal']['Obidos']['abs_lag0'])):
                print(comb + ':     ' +
                      str(corrs_Amazon['seasonal'][comb]['abs_lag0']))
    
    ##### Plot results. ###########################################
    
    print('Plotting...')
    plot = plot_corrs(corrs_Amazon)


################################################################################

################################################################################
# Plotting function.

def plot_corrs(data_dict):
    #
    # Define font details.
    plt.rcParams.update({'font.size': 14})
    #
    # Define width of scatter in x-direction.
    xs = 0.5
    
    # Specify a few details in advance.
    panel_list = ['abs', 'anom']
    panel_names = ['absolute', 'anomaly']
    time_lags = list(range(0,data_dict['max_lag']+1))
    special_colors = {'Obidos': '#268bd2',
                      'P_GPCP': '#dc322f',
                      'P_CMAP': '#b58900',
                      'P-E_local': '#6c71c4'}
    special_colors2 = {'PEG_Ens':'#d33682',
                       'CHG_Ens':'#6c71c4',
                       #'all_Ens':'#cb4b16',
                       'PEG_ERA5_ERA5_JPL':'#2aa198',
                       'PEG_CHIRPS_ERA5_JPL':'#859900'}
    # Set up over all plot.
    # One panel each for abs and anom.
    fig, axs = plt.subplots(1, len(panel_list),
                            figsize=(7, 4), sharey=True)
    
    # Loop over panels.
    for p in range(len(panel_list)):
        # Initialize variable list.
        names = []
        values = []
        names_2 = []
        values_2 = []
        colors_2 = []
        names_Ens = []
        values_Ens = []
        colors_Ens = []
        # Loop over time lags and data combinations.
        for comb in data_dict['seasonal']:
            for t in range(len(time_lags)):
                # Add data to lists.
                if (comb in special_colors.keys()):
                    names_2.append(t + xs*np.random.rand(1)[0] - (xs/2.0))
                    values_2.append(data_dict['seasonal'][comb]\
                                    [panel_list[p] + '_lag' +
                                     str(time_lags[t])])
                    colors_2.append(special_colors[comb])
                elif (comb in special_colors2.keys()):
                    names_Ens.append(t + xs*np.random.rand(1)[0] - (xs/2.0))
                    values_Ens.append(data_dict['seasonal'][comb]\
                                    [panel_list[p] + '_lag' +
                                     str(time_lags[t])])
                    colors_Ens.append(special_colors2[comb])
                    
                else:
                    names.append(t + xs*np.random.rand(1)[0] - (xs/2.0))
                    values.append(data_dict['seasonal'][comb]\
                                  [panel_list[p] + '_lag' +
                                   str(time_lags[t])])
        
        # Make the plot panel.        
        axs[p].scatter(names, values, c='#002b36', s=8, marker='.')
        axs[p].scatter(names_2, values_2, c=colors_2, s=15,
                       marker='o', alpha=0.5)
        axs[p].scatter(names_Ens, values_Ens, c=colors_Ens, s=15,
                       marker='s', alpha=0.5)
        # Set some details.
        axs[p].title.set_text(panel_names[p])
        axs[0].set_ylabel('correlation coefficient')
        axs[p].set_xlabel('time lag / seasons')
        axs[p].set_ylim(bottom=-1, top=1)
        axs[p].set_xlim(-0.5, len(time_lags)-0.5)
        axs[p].set_xticks(range(len(time_lags)))
        axs[p].set_xticklabels(time_lags)
        axs[p].xaxis.set_tick_params(which='both', bottom=True, top=True)
        axs[p].yaxis.set_tick_params(which='both', left=True, right=True)
        axs[p].set_yticks(np.array([-1.0,-0.5,0.0,0.5,1.0]))
        axs[p].set_yticks(np.array([-0.75,-0.25,0.25,0.75]),minor=True)
    #
    # Add legends.
    legend_entries = []
    legend_entries2 = []
    for comb in special_colors.keys():
        legend_entries.append(mlines.Line2D([], [],
                                            color=special_colors[comb],
                                            marker='o',
                                            linestyle='None',
                                            label=comb))
    for comb in special_colors2.keys():
        legend_entries2.append(mlines.Line2D([], [],
                                             color=special_colors2[comb],
                                             marker='s',
                                             linestyle='None',
                                             label=comb))
    legend1 = axs[0].legend(handles=legend_entries,
                            loc='upper left',
                            fontsize='x-small',
                            markerscale=0.7,
                            labelspacing=0.4,
                            handletextpad=0.05,)
    axs[0].add_artist(legend1)
    legend2 = axs[1].legend(handles=legend_entries2,
                            loc='upper left',
                            fontsize='x-small',
                            markerscale=0.7,
                            labelspacing=0.4,
                            handletextpad=0.05,)
    
    
    # Tidy layout.
    fig.tight_layout()
            
    # Get git info for this script.
    sys.path.append(os.path.expanduser('~') +\
                    '/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    
    # Add git info to footer.
    plt.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    
    # Show the plot.
    #plt.show()
    # Or save the figure.
    fig.savefig(os.path.expanduser('~') +\
                '/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.pdf',
                format='pdf')
    
    return(fig)

################################################################################
### Correlation functions.

# Simple function to return seasonal mean with NaN value if not
# all months are present for each season.
# Input is a 1D time series as an xarray data array.
# Output is a 1D time series with one data point per season.
def seasonal_mean(ts):
    ts_s = ts.resample(time='QS-DEC').mean()
    ts_count = ts.resample(time='QS-DEC').count()
    ts_s[ts_count < 3] = np.nan
    return ts_s


def corrs_all(all_vars, sal, max_lag=2):
    ###############################################################
    # Returns dictionaries of statistics of correlation coefficient:
    # Monthly, seasonal and annual,
    # each of which has results from different combinations
    # of budget terms, for both absolute values and anomalies.
    # The results are each the output from corr_stats (function
    # below).
    #
    # Input:
    #     all_vars:     a dictionary of variables output by
    #                   get_budget_vars (below).
    #     sal:          an xarray data array of salinity
    #                   averaged over a small ocean region.
    # Optional input
    #     max_lag:      maximum time lag for which correlation
    #                   is calculated. Defaults to 2.
    #
    # Output:
    #     resids_all:   a dictionary with the following
    #                   structure of sub-dictionaries:
    #     {'monthly': {<'budget_terms_combination_1'>: corr_stats,
    #                  <'budget_terms_combination_2'>: corr_stats,
    #                  ... },
    #      'max_lag': max_lag
    #     }
    # The different combinations are named according to their input
    # data and method (e.g., 'PE_GPCP_GLEAM_JPL' [P-E method] or
    # 'conv_ERA5_ERA5_GFZ' [atmospheric convergence method].
    ###############################################################
    
    # Initialize final output dictionary.
    output = {'max_lag': max_lag}
    
    # Loop over timesteps.
    freq_list = ['seasonal']
    for f in freq_list:
        # Initialize sub-dictionary.
        temp = {}
        # Start adding residuals to this.
        temp['Obidos'] = corr_stats(seasonal_mean(all_vars['streamflow']),
                                    seasonal_mean(sal),
                                    freq=f, max_lag=max_lag)
        temp['P_GPCP'] = corr_stats(seasonal_mean(all_vars['pr_GPCP']),
                                    seasonal_mean(sal),
                                    freq=f, max_lag=max_lag)
        temp['P_CMAP'] = corr_stats(seasonal_mean(all_vars['pr_CMAP']),
                                    seasonal_mean(sal),
                                    freq=f, max_lag=max_lag)
        temp['P-E_local'] = corr_stats(seasonal_mean(all_vars['P-E_local']),
                                       seasonal_mean(sal),
                                       freq=f, max_lag=max_lag)
        temp['PEG_Ens'] = corr_stats(seasonal_mean(all_vars['pr_Ens']) -
                                     seasonal_mean(all_vars['evap_Ens']) -
                                     all_vars['dSdt_' + f[0] + '_Ens'],
                                     seasonal_mean(sal),
                                     freq=f, max_lag=max_lag)
        temp['CHG_Ens'] = corr_stats(seasonal_mean(all_vars['conv_Ens']) -
                                     all_vars['dtcwdt_' + f[0] + '_Ens'] -
                                     all_vars['dSdt_' + f[0] + '_Ens'],
                                     seasonal_mean(sal),
                                     freq=f, max_lag=max_lag)
        # Loop over GRACE methods.
        GRACE_list = ['JPL']
        pr_list = ['GPCP', 'CMAP', 'CHIRPS', 'ERA5', 'MERRA2']
        evap_list = ['GLEAM', 'CLM', 'Noah', 'ERA5', 'MERRA2']
        for g in GRACE_list:
            for p in pr_list:
                for e in evap_list:
                    temp['PEG_' + p + '_' + e + '_' + g] = corr_stats(
                        seasonal_mean(all_vars['pr_' + p]) -
                        seasonal_mean(all_vars['evap_' + e]) - 
                        all_vars['dSdt_' + f[0] + '_' + g],
                        seasonal_mean(sal),
                        freq=f, max_lag=max_lag)
            temp['CHG_ERA5_ERA5_' + g] = corr_stats(
                seasonal_mean(all_vars['vimd_ERA5']) -
                all_vars['dtcwdt_' + f[0] + '_ERA5'] - \
                all_vars['dSdt_' + f[0] + '_' + g],
                seasonal_mean(sal),
                freq=f, max_lag=max_lag)
            temp['CHG_MERRA2_MERRA2_' + g] = corr_stats(
                seasonal_mean(all_vars['viwvc_MERRA2']) -
                all_vars['dtcwdt_' + f[0] + '_MERRA2'] - \
                all_vars['dSdt_' + f[0] + '_' + g],
                seasonal_mean(sal),
                freq=f, max_lag=max_lag)
        
        # Add all these to the final output.
        output[f] = temp
        
    # Return the output.
    return(output)


def corr_stats(streamflow, sal, freq="monthly", max_lag=2):
    ###############################################################
    # Returns dictionary of statistics of correlation statistics.
    # Input:
    #     streamflow (or could be precipitation for example)
    #     salinity
    # Optional arguments:
    #     freq: averaging frequency of time series.
    #           "monthly" is the default but
    #           "seasonal" is also supported.
    #     max_lag:  maximum time lag (streamflow leads salinity)
    #               to calculate correlations for. Defaults to 2.
    #
    # Output:
    # Dictionary containing several correlation coefficients:
    #     abs_lag0:    correlation of absolute values at zero lag
    #     anom_lag0:   correlation of anomalies at zero lag
    #     abs_lag1:    correlation of absolute values at 1-month lag
    #     anom_lag1:   correlation of anomalies at 1-month lag
    #     etc....
    # NOTE: the lags are streamflow leading salinity
    # (e.g., pairs of data are (streamflow_Jan, salinity_Feb). 
    ###############################################################
    
    # Initialize output dictionary.
    stats = {}
    
    # Check for valid frequency input.
    if ~np.isin(freq, ["monthly","seasonal"]):
        print("Invalid option for freq")
    
    else:
        # Calcualte anomalies.
        streamflow_anom = streamflow.groupby('time.month') - \
                          streamflow.groupby('time.month').mean('time')
        sal_anom = sal.groupby('time.month') - \
                   sal.groupby('time.month').mean('time')
        
        # Calculate time overlap.
        st_time = max([min(streamflow.time), min(sal.time)])
        end_time = min([max(streamflow.time), max(sal.time)])
        
        # Get subsets of data.
        v1 = streamflow.sel(time=slice(st_time,end_time)).data
        v1a = streamflow_anom.sel(time=slice(st_time,end_time)).data
        v2 = sal.sel(time=slice(st_time,end_time)).data
        v2a = sal_anom.sel(time=slice(st_time,end_time)).data
        #
        # Mask out missing values.
        mask = (~np.isnan(v1) & ~np.isnan(v2))
        v1 = v1[mask]
        v2 = v2[mask]
        v1a = v1a[mask]
        v2a = v2a[mask]
        
        # Calculate correlation coefficients.
        stats['abs_lag0'] = np.corrcoef(v1, v2)[0,1]
        stats['anom_lag0'] = np.corrcoef(v1a, v2a)[0,1]
        if max_lag > 0:
            for l in range(1,max_lag+1):     
                stats['abs_lag' + str(l)] = np.corrcoef(v1[:-l],
                                                        v2[l:])[0,1]
                stats['anom_lag' + str(l)] = np.corrcoef(v1a[:-l],
                                                         v2a[l:])[0,1]
    
    # Return values.
    return(stats)


################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    main()

