"""
Plot time series of Amazon runoff estimates using several different methods.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
###########################################

# Function for git commit hash:
def get_git_revision_hash():
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    if sys.version_info[0] >= 3:
        return hash.decode('utf-8')
    else:
        return hash
###########################################


# Specify file locations. 
precip_filename = '~/Data/satellite/AmazonBasinAverage_GPCP.v2.3.precip.mon.mean.nc'
evap_conv_filename = '~/Data/reanalysis/ERA-int/AmazonCongo/AmazonBasinAverage_era_interim_2010-2018.nc'
GRACE_filename = '~/Data/satellite/GRACE/AmazonBasinAverage_GRCTellus.JPL.200204_201706.GLO.RL06M.MSCNv01CRIv01.nc'
hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
sal_filename = '~/Data/satellite/SMOS/SMOS_1x1deg_timeseries.nc'

plot_dir = '/Users/jameseyre/Documents/plots/AmazonCongo/'

# Specify time period for comparison.
st_time = np.datetime64('2010-01-01')
end_time = np.datetime64('2016-12-31')
st_time_GR = np.datetime64('2009-10-01')
end_time_GR = np.datetime64('2017-03-31')


##### Read data. ###########################################

print('Reading data...')

ds_pr = xr.open_dataset(precip_filename)
ds_e = xr.open_dataset(evap_conv_filename)
ds_G = xr.open_dataset(GRACE_filename)
ds_hy = xr.open_dataset(hybam_filename)
ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
ds_sal = xr.open_dataset(sal_filename)

pr = ds_pr.precip.loc[dict(time=slice(st_time, end_time))]
evap = ds_e.evaporation.loc[dict(time=slice(st_time, end_time))]
conv_rate = ds_e.convergence_rate.loc[dict(time=slice(st_time, end_time))]
mass_anom = ds_G.lwe_thickness.loc[dict(time=slice(st_time_GR, end_time_GR))]
ro_ob = ds_hy.Q.loc[dict(time=slice(st_time, end_time))]
sss = ds_sal.SSS.loc[dict(time=slice(st_time, end_time))]

# Take salinity average over region near mouth.
sal_region = np.array([7.0,0.0,-55.0,-47.5])
sss_av = sss.loc[dict(latitude=slice(sal_region[0],sal_region[1]),
                      longitude=slice(sal_region[2],sal_region[3]))].\
         mean(['latitude','longitude'])

# Interpolate mass anomalies to regular time.
mass_anom_int = mass_anom.interp(time=xr.concat([pr.time[0] - np.timedelta64(31,'D'),pr.time],'time'))
# Apply some smoothing.
temp = mass_anom_int.copy()
sm = 1 # ==> 1+(2*sm) centered running average
for t in range(sm,len(mass_anom_int)-sm):
    mass_anom_int[t] = np.mean(temp[t-sm:t+1+sm])

# Get factor to correct Obidos streamflow to mouth.
ob_to_mouth = ds_DT.ratio_m2s[0].data

# Get basin area for unit conversion.
basin_area = evap.attrs['region_area_squareMetres']


##### Calculate runoff estimates. ###########################################

print('Calculating runoff...')

# GRACE mass change conversion.
delta_S = mass_anom_int[1:]
delta_S.data = mass_anom_int[1:].data - mass_anom_int[:-1].data
delta_t = mass_anom_int.time[1:]
delta_t.data = mass_anom_int.time[1:].values - mass_anom_int.time[:-1].values
delta_days = delta_t.astype('timedelta64[D]')
delta_days = delta_days / np.timedelta64(1, 'D')
# Rate in cm per day:
mass_change_rate = delta_S/delta_days

# Perform units conversions
# (everything to cubic meters per second at the mouth).
pr_m3s = pr*(basin_area/(1000.0*24.0*60.0*60.0))
evap_m3s = evap*(-1.0*basin_area/(24.0*60.0*60.0))
conv_m3s = conv_rate*(basin_area/(1000.0*24.0*60.0*60.0))
mass_m3s = mass_change_rate*(basin_area/(100.0*24.0*60.0*60.0))

# Combine quantities.
ro_DT = ro_ob*ob_to_mouth
ro_PmE = pr_m3s - \
         evap_m3s - \
         mass_m3s
ro_conv = conv_m3s - \
          mass_m3s


##### Calculate climatology. ###########################################

print('Calculating climatologies...')

# Monthly climatology.
ro_DT_clim = ro_DT.groupby('time.month').mean('time')
ro_PmE_clim = ro_PmE.groupby('time.month').mean('time')
ro_conv_clim = ro_conv.groupby('time.month').mean('time')
sss_clim = sss_av.groupby('time.month').mean('time')

# Monthly anomalies.
ro_DT_anom = ro_DT.groupby('time.month') - ro_DT_clim
ro_PmE_anom = ro_PmE.groupby('time.month') - ro_PmE_clim
ro_conv_anom = ro_conv.groupby('time.month') - ro_conv_clim
sss_anom = sss_av.groupby('time.month') - sss_clim

# Annual means
print(ro_DT_clim.mean('month').data)
print(ro_PmE_clim.mean('month').data)
print(ro_conv_clim.mean('month').data)

# Correlations.
print('Absolute correlations:')
print(np.corrcoef(sss_av.data[:-2], ro_DT.data[:-1])[0,1])
print(np.corrcoef(sss_av.data, ro_PmE.data)[0,1])
print(np.corrcoef(sss_av.data, ro_conv.data)[0,1])
print('Anomaly correlations:')
print(np.corrcoef(sss_anom.data[:-2], ro_DT_anom.data[:-1])[0,1])
print(np.corrcoef(sss_anom.data, ro_PmE_anom.data)[0,1])
print(np.corrcoef(sss_anom.data, ro_conv_anom.data)[0,1])


##### Make plots. ###########################################

print('Plotting...')

plt.rcParams.update({'font.size': 14})

# First figure.
fig1 = plt.figure(figsize=(9, 5))
# Data.
plt.plot(ro_DT_clim.month, ro_DT_clim, 'k-',label='Dai_Tren')
plt.plot(ro_PmE_clim.month, ro_PmE_clim, 'r--',label='P-E')
plt.plot(ro_conv_clim.month, ro_conv_clim, 'b:',label='convergence')
# Details.
plt.xlim(0,13)
plt.ylabel('Discharge / '+ r'$m^{3} s^{-1}$')
plt.xlabel('Month')
# Legend
plt.legend()
# Tidy layout.
plt.tight_layout()
# Add info to footer. 
txtl = 'compare_Amazon_runoff_estimates.py'
plt.text(0.02,0.02,txtl, transform=fig1.transFigure, size=6)
txtr = get_git_revision_hash()
plt.text(0.75,0.02,txtr, transform=fig1.transFigure, size=6)

# Save to PDF file.
plt.savefig(plot_dir + 'compare_Amazon_runoff_estimates_annualCycle.pdf',format='pdf')

#####

#Second figure.
fig2, ax1 = plt.subplots(figsize=(12, 5))

# Details.
color = 'k'
ax1.set_xlabel('Year')
ax1.set_ylabel('Discharge anomaly/ '+ r'$m^{3} s^{-1}$', color=color)
# Data.
ax1.plot(ro_DT.time, ro_DT_anom, label='Dai_Tren', color=color, linestyle='solid')
ax1.plot(ro_PmE.time, ro_PmE_anom, label='P-E', color=color, linestyle='dashed')
ax1.plot(ro_conv.time, ro_conv_anom, label='conv', color=color, linestyle='dotted')
ax1.tick_params(axis='y', labelcolor=color)
# Legend
ax1.legend(loc='upper right')

# Overlay salinity plot with other axis.
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
#
color = 'tab:red'
ax2.set_ylabel('SSS anomaly / PSU', color=color)  
ax2.plot(sss_av.time, -1.0*sss_anom, color=color, label='-1 x Salinity')
ax2.tick_params(axis='y', labelcolor=color)
# Legend
ax2.legend(loc='lower center')
# Tidy layout
fig2.tight_layout()  # otherwise the right y-label is slightly clipped
# Add info to footer. 
plt.tight_layout()
txtl = 'compare_Amazon_runoff_estimates.py'
plt.text(0.02,0.02,txtl, transform=fig2.transFigure, size=6)
txtr = get_git_revision_hash()
plt.text(0.75,0.02,txtr, transform=fig2.transFigure, size=6)

# Save figure as PDF.
plt.savefig(plot_dir + 'compare_Amazon_runoff_estimates_anomaly.pdf',
            format='pdf')
