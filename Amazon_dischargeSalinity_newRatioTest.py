"""
Plot correlation coefficient of streamflow with salinity from different combinations of seasonal average data.

Run it from plotenv conda environment.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################

################################################################################
def main():
    
    ##### Read hydrology data. ###########################################
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Fill gaps in streamflow (inserts nan for missing months)
    ro_ob = ro_ob.resample(time='1M').asfreq()
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    
    # Get factor to correct Obidos streamflow to mouth.
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    ob_to_mouth = ds_DT.ratio_m2s[0].data
    
    # Scale up Obidos streamflow to river mouth
    # using Dai and Trenberth method.
    ro_DT = ro_ob.copy()
    ro_DT = ro_DT*ob_to_mouth
    #
    # Scale up with alternative factors.
    fac_new = np.full(12, ob_to_mouth)
    fac_new[5:9] = 1.15
    ro_new = ro_ob.copy()
    for t in range(len(ro_new)):
        ro_new[t] = ro_ob[t]*fac_new[ro_new.time[t].dt.month - 1]
    
    
    ##### Read salinity data. ###########################################
    
    # Define region to average over.
    sal_region = np.array([0.0,3.0,-52.5,-47.5])
    sal_st_time = '2010-02-01'
    sal_end_time = '2017-12-31'
    
    # Open and read file.
    sal_filename_wildcard = os.path.expanduser('~') +\
                            '/Data/satellite/SMOS/' + \
                            'SMOS_L3_DEBIAS_LOCEAN_AD_*' + \
                            '_EASE_09d_25km_v03_monthly.nc'
    ds_sal = xr.open_mfdataset(sal_filename_wildcard, concat_dim='time')
    # Don't get the first (incomplete) month.
    sss = ds_sal.SSS.sel(time=slice(sal_st_time,sal_end_time),
                         lat=slice(sal_region[0],sal_region[1]),
                         lon=slice(sal_region[2],sal_region[3]))\
                         .mean(['lat','lon']).load()
    
    
    
    ##### Calculate correlations. ###########################################
    
    print('Processing...')
    
    # Define maximum time lag for correlations.
    max_lag = 0
    
    # Calculate correlations of streamflow estimates with salinity.
    r_DT_m = corr_stats(ro_DT, sss, freq='monthly', max_lag=max_lag)
    r_new_m = corr_stats(ro_new, sss, freq='monthly', max_lag=max_lag)
    r_DT_s = corr_stats(seasonal_mean(ro_DT),
                        seasonal_mean(sss),
                        freq='seasonal', max_lag=max_lag)
    r_new_s = corr_stats(seasonal_mean(ro_new),
                         seasonal_mean(sss), 
                         freq='seasonal', max_lag=max_lag)
    #
    # Print out.
    print('---------- Monthly ----------')
    print('----- Dai et al. 2009:')
    print('Absolute:')
    print(r_DT_m)
    print(r_DT_m['abs_lag0'])
    print('Anomaly:')
    print(r_DT_m['anom_lag0'])
    print('----- New factors:')
    print(fac_new)
    print('Absolute:')
    print(r_new_m['abs_lag0'])
    print('Anomaly:')
    print(r_new_m['anom_lag0'])
    print('---------- Seasonal ----------')
    print('----- Dai et al. 2009:')
    print('Absolute:')
    print(r_DT_s['abs_lag0'])
    print('Anomaly:')
    print(r_DT_s['anom_lag0'])
    print('----- New factors:')
    print(fac_new)
    print('Absolute:')
    print(r_new_s['abs_lag0'])
    print('Anomaly:')
    print(r_new_s['anom_lag0'])


################################################################################


################################################################################
### Correlation functions.

# Simple function to return seasonal mean with NaN value if not
# all months are present for each season.
# Input is a 1D time series as an xarray data array.
# Output is a 1D time series with one data point per season.
def seasonal_mean(ts):
    ts_s = ts.resample(time='QS-DEC').mean()
    ts_count = ts.resample(time='QS-DEC').count()
    ts_s[ts_count < 3] = np.nan
    return ts_s

def corr_stats(streamflow, sal, freq="monthly", max_lag=2):
    ###############################################################
    # Returns dictionary of statistics of correlation statistics.
    # Input:
    #     streamflow (or could be precipitation for example)
    #     salinity
    # Optional arguments:
    #     freq: averaging frequency of time series.
    #           "monthly" is the default but
    #           "seasonal" is also supported.
    #     max_lag:  maximum time lag (streamflow leads salinity)
    #               to calculate correlations for. Defaults to 2.
    #
    # Output:
    # Dictionary containing several correlation coefficients:
    #     abs_lag0:    correlation of absolute values at zero lag
    #     anom_lag0:   correlation of anomalies at zero lag
    #     abs_lag1:    correlation of absolute values at 1-month lag
    #     anom_lag1:   correlation of anomalies at 1-month lag
    #     etc....
    # NOTE: the lags are streamflow leading salinity
    # (e.g., pairs of data are (streamflow_Jan, salinity_Feb). 
    ###############################################################
    
    # Initialize output dictionary.
    stats = {}
    
    # Check for valid frequency input.
    if ~np.isin(freq, ["monthly","seasonal"]):
        print("Invalid option for freq")
    
    else:
        # Calcualte anomalies.
        streamflow_anom = streamflow.groupby('time.month') - \
                          streamflow.groupby('time.month').mean('time')
        sal_anom = sal.groupby('time.month') - \
                   sal.groupby('time.month').mean('time')
        
        # Calculate time overlap.
        st_time = max([min(streamflow.time), min(sal.time)])
        end_time = min([max(streamflow.time), max(sal.time)])
        
        # Get subsets of data.
        v1 = streamflow.sel(time=slice(st_time,end_time)).data
        v1a = streamflow_anom.sel(time=slice(st_time,end_time)).data
        v2 = sal.sel(time=slice(st_time,end_time)).data
        v2a = sal_anom.sel(time=slice(st_time,end_time)).data
        #
        # Mask out missing values.
        mask = (~np.isnan(v1) & ~np.isnan(v2))
        v1 = v1[mask]
        v2 = v2[mask]
        v1a = v1a[mask]
        v2a = v2a[mask]
        
        # Calculate correlation coefficients.
        stats['abs_lag0'] = np.corrcoef(v1, v2)[0,1]
        stats['anom_lag0'] = np.corrcoef(v1a, v2a)[0,1]
        if max_lag > 0:
            for l in range(1,max_lag+1):     
                stats['abs_lag' + str(l)] = np.corrcoef(v1[:-l],
                                                        v2[l:])[0,1]
                stats['anom_lag' + str(l)] = np.corrcoef(v1a[:-l],
                                                         v2a[l:])[0,1]
    
    # Return values.
    return(stats)


################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    main()

