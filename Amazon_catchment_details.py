## From my shapefiles:
areas = {'Amazon': 6068561.85704,
         'Obidos': 4758114.48155,
         'units': 'km^2'}

## From Dai and Trenberth, 2002:
#areas = {'Amazon': 5854000.0,
#         'Obidos': 4619000.0,
#         'units': 'km^2'}

def check_catchment_areas():
    import geopandas as gpd
    import shapely

    # Load files.
    a1 = gpd.read_file("/Users/jameseyre/Data/in-situ/hybam/amazlm_1608.shp")
    o1 = gpd.read_file("/Users/jameseyre/Data/in-situ/hybam/Obidos_catchment.shp")
    s1 = gpd.read_file("/Users/jameseyre/Data/in-situ/hybam/bassin/bacias_corr.shp") 
    a2 = gpd.read_file("/Users/jameseyre/Data/ancillary/amapoly_ivb/amapoly_ivb.shp")
    a3 = gpd.read_file("/Users/jameseyre/Data/in-situ/hybam/Amazon_catchment.shp")
    a4 = gpd.read_file("/Users/jameseyre/Data/ancillary/Aqueduct_river_basins_AMAZONAS/Aqueduct_river_basins_AMAZONAS.shp")

    # Add CRS to those without.
    a1.crs = {'init': 'epsg:4326'}
    o1.crs = {'init': 'epsg:4326'}
    s1.crs = {'init': 'epsg:4326'}
    a3.crs = {'init': 'epsg:4326'}

    # Convert HYBAM Amazon shapefile linestring to polygon.
    a1_map = shapely.geometry.mapping(a1.geometry[0][0])
    lats = [x[1] for x in a1_map['coordinates']]
    lons = [x[0] for x in a1_map['coordinates']]
    polyg = shapely.geometry.Polygon(zip(lons, lats))
    a1 = gpd.GeoDataFrame(index=[0], crs=a1.crs, geometry=[polyg])

    # Convert all CRS to EASE global equal area.
    a1 = a1.to_crs({'init': 'epsg:3410'})
    a2 = a2.to_crs({'init': 'epsg:3410'})
    o1 = o1.to_crs({'init': 'epsg:3410'})
    s1 = s1.to_crs({'init': 'epsg:3410'})
    a3 = a3.to_crs({'init': 'epsg:3410'})
    a4 = a4.to_crs({'init': 'epsg:3410'})

    # Get arrays of subcatchment areas.
    s1a = s1.area
    s1ap = s1['AREA']
    s1gc = s1['GRIDCODE']
    obidos_list = ((s1gc <= 256) | (s1gc >= 312))

    # Perform checks.
    print('---------------------------------------')
    print('-------------Amazon basin:-------------')
    print('HYBAM shapefile calculated:')
    print(a1.area/1000000.0)
    print('HYBAM subcatchment provided, summed:')
    print(sum(s1['AREA']))
    print('HYBAM subcatchment calculated, summed:')
    print(sum(s1.area)/1000000.0)
    print('My Amazon shapefile, calculated:')
    print(a3.area/1000000.0)
    print('Harvard shapefile provided:')
    print(a2['AREAPROJ']/1000000)
    print('Harvard shapefile calculated:')
    print(a2.area/1000000)
    print('wateractionhub shapefile calculated:')
    print(a4.area/1000000)
    #
    print('---------------------------------------')
    print('-------------Obidos basin:-------------')
    print('My Obidos shapefile, calculated:')
    print(o1.area/1000000.0)
    print('HYBAM subcatchment provided, summed:')
    print(sum(s1ap.loc[obidos_list]))
    print('HYBAM subcatchment calculated, summed:')
    print(sum(s1a.loc[obidos_list])/1000000.0)
    print('---------------------------------------')
