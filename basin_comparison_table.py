"""
Prints text to make a LaTeX table for comparison of Amazon and Obidos basins.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################


################################################################################
def main():
    
    ##### Read hydrology data. ###########################################
    print('Reading data...')
    #
    # Print header stuff.
    print('\\begin{table}')
    print('\\caption{INSERT CAPTION HERE}')
    print('\\centering')
    print('\\begin{tabular}{l l c c c}')
    print('\\hline')
    print('Quantity & Dataset & Amazon & Obidos & ratio \\\\')
    
    # Get catchment area details.
    amazon_area = Amazon_catchment_details.areas['Amazon']
    obidos_area = Amazon_catchment_details.areas['Obidos']
    print('Area (' + Amazon_catchment_details.areas['units'] + ') & & ' + 
          '%.3g' % amazon_area + ' & ' + 
          '%.3g' % obidos_area + ' & ' +
          '% 3.2f' % (amazon_area/obidos_area) + 
          ' \\\\')
    
    # Get water budget variables.
    # All in m3 s-1.
    amazon_vars = get_budget_vars('Amazon')
    obidos_vars = get_budget_vars('Obidos')
    #
    # Specify units and conversion factor (m3 s-1 --> mm day-1)
    ca = 60.0*60.0*24.0*1000.0/(amazon_area*1000.0*1000.0)
    co = 60.0*60.0*24.0*1000.0/(obidos_area*1000.0*1000.0)
    units = 'mm day^{-1}'
    # Or just overwrite these with 1.0 to use m3 s-1.
    ca = 1.0
    co = 1.0
    units = 'm^3 s^{-1}'
    
    # Specify years to calculate averages for.
    st_yr = 2003
    end_yr = 2018
    
    # Calculate annual averages.
    pr_GPCP_ann_amazon = amazon_vars['pr_GPCP'].\
                         loc[dict(time=slice(str(st_yr) + '-01-01',
                                             str(end_yr) + '-12-31'))].\
                         groupby('time.month').mean().mean() * ca
    pr_GPCP_ann_obidos = obidos_vars['pr_GPCP'].\
                         loc[dict(time=slice(str(st_yr) + '-01-01',
                                             str(end_yr) + '-12-31'))].\
                         groupby('time.month').mean().mean() * co
    pr_CMAP_ann_amazon = amazon_vars['pr_CMAP'].\
                         loc[dict(time=slice(str(st_yr) + '-01-01',
                                             str(end_yr) + '-12-31'))].\
                         groupby('time.month').mean().mean() * ca
    pr_CMAP_ann_obidos = obidos_vars['pr_CMAP'].\
                         loc[dict(time=slice(str(st_yr) + '-01-01',
                                             str(end_yr) + '-12-31'))].\
                         groupby('time.month').mean().mean() * co
    pr_CHIRPS_ann_amazon = amazon_vars['pr_CHIRPS'].\
                           loc[dict(time=slice(str(st_yr) + '-01-01',
                                               str(end_yr) + '-12-31'))].\
                           groupby('time.month').mean().mean() * ca
    pr_CHIRPS_ann_obidos = obidos_vars['pr_CHIRPS'].\
                           loc[dict(time=slice(str(st_yr) + '-01-01',
                                               str(end_yr) + '-12-31'))].\
                           groupby('time.month').mean().mean() * co
    pr_ERA5_ann_amazon = amazon_vars['pr_ERA5'].\
                         loc[dict(time=slice(str(st_yr) + '-01-01',
                                             str(end_yr) + '-12-31'))].\
                         groupby('time.month').mean().mean() * ca
    pr_ERA5_ann_obidos = obidos_vars['pr_ERA5'].\
                         loc[dict(time=slice(str(st_yr) + '-01-01',
                                             str(end_yr) + '-12-31'))].\
                         groupby('time.month').mean().mean() * co
    pr_MERRA2_ann_amazon = amazon_vars['pr_MERRA2'].\
                           loc[dict(time=slice(str(st_yr) + '-01-01',
                                               str(end_yr) + '-12-31'))].\
                           groupby('time.month').mean().mean() * ca
    pr_MERRA2_ann_obidos = obidos_vars['pr_MERRA2'].\
                           loc[dict(time=slice(str(st_yr) + '-01-01',
                                               str(end_yr) + '-12-31'))].\
                           groupby('time.month').mean().mean() * co
    evap_ERA5_ann_amazon = amazon_vars['evap_ERA5'].\
                           loc[dict(time=slice(str(st_yr) + '-01-01',
                                               str(end_yr) + '-12-31'))].\
                           groupby('time.month').mean().mean() * ca
    evap_ERA5_ann_obidos = obidos_vars['evap_ERA5'].\
                           loc[dict(time=slice(str(st_yr) + '-01-01',
                                               str(end_yr) + '-12-31'))].\
                           groupby('time.month').mean().mean() * co
    evap_GLEAM_ann_amazon = amazon_vars['evap_GLEAM'].\
                            loc[dict(time=slice(str(st_yr) + '-01-01',
                                                str(end_yr) + '-12-31'))].\
                            groupby('time.month').mean().mean() * ca
    evap_GLEAM_ann_obidos = obidos_vars['evap_GLEAM'].\
                            loc[dict(time=slice(str(st_yr) + '-01-01',
                                                str(end_yr) + '-12-31'))].\
                            groupby('time.month').mean().mean() * co
    evap_MERRA2_ann_amazon = amazon_vars['evap_MERRA2'].\
                            loc[dict(time=slice(str(st_yr) + '-01-01',
                                                str(end_yr) + '-12-31'))].\
                            groupby('time.month').mean().mean() * ca
    evap_MERRA2_ann_obidos = obidos_vars['evap_MERRA2'].\
                             loc[dict(time=slice(str(st_yr) + '-01-01',
                                                 str(end_yr) + '-12-31'))].\
                             groupby('time.month').mean().mean() * co
    dSdt_JPL_ann_amazon = amazon_vars['dSdt_m_JPL'].\
                          loc[dict(time=slice(str(st_yr) + '-01-01',
                                              str(end_yr) + '-12-31'))].\
                          groupby('time.month').mean().mean() * ca
    dSdt_JPL_ann_obidos = obidos_vars['dSdt_m_JPL'].\
                          loc[dict(time=slice(str(st_yr) + '-01-01',
                                              str(end_yr) + '-12-31'))].\
                          groupby('time.month').mean().mean() * co
    dSdt_CSR_ann_amazon = amazon_vars['dSdt_m_CSR'].\
                          loc[dict(time=slice(str(st_yr) + '-01-01',
                                              str(end_yr) + '-12-31'))].\
                          groupby('time.month').mean().mean() * ca
    dSdt_CSR_ann_obidos = obidos_vars['dSdt_m_CSR'].\
                          loc[dict(time=slice(str(st_yr) + '-01-01',
                                              str(end_yr) + '-12-31'))].\
                          groupby('time.month').mean().mean() * co
    dSdt_GFZ_ann_amazon = amazon_vars['dSdt_m_GFZ'].\
                          loc[dict(time=slice(str(st_yr) + '-01-01',
                                              str(end_yr) + '-12-31'))].\
                          groupby('time.month').mean().mean() * ca
    dSdt_GFZ_ann_obidos = obidos_vars['dSdt_m_GFZ'].\
                          loc[dict(time=slice(str(st_yr) + '-01-01',
                                              str(end_yr) + '-12-31'))].\
                          groupby('time.month').mean().mean() * co
    conv_ERA5_ann_amazon = amazon_vars['vimd_ERA5'].\
                           loc[dict(time=slice(str(st_yr) + '-01-01',
                                               str(end_yr) + '-12-31'))].\
                           groupby('time.month').mean().mean() * ca
    conv_ERA5_ann_obidos = obidos_vars['vimd_ERA5'].\
                           loc[dict(time=slice(str(st_yr) + '-01-01',
                                               str(end_yr) + '-12-31'))].\
                           groupby('time.month').mean().mean() * co
    conv_MERRA2_ann_amazon = amazon_vars['viwvc_MERRA2'].\
                             loc[dict(time=slice(str(st_yr) + '-01-01',
                                                 str(end_yr) + '-12-31'))].\
                             groupby('time.month').mean().mean() * ca
    conv_MERRA2_ann_obidos = obidos_vars['viwvc_MERRA2'].\
                             loc[dict(time=slice(str(st_yr) + '-01-01',
                                                 str(end_yr) + '-12-31'))].\
                             groupby('time.month').mean().mean() * co
    dtcwdt_ERA5_ann_amazon = amazon_vars['dtcwdt_m_ERA5'].\
                             loc[dict(time=slice(str(st_yr) + '-01-01',
                                                 str(end_yr) + '-12-31'))].\
                             groupby('time.month').mean().mean() * ca
    dtcwdt_ERA5_ann_obidos = obidos_vars['dtcwdt_m_ERA5'].\
                             loc[dict(time=slice(str(st_yr) + '-01-01',
                                                 str(end_yr) + '-12-31'))].\
                             groupby('time.month').mean().mean() * co
    dtcwdt_MERRA2_ann_amazon = amazon_vars['dtcwdt_m_MERRA2'].\
                               loc[dict(time=slice(str(st_yr) + '-01-01',
                                                   str(end_yr) + '-12-31'))].\
                               groupby('time.month').mean().mean() * ca
    dtcwdt_MERRA2_ann_obidos = obidos_vars['dtcwdt_m_MERRA2'].\
                               loc[dict(time=slice(str(st_yr) + '-01-01',
                                                   str(end_yr) + '-12-31'))].\
                               groupby('time.month').mean().mean() * co
    PE_GPCP_ERA5_JPL_amazon = amazon_vars['pr_GPCP'] - \
                              amazon_vars['evap_ERA5'] - \
                              amazon_vars['dSdt_m_JPL']
    PE_GPCP_ERA5_JPL_ann_amazon = PE_GPCP_ERA5_JPL_amazon.\
                                  loc[dict(time=slice(str(st_yr) + '-01-01',
                                                      str(end_yr) +
                                                      '-12-31'))].\
                                  groupby('time.month').mean().mean()
    PE_GPCP_ERA5_JPL_obidos = obidos_vars['pr_GPCP'] - \
                              obidos_vars['evap_ERA5'] - \
                              obidos_vars['dSdt_m_JPL']
    PE_GPCP_ERA5_JPL_ann_obidos = PE_GPCP_ERA5_JPL_obidos.\
                                  loc[dict(time=slice(str(st_yr) + '-01-01',
                                                      str(end_yr) +
                                                      '-12-31'))].\
                                  groupby('time.month').mean().mean()
    PE_ERA5_ERA5_JPL_amazon = amazon_vars['pr_ERA5'] - \
                              amazon_vars['evap_ERA5'] - \
                              amazon_vars['dSdt_m_JPL']
    PE_ERA5_ERA5_JPL_ann_amazon = PE_ERA5_ERA5_JPL_amazon.\
                                  loc[dict(time=slice(str(st_yr) + '-01-01',
                                                      str(end_yr) +
                                                      '-12-31'))].\
                                  groupby('time.month').mean().mean()
    PE_ERA5_ERA5_JPL_obidos = obidos_vars['pr_ERA5'] - \
                              obidos_vars['evap_ERA5'] - \
                              obidos_vars['dSdt_m_JPL']
    PE_ERA5_ERA5_JPL_ann_obidos = PE_ERA5_ERA5_JPL_obidos.\
                                  loc[dict(time=slice(str(st_yr) + '-01-01',
                                                      str(end_yr) +
                                                      '-12-31'))].\
                                  groupby('time.month').mean().mean()
    
    # Get Obidos observations.
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    # Fill gaps in streamflow (inserts nan for missing months)
    ro_ob = ro_ob.resample(time='1M').asfreq()
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    # Get factor to correct Obidos streamflow to mouth.
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    ob_to_mouth = ds_DT.ratio_m2s[0]
    # Scale up Obidos streamflow to river mouth.
    ro_DT = ro_ob.copy()
    ro_DT = ro_DT*ob_to_mouth
    #Take annual average.
    streamflow_ann_obidos = ro_ob.\
                            loc[dict(time=slice(str(st_yr) + '-01-01',
                                                str(end_yr) + '-12-31'))].\
                            groupby('time.month').mean().mean()
    streamflow_ann_amazon = ro_DT.\
                            loc[dict(time=slice(str(st_yr) + '-01-01',
                                                str(end_yr) + '-12-31'))].\
                            groupby('time.month').mean().mean()
    
    # Print out table stuff.
    print('Precipitation (' + units + ') & GPCP & ' + 
          '%.3g' % pr_GPCP_ann_amazon + ' & ' + 
          '%.3g' % pr_GPCP_ann_obidos + ' & ' +
          '% 3.2f' % (pr_GPCP_ann_amazon/pr_GPCP_ann_obidos) + 
          ' \\\\')
    print(' & CMAP & ' + 
          '%.3g' % pr_CMAP_ann_amazon + ' & ' + 
          '%.3g' % pr_CMAP_ann_obidos + ' & ' +
          '% 3.2f' % (pr_CMAP_ann_amazon/pr_CMAP_ann_obidos) + 
          ' \\\\')
    print(' & CHIRPS & ' + 
          '%.3g' % pr_CHIRPS_ann_amazon + ' & ' + 
          '%.3g' % pr_CHIRPS_ann_obidos + ' & ' +
          '% 3.2f' % (pr_CMAP_ann_amazon/pr_CMAP_ann_obidos) + 
          ' \\\\')
    print(' & ERA5 & ' + 
          '%.3g' % pr_ERA5_ann_amazon + ' & ' + 
          '%.3g' % pr_ERA5_ann_obidos + ' & ' +
          '% 3.2f' % (pr_ERA5_ann_amazon/pr_ERA5_ann_obidos) + 
          ' \\\\')
    print(' & MERRA2 & ' + 
          '%.3g' % pr_MERRA2_ann_amazon + ' & ' + 
          '%.3g' % pr_MERRA2_ann_obidos + ' & ' +
          '% 3.2f' % (pr_MERRA2_ann_amazon/pr_MERRA2_ann_obidos) + 
          ' \\\\')
    print('Evaporation (' + units + ') & GLEAM & ' + 
          '%.3g' % evap_GLEAM_ann_amazon + ' & ' + 
          '%.3g' % evap_GLEAM_ann_obidos + ' & ' +
          '% 3.2f' % (evap_GLEAM_ann_amazon/evap_GLEAM_ann_obidos) + 
          ' \\\\')
    print(' & ERA5 & ' + 
          '%.3g' % evap_ERA5_ann_amazon + ' & ' + 
          '%.3g' % evap_ERA5_ann_obidos + ' & ' +
          '% 3.2f' % (evap_ERA5_ann_amazon/evap_ERA5_ann_obidos) + 
          ' \\\\')
    print(' & MERRA2 & ' + 
          '%.3g' % evap_MERRA2_ann_amazon + ' & ' + 
          '%.3g' % evap_MERRA2_ann_obidos + ' & ' +
          '% 3.2f' % (evap_MERRA2_ann_amazon/evap_MERRA2_ann_obidos) + 
          ' \\\\')
    print('Terrestrial storage change (' + units + ') & JPL & ' + 
          '%.3g' % dSdt_JPL_ann_amazon + ' & ' + 
          '%.3g' % dSdt_JPL_ann_obidos + ' & ' +
          '% 3.2f' % (dSdt_JPL_ann_amazon/dSdt_JPL_ann_obidos) + 
          ' \\\\')
    print(' & CSR & ' + 
          '%.3g' % dSdt_CSR_ann_amazon + ' & ' + 
          '%.3g' % dSdt_CSR_ann_obidos + ' & ' +
          '% 3.2f' % (dSdt_CSR_ann_amazon/dSdt_CSR_ann_obidos) + 
          ' \\\\')
    print(' & GFZ & ' + 
          '%.3g' % dSdt_GFZ_ann_amazon + ' & ' + 
          '%.3g' % dSdt_GFZ_ann_obidos + ' & ' +
          '% 3.2f' % (dSdt_GFZ_ann_amazon/dSdt_GFZ_ann_obidos) + 
          ' \\\\')
    print('Atmospheric convergence (' + units + ') & ERA5 & ' + 
          '%.3g' % conv_ERA5_ann_amazon + ' & ' + 
          '%.3g' % conv_ERA5_ann_obidos + ' & ' +
          '% 3.2f' % (conv_ERA5_ann_amazon/conv_ERA5_ann_obidos) + 
          ' \\\\')
    print(' & MERRA2 & ' + 
          '%.3g' % conv_MERRA2_ann_amazon + ' & ' + 
          '%.3g' % conv_MERRA2_ann_obidos + ' & ' +
          '% 3.2f' % (conv_MERRA2_ann_amazon/conv_MERRA2_ann_obidos) + 
          ' \\\\')
    print('Atmospheric storage change (' + units + ') & ERA5 & ' + 
          '%.3g' % dtcwdt_ERA5_ann_amazon + ' & ' + 
          '%.3g' % dtcwdt_ERA5_ann_obidos + ' & ' +
          '% 3.2f' % (dtcwdt_ERA5_ann_amazon/dtcwdt_ERA5_ann_obidos) + 
          ' \\\\')
    print(' & MERRA2 & ' + 
          '%.3g' % dtcwdt_MERRA2_ann_amazon + ' & ' + 
          '%.3g' % dtcwdt_MERRA2_ann_obidos + ' & ' +
          '% 3.2f' % (dtcwdt_MERRA2_ann_amazon/dtcwdt_MERRA2_ann_obidos) + 
          ' \\\\')
    print('Streamflow (m^3 s^{-1}) & Obidos (and D09) & ' + 
          '%.3g' % streamflow_ann_amazon + ' & ' + 
          '%.3g' % streamflow_ann_obidos + ' & ' +
          '% 3.2f' % (streamflow_ann_amazon/
                      streamflow_ann_obidos) + 
          ' \\\\')
    print(' & PE\_GPCP\_ERA5\_JPL & ' + 
          '%.3g' % PE_GPCP_ERA5_JPL_ann_amazon + ' & ' + 
          '%.3g' % PE_GPCP_ERA5_JPL_ann_obidos + ' & ' +
          '% 3.2f' % (PE_GPCP_ERA5_JPL_ann_amazon/
                      PE_GPCP_ERA5_JPL_ann_obidos) + 
          ' \\\\')
    print(' & PE\_ERA5\_ERA5\_JPL & ' + 
          '%.3g' % PE_ERA5_ERA5_JPL_ann_amazon + ' & ' + 
          '%.3g' % PE_ERA5_ERA5_JPL_ann_obidos + ' & ' +
          '% 3.2f' % (PE_ERA5_ERA5_JPL_ann_amazon/
                      PE_ERA5_ERA5_JPL_ann_obidos) + 
          ' \\\\')
    
    print('\\end{tabular}')
    print('\\end{table}')
    
    return 


################################################################################


###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    pr_ann_cy = main()
