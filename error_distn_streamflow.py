"""
Plot distribution of differences between streamflow estimates to check 
if they are normally distributed or not.

Run from plotenv conda environment.
"""

###########################################
import numpy as np
import xarray as xr
from scipy import stats
from scipy.stats import norm, shapiro, anderson
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
import mpl_toolkits.axes_grid1.inset_locator as mpl_il
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################


################################################################################
def main():
    
    ##### Read data. ###########################################
    print('Reading data...')
    #
    # Select basin and data sets.
    plot_basin = 'Obidos'
    v1_name = 'Gauge obs.'
    v2_name = 'conv_ERA5_ERA5_JPL'
    v1 = get_streamflow(plot_basin)
    v2 = get_r_hat(plot_basin, v2_name)
    #
    # Subset to a single calendar month.
    plot_month = 12
    v1 = v1[v1.time.dt.month == plot_month]
    v2 = v2[v2.time.dt.month == plot_month]
    #
    #
    ##### Make plots. ###########################################
    print('Plotting...')
    #
    plot_dist(v1.dropna(dim='time'), v2.dropna(dim='time'),
              plot_basin,
              v1_name, v2_name)
    #plot_qq(diff.dropna(dim='time'), plot_basin, v1, v2, norm)
    #
    return(v1,v2)

################################################################################
# Plotting functions.

def plot_dist(var1, var2, basin, var1_name, var2_name):
    #
    #---- Histograms
    fig, ax = plt.subplots(1, 1, figsize = [8,5])
    ax.hist(var1, bins=20, density=True, color='#2aa198', alpha=0.25)
    ax.hist(var2, bins=20, density=True, color='#d33682', alpha=0.25)
    ax.set_title(var1_name + ', ' + var2_name + ' (' + basin + ')')
    ax.set_xlabel(r'$m^3\ s^{-1}$')
    #
    #---- Add normal distributions.
    halfwidth1 = np.max([np.nanmean(var1) - np.nanmin(var1),
                        np.nanmax(var1) - np.nanmean(var1)])
    x1 = np.linspace(np.nanmean(var1) - halfwidth1,
                    np.nanmean(var1) + halfwidth1, 200)
    ax.plot(x1, norm.pdf(x1, loc=np.nanmean(var1), scale=np.nanstd(var1)),
            c='#2aa198')
    halfwidth2 = np.max([np.nanmean(var2) - np.nanmin(var2),
                        np.nanmax(var2) - np.nanmean(var2)])
    x2 = np.linspace(np.nanmean(var2) - halfwidth2,
                    np.nanmean(var2) + halfwidth2, 200)
    ax.plot(x2, norm.pdf(x2, loc=np.nanmean(var2), scale=np.nanstd(var2)),
            c='#d33682')
    #
    #---- Run Shapiro-Wilks test on differences.
    print('----- Shapiro-Wilks -----')
    print('----- Sample 1')
    stat1, p1 = shapiro(var1)
    print('Statistic=%.3f, p=%.3f' % (stat1, p1))
    # interpret
    alpha = 0.05
    if p1 > alpha:
        print('Sample 1 looks Gaussian -- fail to reject H0 at alpha=%.2f' % \
              alpha)
    else:
        print('Sample 1 does not look Gaussian -- reject H0 at alpha=%.2f' % \
              alpha)
    print('----- Sample 2')
    stat2, p2 = shapiro(var2)
    print('Statistic=%.3f, p=%.3f' % (stat2, p2))
    # interpret
    alpha = 0.05
    if p2 > alpha:
        print('Sample 2 looks Gaussian -- fail to reject H0 at alpha=%.2f' % \
              alpha)
    else:
        print('Sample 2 does not look Gaussian -- reject H0 at alpha=%.2f' % \
              alpha)
    #
    #
    # Display the plot.
    plt.show()
    #
    return
    #---- Add some info to plot.
    infotext = 'N = ' + '{:d}'.format(len(diffs)) + \
               '\nMean = ' + '{:.3g}'.format(np.nanmean(diffs)) + \
               '\nStd. dev. = ' + '{:.3g}'.format(np.nanstd(diffs)) + \
               '\n--- Shapiro-Wilks ---' + \
               '\nStatistic=' + '{:.3f}'.format(stat) + \
               '\np=' + '{:.5f}'.format(p)
    ax.text(0.05, 0.95, infotext,
            verticalalignment='top', horizontalalignment='left',
            transform=ax.transAxes)
    #
    #---- Add inset plot.
    ax2 = mpl_il.inset_axes(ax,
                            width='25%',
                            height='25%')
    res = stats.probplot(diffs, dist='norm',
                         sparams=(np.nanmean(diffs), np.nanstd(diffs)),
                         plot=ax2)
    ax2.set_title('')
    if nrm == 'total':
        ax2.set_xlabel('Theoretical /\n' + r'$m^3\ s^{-1}$')
        ax2.set_ylabel('Observed /\n' + r'$m^3\ s^{-1}$')
    elif nrm == 'area':
        ax2.set_xlabel('Theoretical /\n' + r'$mm\ day^{-1}$')
        ax2.set_ylabel('Observed /\n' + r'$mm\ day^{-1}$')
    else:
        print('Normalizaton (option \'norm\') not valid.')
    #
    # Display the plot.
    plt.show()
    #
    return

##### Quantile plot.
def plot_qq(diffs, basin, var1, var2, nrm):
    fig, ax = plt.subplots(1, 1, figsize = [8,5])
    res = stats.probplot(diffs, dist='norm',
                         sparams=(np.nanmean(diffs), np.nanstd(diffs)),
                         plot=plt)
    ax.set_title('Normal Probability Plot')
    if nrm == 'total':
        ax.set_xlabel('Theoretical quantiles / ' + r'$m^3\ s^{-1}$')
        ax.set_ylabel('Ordered values / ' + r'$m^3\ s^{-1}$')
    elif nrm == 'area':
        ax.set_xlabel('Theoretical quantiles / ' + r'$mm\ day^{-1}$')
        ax.set_ylabel('Ordered values / ' + r'$mm\ day^{-1}$')
    else:
        print('Normalizaton (option \'norm\') not valid.')
    #
    # Display the plot.
    #plt.show()
    #
    return(fig, ax)


################################################################################
# Data load functions.

def get_r_hat(basin, comb):
    #
    # Read all data.
    all_vars = get_budget_vars(basin)
    #
    # Get required combination data.
    r_hat_type = comb.split('_')[0]
    pr_name = comb.split('_')[1]
    evap_name = comb.split('_')[2]
    GRACE_name = comb.split('_')[3]
    #
    # Get required variables (in m3 s-1).
    if r_hat_type == 'PE':
        pr = all_vars['pr_' + pr_name]
        evap = all_vars['evap_' + evap_name]
        dSdt = all_vars['dSdt_m_' + GRACE_name]
        r_hat = pr - evap - dSdt
    elif r_hat_type == 'conv':
        if pr_name == 'ERA5':
            conv = all_vars['vimd_ERA5']
        elif pr_name == 'MERRA2':
            conv =  all_vars['viwvc_MERRA2']
        else:
            sys.exit('Must use ERA5 or MERRA2 with convergence method.')
        dtcwdt = all_vars['dtcwdt_m_' + evap_name]
        dSdt = all_vars['dSdt_m_' + GRACE_name]
        r_hat = conv - dtcwdt - dSdt
    else:
        sys.exit('r_hat_type must be one of \'PE\' and \'conv\'.')
    #
    # Subset to just since 2000.
    r_hat = r_hat.sel(time=slice('2000-01-01','2019-12-31'))
    #
    return(r_hat)

def get_streamflow(basin):
    #
    # Get Obidos data.
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    #
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    #
    # Scale up for Amazon.
    if basin == 'Amazon':
        # Get factor to correct Obidos streamflow to mouth.
        DaiTren_filename = '~/Data/in-situ/DaiTrenberth/' + \
                           'coastal-stns-Vol-monthly.updated-Aug2014.nc'
        ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
        ob_to_mouth = ds_DT.ratio_m2s[0].data
        #
        output = ro_ob*ob_to_mouth
    elif basin == 'Obidos':
        output = ro_ob
    else:
        sys.exit('r_hat_type must be one of \'Amazon\' and \'Obidos\'.')
    #
    # Subset to just since 2000.
    #output = output.sel(time=slice('2000-01-01','2019-12-31'))
    #
    return(output)

################################################################################

###########################################
# Now actually execute the script.
###########################################

if __name__ == '__main__':
    v1,v2 = main()
