"""
Plot map of correlations between streamflow and salinity. 
===============
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import xesmf as xe
import iris
import datetime
from iris.time import PartialDateTime
import cf_units
import glob
import scipy.stats
import os
import sys
import subprocess
# For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

# Function for git commit hash:
def get_git_revision_hash():
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    if sys.version_info[0] >= 3:
        return hash.decode('utf-8')
    else:
        return hash
    
###########################################


###### Load data. #################################################

print('Reading data...')

# Specify file locations. 
sal_filename_wildcard = '/Users/jameseyre/Data/satellite/SMOS/SMOS_L3_DEBIAS_LOCEAN_AD_*_EASE_09d_25km_v03_monthly.nc'
hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'

plot_dir = '/Users/jameseyre/Documents/plots/AmazonCongo/'

# Specify time period for comparison.
st_time = np.datetime64('2010-02-01')
end_time = np.datetime64('2016-10-31')

# Load datasets.
ds_hy = xr.open_dataset(hybam_filename)
ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
ds_sal = xr.open_mfdataset(sal_filename_wildcard, concat_dim='time')

# Get required variables. 
ro_ob = ds_hy.Q.loc[dict(time=slice(st_time, end_time))]
sss = ds_sal.SSS.loc[dict(time=slice(st_time, end_time))]
sss = sss.load()

# Get factor to correct Obidos streamflow to mouth.
ob_to_mouth = ds_DT.ratio_m2s[0].data
ro_DT = ro_ob*ob_to_mouth

###### Calculate correlations. ###############################################

print('Calculating correlations...')

# Define function.
corr_map = np.vectorize(scipy.stats.pearsonr,signature='(n),(n)->(),()')

# Define dataarray to hold answer.
r_maps = sss[0,:,:].copy()
r_maps = r_maps.load()

# Do the calculation.
(r_maps[:,:],dummy) = corr_map(sss.transpose('lat','lon','time'),
                               ro_DT)

###### Plot  maps. ###############################################

# 2d lat and lon arrays for plotting.
dlatm, dlonm = np.meshgrid(sss.lat,sss.lon,indexing='ij')

# Define map projection.
projection = ccrs.PlateCarree()

# Open figure.
fig = plt.figure(figsize=(8, 4.5))
ax = plt.axes(projection=projection)

# Plot correlations.
p = ax.pcolormesh(dlonm, dlatm, r_maps,
                  vmin=-1.0, vmax=1.0, 
                  transform=projection,
                  cmap='RdBu_r')
#p = ax.contourf(r_maps.lon, r_maps.lat, r_maps,
#                vmin=-1.0, vmax=1.0,
#                transform=projection,
#                cmap='RdBu_r')
# Plot Obidos location.
plt.plot(-55.0,-1.9,color='red',marker='o', transform=projection)
# Set map spatial extent.
ax.set_extent([-70, 0, -20, 25], projection)
# Set gridline and label details.
gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                  linewidth=2, color='gray', alpha=0.5, linestyle='--')
gl.xlabels_top = False
gl.ylabels_right = False
gl.ylocator = mticker.FixedLocator(np.arange(-90,91,15))
gl.xlocator = mticker.FixedLocator(np.arange(-180,181,15))
# Set geographic features.
ax.coastlines()
ax.add_feature(cfeature.RIVERS,edgecolor='dodgerblue')
ax.add_feature(cfeature.LAKES,edgecolor='dodgerblue', facecolor='dodgerblue')
ax.add_feature(cfeature.LAND, facecolor='lightgray')
# Add colorbar for correlations.
cb = fig.colorbar(p, orientation='vertical', extend='max',
                  aspect=65,shrink=0.9,
                  pad=0.05, extendrect='True')
# Add info to footer. 
#plt.tight_layout()
txtl = 'AmazonFlow_SSS_correlation_map.py            Last commit: ' + \
       get_git_revision_hash()
plt.text(0.02,0.02,txtl, transform=fig.transFigure, size=5)

# Display the plot
# plt.show()

# Save the figure.
plt.savefig(plot_dir + 'AmazonFlow_SSS_correlation_map.png',
            format='png', dpi=900)
