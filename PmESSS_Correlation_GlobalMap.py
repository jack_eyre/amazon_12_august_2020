"""
Plot global maps of correlations between (precip. minus evaporation) 
and salinity (inc. anomalies). 
===============
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import xesmf as xe
import iris
import datetime
from iris.time import PartialDateTime
import cf_units
import glob
import scipy.stats
import os
import sys
import subprocess
# For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

# Function for git commit hash:
def get_git_revision_hash():
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    if sys.version_info[0] >= 3:
        return hash.decode('utf-8')
    else:
        return hash
    
###########################################


print('Reading data...')

###### Load precip. data. #################################################

data_dir = '/Users/jameseyre/Data/'
plot_dir = '/Users/jameseyre/Documents/plots/AmazonCongo/'

# Get precip data.
GPCP_filename = data_dir + 'satellite/GPCP.v2.3.precip.mon.mean.nc'
GPCP = xr.open_dataset(GPCP_filename)
precip = GPCP['precip'].sel(time=slice('2011-09-01','2015-05-31'))
precip.coords['month'] = ('time', precip['time.month'])
precip.coords['year'] =  ('time', precip['time.year'])

###### Load evap. data.## #################################################

ERA_filename = data_dir + 'reanalysis/ERA-int/AmazonCongo/era_interim_mdfa_sfc_fc_2010-2018.nc'
ds_ERA = xr.open_dataset(ERA_filename)

# Read for required periods, and remove 90 and -90 latitudes
# because they mess up conservative regridding.
evap = ds_ERA.e.sel(time=slice('2011-09-01','2015-05-31'),
                    latitude=slice(89,-89))
evap.coords['month'] = ('time', evap['time.month'])
evap.coords['year'] =  ('time', evap['time.year'])

###### Load salinity data. #################################################

# Get salinity file list.
aq_fn_list = glob.glob(data_dir + 'satellite/aquarius/' +
                       '*_L3m_MO_SCI_V5.0_SSS_1deg.bz2.nc4')
aq_cubes = iris.load(aq_fn_list, 'Sea Surface Salinity')

# Define lat and lon coordinates.
lats =  iris.coords.DimCoord(points=np.arange(89.5,-90.0,-1.0),
                             standard_name='latitude',
                             long_name='latitude',
                             var_name='latitude',
                             units='degrees_north')
lats.guess_bounds(bound_position=0.5)
lons = iris.coords.DimCoord(np.arange(-179.5,180.0,1.0),
                            standard_name='longitude',
                            long_name='longitude',
                            var_name='longitude',
                            units='degrees_east')
lons.guess_bounds(bound_position=0.5)

# Loop over individual monthly cubes and add metadata.
for cube in aq_cubes:
    # Add spatial coordinates.
    cube.add_dim_coord(lats, 0)
    cube.add_dim_coord(lons, 1)
    
    # Get time info from attributes.
    t1 = cube.attributes['H5_GLOBAL.time_coverage_start']
    t2 = cube.attributes['H5_GLOBAL.time_coverage_end']
    
    # Convert to datetime objects.
    dt1 = datetime.datetime(int(t1[0:4]),
                            int(t1[5:7]),
                            int(t1[8:10]),
                            int(t1[11:13]),
                            int(t1[14:16]),
                            round(float(t1[17:23]))%60)
    dt2 = datetime.datetime(int(t2[0:4]),
                            int(t2[5:7]),
                            int(t2[8:10]),
                            int(t2[11:13]),
                            int(t2[14:16]),
                            round(float(t2[17:23]))%60)
    dtmean = dt1 + (dt2-dt1)/2
    
    # Add as scalar coordinates.
    time = iris.coords.AuxCoord(dtmean,
                                standard_name='time',
                                var_name='time',
                                long_name='time_in_datetime_format',
                                bounds=np.array([dt1,dt2]))
    cube.add_aux_coord(time)
    
    # Get rid of mis-matched attributes.
    diff_atts = ['H5_GLOBAL.product_name', 'H5_GLOBAL.date_created', 'H5_GLOBAL.history', 'H5_GLOBAL.time_coverage_start', 'H5_GLOBAL.time_coverage_end', 'H5_GLOBAL.start_orbit_number', 'H5_GLOBAL.end_orbit_number', 'H5_GLOBAL.data_bins', 'H5_GLOBAL.data_minimum', 'H5_GLOBAL.data_maximum', 'H5_GLOBAL._lastModified', 'H5_GLOBAL.id', 'H5_GLOBAL.source']
    for att in diff_atts:
        del cube.attributes[att]

# Merge cubes together to form a single salinity cube.
aq_salinity = aq_cubes.merge_cube()

# Add time dim_coord (and remove time aux_coord).
dt_all = aq_salinity.coord('time')
t_units = 'hours since 2011-08-01 00:00:00'
t_all = cf_units.date2num(dt_all.points, t_units, cf_units.CALENDAR_STANDARD)
time = iris.coords.DimCoord(t_all,standard_name='time',long_name='time',var_name='time',units=t_units)
aq_salinity.remove_coord('time')
aq_salinity.add_dim_coord(time,0)

# Convert to xarray.
aq_da = xr.DataArray.from_iris(aq_salinity)
aq_da.coords['month'] = ('time', aq_da['time.month'])
aq_da.coords['year'] =  ('time', aq_da['time.year'])


###### Calculate climatologies. ###############################################

# Climatology.
aq_clim = aq_da.sel(time=slice('2011-09-01','2015-05-31')).groupby('time.month').mean('time')
precip_clim = precip.groupby('time.month').mean('time')
evap_clim = evap.groupby('time.month').mean('time')

# Anomaly from climatological seasonal cycle.
aq_anom = aq_da.sel(time=slice('2011-09-01','2015-05-31')).groupby('time.month') - aq_clim
precip_anom = precip.groupby('time.month') - precip_clim
evap_anom = evap.groupby('time.month') - evap_clim


###### Regrid to common grid. ###############################################
# Uses ESMPy.
# Useful webpages:
#     http://www.earthsystemmodeling.org/esmf_releases/last_built/esmpy_doc/html/examples.html
#     https://github.com/nawendt/esmpy-tutorial/blob/master/esmpy_tutorial.ipynb
#     https://media.readthedocs.org/pdf/xesmf/stable/xesmf.pdf
################################################################################

print('Regridding Aquarius...')

# Get grid sizes.
GPCP_shape = np.array(precip.shape)
aq_shape = np.array(aq_da.shape)

# Create 2D lat and lon arrays.
slat1d =  np.array(aq_da['latitude'])
slon1d =  np.array(aq_da['longitude'])
slatm, slonm = np.meshgrid(slat1d,slon1d,indexing='ij')
dlat1d = np.array(precip['lat'])
dlon1d = np.array(precip['lon'])
dlatm, dlonm = np.meshgrid(dlat1d,dlon1d,indexing='ij')
# Now for corners:
s_delta = np.absolute(slat1d[1] - slat1d[0])
if (s_delta != np.absolute(slon1d[1] - slon1d[0])):
    print('ACHTUNG: source grid spacing not regular')
d_delta = np.absolute(dlat1d[1] - dlat1d[0])
if (d_delta != np.absolute(dlon1d[1] - dlon1d[0])):
    print('ACHTUNG: destination grid spacing not regular')
slatc1d = slat1d[0] + (s_delta/2.0) - np.arange(aq_shape[1]+1)*s_delta
slonc1d = slon1d[0] - (s_delta/2.0) + np.arange(aq_shape[2]+1)*s_delta
slatcm, sloncm = np.meshgrid(slatc1d,slonc1d,indexing='ij')
dlatc1d = dlat1d[0] - (d_delta/2.0) + np.arange(GPCP_shape[1]+1)*d_delta
dlonc1d = dlon1d[0] - (d_delta/2.0) + np.arange(GPCP_shape[2]+1)*d_delta
dlatcm, dloncm = np.meshgrid(dlatc1d,dlonc1d,indexing='ij')

ds = xr.Dataset({'lat': (['x_in', 'y_in'], slatm),
                 'lon': (['x_in', 'y_in'], slonm),
                 'lat_b': (['xb_in', 'yb_in'], slatcm),
                 'lon_b': (['xb_in', 'yb_in'], sloncm),
                }
               )
dd = xr.Dataset({'lat': (['x_out', 'y_out'], dlatm),
                 'lon': (['x_out', 'y_out'], dlonm),
                 'lat_b': (['xb_out', 'yb_out'], dlatcm),
                 'lon_b': (['xb_out', 'yb_out'], dloncm),
                }
               )

grid_aq = {'lon': slon1d, 'lat': slat1d,
           'lon_b': slonc1d, 'lat_b': slatc1d}
grid_out = {'lon': dlon1d, 'lat': dlat1d,
            'lon_b': dlonc1d, 'lat_b': dlatc1d}

#regridder = xe.Regridder(ds, dd, 'conservative',
#                         reuse_weights=True,
#                         periodic=True)
regridder = xe.Regridder(grid_aq, grid_out,
                         'conservative',
                         reuse_weights=True,
                         periodic=True)
aq_rg_GPCP = regridder(aq_da.sel(time=slice('2011-09-01','2015-05-31')))
aq_clim_rg_GPCP = regridder(aq_clim)
aq_anom_rg_GPCP = regridder(aq_anom)

################################################################################

print('Regridding ERA-Interim...')

# Do this a slightly simpler way than above....

# Create lat and lon arrays.
elat1d = evap.latitude.values
elon1d = evap.longitude.values
e_delta = np.absolute(elat1d[1] - elat1d[0])
if (e_delta != np.absolute(elon1d[1] - elon1d[0])):
    print('ACHTUNG: source grid spacing may not be regular')

# Create lat and lon bounds arrays.
if (elat1d[0] == np.max(elat1d)):
    elatc1d = np.append(elat1d + (e_delta/2.0), elat1d[-1] - (e_delta/2.0))
elif (elat1d[0] == np.min(elat1d)):
    elatc1d = np.append(elat1d - (e_delta/2.0), elat1d[-1] + (e_delta/2.0))
#
if (elon1d[0] == np.max(elon1d)):
    elonc1d = np.append(elon1d + (e_delta/2.0), elon1d[-1] - (e_delta/2.0))
elif (elon1d[0] == np.min(elon1d)):
    elonc1d = np.append(elon1d - (e_delta/2.0), elon1d[-1] + (e_delta/2.0))

# Create dictionaries of coordinates and bounds.
grid_in = {'lon': elon1d, 'lat': elat1d,
           'lon_b': elonc1d, 'lat_b': elatc1d}
grid_out = {'lon': dlon1d, 'lat': dlat1d,
            'lon_b': dlonc1d, 'lat_b': dlatc1d}

# Create the regridding tool.
regridder_ERA = xe.Regridder(grid_in, grid_out,
                             'conservative',
                             reuse_weights=True,
                             periodic=True)
evap_out = regridder_ERA(evap)
evap_out_clim = regridder_ERA(evap_clim)
evap_out_anom = regridder_ERA(evap_anom)

##### Calculate P-E. ###############################################

print('Calculating P-E ...')

# Including units conversions. 
# evap is negative here, so actually add it.
PmE = precip + 1000.0*evap_out

# And climatology and anomalies.
PmE_clim = precip_clim + 1000.0*evap_out_clim
PmE_anom = precip_anom + 1000.0*evap_out_anom

###### Calculate correlations. ###############################################

print('Calculating correlations...')

# Define function.
corr_map = np.vectorize(scipy.stats.pearsonr,signature='(n),(n)->(),()')

# Define dataarray to hold answer.
r_maps = xr.DataArray(np.zeros((3,GPCP_shape[1],GPCP_shape[2])),
                      coords=[('var', [1,2,3]),
                              ('lat', dlat1d),
                              ('lon', dlon1d)])
r_maps['var_type'] = ('var', ['absolute','anomaly','mean annual cycle'])

# Do the calculation.
(r_maps[0,:,:],dummy) = corr_map(aq_rg_GPCP.transpose('lat','lon','time'),
                                 PmE.transpose('lat','lon','time'))
(r_maps[1,:,:],dummy) = corr_map(aq_anom_rg_GPCP.transpose('lat',
                                                           'lon',
                                                           'time'),
                                 PmE_anom.transpose('lat','lon','time'))
(r_maps[2,:,:],dummy) = corr_map(aq_clim_rg_GPCP.transpose('lat',
                                                           'lon',
                                                           'month'),
                                 PmE_clim.transpose('lat','lon','month'))


###### Plot  maps. ###############################################

projection = ccrs.PlateCarree()
axes_class = (GeoAxes,
              dict(map_projection=projection))

fig = plt.figure(figsize=(12, 5))
axgr = AxesGrid(fig, 111, axes_class=axes_class,
                nrows_ncols=(1, 3),
                axes_pad=0.25,
                cbar_location='bottom',
                cbar_mode='single',
                cbar_pad=0.1,
                cbar_size='3%',
                direction='row',
                label_mode='')  # note the empty label_mode

for i, ax in enumerate(axgr):
    ax.set_extent([-70, 0, -20, 25], projection)
    ax.coastlines()
    ax.add_feature(cfeature.RIVERS,edgecolor='dodgerblue')
    ax.add_feature(cfeature.LAKES,edgecolor='dodgerblue',
                   facecolor='dodgerblue')
    ax.add_feature(cfeature.LAND, facecolor='lightgray')
    ax.set_title('Corr(P-E,SSS): '+r_maps['var_type'].data[i])
    ax.set_xticks(np.array([-60, -45, -30, -15]), crs=projection)
    ax.set_yticks(np.array([-15, 0, 15]), crs=projection)
    lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                       degree_symbol='')
    lat_formatter = LatitudeFormatter(degree_symbol='')
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    # Actually draw the plot.
    p = ax.pcolormesh(dlonm, dlatm, r_maps[i,:,:],
                      vmin=-1.0, vmax=1.0, 
                      transform=projection,
                      cmap='RdBu_r')
        
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)

# Draw the color bar (shared by all plots).
axgr.cbar_axes[0].colorbar(p)

# Add info to footer. 
txtl = 'compare_SSS_datasets.py            Last commit: ' + \
       get_git_revision_hash()
anchored_text = AnchoredText(txtl, loc=3,borderpad=0,frameon=False,
                             prop=dict(size=5),
                             bbox_to_anchor=(0., 0.),
                             bbox_transform=fig.transFigure) #ax.transAxes)
axgr[0].add_artist(anchored_text)


# Display the plot
#plt.show()

# Save the figure.
plt.savefig('/Users/jameseyre/Documents/plots/AmazonCongo/PmESSS_Correlation_AmazonMap.pdf')
#plt.savefig('/Users/jameseyre/Documents/plots/AmazonCongo/PmESSS_Correlation_GLobalMap.pdf')
#####
