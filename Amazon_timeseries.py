"""
Plot time series of Amazon runoff estimates using several different methods.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################


################################################################################
def main():
    
    ##### Read hydrology data. ###########################################
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Fill gaps in streamflow (inserts nan for missing months)
    ro_ob = ro_ob.resample(time='1M').asfreq()
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    
    # Get factor to correct Obidos streamflow to mouth.
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    ob_to_mouth = ds_DT.ratio_m2s[0].data
    
    # Scale up Obidos streamflow to river mouth.
    ro_DT = ro_ob.copy()
    ro_DT = ro_DT*ob_to_mouth
    
    # Get other water budget variables (custom load functions below).
    # All in m3 s-1.
    all_vars = get_budget_vars('Amazon')
    
    # Define which streamflow estimates to plot.
    streamflow_Amazon = {}
    
    # Add streamflow to dictionary.
    streamflow_Amazon['observed'] = ro_DT
    
    # Add residual methods.
    streamflow_Amazon['PE_GPCP_ERA5_JPL'] = all_vars['pr_GPCP'] - \
                                            all_vars['evap_ERA5'] - \
                                            all_vars['dSdt_m_JPL']
    streamflow_Amazon['PE_ERA5_ERA5_JPL'] = all_vars['pr_ERA5'] - \
                                            all_vars['evap_ERA5'] - \
                                            all_vars['dSdt_m_JPL']
    streamflow_Amazon['all_Ens'] = 0.4*all_vars['conv_Ens'] + \
                                   0.6*all_vars['pr_Ens'] - \
                                   0.4*all_vars['dtcwdt_m_Ens'] - \
                                   0.6*all_vars['evap_Ens'] - \
                                   all_vars['dSdt_m_Ens']
    
    
    ##### Read salinity data. ###########################################
    
    # Define region to average over.
    sal_region = np.array([0.0,7.5,-55.0,-45.0])
    
    # Open and read file.
    sal_filename_wildcard = '/Users/jameseyre/Data/satellite/SMOS/' + \
                            'SMOS_L3_DEBIAS_LOCEAN_AD_*' + \
                            '_EASE_09d_25km_v03_monthly.nc'
    ds_sal = xr.open_mfdataset(sal_filename_wildcard, concat_dim='time')
    # Don't get the first (incomplete) month.
    sss = ds_sal.SSS.sel(time=slice('2010-02-01','2017-12-31'),
                         lat=slice(sal_region[0],sal_region[1]),
                         lon=slice(sal_region[2],sal_region[3]))\
                         .mean(['lat','lon']).load()
    
    ##### Plot results. ###########################################
    
    print('Plotting...')
    plot = plot_timeseries(streamflow_Amazon, sss)


################################################################################

################################################################################
# Plotting function.

def plot_timeseries(q, S):
    #
    # Get time period of overlap.
    time_min = min(q['observed'].time)
    time_max = max(q['observed'].time)
    for k in q.keys():
        time_min = max([time_min, min(q[k].time)])
        time_max = min([time_max, max(q[k].time)])
    time_min = max([time_min, min(S.time)])
    time_max = min([time_max, max(S.time)])
    
    # Define colors.
    custom_colors = ['#2aa198','#6c71c4','#cb4b16','#859900']
    
    # Set up plot with observed streamflow.
    fig = plt.figure(figsize=(9, 6))
    fig.subplots_adjust(wspace=0, hspace=0.25)
    
    # Top left - seasonal cycle.
    ax1 = fig.add_subplot(221)
    ax1.set_ylabel('Discharge / '+ r'$m^{3} s^{-1}$')
    ax1.set_xlabel('Month')
    ax1.set_xlim(0,13)
    ax1.set_xticks(np.arange(1,13,1))
    ax1.set_xticklabels(['J','F','M','A','M','J',
                         'J','A','S','O','N','D'])
    # Top right - annual means.
    ax2 = fig.add_subplot(222, sharey=ax1)
    ax2.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax2.set_xlabel('Year')
    plt.setp(ax2.get_yticklabels(), visible=False)
    # Bottom both columns - monthly anomalies.
    ax3 = fig.add_subplot(212)
    ax3.set_ylabel('Discharge anomaly / '+ r'$m^{3} s^{-1}$')
    ax3.set_xlabel('Year')
    
    # Loop over streamflow estimates.
    i = 0
    for k in q.keys():
        # Subset data.
        temp = q[k].sel(time=slice(time_min, time_max))
        # Select color.
        if (k == 'observed'):
            color = 'k'
        else:
            color = custom_colors[i]
            i = i+1
        # Calculate each quantity and plot.
        clim = temp.groupby('time.month').mean('time')
        ax1.plot(clim.month, clim, color, label=k)
        ann_mean = temp.groupby('time.year').mean('time')
        ax2.plot(ann_mean.year, ann_mean, color, label=k)
        anom = temp.groupby('time.month') - clim
        ax3.plot(anom.time, anom, color, label=k)
    
    # Add legend to one of the plots.
    ax3.legend(loc='lower left', ncol=4)
    
    # Add salinity with right hand axes.
    ax1s = ax1.twinx()
    ax2s = ax2.twinx()
    ax3s = ax3.twinx()
    color = 'k--'
    temp = -1.0*S.sel(time=slice(time_min, time_max))
    clim = temp.groupby('time.month').mean('time')
    ax1s.plot(clim.month, clim, color, label='salinity')
    ann_mean = temp.groupby('time.year').mean('time')
    ax2s.plot(ann_mean.year, ann_mean, color, label='salinity')
    anom = temp.groupby('time.month') - clim
    ax3s.plot(anom.time, anom, color, label='salinity')
    # Set a few plot details.
    ax2s.set_ylim(ax1s.get_ylim())
    plt.setp(ax1s.get_yticklabels(), visible=False)
    plt.setp(ax2s.get_yticklabels(), visible=True)
    ax1s.tick_params(axis='y', colors='red')
    ax2s.tick_params(axis='y', colors='red')
    ax3s.tick_params(axis='y', colors='red')
    ax2s.set_ylabel('SSS / PSU', color='r')
    ax3s.set_ylabel('SSS anomaly / PSU', color='r')
    
    # Add labels to upper right.
    plt.text(0.9, 0.9,'a', ha='left', va='center',
             transform=ax1.transAxes)
    plt.text(0.9, 0.9,'b', ha='left', va='center',
             transform=ax2.transAxes)
    plt.text(0.95, 0.9,'c', ha='left', va='center',
             transform=ax3.transAxes)
    
    # Add titles to left.
    plt.text(0.05, 0.1,'Amazon mean annual cycle', ha='left', va='center',
             transform=ax1.transAxes)
    plt.text(0.05, 0.1,'Amazon annual mean', ha='left', va='center',
             transform=ax2.transAxes)
    plt.text(0.025, 0.9,'Amazon monthly anomaly', ha='left', va='center',
             transform=ax3.transAxes)
    
    # Get git info for this script.
    sys.path.append('/Users/jameseyre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    
    # Add git info to footer.
    plt.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    
    # Show the plot.
    #plt.show()
    # Or save the figure.
    fig.savefig('/Users/jameseyre/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.pdf',
                format='pdf')


################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    main()
