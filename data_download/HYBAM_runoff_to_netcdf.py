"""
Convert runoff CSV files to NetCDF for Amazon, Orinoco and Congo.
"""

###########################################
import numpy as np
import xarray as xr
import pandas as pd
import datetime
import calendar
import os
import subprocess
import sys
###########################################

# Specify data location details.
data_dir = '~/Data/in-situ/hybam/'
work_dir = '~/Documents/code/AmazonCongo/data_download/'

# Filenames all have .csv appended.
filenames = ['Amazon_Obidos',
             'Congo_BrazzavilleBeach',
             'Orinoco_CiudadBolivar']
filename_tail = '.csv'

# New file names will be the same with netcdf extension.
new_filename_tail = '.nc'

##### A few other details. ######### ###########################################

river_names = ['Amazon','Congo','Orinoco']
source_files = ['_862FC2E05D93FE5861EE58D92F12F5E3_1550794718910param.xls',
                '_1ABE7A2A55A2C1BF65C5D6F54AAADC28_1550866484141param.xls',
                '_2ECE533557BBF27ECEEF5F0CAAA5AC78_1550867617591param.xls']

# For Git commit hash. 
def get_git_revision_hash():
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    if sys.version_info[0] >= 3:
        return hash.decode('utf-8')
    else:
        return hash


##### Loop over files ######### #############################################
##### and convert each in turn.

for ff in range(len(filenames)):

    df = pd.read_csv(data_dir + filenames[ff] + filename_tail,
                     index_col='time')
    Q = df['Q_m3s-1'].copy()
    
    # Sort missing data.
    Q[Q==0] = np.nan
    
    # Construct datetime array.
    dts = pd.to_datetime(df[['year','month','day']])

    # Construct xarray to save to file.
    ds_out = xr.Dataset({'Q': (['time'], Q)},
                        coords={'time':(['time'],dts)})
    
    # Set up encoding.
    t_units = 'days since 1960-01-01 00:00:00'
    t_cal = 'standard'
    fill_val = 9.96921e+36
    str_enc = 'S1'
    wr_enc = {'Q':{'_FillValue':fill_val},
              'time':{'units':t_units,'calendar':t_cal,
                      '_FillValue':fill_val}}

    # Add other metadata.
    ds_out.Q.attrs['units'] = 'm^3 s^{-1}'
    ds_out.Q.attrs['long_name'] = 'monthly_mean_discharge'
    ds_out.time.attrs['long_name'] = 'time'
    ds_out.attrs['station_id'] = np.array2string(df['station_id'][1])
    ds_out.attrs['station_name'] = df['station_name'][1]
    ds_out.attrs['river_name'] = river_names[ff]
    ds_out.attrs['pp_comment'] = 'File converted to NetCDF by Jack Reeves Eyre (University of Arizona)'
    ds_out.attrs['pp_script'] = 'HYBAM_runoff_to_netcdf.py'
    ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/amazoncongo/src/master/'
    ds_out.attrs['pp_script_last_commit'] = get_git_revision_hash()
    ds_out.attrs['source_file'] = data_dir + source_files[ff]
    ds_out.attrs['creation_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    
    # Save out file.
    ds_out.to_netcdf(path=data_dir + filenames[ff] + new_filename_tail,
                     mode='w',
                     encoding=wr_enc,
                     unlimited_dims=['time'])

