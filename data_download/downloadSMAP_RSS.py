"""
Download SMAP satellite monthly sea surface salinity data - RSS retrieval.
===============
"""

###########################################
import numpy as np
import datetime
import calendar
import urllib.request
###########################################

# Specify data location details.
save_dir = '/Users/jameseyre/Data/satellite/SMAP/'
url_base = 'ftp://podaac-ftp.jpl.nasa.gov/allData/smap/L3/RSS/V3/monthly/SCI/70KM/'
filename_head = 'RSS_smap_SSS_L3_monthly_70km_'
filename_tail = '_FNL_v03.0.nc'

# Specify dates of available data.
time_min = datetime.datetime(2015,4,1,0,0,0)
time_max = datetime.datetime(2019,2,1,0,0,0)

# Loop over months and pull each file. 
mm = time_min.month
yy = time_min.year
while datetime.datetime(yy,mm,1,0,0,0) < time_max:

    # Construct filename of target.
    old_filename = filename_head + \
                   "{:04d}".format(yy) + "_" + \
                   "{:02d}".format(mm) + \
                   filename_tail

    # Construct filename to save to.
    new_filename = old_filename

    # Do the download.
    urllib.request.urlretrieve(url_base + "{:04d}".format(yy) + \
                               '/' + old_filename,
                               save_dir + new_filename)

    # Increment for next month.
    if mm == 12:
        mm = 1
        yy += 1
    else:
        mm +=1
        
