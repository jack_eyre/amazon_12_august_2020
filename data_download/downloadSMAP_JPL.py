"""
Download SMAP satellite monthly sea surface salinity data.
===============
"""

###########################################
import numpy as np
import datetime
import calendar
import urllib.request
###########################################

# Specify data location details.
save_dir = '/Users/jameseyre/Data/satellite/SMAP/'
url_base = 'ftp://podaac.jpl.nasa.gov/allData/smap/L3/JPL/V4.2/monthly/'
filename_head = 'SMAP_L3_SSS_'
filename_tail = '_MONTHLY_V4.2.nc'

# Specify dates of available data.
time_min = datetime.datetime(2015,4,1,0,0,0)
time_max = datetime.datetime(2019,2,1,0,0,0)

# Loop over months and pull each file. 
mm = time_min.month
yy = time_min.year
while datetime.datetime(yy,mm,1,0,0,0) < time_max:

    # Construct filename of target.
    old_filename = filename_head + \
                   "{:04d}".format(yy) + \
                   "{:02d}".format(mm) + \
                   filename_tail

    # Construct filename to save to.
    new_filename = old_filename

    # Do the download.
    urllib.request.urlretrieve(url_base + "{:04d}".format(yy) + \
                               '/' + old_filename,
                               save_dir + new_filename)

    # Increment for next month.
    if mm == 12:
        mm = 1
        yy += 1
    else:
        mm +=1
        
