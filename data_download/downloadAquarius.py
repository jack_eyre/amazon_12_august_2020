"""
Download Aquarius satellite salinity data.
===============
Downloads monthly data.
"""

###########################################
import numpy as np
import datetime
import calendar
import urllib.request
###########################################

# Specify data location details.
save_dir = '/Users/jameseyre/Data/satellite/aquarius/'
url_base = 'https://podaac-opendap.jpl.nasa.gov/opendap/allData/aquarius/L3/mapped/V5/monthly/SCI/'
filename_tail = 'L3m_MO_SCI_V5.0_SSS_1deg.bz2.nc4'

# Specify dates of available data.
time_min = datetime.datetime(2011,8,25,1,45,23)
time_max = datetime.datetime(2015,6,7,12,45,21)

# Loop over months and pull each file. 
mm = time_min.month
yy = time_min.year
while datetime.datetime(yy,mm,1,0,0,0) < time_max:

    # Construct filename of target.
    doy1 = datetime.datetime(yy,mm,1,0,0,0).timetuple().tm_yday
    ldom = calendar.monthrange(yy,mm)[1]
    doy2 = datetime.datetime(yy,mm,ldom,0,0,0).timetuple().tm_yday
    old_filename = 'Q' + "{:04d}".format(yy) + \
               "{:03d}".format(doy1) + \
               "{:04d}".format(yy) + \
               "{:03d}".format(doy2) + '.' + filename_tail

    # Construct filename to save to.
    new_filename = "{:04d}".format(yy) + '_' + "{:02d}".format(mm) + \
                   '_' + filename_tail

    # Do the download.
    urllib.request.urlretrieve(url_base + "{:04d}".format(yy) + \
                               '/' + old_filename,
                               save_dir + new_filename)

    # Increment for next month.
    if mm == 12:
        mm = 1
        yy += 1
    else:
        mm +=1
        
