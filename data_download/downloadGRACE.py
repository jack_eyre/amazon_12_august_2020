"""
Download GRACE satellite gravity anomaly data, plus associated files.
"""

# Should work in 'plotenv' conda environment.

###########################################
import urllib.request
###########################################

# Specify where to save.
save_dir = '/Users/jameseyre/Data/satellite/GRACE/'

# --- JPL.
url = 'https://podaac-tools.jpl.nasa.gov/drive/files/allData/tellus/L3/mascon/RL06/JPL/CRI/netcdf/'
filename = 'GRCTellus.JPL.200204_201706.GLO.RL06M.MSCNv01CRIv01.nc'
urllib.request.urlretrieve(url + filename,
                           save_dir + filename)
filename = 'CLM4.SCALE_FACTOR.JPL.MSCNv01CRIv01.nc'
urllib.request.urlretrieve(url + filename,
                           save_dir + filename)
filename = 'JPL_MSCNv01_PLACEMENT.nc'
urllib.request.urlretrieve(url + filename,
                           save_dir + filename)
filename = 'LAND_MASK.CRIv01.nc'
urllib.request.urlretrieve(url + filename,
                           save_dir + filename)

# --- CSR.
url = 'https://dataverse.tdl.org/dataset.xhtml?persistentId=doi:10.18738/T8/UN91VR&version=1.0/'
filename = 'CSR_GRACE_RL06_Mascons_all-corrections_v01.nc'
urllib.request.urlretrieve(url + filename,
                           save_dir + filename)
# Dataset and file citations also available from this website;
# downloaded manually.

# --- GSFC.
url = 'https://neptune.gsfc.nasa.gov/uploads/grace/mascons_2.4/'
filename = 'GSFC.glb.200301_201607_v02.4-ICE6G.h5'
urllib.request.urlretrieve(url + filename,
                           save_dir + filename)

# --- GFZ.
url = 'ftp://isdcftp.gfz-potsdam.de/grace/GravIS/GFZ/Level-3/TWS/'
filename_list = ['GravIS_RL06_TWS_Technical_Note.pdf',
                 'GRAVIS-3_2003-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2004-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2005-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2006-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2007-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2008-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2009-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2010-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2011-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2012-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2013-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2014-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2015-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2016-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2017-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2018-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
                 'GRAVIS-3_2019-----------_GFZOP_0600_TWS_GRID_GFZ_0001.nc']
for fn in filename_list:
    urllib.request.urlretrieve(url + fn,
                               save_dir + fn)
