# downloadERA-int.py
# Downloads some ERA-interim data from ECMWF servers.
#
# !!!!!
#
# Used package versions:
#      ecmwf-api-client 1.5.0 and
#      python 3.6.3
# This configuration isn't currently available
# from Conda channels so have to configure manually.

###########################################
from ecmwfapi import ECMWFDataServer
###########################################

# Open connection to server.
server = ECMWFDataServer()

####### Calling function. #####################################################

def retrieve_era_interim():

    # Specify file locations etc.
    data_dir = "/Users/jameseyre/Data/reanalysis/ERA-int/AmazonCongo/"

    # Specify date details. 
    yearStart = 2010
    yearEnd = 2018
    months = [1,2,3,4,5,6,7,8,9,10,11,12]

    # Get decades (for efficient request: this is how they are
    # stored on the ECMWF tape drives).
    years = range(yearStart, yearEnd+1)
    print('Years: ',years)
    decades = list(set([divmod(i, 10)[0] for i in years]))
    decades = [x * 10 for x in decades]
    decades.sort()
    print('Decades:', decades)
    
    # Loop through decades and create a month list.
    for d in decades:
        requestDates=''
        for y in years:
            if ((divmod(y,10)[0])*10) == d:
                for m in months:
                    requestDates = requestDates+str(y)+"{:02d}".format(m)+'01/'
        # Remove final "/".
        requestDates = requestDates[:-1]
        print('Requesting dates: ', requestDates)
        
        # Construct file names - stream, level, type and years.
        dmin = max([d,yearStart])
        dmax = min([d+9,yearEnd])
        target_sfc_an = data_dir + 'era_interim_moda_sfc_an_' + \
                        str(dmin) + '-' + str(dmax) + '.nc'
        target_sfc_fc = data_dir + 'era_interim_mdfa_sfc_fc_' + \
                        str(dmin) + '-' + str(dmax) + '.nc'

        # Now call the actual request functions. 
        era_interim_request_sfc_an(requestDates, d, target_sfc_an)
        era_interim_request_sfc_fc(requestDates, d, target_sfc_fc)

# Request for surface analysis (monthly mean of daily means).
def era_interim_request_sfc_an(requestDates, decade, target):
    server.retrieve({
        "class": "ei",
        "dataset": "interim",
        "expver": "1",
        "grid": "1.0/1.0",
        "levtype": "sfc",
        "param": "71.162/72.162",
        "stream": "moda",
        "type": "an",
        "format": "netcdf",
        "date": requestDates,
        "decade": decade,
        "target": target,
    })

# Request for surface forecast (monthly mean of forecast accumulations).
def era_interim_request_sfc_fc(requestDates, decade, target):
    server.retrieve({
        "class": "ei",
        "dataset": "interim",
        "expver": "1",
        "grid": "1.0/1.0",
        "levtype": "sfc",
        "param": "147.128/182.128/205.128/228.128",
        "step": "0-12",
        "stream": "mdfa",
        "type": "fc",
        "format": "netcdf",
        "date": requestDates,
        "decade": decade,
        "target": target,
    })

if __name__ == '__main__':
    retrieve_era_interim()
