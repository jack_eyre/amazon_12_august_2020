# downloadERA5.py
# Downloads some ERA5 data from ECMWF servers.
#
# Run from Conda environment "copernicus" which 
# includes the cdsapi installed manually using pip.

###########################################
import calendar
import cdsapi
###########################################

# Open connection to server.
server = cdsapi.Client()

####### Calling functions. #####################################################

###### This calls functions to get monthly averages of variables listed
#      in the CDS API (hence can use slightly simpler parameters).
def retrieve_era5_monthly():

    # Specify file locations etc.
    data_dir = "/Users/jameseyre/Data/reanalysis/ERA5/"

    # Specify date details. 
    yearStart = 2000
    yearEnd = 2018
    months = [1,2,3,4,5,6,7,8,9,10,11,12]
    monthlist = ["{:02d}".format(i) for i in months]

    # Get decades (for efficient request: this is how they are
    # stored on the ECMWF tape drives).
    years = range(yearStart, yearEnd+1)
    print('Years: ',years)
    decades = list(set([divmod(i, 10)[0] for i in years]))
    decades = [x * 10 for x in decades]
    decades.sort()
    print('Decades:', decades)
    
    # Loop through decades and create a month list.
    for d in decades:
        #
        # Get years in this decade.
        dmin = max([d,yearStart])
        dmax = min([d+9,yearEnd])
        yearlist = [str(i) for i in range(dmin,dmax+1)]
        
        # Construct file names - stream, level, type and years.
        target = data_dir + 'era5_moda_sfc_' + \
                 str(dmin) + '-' + str(dmax) + '.nc'

        # Now call the actual request functions. 
        era5_request_mon_cds(yearlist, monthlist, target)

###### Retrieval functions. ####################################################


###### For retrieving monthly data listed in CDS API
#      (seems to have slightly simplified usage compared to
#       full MARS request).
#
#      CDS API uses analysis where available, and forecasts where not
#      (e.g., accumulations are only forecast; total column water is
#      available in both, so the analysis is used).
#
#      When no grid is specified, but netcdf format is requested,
#      the data are provided on a regular 0.25x0.25 lat/lon grid.
#      The interpolation method used by ECMWF is NON-CONSERVATIVE!
#      If no grid is specified and GRIB format is requested, the
#      data are provided on the native reduced Gaussian grid (close
#      to 0.25x0.25 but not exactly the same). 
######
def era5_request_mon_cds(yy,mm,targ):
    server.retrieve(
        'reanalysis-era5-single-levels-monthly-means',
        {'product_type':'monthly_averaged_reanalysis',
         'variable':[
             'vertically_integrated_moisture_divergence',
             'total_column_water',
             'total_column_water_vapour',
             'evaporation',
             'total_precipitation'
         ],
         'year':yy,
         'month':mm,
         'time':'00:00',
         'format':'netcdf'
        },
        targ)

###### For retrieving surface analysis from MARS.
def era5_request_mars_sfc_an(requestDates, target):
    server.retrieve({
        "class": "ea",
        "dataset": "era5",
        "expver": "0001",
        "type": "an",
        "stream": "oper",
        "levtype": "sfc",
        "grid": "1.0/1.0",
        "param": "",
        "date": requestDates,
        "time": "",
        "format": "netcdf",
        "target": target,
    })

###### For retrieving surface forecast from MARS.
def era5_request_mars_sfc_fc(requestDates, target):
    server.retrieve({
        "class": "ea",
        "dataset": "era5",
        "expver": "0001",
        "type": "fc",
        "stream": "oper",
        "levtype": "sfc",
        "grid": "1.0/1.0",
        "param": "",
        "step": "0-12",
        "time": "06/18",
        "date": requestDates,
        "format": "netcdf",
        "target": target,
    })

if __name__ == '__main__':
    retrieve_era5_monthly()
