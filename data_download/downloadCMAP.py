"""
Download CMAP monthly precipitation files.
"""

###########################################
import urllib.request
###########################################


# Specify where to save.
save_dir = '~/Data/satellite/CMAP/'

# Web addresses and filenames.
url_standard = 'ftp://ftp.cdc.noaa.gov/Datasets/cmap/std/precip.mon.mean.nc'
url_enhanced = 'ftp://ftp.cdc.noaa.gov/Datasets/cmap/enh/precip.mon.mean.nc'
#
save_standard = 'precip.mon.mean.std.nc'
save_enhanced = 'precip.mon.mean.enh.nc'

# Download the files.
urllib.request.urlretrieve(url_standard,
                           save_dir + save_standard)
urllib.request.urlretrieve(url_enhanced,
                           save_dir + save_enhanced)

