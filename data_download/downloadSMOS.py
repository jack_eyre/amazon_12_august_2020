"""
Download SMOS satellite salinity data and average to monthly.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import urllib.request
from ftplib import FTP
import os
import subprocess
import sys
###########################################

# Specify data location details.
save_dir = '/Users/jameseyre/Data/satellite/SMOS/'
ftp_url = 'ftp.ifremer.fr'
ftp_uname = 'ext-catds-cecos-locean'
ftp_pword = 'catds2010'
ftp_dir = 'Ocean_products/L3_DEBIAS_LOCEAN_v3/debiasedSSS_09days_v3'

# Specify filename formats.
filename_root = 'SMOS_L3_DEBIAS_LOCEAN_AD_'
filename_tail = '_EASE_09d_25km_v03.nc'
filename_tail_new = '_EASE_09d_25km_v03_monthly.nc'

# Specify dates of available data.
time_min = datetime.datetime(2010,1,16,0,0,0)
time_max = datetime.datetime(2017,12,26,0,0,0)
dt = datetime.timedelta(days=4)

##### Log into FTP page ######################################################

ftp = FTP(ftp_url)
ftp.login(ftp_uname, ftp_pword)
ftp.cwd(ftp_dir)

##### Loop over months ####### ###############################################
##### and calculate average for each.

# Define as a function so we can use other functions defined later:
def main():
    
    # Initialize monthly variables.
    ft = time_min
    print(ft)
    
    while ft < time_max:
        
        # Get month and year.
        fm = ft.month
        fy = ft.year
        
        # Get number of days in this month.
        global d_in_m
        d_in_m = calendar.monthrange(fy,fm)[1]
        
        # Download and load files for this month.
        print('Downloading data')
        [SSS_4d, filelist, wts, t_bnds] = dl_SSS(fy,fm)
        if (np.sum(wts) != d_in_m):
            print("ACHTUNG: sum of weights =/= days in month")

        # Take weighted average over time.
        print('Averaging data')
        SSS_m = SSS_4d[0].astype('float64')*wts[0]
        for i in np.arange(1,len(wts)):
            SSS_m.data = SSS_m.data + SSS_4d[i]*wts[i]
        SSS_m = SSS_m/np.sum(wts) 

        # Save the array to a new netcdf file.
        save_nc(fy,fm,SSS_m,t_bnds,filelist,wts)

        # Increment for next month.
        ft = ft + datetime.timedelta(days=31)
        ft = ft - datetime.timedelta(days=(ft.day-1))
    
  
def dl_SSS(fy,fm):
    # Download files for this month, and load arrays into a list.
    # Also give weights showing how much each array's 4-day period
    # is in the specified month, a list of files used and the date
    # bounds.
    # SSS_4d is a list of xarray data arrays.
    # fl is a list of strings.
    # wts is a numpy array.
    # t_b is an xarray data array of datetime objects.

    # Get list of files.
    fl = ftp.nlst(filename_root + "{:04d}".format(fy) + \
                  "{:02d}".format(fm) + '*')

    # Get dates from files.
    file_dates = []
    for i in np.arange(0,len(fl)):
        file_dates.append(datetime.datetime(int(fl[i][25:29]),
                                            int(fl[i][29:31]),
                                            int(fl[i][31:33]),0,0,0))

    # If first file not from 1st of month, add last file from previous
    # month to the file list (except for Jan-2010).
    if min(file_dates).day != 1:
        if fy == 2010 and fm == 1:
            print('Incomplete data for Jan-2010')
        else:             
            file_dates.insert(0,min(file_dates) - datetime.timedelta(days=4))
            fl.insert(0,ftp.nlst(filename_root +
                                 file_dates[0].strftime('%Y%m%d') +
                                 '*')[0])

    # Now download the files (if they haven't already been downloaded).
    for i in np.arange(0,len(fl)):
        exists = os.path.isfile(save_dir + fl[i])
        if exists:
            print(save_dir + fl[i] + ' already exists')
        else:
            print(fl[i])
            localfile = open(save_dir + fl[i], "wb")
            ftp.retrbinary('RETR ' + fl[i], localfile.write)
            localfile.close()

    # Now open each file and get salinity as an xarray dataarray.
    # At the same time, calculate weights.
    d_s = datetime.datetime(fy,fm,1,0,0,0)
    d_e = datetime.datetime(fy,fm,d_in_m,0,0,0) + datetime.timedelta(days=1)
    SSS = []
    w = np.zeros(len(fl))
    for i in np.arange(0,len(fl)):
        file_4d = xr.open_dataset(save_dir + fl[i])
        SSS.append(file_4d['SSS'])
        w[i] = min([4,(d_e-file_dates[i]).days,4+(file_dates[i]-d_s).days])

    # Get time bounds.
    t_b = [max([min(file_dates), datetime.datetime(fy,fm,1,0,0,0)]),
           min([max(file_dates) + datetime.timedelta(days=4),
                datetime.datetime(fy,fm,d_in_m,0,0,0)])]
    t_b = xr.DataArray(t_b,dims=['n_bnds'])

    # Return output variables.
    return([SSS, fl, w, t_b])

def save_nc(fy,fm,SSS,t_b,flst,wgts):
    # Save monthly SSS array in new netcdf file.
    # fy, fm are integers used to construct "yyyymm" type filename.
    # SSS is the xarray data array of monthly average salinity values.

    SSS = SSS.expand_dims('dummyname')
    t_b = t_b.expand_dims('dummyname')

    # Construct xarray dataset.
    ds = xr.Dataset({'SSS': (['time', 'lat', 'lon'], SSS),
                     'time_bounds': (['time', 'n_bnds'], t_b)},
                    coords={'time':(['time'],[t_b.data[0,0]]),
                            'lat':(['lat'],SSS.coords['lat']),
                            'lon':(['lon'],SSS.coords['lon']),
                            'n_bnds':(['n_bnds'],np.array([1,2]))})

    # Set up encoding.
    t_units = 'days since 2010-01-01 00:00:00'
    t_cal = 'proleptic_gregorian'
    fill_val = 9.96921e+36
    wr_enc = {'SSS':{'_FillValue':fill_val},
              'time_bounds':{'units':t_units,'calendar':t_cal,
                             '_FillValue':fill_val},
              'time':{'units':t_units,'calendar':t_cal,
                      '_FillValue':fill_val},
              'lat':{'_FillValue':fill_val},
              'lon':{'_FillValue':fill_val}}

    # Add other metadata.
    ds.SSS.attrs['units'] = 'psu'
    ds.SSS.attrs['long_name'] = 'SMOS monthly mean sea surface salinity'
    ds.SSS.attrs['standard_name'] = 'sea_surface_salinity'
    ds.lat.attrs['units'] = 'degrees_north'
    ds.lat.attrs['long_name'] = 'latitude'
    ds.lat.attrs['standard_name'] = 'latitude'
    ds.lon.attrs['units'] = 'degrees_east'
    ds.lon.attrs['long_name'] = 'longitude'
    ds.lon.attrs['standard_name'] = 'longitude'
    ds.time.attrs['long_name'] = 'time'
    ds.time.attrs['standard_name'] = 'time'
    ds.attrs['Conventions'] = 'CF-1.6'
    ds.attrs['title'] = 'SMOS SSS - LOCEAN_ACRI_v2018'
    ds.attrs['institution'] = 'CEC-OS LOCEAN/IPSL/ACRI-ST'
    ds.attrs['source'] = 'SMOS L2OS RE05 (CATDS, 3rd reprocessing RE05 + operationnal processing) and L1_v6 (ESA, 2nd reprocessing) products'
    ds.attrs['references'] = 'http://catds.ifremer.fr/Products/Available-products-from-CEC-OS/Locean-v2018'
    ds.attrs['history'] = 'Processed on 2018-06-14 by $Id: prepare_data_CEC_CATDSformat.m 2018-06-14 vergely $'
    ds.attrs['pp_comment'] = 'Monthly means calculated by Jack Reeves Eyre (University of Arizona)'
    ds.attrs['pp_script'] = 'downloadSMOS.py'
    ds.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/amazoncongo/src/master/'
    ds.attrs['pp_script_commit'] = get_git_revision_hash()
    ds.attrs['creation_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    ds.attrs['source_file_weights'] = wgts
    ds.attrs['source_files'] = flst

    # Construct file name.
    new_file_name = save_dir + filename_root + "{:04d}".format(fy) + \
                  "{:02d}".format(fm) + '_EASE_09d_25km_v03_monthly.nc'

    # Write new file.
    ds.to_netcdf(path=new_file_name, mode='w',
                 encoding=wr_enc,unlimited_dims=['time'])



# Function for git commit hash:
def get_git_revision_hash():
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    if sys.version_info[0] >= 3:
        return hash.decode('utf-8')
    else:
        return hash

# Now actually execute the script.
if __name__ == '__main__':
    main()
