"""
Plot time series of Amazon runoff estimates using several different methods.
"""

###########################################
import numpy as np
import xarray as xr
import pandas as pd
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
###########################################

def main():
    
    ######################################################### 
    # Load data.
    S_mon = load_GRACE_monthly()
    
    #########################################################
    # Do the differentiations.
    m1 = deltaS_m_cen2(S_mon)
    m2 = deltaS_m_for2(S_mon)
    m3 = deltaS_m_back2(S_mon)
    m4 = deltaS_m_cen4(S_mon)
    m5 = deltaS_m_Pellet4(S_mon)
    m6 = deltaS_m_Eicker4(S_mon)

    s1 = deltaS_s_cen2_ext(S_mon)
    s2 = deltaS_s_cen2_int(S_mon)
    s3 = deltaS_s_for2(S_mon)
    s4 = deltaS_s_back2(S_mon)

    a1 = deltaS_a_cen2_ext(S_mon)
    a2 = deltaS_a_cen2_int(S_mon)
    a3 = deltaS_a_for2(S_mon)
    a4 = deltaS_a_back2(S_mon)

    
    #########################################################
    # Plot time series.
    print('Plotting...')
    plot_dSdt([m1, m2, m3, m4, m5, m6],
              [s1, s2, s3, s4],
              [a1, a2, a3, a4])

###########################################


##### Data load function. ###################################

def load_GRACE_monthly(basin, data_center):
    
    # Specify time period for comparison.
    st_time = np.datetime64('2002-04-01')
    end_time = np.datetime64('2017-07-01')
    
    # Define file names.
    fn_dict = {'JPL': 'GRCTellus.JPL.200204_201706.GLO.RL06M.MSCNv01CRIv01.nc',
               'GFZ': 'GRAVIS-3_2002-2019_GFZOP_0600_TWS_GRID_GFZ_0001.nc',
               'CSR': 'CSR_GRACE_RL06_Mascons_all-corrections_v01.nc'}
    
    # Read the data.
    GRACE_filename = '~/Data/satellite/GRACE/' + basin + \
                     'BasinAverage_' + fn_dict[data_center]
    ds_G = xr.open_dataset(GRACE_filename)
    mass_anom = ds_G.lwe_thickness
    
    #########################################################
    # Tidy up time series.
    # Want all dates on same day of month,
    # no duplicate months, and temporally
    # complete time series.
    
    # Put all months on same day of month.
    old_dates_first = ds_G.time.astype('datetime64[M]')
    mass_anom = mass_anom.assign_coords(time=old_dates_first)
    
    # Average duplicated months.
    if (np.any(mass_anom.indexes['time'].duplicated())):
        repeats = np.argwhere(mass_anom.indexes['time'].duplicated())
        for it in repeats:
            mass_anom[it[0]-1] = np.mean(mass_anom.sel(
                time=mass_anom.time.isel(time=it[0]).data))
        mass_anom = mass_anom.sel(time=~mass_anom.indexes['time'].duplicated())

    # Create temporally complete array.
    new_dates = pd.to_datetime(np.arange(st_time,end_time,
                                         dtype='datetime64[M]'))
    S_mon = xr.DataArray(np.empty(new_dates.shape),
                         coords=[new_dates],
                         dims=['time'])
    S_mon.data[:] = np.nan
    S_mon.loc[dict(time=mass_anom.sel(time=slice(st_time,end_time)).time)] = \
                   mass_anom.sel(time=slice(st_time,end_time)).data
    
    # Interpolate to fill gaps.
    S_mon = S_mon.interpolate_na(dim='time',
                                 method='linear',
                                 limit=3,
                                 use_coordinate=False)

    # Copy attributes.
    S_mon.attrs = mass_anom.attrs
    S_mon.attrs['time_coordinate_convention'] = 'First day of month.'
    S_mon.attrs['interpolation'] = 'Gaps filled by linear interpolation in time.'

    return(S_mon)


##### Differentiation functions. ###################################

# Centered difference, second order (2 terms).
def deltaS_m_cen2(S):

    # Create new arrays.
    dS = xr.full_like(S, np.nan)
    dSdt = xr.full_like(S, np.nan)
    
    # Calculate change of storage.
    dS[1:-1] = S[2:].data - S[:-2].data

    # Calculate time step.
    # We want number of days between midpoint of following month
    # and midpoint of preceding month.
    # In our variables, times are (arbitrarily) on first day of month, so we use
    # dt(i) = [t(i+1) + 0.5*(t(i+2) - t(i+1))] - [t(i-1) + 0.5(t(i) - t(i-1))]
    #       = 0.5*(t(i+2) - t(i) + t(i+1) - t(i-1))
    t = S.time
    dt = np.array(np.empty(t.shape), dtype='timedelta64[D]')
    dt[:] = np.datetime64('nat')
    tm1 = np.array(np.empty(t.shape), dtype='datetime64[ns]')
    tm1[:] = np.datetime64('nat')
    tm2 = np.array(np.empty(t.shape), dtype='datetime64[ns]')
    tm2[:] = np.datetime64('nat')
    tm1[1:-2] = t[2:-1].data + 0.5*(t[3:].data - t[2:-1].data)
    tm2[1:-1] = t[0:-2].data + 0.5*(t[1:-1].data - t[0:-2].data)
    dinm = calendar.monthrange(t[-1].dt.year.data, t[-1].dt.month.data)[1]
    tm1[-2] = t[-1].data + \
              0.5*(np.timedelta64(int(dinm), 'D').astype('timedelta64[ns]'))
    dt = tm1 - tm2
    
    # Convert to rate of change.
    dSdt[1:-1] = dS[1:-1]/(dt[1:-1]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Centered difference: dSdt(i) = (S(i+1) - S(i-1)) / (middle_of_month(i+1) - middle_of_month(i-1))'
    dSdt.attrs['plot_label'] = 'Center_2'

    # Return value.
    return(dSdt)

# Forward difference.
def deltaS_m_for2(S):

    # Create new arrays.
    dS = xr.full_like(S, np.nan)
    dSdt = xr.full_like(S, np.nan)
    
    # Calculate change of storage.
    dS[:-1] = S[1:].data - S[:-1].data

    # Calculate time step.
    t = S.time
    dt = np.array(np.empty(t.shape), dtype='timedelta64[ns]')
    dt[:] = np.datetime64('nat')
    dt[:-1] = t[1:].data - t[:-1].data
    
    # Convert to rate of change.
    dSdt[:-1] = dS[:-1]/(dt[:-1]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Forward difference: dSdt(i) = (S(i+1) - S(i)) / (first_of_month(i+1) - first_of_month(i))'
    dSdt.attrs['plot_label'] = 'Forward_2'

    return(dSdt)

# Backward difference.
# (Will be almost the same as forward difference, but shifted one month and
#  with slightly different values due to durations of months.)
def deltaS_m_back2(S):

    # Create new arrays.
    dS = xr.full_like(S, np.nan)
    dSdt = xr.full_like(S, np.nan)
    
    # Calculate change of storage.
    dS[1:] = S[1:].data - S[:-1].data

    # Calculate time step.
    t = S.time
    dt = np.array(np.empty(t.shape), dtype='timedelta64[ns]')
    dt[:] = np.datetime64('nat')
    dt[1:-1] = t[2:].data - t[1:-1].data
    dt[-1] = np.timedelta64(int(calendar.monthrange(t[-1].dt.year.data, t[-1].dt.month.data)[1]), 'D')
    
    # Convert to rate of change.
    dSdt[:-1] = dS[:-1]/(dt[:-1]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Backward difference: dSdt(i) = (S(i) - S(i-1)) / (last_of_month(i) - last_of_month(i-1))'
    dSdt.attrs['plot_label'] = 'Backward_2'

    return(dSdt)

# Centered difference, fourth order (4 terms).
def deltaS_m_cen4(S):

    # Create new arrays.
    dS = xr.full_like(S, np.nan)
    dSdt = xr.full_like(S, np.nan)
    
    # Calculate change of storage.
    dS[2:-2] = (S[:-4].data - 8.0*S[1:-3].data +
                8.0*S[3:-1].data - S[4:].data)/12.0

    # Calculate time step.
    # Approximate as 30 days due to different month durations.
    t = S.time
    dt = np.array(np.empty(t.shape), dtype='timedelta64[ns]')
    dt[:] = np.timedelta64(30,'D')
    
    # Convert to rate of change.
    dSdt[2:-2] = dS[2:-2]/(dt[2:-2]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Fourth order centered difference: dSdt(i) = (1/12)*(-S(i+2) + 8*S(i+1) - 8*S(i-1) + S(i-2)) / (30 days)'
    dSdt.attrs['plot_label'] = 'Center_4'

    # Return value.
    return(dSdt)

# Centered difference, fourth order (4 terms).
def deltaS_m_Pellet4(S):

    # Create new arrays.
    dS = xr.full_like(S, np.nan)
    dSdt = xr.full_like(S, np.nan)
    
    # Calculate change of storage.
    dS[2:-2] = (-5.0*S[:-4].data - 9.0*S[1:-3].data +
                9.0*S[3:-1].data + 5.0*S[4:].data)/24.0

    # Calculate time step.
    # Approximate as 30 days due to different month durations.
    t = S.time
    dt = np.array(np.empty(t.shape), dtype='timedelta64[ns]')
    dt[:] = np.timedelta64(30,'D')
    
    # Convert to rate of change.
    dSdt[2:-2] = dS[2:-2]/(dt[2:-2]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Fourth order centered difference with alternative weights: dSdt(i) = (1/24)*(5*S(i+2) + 9*S(i+1) - 9*S(i-1) - 5*S(i-2)) / (30 days)'
    dSdt.attrs['plot_label'] = 'Pellet_4'

    # Return value.
    return(dSdt)

# Centered difference, fourth order (4 terms).
def deltaS_m_Eicker4(S):

    # Create new arrays.
    dS = xr.full_like(S, np.nan)
    dSdt = xr.full_like(S, np.nan)
    
    # Calculate change of storage.
    dS[2:-2] = (-1.0*S[:-4].data - 2.0*S[1:-3].data +
                2.0*S[3:-1].data + S[4:].data)/8.0

    # Calculate time step.
    # Approximate as 30 days due to different month durations.
    t = S.time
    dt = np.array(np.empty(t.shape), dtype='timedelta64[ns]')
    dt[:] = np.timedelta64(30,'D')
    
    # Convert to rate of change.
    dSdt[2:-2] = dS[2:-2]/(dt[2:-2]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Fourth order centered difference with alternative weights: dSdt(i) = (1/8)*(S(i+2) + 2*S(i+1) - 2*S(i-1) - S(i-2)) / (30 days)'
    dSdt.attrs['plot_label'] = 'Eicker_4'

    # Return value.
    return(dSdt)

# Seasonal centered difference, second order, using months external
# to each season.
def deltaS_s_cen2_ext(S):

    # Calculate seasonal averages (unweighted though doesn't really matter as
    # we just want this for the array structure).
    S_s = S.resample(time='QS-DEC').mean()
    
    # Create new arrays.
    dS = xr.full_like(S_s, np.nan)
    dSdt = xr.full_like(S_s, np.nan)

    # Calculate change of storage.
    dS[1:-1] = S.resample(time='QS-DEC').first().data[2:] - \
               S.resample(time='QS-DEC').last().data[:-2]
    
    # Calculate time step.
    # We want number of days between midpoint of following month
    # and midpoint of preceding month.
    # Note that, in our variables, times are (arbitrarily) on
    # the first day of the month.
    t = S.time
    dt = np.array(np.empty(S_s.time.shape), dtype='timedelta64[ns]')
    dt[:] = np.datetime64('nat')
    t_end = np.array(np.empty(S_s.time.shape), dtype='datetime64[ns]')
    t_end[:] = np.datetime64('nat')
    t_st = np.array(np.empty(S_s.time.shape), dtype='datetime64[ns]')
    t_st[:] = np.datetime64('nat')
    t_end[1:-2] = t.resample(time='QS-DEC').first().data[2:-1] + \
                  0.5*(t.resample(time='QS-NOV').last().data[2:-1] -
                       t.resample(time='QS-DEC').first().data[2:-1])
    dinm = calendar.monthrange(t.resample(time='QS-DEC').first()[-1]\
                               .dt.year.data,
                               t.resample(time='QS-DEC').first()[-1]\
                               .dt.month.data)[1]
    t_end[-2] = t.resample(time='QS-DEC').first().data[-1] + \
                0.5*(np.timedelta64(int(dinm), 'D').astype('timedelta64[ns]'))
    t_st[1:-1] = t.resample(time='QS-DEC').last().data[:-2] + \
                 0.5*(t.resample(time='QS-DEC').first().data[1:-1] -
                      t.resample(time='QS-DEC').last().data[:-2])
    dt = t_end - t_st
    
    # Convert to rate of change.
    dSdt[1:-1] = dS[1:-1]/(dt[1:-1]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Seasonal centered difference using months outside season: dSdt(i:i+2) = (S(i+3) - S(i-1)) / (middle_of_month(i+3) - middle_of_month(i-1))'
    dSdt.attrs['plot_label'] = 'Center_2_ext'
    
    return(dSdt)

# Seasonal centered difference, second order, using months internal
# to each season.
def deltaS_s_cen2_int(S):

    # Calculate seasonal averages (unweighted though doesn't really matter as
    # we just want this for the array structure).
    S_s = S.resample(time='QS-DEC').mean()
    
    # Create new arrays.
    dS = xr.full_like(S_s, np.nan)
    dSdt = xr.full_like(S_s, np.nan)

    # Calculate change of storage.
    dS[1:-1] = S.resample(time='QS-DEC').last().data[1:-1] - \
               S.resample(time='QS-DEC').first().data[1:-1]
    
    # Calculate time step.
    # We want number of days between midpoint of following month
    # and midpoint of preceding month.
    # Note that, in our variables, times are (arbitrarily) on
    # the first day of the month.
    t = S.time
    dt = np.array(np.empty(S_s.time.shape), dtype='timedelta64[ns]')
    dt[:] = np.datetime64('nat')
    t_end = np.array(np.empty(S_s.time.shape), dtype='datetime64[ns]')
    t_end[:] = np.datetime64('nat')
    t_st = np.array(np.empty(S_s.time.shape), dtype='datetime64[ns]')
    t_st[:] = np.datetime64('nat')
    t_end[1:-1] = t.resample(time='QS-DEC').last().data[1:-1] + \
                  0.5*(t.resample(time='QS-DEC').first().data[2:] -
                       t.resample(time='QS-DEC').last().data[1:-1])
    t_st[1:-1] = t.resample(time='QS-DEC').first().data[1:-1] + \
                 0.5*(t.resample(time='QS-NOV').last().data[1:-1] -
                      t.resample(time='QS-DEC').first().data[1:-1])
    dt = t_end - t_st
    
    # Convert to rate of change.
    dSdt[1:-1] = dS[1:-1]/(dt[1:-1]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Seasonal centered difference using months inside season: dSdt(i:i+2) = (S(i+2) - S(i)) / (middle_of_month(i+2) - middle_of_month(i))'
    dSdt.attrs['plot_label'] = 'Center_2_int'
    
    return(dSdt)

# Seasonal forward difference, second order.
def deltaS_s_for2(S):

    # Calculate seasonal averages (unweighted though doesn't really matter as
    # we just want this for the array structure).
    S_s = S.resample(time='QS-DEC').mean()
    
    # Create new arrays.
    dS = xr.full_like(S_s, np.nan)
    dSdt = xr.full_like(S_s, np.nan)

    # Calculate change of storage.
    dS[1:-1] = S.resample(time='QS-DEC').first().data[2:] - \
               S.resample(time='QS-DEC').first().data[1:-1]
    
    # Calculate time step.
    t = S.time
    dt = np.array(np.empty(S_s.time.shape), dtype='timedelta64[ns]')
    dt[:] = np.datetime64('nat')
    dt[1:-1] = t.resample(time='QS-DEC').first().data[2:] - \
               t.resample(time='QS-DEC').first().data[1:-1]
    
    # Convert to rate of change.
    dSdt[1:-1] = dS[1:-1]/(dt[1:-1]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Seasonal forward difference: dSdt(i:i+2) = (S(i+3) - S(i)) / (first_of_month(i+3) - first_of_month(i))'
    dSdt.attrs['plot_label'] = 'Forward_2'
    
    return(dSdt)

# Seasonal backward difference, second order.
def deltaS_s_back2(S):

    # Calculate seasonal averages (unweighted though doesn't really matter as
    # we just want this for the array structure).
    S_s = S.resample(time='QS-DEC').mean()
    
    # Create new arrays.
    dS = xr.full_like(S_s, np.nan)
    dSdt = xr.full_like(S_s, np.nan)

    # Calculate change of storage.
    dS[1:-1] = S.resample(time='QS-DEC').last().data[1:-1] - \
               S.resample(time='QS-DEC').last().data[:-2]
    
    # Calculate time step.
    t = S.time
    dt = np.array(np.empty(S_s.time.shape), dtype='timedelta64[ns]')
    dt[:] = np.datetime64('nat')
    dt[1:-1] = t.resample(time='QS-DEC').first().data[2:] - \
               t.resample(time='QS-DEC').first().data[1:-1]
    
    # Convert to rate of change.
    dSdt[1:-1] = dS[1:-1]/(dt[1:-1]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Seasonal backward difference: dSdt(i:i+2) = (S(i+2) - S(i-1)) / (first_of_month(i+3) - first_of_month(i))'
    dSdt.attrs['plot_label'] = 'Backward_2'
    
    return(dSdt)

# Annual centered difference, second order, using months external
# to each season.
def deltaS_a_cen2_ext(S):

    # Calculate annual averages (unweighted though doesn't really matter as
    # we just want this for the array structure).
    S_a = S.groupby('time.year').mean()
    
    # Create new arrays.
    dS = xr.full_like(S_a, np.nan)
    dSdt = xr.full_like(S_a, np.nan)

    # Calculate change of storage.
    dS[1:-1] = S.resample(time='A').first().data[2:] - \
               S.resample(time='A').last().data[:-2]

    # Calculate time step.
    # We want number of days between midpoint of following month
    # and midpoint of preceding month.
    # Note that, in our variables, times are (arbitrarily) on
    # the first day of the month.
    t = S.time
    t_a = S_a.year
    dt = np.array(np.empty(t_a.shape), dtype='timedelta64[ns]')
    dt[:] = np.datetime64('nat')
    t_end = np.array(np.empty(t_a.shape), dtype='datetime64[ns]')
    t_end[:] = np.datetime64('nat')
    t_st = np.array(np.empty(t_a.shape), dtype='datetime64[ns]')
    t_st[:] = np.datetime64('nat')
    t_st[1:-1] = t.resample(time='A').last().data[:-2] + \
                 0.5*(t.resample(time='A').first().data[1:-1] -
                      t.resample(time='A').last().data[:-2])
    t_end[1:-2] = t.resample(time='A').first().data[2:-1] + \
                  0.5*(t.resample(time='AS-FEB').first().data[2:-1] -
                       t.resample(time='A').first().data[2:-1])
    dinm = calendar.monthrange(t.resample(time='A').first()[-1]\
                               .dt.year.data,
                               t.resample(time='A').first()[-1]\
                               .dt.month.data)[1]
    t_end[-2] = t.resample(time='A').first().data[-1] + \
                0.5*(np.timedelta64(int(dinm), 'D').astype('timedelta64[ns]'))
    dt = t_end - t_st
    
    # Convert to rate of change.
    dSdt[1:-1] = dS[1:-1]/(dt[1:-1]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Annual centered difference using months outside year: dSdt(i:i+11) = (S(i+12) - S(i-1)) / (middle_of_month(i+12) - middle_of_month(i-1))'
    dSdt.attrs['plot_label'] = 'Center_2_ext'

    return(dSdt)

# Annual centered difference, second order, using months internal
# to each season.
def deltaS_a_cen2_int(S):

    # Calculate annual averages (unweighted though doesn't really matter as
    # we just want this for the array structure).
    S_a = S.groupby('time.year').mean()
    
    # Create new arrays.
    dS = xr.full_like(S_a, np.nan)
    dSdt = xr.full_like(S_a, np.nan)

    # Calculate change of storage.
    dS[1:-1] = S.resample(time='A').last().data[1:-1] - \
               S.resample(time='A').first().data[1:-1]

    # Calculate time step.
    # We want number of days between midpoint of following month
    # and midpoint of preceding month.
    # Note that, in our variables, times are (arbitrarily) on
    # the first day of the month.
    t = S.time
    t_a = S_a.year
    dt = np.array(np.empty(t_a.shape), dtype='timedelta64[ns]')
    dt[:] = np.datetime64('nat')
    t_end = np.array(np.empty(t_a.shape), dtype='datetime64[ns]')
    t_end[:] = np.datetime64('nat')
    t_st = np.array(np.empty(t_a.shape), dtype='datetime64[ns]')
    t_st[:] = np.datetime64('nat')
    t_st[1:-1] = t.resample(time='A').first().data[1:-1] + \
                 0.5*(t.resample(time='AS-FEB').first().data[1:-1] -
                      t.resample(time='A').first().data[1:-1])
    t_end[1:-1] = t.resample(time='A').last().data[1:-1] + \
                  0.5*(t.resample(time='A').first().data[2:] -
                       t.resample(time='A').last().data[1:-1])
    dt = t_end - t_st
    
    # Convert to rate of change.
    dSdt[1:-1] = dS[1:-1]/(dt[1:-1]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Annual centered difference using months inside year: dSdt(i:i+11) = (S(i+11) - S(i)) / (middle_of_month(i+11) - middle_of_month(i))'
    dSdt.attrs['plot_label'] = 'Center_2_int'

    return(dSdt)

# Annual forward difference, second order.
def deltaS_a_for2(S):

    # Calculate annual averages (unweighted though doesn't really matter as
    # we just want this for the array structure).
    S_a = S.groupby('time.year').mean()
    
    # Create new arrays.
    dS = xr.full_like(S_a, np.nan)
    dSdt = xr.full_like(S_a, np.nan)

    # Calculate change of storage.
    dS[1:-1] = S.resample(time='A').first().data[2:] - \
               S.resample(time='A').first().data[1:-1]
    
    # Calculate time step.
    t = S.time
    t_a = S_a.year
    dt = np.array(np.empty(t_a.shape), dtype='timedelta64[ns]')
    dt[:] = np.datetime64('nat')
    dt[1:-1] = t.resample(time='A').first().data[2:] - \
               t.resample(time='A').first().data[1:-1]
    
    # Convert to rate of change.
    dSdt[1:-1] = dS[1:-1]/(dt[1:-1]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Annual forward difference: dSdt(i:i+11) = (S(i+12) - S(i)) / (first_of_month(i+12) - first_of_month(i))'
    dSdt.attrs['plot_label'] = 'Forward_2'
    
    return(dSdt)

# Annual backward difference, second order.
def deltaS_a_back2(S):

    # Calculate annual averages (unweighted though doesn't really matter as
    # we just want this for the array structure).
    S_a = S.groupby('time.year').mean()
    
    # Create new arrays.
    dS = xr.full_like(S_a, np.nan)
    dSdt = xr.full_like(S_a, np.nan)

    # Calculate change of storage.
    dS[1:-1] = S.resample(time='A').last().data[1:-1] - \
               S.resample(time='A').last().data[:-2]
    
    # Calculate time step.
    t = S.time
    t_a = S_a.year
    dt = np.array(np.empty(t_a.shape), dtype='timedelta64[ns]')
    dt[:] = np.datetime64('nat')
    dt[1:-1] = t.resample(time='A').first().data[2:] - \
               t.resample(time='A').first().data[1:-1]
    
    # Convert to rate of change.
    dSdt[1:-1] = dS[1:-1]/(dt[1:-1]/np.timedelta64(1,'D'))
    dSdt.attrs['units'] = S.attrs['units'] + ' per day'
    dSdt.attrs['time_derivative_method'] = 'Annual backward difference: dSdt(i:i+11) = (S(i+11) - S(i-1)) / (first_of_month(i+12) - first_of_month(i))'
    dSdt.attrs['plot_label'] = 'Backward_2'
    
    return(dSdt)

    
##### Plotting function. ###########################################

def plot_dSdt(dSdt_m_list,dSdt_s_list,dSdt_a_list):

    plot_dir = '/Users/jameseyre/Documents/plots/AmazonCongo/'
     
    fig1, (ax1,ax2,ax3) = plt.subplots(3,1,figsize=(9,6))
    
    ##### Overall details.
    color = 'k'
    st_time = np.datetime64('2002-01-01T00:00:00')
    end_time = np.datetime64('2018-01-01T00:00:00')

    ##### Monthly figure.
    ax1.set_title('Monthly', loc='left')
    ax1.set_ylabel('dS/dt / '+ r'$cm\ day^{-1}$', color=color)
    ax1.xaxis.set_tick_params(which='both', 
                              labelbottom=False, labeltop=False)
    ax1.set_xlim(st_time, end_time)
    # Data.
    for m in dSdt_m_list:
        ax1.plot(m.time, m,
                 label=m.attrs['plot_label'])
    # Legend
    ax1.legend(loc='upper right')

    ##### Seasonal figure.
    ax2.set_title('Seasonal', loc='left')
    ax2.set_ylabel('dS/dt / '+ r'$cm\ day^{-1}$', color=color)
    ax2.xaxis.set_tick_params(which='both', 
                              labelbottom=False, labeltop=False)
    ax2.set_xlim(st_time, end_time)
    # Data.
    for s in dSdt_s_list:
        ax2.plot(s.time, s,
                 label=s.attrs['plot_label'])
    # Legend
    ax2.legend(loc='upper right')

    ##### Annual figure.
    ax3.set_title('Annual', loc='left')
    ax3.set_ylabel('dS/dt / '+ r'$cm\ day^{-1}$', color=color)
    ax3.set_xlabel('Time', color=color)
    ax3.set_xlim(pd.to_datetime(st_time).year,
                 pd.to_datetime(end_time).year)
    # Data.
    for a in dSdt_a_list:
        ax3.plot(a.year, a,
                 label=a.attrs['plot_label'])
    # Legend
    ax3.legend(loc='upper right')
    
    
    # Tidy layout
    fig1.tight_layout()  # otherwise the right y-label is slightly clipped 
    plt.tight_layout()
    
    # Add info to footer.
    [txtl, hash, clean] = get_git_revision_info()
    plt.text(0.02,0.01,txtl, transform=fig1.transFigure, size=4)

    # Show figure.
    #plt.show()
    
    # Save figure as PDF.
    fig1.savefig(plot_dir +
                 os.path.splitext(os.path.basename(__file__))[0] +
                 '.pdf',
                 format='pdf')


###########################################
# Function for git commit hash:
def get_git_revision_info():
    
    import sys
    import subprocess
    import os
    
    # Get info.
    filename = os.path.basename(__file__)
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    clean =  subprocess.check_output(['git', 'diff',
                                      '--name-only',
                                      filename]).strip()
    
    # Change string format if python version 3.x.
    if sys.version_info[0] >= 3:
        #filename = filename.decode('utf-8')
        hash = hash.decode('utf-8')
        clean =  clean.decode('utf-8')
    
    # Put return string together.    
    info = filename + '          '
    if (clean == ''):
        info = info + 'Up to date. Last commit: ' + hash
    else:
        info = info + 'Untracked changes since last commit: ' + hash
    
    return [info, hash, clean]


###########################################
# Now actually execute the script.
if __name__ == '__main__':
    main()
