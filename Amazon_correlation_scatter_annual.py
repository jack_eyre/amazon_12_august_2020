"""
Plot correlation coefficient of streamflow with salinity from different combinations of annual average data.

Run it from plotenv conda environment.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################

################################################################################
def main():
    
    ##### Read hydrology data. ###########################################
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Fill gaps in streamflow (inserts nan for missing months)
    ro_ob = ro_ob.resample(time='1M').asfreq()
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    
    # Get factor to correct Obidos streamflow to mouth.
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    ob_to_mouth = ds_DT.ratio_m2s[0].data
    
    # Scale up Obidos streamflow to river mouth.
    ro_DT = ro_ob.copy()
    ro_DT = ro_DT*ob_to_mouth
    
    # Get other water budget variables (custom load functions below).
    # All in m3 s-1.
    budget_Amazon = get_budget_vars('Amazon')
    #
    # Add streamflow to dictionary.
    budget_Amazon['streamflow'] = ro_ob
    
    
    ##### Read salinity data. ###########################################
    
    # Define region to average over.
    sal_region = np.array([0.0,3.0,-52.5,-47.5])
    sal_st_time = '2010-02-01'
    sal_end_time = '2017-12-31'
    
    # Open and read file.
    sal_filename_wildcard = os.path.expanduser('~') + \
                            '/Data/satellite/SMOS/' + \
                            'SMOS_L3_DEBIAS_LOCEAN_AD_*' + \
                            '_EASE_09d_25km_v03_monthly.nc'
    ds_sal = xr.open_mfdataset(sal_filename_wildcard, concat_dim='time')
    # Don't get the first (incomplete) month.
    sss = ds_sal.SSS.sel(time=slice(sal_st_time,sal_end_time),
                         lat=slice(sal_region[0],sal_region[1]),
                         lon=slice(sal_region[2],sal_region[3]))\
                         .mean(['lat','lon']).load()
    
    ##### Read P-E data. ###############################################
    
    # Open files.
    pr_ds = xr.open_dataset('~/Data/satellite/GPCP/' +
                            'GPCP.v2.3.precip.mon.mean.nc')
    evap_ds = xr.open_mfdataset(os.path.expanduser('~') +
                                '/Data/reanalysis/ERA5/' +
                                'era5_moda_sfc_' + '*.nc')
    
    # Read data (same region and period as salinity).
    # (Have to account for longitude format (0,360) -> (-180,180).)
    pr_local = pr_ds.precip.sel(time=slice(sal_st_time,sal_end_time),
                                lat=slice(sal_region[0],sal_region[1]),
                                lon=slice(sal_region[2]+360.0,
                                          sal_region[3]+360.0)).load()
    evap_local = evap_ds.e.sel(time=slice(sal_st_time,sal_end_time),
                               latitude=slice(sal_region[1],
                                              sal_region[0]),
                               longitude=slice(sal_region[2]+360.0,
                                               sal_region[3]+360.0)).load()
    
    # Just evaporation needs changing.
    # It already has an implicit "per day" so just
    # needs conversion from m to mm.
    evap_local = evap_local*1000.0
    evap_local.attrs['units'] = 'mm/day'
    
    # Calculate area averaged P-E.
    PmE_local = pr_local.mean(['lat','lon']) - \
                evap_local.mean(['latitude','longitude'])
    
    # Add to data dictionary.
    budget_Amazon['P-E_local'] = PmE_local
    
    ##### Calculate correlations. ###########################################
    
    print('Processing...')
    
    # Define maximum time lag for correlations.
    max_lag = 1
    
    # Calculate correlations of streamflow estimates with salinity.
    corrs_Amazon = corrs_all(budget_Amazon, sss, max_lag)
    
    # Print out results for combinations that meet a criterion.
    if 1:
        print('Correlations (absolute, lag-0) larger than Obidos streamflow:')
        for comb in corrs_Amazon['annual']:
            if (abs(corrs_Amazon['annual'][comb]['abs_lag0']) >=
                abs(corrs_Amazon['annual']['Obidos']['abs_lag0'])):
                print(comb + ':     ' +
                      str(corrs_Amazon['annual'][comb]['abs_lag0']))
    
    ##### Plot results. ###########################################
    
    print('Plotting...')
    plot = plot_corrs(corrs_Amazon)


################################################################################

################################################################################
# Plotting function.

def plot_corrs(data_dict):
    
    # Specify a few details in advance.
    panel_list = ['abs','abs']
    panel_names = ['absolute','absolute']
    time_lags = list(range(0,data_dict['max_lag']+1))
    special_colors = {'Obidos': '#268bd2',
                      'precip_GPCP': '#dc322f',
                      'precip_CMAP': '#b58900',
                      'P-E_local': '#6c71c4'}
    special_colors2 = {'PE_Ens':'#d33682',
                       'conv_Ens':'#6c71c4',
                       #'all_Ens':'#cb4b16',
                       'PE_ERA5_ERA5_JPL':'#2aa198',
                       'PE_GPCP_ERA5_JPL':'#859900'}
    # Set up over all plot.
    fig, axs = plt.subplots(1, len(panel_list),
                            figsize=(7, 4), sharey=True)
    
    # Loop over panels.
    for p in range(len(panel_list)):
        # Initialize variable list.
        names = []
        values = []
        names_2 = []
        values_2 = []
        colors_2 = []
        names_Ens = []
        values_Ens = []
        colors_Ens = []
        # Loop over time lags and data combinations.
        for comb in data_dict['annual']:
            for t in range(len(time_lags)):
                # Add data to lists.
                if (comb in special_colors.keys()):
                    names_2.append(t + 0.3*np.random.rand(1)[0] - 0.15)
                    values_2.append(data_dict['annual'][comb]\
                                    [panel_list[p] + '_lag' +
                                     str(time_lags[t])])
                    colors_2.append(special_colors[comb])
                elif (comb in special_colors2.keys()):
                    names_Ens.append(t + 0.3*np.random.rand(1)[0] - 0.15)
                    values_Ens.append(data_dict['annual'][comb]\
                                    [panel_list[p] + '_lag' +
                                     str(time_lags[t])])
                    colors_Ens.append(special_colors2[comb])
                    
                else:
                    names.append(t + 0.3*np.random.rand(1)[0] - 0.15)
                    values.append(data_dict['annual'][comb]\
                                  [panel_list[p] + '_lag' +
                                   str(time_lags[t])])
        
        # Make the plot panel.        
        axs[p].scatter(names, values, c='#002b36', s=6, marker='.')
        axs[p].scatter(names_2, values_2, c=colors_2, s=10, marker='o')
        axs[p].scatter(names_Ens, values_Ens, c=colors_Ens, s=10, marker='s')
        # Set some details.
        axs[p].title.set_text(panel_names[p])
        axs[0].set_ylabel('correlation coefficient')
        axs[p].set_xlabel('time lag / seasons')
        axs[p].set_ylim(bottom=-1, top=1)
        axs[p].set_xlim(-0.75, len(time_lags)-0.25)
        axs[p].set_xticks(range(len(time_lags)))
        axs[p].set_xticklabels(time_lags)
    #
    # Add legends.
    legend_entries = []
    legend_entries2 = []
    for comb in special_colors.keys():
        legend_entries.append(mlines.Line2D([], [],
                                            color=special_colors[comb],
                                            marker='o',
                                            linestyle='None',
                                            label=comb))
    for comb in special_colors2.keys():
        legend_entries2.append(mlines.Line2D([], [],
                                             color=special_colors2[comb],
                                             marker='s',
                                             linestyle='None',
                                             label=comb))
    legend1 = axs[1].legend(handles=legend_entries,
                            loc='upper right',
                            fontsize='small',
                            markerscale=0.7,
                            labelspacing=0.4)
    axs[1].add_artist(legend1)
    legend2 = axs[1].legend(handles=legend_entries2,
                            loc='upper left',
                            fontsize='small',
                            markerscale=0.7,
                            labelspacing=0.4)
    
    # Tidy layout.
    fig.tight_layout()
            
    # Get git info for this script.
    sys.path.append(os.path.expanduser('~/Documents/code/'))
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    
    # Add git info to footer.
    plt.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    
    # Show the plot.
    #plt.show()
    # Or save the figure.
    fig.savefig(os.path.expanduser('~') +
                '/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.pdf',
                format='pdf')
    
    return(fig)

################################################################################
### Correlation functions.

# Simple function to return annual mean with NaN value if not
# all months are present for each year.
# Input is a 1D time series as an xarray data array.
# Output is a 1D time series with one data point per season.
def annual_mean(ts):
    ts_a = ts.groupby('time.year').mean(dim='time')
    ts_count = ts.groupby('time.year').count(dim='time')
    ts_a[ts_count < 12] = np.nan
    ts_a = ts_a.rename({"year":"time"})
    return ts_a


def corrs_all(all_vars, sal, max_lag=2):
    ###############################################################
    # Returns dictionaries of statistics of correlation coefficient:
    # annual
    # which has results from different combinations
    # of budget terms, for absolute values.
    # The results are each the output from corr_stats (function
    # below).
    #
    # Input:
    #     all_vars:     a dictionary of variables output by
    #                   get_budget_vars (below).
    #     sal:          an xarray data array of salinity
    #                   averaged over a small ocean region.
    # Optional input
    #     max_lag:      maximum time lag for which correlation
    #                   is calculated. Defaults to 2.
    #
    # Output:
    #     resids_all:   a dictionary with the following
    #                   structure of sub-dictionaries:
    #     {'monthly': {<'budget_terms_combination_1'>: corr_stats,
    #                  <'budget_terms_combination_2'>: corr_stats,
    #                  ... },
    #      'max_lag': max_lag
    #     }
    # The different combinations are named according to their input
    # data and method (e.g., 'PE_GPCP_GLEAM_JPL' [P-E method] or
    # 'conv_ERA5_ERA5_GFZ' [atmospheric convergence method].
    ###############################################################
    
    # Initialize final output dictionary.
    output = {'max_lag': max_lag}
    
    # Loop over timesteps.
    freq_list = ['annual']
    for f in freq_list:
        # Initialize sub-dictionary.
        temp = {}
        # Start adding residuals to this.
        print('Obidos')
        temp['Obidos'] = corr_stats(annual_mean(all_vars['streamflow']),
                                    annual_mean(sal),
                                    freq=f, max_lag=max_lag)
        print('precip_GPCP')
        temp['precip_GPCP'] = corr_stats(annual_mean(all_vars['pr_GPCP']),
                                         annual_mean(sal),
                                         freq=f, max_lag=max_lag)
        print('precip_CMAP')
        temp['precip_CMAP'] = corr_stats(annual_mean(all_vars['pr_CMAP']),
                                         annual_mean(sal),
                                         freq=f, max_lag=max_lag)
        print('P-E_local')
        temp['P-E_local'] = corr_stats(annual_mean(all_vars['P-E_local']),
                                       annual_mean(sal),
                                       freq=f, max_lag=max_lag)
        print('PE_Ens')
        temp['PE_Ens'] = corr_stats(annual_mean(all_vars['pr_Ens']) -
                                    annual_mean(all_vars['evap_Ens']) -
                                    all_vars['dSdt_' + f[0] + '_Ens'].\
                                    rename({"year":"time"}),
                                    annual_mean(sal),
                                    freq=f, max_lag=max_lag)
        print('conv_Ens')
        temp['conv_Ens'] = corr_stats(annual_mean(all_vars['conv_Ens']) -
                                      all_vars['dtcwdt_' + f[0] + '_Ens'].\
                                      rename({"year":"time"}) -
                                      all_vars['dSdt_' + f[0] + '_Ens'].\
                                      rename({"year":"time"}),
                                      annual_mean(sal),
                                      freq=f, max_lag=max_lag)
        # Loop over GRACE methods.
        GRACE_list = ['JPL']
        pr_list = ['GPCP', 'CMAP', 'CHIRPS', 'ERA5', 'MERRA2']
        evap_list = ['GLEAM', 'CLM', 'Noah', 'ERA5', 'MERRA2']
        for g in GRACE_list:
            for p in pr_list:
                for e in evap_list:
                    print('PE_' + p + '_' + e + '_' + g)
                    temp['PE_' + p + '_' + e + '_' + g] = corr_stats(
                        annual_mean(all_vars['pr_' + p]) -
                        annual_mean(all_vars['evap_' + e]) - 
                        all_vars['dSdt_' + f[0] + '_' + g].\
                        rename({"year":"time"}),
                        annual_mean(sal),
                        freq=f, max_lag=max_lag)
            print('conv_ERA5_ERA5_' + g)
            temp['conv_ERA5_ERA5_' + g] = corr_stats(
                annual_mean(all_vars['vimd_ERA5']) -
                all_vars['dtcwdt_' + f[0] + '_ERA5'].\
                rename({"year":"time"}) - \
                all_vars['dSdt_' + f[0] + '_' + g].\
                rename({"year":"time"}),
                annual_mean(sal),
                freq=f, max_lag=max_lag)
            print('conv_MERRA2_MERRA2_' + g)
            temp['conv_MERRA2_MERRA2_' + g] = corr_stats(
                annual_mean(all_vars['viwvc_MERRA2']) -
                all_vars['dtcwdt_' + f[0] + '_MERRA2'].\
                rename({"year":"time"}) - \
                all_vars['dSdt_' + f[0] + '_' + g].\
                rename({"year":"time"}),
                annual_mean(sal),
                freq=f, max_lag=max_lag)
        
        # Add all these to the final output.
        output[f] = temp
        
    # Return the output.
    return(output)


def corr_stats(streamflow, sal, freq="annual", max_lag=2):
    ###############################################################
    # Returns dictionary of statistics of correlation statistics.
    # Input:
    #     streamflow (or could be precipitation for example)
    #     salinity
    # Optional arguments:
    #     freq: averaging frequency of time series.
    #           "annual" is the default because this version does
    #           not calculate anomaly correlations.
    #     max_lag:  maximum time lag (streamflow leads salinity)
    #               to calculate correlations for. Defaults to 2.
    #
    # Output:
    # Dictionary containing several correlation coefficients:
    #     abs_lag0:    correlation of absolute values at zero lag
    #     abs_lag1:    correlation of absolute values at 1-month lag
    #     etc....
    # NOTE: the lags are streamflow leading salinity
    # (e.g., pairs of data are (streamflow_Jan, salinity_Feb). 
    ###############################################################
    
    # Initialize output dictionary.
    stats = {}
    
    # Check for valid frequency input.
    if ~np.isin(freq, ["annual"]):
        print("Invalid option for freq")
    
    else:
        # Calculate time overlap.
        st_time = max([min(streamflow.time), min(sal.time)])
        end_time = min([max(streamflow.time), max(sal.time)])
        
        # Get subsets of data.
        v1 = streamflow.sel(time=slice(st_time,end_time)).data
        v2 = sal.sel(time=slice(st_time,end_time)).data
        #
        # Mask out missing values.
        mask = (~np.isnan(v1) & ~np.isnan(v2))
        v1 = v1[mask]
        v2 = v2[mask]
        print(len(v1))
        print(len(v2))
        
        # Calculate correlation coefficients.
        stats['abs_lag0'] = np.corrcoef(v1, v2)[0,1]
        print(stats['abs_lag0'])
        if max_lag > 0:
            for l in range(1,max_lag+1):     
                stats['abs_lag' + str(l)] = np.corrcoef(v1[:-l],
                                                        v2[l:])[0,1]
    
    # Return values.
    return(stats)


################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    main()

