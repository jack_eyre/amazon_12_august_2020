"""
Prints text to make a LaTeX table of Amazon water cycle summary stats.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib.gridspec import GridSpec
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################


################################################################################
def main():
    
    ##### Read hydrology data. ###########################################
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    
    # Get factor to correct Obidos streamflow to mouth.
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    ob_to_mouth = ds_DT.ratio_m2s[0].data
    
    # Scale up Obidos streamflow to river mouth.
    ro_DT = ro_ob.copy()
    ro_DT = ro_DT*ob_to_mouth
    
    # Get water budget variables.
    # All in m3 s-1.
    all_vars = get_budget_vars('Amazon')
    # Add streamflow.
    all_vars['ro_DT'] = ro_DT
    
    # Get catchment area details.
    amazon_area = Amazon_catchment_details.areas['Amazon']
    
    # Specify units and conversion factor (m3 s-1 --> mm day-1)
    global ca
    ca = 60.0*60.0*24.0*1000.0/(amazon_area*1000.0*1000.0)
    units = 'mm day$^{-1}$'
    
    # Specify time period.
    st_time = np.datetime64('2003-01-01')
    end_time = np.datetime64('2018-12-31')
    
    ##### Print results. ###########################################
    #
    # Layout:
    #
    #  |Quantity 1  | Dataset 1 | mean |  max | max-month | min | min-month | 
    #  |            | Dataset 2 | mean |  max | max-month | min | min-month | 
    #  |Quantity 2  | Dataset 1 | mean |  max | max-month | min | min-month | 
    #  |            | Dataset 2 | mean |  max | max-month | min | min-month |
    #
    # etc.
    #
    ##### Print header stuff. ########################################
    print('\\begin{table}')
    print('\\caption{INSERT CAPTION HERE}')
    print('\\centering')
    print('\\begin{tabular}{l l c c c c c}')
    print('\\hline')
    print(' & & \\multicolumn{5}{c}{Mean annual cycle} \\\\')
    print('Quantity (' + units +
          ') & Dataset & Mean & Max. & Month & Min. & Month \\\\')
    print('\\hline')
    #
    # Reset units so they don't appear on every line.
    units = ''
    #
    ##### Precipitation data. ########################################
    pr_list = ['GPCP', 'CMAP', 'CHIRPS', 'ERA5', 'MERRA2']
    var_name = 'precipitation'
    for p in pr_list:
        print_row(var_name, units,
                  p, all_vars['pr_' + p],
                  st_time, end_time)
        var_name = ''
    #
    ##### Evaporation data. ########################################
    evap_list = ['GLEAM', 'CLM', 'Noah', 'ERA5', 'MERRA2']
    var_name = 'evaporation'
    for e in evap_list:
        print_row(var_name, units,
                  e, all_vars['evap_' + e],
                  st_time, end_time)
        var_name = ''
    #
    ##### Convergence, dqdt and P-E ################################
    print_row('P$-$E', units, 'ERA5',
              all_vars['pr_ERA5'] - all_vars['evap_ERA5'],
              st_time, end_time)
    print_row('', units, 'MERRA2',
              all_vars['pr_MERRA2'] - all_vars['evap_MERRA2'],
              st_time, end_time)
    print_row('$- \\nabla\\cdot(q\\vec{v})$', units, 'ERA5',
              all_vars['vimd_ERA5'], st_time, end_time)
    print_row('', units, 'MERRA2',
              all_vars['viwvc_MERRA2'], st_time, end_time)
    print_row('$- \\frac{dS_{atm}}{dt}$', units, 'ERA5',
              all_vars['dtcwdt_m_ERA5'], st_time, end_time)
    print_row('', units, 'MERRA2',
              all_vars['dtcwdt_m_MERRA2'], st_time, end_time)
    #
    ##### dSdt data. ########################################
    GRACE_list = ['JPL', 'CSR', 'GFZ']
    var_name = '$\\frac{dS_{terr}}{dt}$'
    for g in GRACE_list:
        print_row(var_name, units,
                  g, all_vars['dSdt_m_' + g],
                  st_time, end_time)
        var_name = ''
    #
    ##### Discharge data. ########################################
    print_row('discharge (Amazon)', units,
              '$1.25 \\times R_{\\acute{O}bidos}$', all_vars['ro_DT'],
              st_time, end_time)
    #
    # Print footer.
    print('\\end{tabular}')
    print('\\end{table}')
    #
    return


##### Post-processing scripts. ###########################################
def print_row(q_name, units, ds_name, ts, st_t, end_t):
    #
    # Define month names.
    monthnames = ['Jan','Feb','Mar','Apr','May','Jun',
                  'Jul','Aug','Sep','Oct','Nov','Dec']
    amp = ' & '
    eol = ' \\\\ '
    #
    # Get seasonal cycle.
    ssn_cy = ts.sel(time=slice(st_t, end_t)).groupby('time.month').mean('time')
    #
    # Get necessary quantities.
    ann_mean = ssn_cy.mean()*ca
    ann_max = ssn_cy.max()*ca
    max_month = monthnames[np.argmax(ssn_cy.data)]
    ann_min = ssn_cy.min()*ca
    min_month = monthnames[np.argmin(ssn_cy.data)]
    #
    # Print out row.
    if units != '':
        q_name = q_name + ' ($' + units + '$)'
    print(q_name + amp +
          ds_name + amp +
          '{:.2f}'.format(ann_mean.data) + amp +
          '{:.2f}'.format(ann_max.data) + amp +
          max_month + amp + 
          '{:.2f}'.format(ann_min.data) + amp +
          min_month + eol)
    #
    return

    
################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    pr_ann_cy = main()
