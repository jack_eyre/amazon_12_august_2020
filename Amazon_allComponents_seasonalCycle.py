"""
Plots mean annual cycle for various water cycle components in Amazon basin.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib.gridspec import GridSpec
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################


################################################################################
def main():
    
    ##### Read hydrology data. ###########################################
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    
    # Get factor to correct Obidos streamflow to mouth.
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    ob_to_mouth = ds_DT.ratio_m2s[0].data
    
    # Scale up Obidos streamflow to river mouth.
    ro_DT = ro_ob.copy()
    ro_DT = ro_DT*ob_to_mouth
    
    # Get water budget variables.
    # All in m3 s-1.
    all_vars = get_budget_vars('Amazon')
    # Add streamflow.
    all_vars['ro_DT'] = ro_DT
    
    # Specify time period.
    st_time = np.datetime64('2003-01-01')
    end_time = np.datetime64('2018-12-31')
    
    ##### Postprocessing. ###########################################
    
    # Get ensemble mean, max and min for each data set.   
    print('Post-processing...')
    ensemble_data = ens_meanMaxMin(all_vars, st_time, end_time)
    
    ##### Plot results. ###########################################
    
    print('Plotting...')
    plot_seasonalCycle(ensemble_data)
    
    return ensemble_data
    
    
##### Post-processing script. ###########################################
def ens_meanMaxMin(all_data, st, et):
    #
    # Define dictionary to hold all required results.
    data_dict = {'precip':{}, 'evap':{},
                 'dSdt':{}, 'discharge':{},
                 'residual':{'PEG_Ens':{}, 'PEG_ERA5_ERA5_JPL':{}}}
    # Precipitation ensemble.
    pr_list = ['GPCP', 'CMAP', 'CHIRPS', 'ERA5', 'MERRA2']
    data_dict['precip']['ens_mean'] = \
        all_data['pr_Ens'].sel(time=slice(st, et)).\
            groupby('time.month').mean('time')
    pr_max = data_dict['precip']['ens_mean'].copy()
    pr_min = pr_max.copy()
    pr_sea_cy = np.zeros((len(pr_list), 12))
    for i, p in enumerate(pr_list):
        pr_sea_cy[i,:] = all_data['pr_' + p].sel(time=slice(st, et)).\
            groupby('time.month').mean('time')
    pr_max.data = np.amax(pr_sea_cy, 0)
    pr_min.data = np.amin(pr_sea_cy, 0)
    data_dict['precip']['ens_max'] = pr_max
    data_dict['precip']['ens_min'] = pr_min 
    #
    # Evaporation ensemble.
    evap_list = ['GLEAM', 'CLM', 'Noah', 'ERA5', 'MERRA2']
    data_dict['evap']['ens_mean'] = \
        all_data['evap_Ens'].sel(time=slice(st, et)).\
            groupby('time.month').mean('time')
    evap_max = data_dict['evap']['ens_mean'].copy()
    evap_min = evap_max.copy()
    evap_sea_cy = np.zeros((len(evap_list), 12))
    for i, p in enumerate(evap_list):
        evap_sea_cy[i,:] = all_data['evap_' + p].sel(time=slice(st, et)).\
            groupby('time.month').mean('time')
    evap_max.data = np.amax(evap_sea_cy, 0)
    evap_min.data = np.amin(evap_sea_cy, 0)
    data_dict['evap']['ens_max'] = evap_max
    data_dict['evap']['ens_min'] = evap_min 
    #
    # dSdt ensemble.
    GRACE_list = ['JPL', 'CSR', 'GFZ']
    data_dict['dSdt']['ens_mean'] = \
        all_data['dSdt_m_Ens'].sel(time=slice(st, et)).\
            groupby('time.month').mean('time')
    dSdt_max = data_dict['dSdt']['ens_mean'].copy()
    dSdt_min = dSdt_max.copy()
    dSdt_sea_cy = np.zeros((len(GRACE_list), 12))
    for i, p in enumerate(GRACE_list):
        dSdt_sea_cy[i,:] = all_data['dSdt_m_' + p].sel(time=slice(st, et)).\
            groupby('time.month').mean('time')
    dSdt_max.data = np.amax(dSdt_sea_cy, 0)
    dSdt_min.data = np.amin(dSdt_sea_cy, 0)
    data_dict['dSdt']['ens_max'] = dSdt_max
    data_dict['dSdt']['ens_min'] = dSdt_min 
    #
    # Discharge.
    data_dict['discharge']['ens_mean'] = \
        all_data['ro_DT'].sel(time=slice(st, et)).\
            groupby('time.month').mean('time')
    #
    # Residuals - plots a line for each.
    resids_ERA5_ERA5_JPL = all_data['pr_ERA5'] - \
                           all_data['evap_ERA5'] - \
                           all_data['dSdt_m_JPL'] - \
                           all_data['ro_DT']
    data_dict['residual']['PEG_ERA5_ERA5_JPL']['ann_cy'] = \
        resids_ERA5_ERA5_JPL.sel(time=slice(st, et)).\
            groupby('time.month').mean('time')
    resids_PEG_Ens = all_data['pr_Ens'] - \
                     all_data['evap_Ens'] - \
                     all_data['dSdt_m_Ens'] - \
                     all_data['ro_DT']
    data_dict['residual']['PEG_Ens']['ann_cy'] = \
        resids_PEG_Ens.sel(time=slice(st, et)).\
            groupby('time.month').mean('time')
    #
    # Colors etc.
    data_dict['precip']['name'] = 'precip.'
    data_dict['precip']['color'] = '#2aa198'
    data_dict['precip']['linetype'] = '-'
    data_dict['evap']['name'] = 'evap.'
    data_dict['evap']['color'] = '#d33682'
    data_dict['evap']['linetype'] = '-'
    data_dict['dSdt']['name'] = r'$\frac{dS_{terr}}{dt}$'
    data_dict['dSdt']['color'] = '#6c71c4'
    data_dict['dSdt']['linetype'] = '-'
    data_dict['discharge']['name'] = r'$R_{Amazon}\ (= 1.25 \times R_{\acute O bidos})$'
    data_dict['discharge']['color'] = '#002b36'
    data_dict['discharge']['linetype'] = '-'
    data_dict['residual']['PEG_Ens']['name'] = 'Residual (Ensemble mean (PEG))'
    data_dict['residual']['PEG_Ens']['color'] = '#b58900'
    data_dict['residual']['PEG_Ens']['linetype'] = '-'
    data_dict['residual']['PEG_ERA5_ERA5_JPL']['name'] = 'Residual (PEG_ERA5_ERA5_JPL)'
    data_dict['residual']['PEG_ERA5_ERA5_JPL']['color'] = '#b58900'
    data_dict['residual']['PEG_ERA5_ERA5_JPL']['linetype'] = ':'
    
    return data_dict


################################################################################
# Plotting function.

def plot_seasonalCycle(ddict):
    #
    # Define font details.
    plt.rcParams.update({'font.size': 14})
    #
    # Define figure layout
    fig,ax = plt.subplots(nrows=1, ncols=1, figsize=(9,6))
    #
    # Plot data by looping over each variable.
    for k in ddict.keys():
        #
        # Plot separate lines for each residual combination.
        if (k == 'residual'):
            for cc in ddict['residual'].keys():
                ax.plot(range(1,13), ddict[k][cc]['ann_cy'],
                    color=ddict[k][cc]['color'],
                    linestyle=ddict[k][cc]['linetype'],
                    label=ddict[k][cc]['name'])
        else:        
            # Ensemble mean.
            ax.plot(range(1,13), ddict[k]['ens_mean'],
                    color=ddict[k]['color'],
                    linestyle=ddict[k]['linetype'],
                    label=ddict[k]['name'])
            # Ensemble max and min.
            if k in ['precip', 'evap', 'dSdt']:
                ax.plot(range(1,13), ddict[k]['ens_max'],
                        color=ddict[k]['color'],
                        linewidth=0.1)
                ax.plot(range(1,13), ddict[k]['ens_min'],
                        color=ddict[k]['color'],
                        linewidth=0.1)
            # Fill between max and min.
                ax.fill_between(range(1,13),
                                ddict[k]['ens_max'],
                                ddict[k]['ens_min'],
                                color=ddict[k]['color'],
                                alpha=0.2)
        
    # Set plot details.
    ax.set_ylim(-300000.0,800000.0)
    ax.set_xlim(0.5,12.5)
    ax.set_ylabel(r'$10^5\ m^3\ s^{-1}$')
    ax.tick_params('y', which='both',
                   left=True, right=True,
                   labelleft=True, labelright=False)
    ax.set_yticks(np.arange(-200000.0, 820000.0, 200000))
    ax.set_yticks(np.arange(-300000.0, 820000.0, 200000), minor=True)
    ax.set_yticklabels([-2,0,2,4,6,8])
    ax.set_xticks(range(1,13))
    ax.set_xticklabels(['J','F','M','A','M','J',
                         'J','A','S','O','N','D'])
    # Add zero line.
    ax.plot(ax.get_xlim(), [0.0,0.0],
            color='#657b83', linestyle='-',linewidth=0.3)
    # Add legend.
    ax.legend(loc='upper right',ncol=2,
              fontsize=12.5,
              frameon=False)
    
    # Get git info for this script.
    sys.path.append(os.path.expanduser('~/Documents/code/'))
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    
    # Add git info to footer.
    plt.text(0.02,0.001,txtl, transform=fig.transFigure, size=3)
    #
    # Show the plot.
    #plt.show()
    # Or save the figure.
    fig.savefig(os.path.expanduser('~') +
                '/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.pdf',
                format='pdf')
    
################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    pr_ann_cy = main()
