"""
Plot distribution of differences between data sets to check if they are 
normally distributed or not.

Run from plotenv conda environment.
"""

###########################################
import numpy as np
import xarray as xr
from scipy import stats
from scipy.stats import norm, shapiro, anderson
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
import mpl_toolkits.axes_grid1.inset_locator as mpl_il
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################


################################################################################
def main():
    
    ##### Read data. ###########################################
    print('Reading data...')
    #
    # Select basin and data sets.
    plot_basin = 'Amazon'
    v1 = 'evap_MERRA2'
    v2 = 'evap_ERA5'
    #
    # Select normaliztion.
    # As total volume flow rate(m3 s-1)
    norm = 'total'
    # Per area (mm day-1)
    #norm = 'area'
    #
    # Get the differences.
    diff = get_diffs(plot_basin, v1, v2, norm)
    #
    ##### Make plots. ###########################################
    print('Plotting...')
    #
    plot_dist(diff.dropna(dim='time'), plot_basin, v1, v2, norm)
    #plot_qq(diff.dropna(dim='time'), plot_basin, v1, v2, norm)
    #
    return(diff)

################################################################################
# Plotting functions.

def plot_dist(diffs, basin, var1, var2, nrm):
    #
    #---- Histogram
    fig, ax = plt.subplots(1, 1, figsize = [8,5])
    ax.hist(diffs, bins=20, density=True)
    ax.set_title(var1 + ' minus ' + var2 + ' (' + basin + ')')
    if nrm == 'total':
        ax.set_xlabel(r'$m^3\ s^{-1}$')
    elif nrm == 'area':
        ax.set_xlabel(r'$mm\ day^{-1}$')
    else:
        print('Normalizaton (option \'norm\') not valid.')
    #plt.set_ylabel('Count')
    #
    #---- Add normal distribution.
    halfwidth = np.max([np.nanmean(diffs) - np.nanmin(diffs),
                        np.nanmax(diffs) - np.nanmean(diffs)])
    x = np.linspace(np.nanmean(diffs) - halfwidth,
                    np.nanmean(diffs) + halfwidth, 200)
    ax.plot(x, norm.pdf(x, loc=np.nanmean(diffs), scale=np.nanstd(diffs)))
    #
    #---- Run Shapiro-Wilks test on differences.
    print('----- Shapiro-Wilks -----')
    stat, p = shapiro(diffs)
    print('Statistic=%.3f, p=%.3f' % (stat, p))
    # interpret
    alpha = 0.05
    if p > alpha:
        print('Sample looks Gaussian -- fail to reject H0 at alpha=%.2f' % \
              alpha)
    else:
        print('Sample does not look Gaussian -- reject H0 at alpha=%.2f' % \
              alpha)
    #
    #---- Add some info to plot.
    infotext = 'N = ' + '{:d}'.format(len(diffs)) + \
               '\nMean = ' + '{:.3g}'.format(np.nanmean(diffs)) + \
               '\nStd. dev. = ' + '{:.3g}'.format(np.nanstd(diffs)) + \
               '\n--- Shapiro-Wilks ---' + \
               '\nStatistic=' + '{:.3f}'.format(stat) + \
               '\np=' + '{:.5f}'.format(p)
    ax.text(0.05, 0.95, infotext,
            verticalalignment='top', horizontalalignment='left',
            transform=ax.transAxes)
    #
    #---- Add inset plot.
    ax2 = mpl_il.inset_axes(ax,
                            width='25%',
                            height='25%')
    res = stats.probplot(diffs, dist='norm',
                         sparams=(np.nanmean(diffs), np.nanstd(diffs)),
                         plot=ax2)
    ax2.set_title('')
    if nrm == 'total':
        ax2.set_xlabel('Theoretical /\n' + r'$m^3\ s^{-1}$')
        ax2.set_ylabel('Observed /\n' + r'$m^3\ s^{-1}$')
    elif nrm == 'area':
        ax2.set_xlabel('Theoretical /\n' + r'$mm\ day^{-1}$')
        ax2.set_ylabel('Observed /\n' + r'$mm\ day^{-1}$')
    else:
        print('Normalizaton (option \'norm\') not valid.')
    #
    # Display the plot.
    plt.show()
    #
    return

##### Quantile plot.
def plot_qq(diffs, basin, var1, var2, nrm):
    fig, ax = plt.subplots(1, 1, figsize = [8,5])
    res = stats.probplot(diffs, dist='norm',
                         sparams=(np.nanmean(diffs), np.nanstd(diffs)),
                         plot=plt)
    ax.set_title('Normal Probability Plot')
    if nrm == 'total':
        ax.set_xlabel('Theoretical quantiles / ' + r'$m^3\ s^{-1}$')
        ax.set_ylabel('Ordered values / ' + r'$m^3\ s^{-1}$')
    elif nrm == 'area':
        ax.set_xlabel('Theoretical quantiles / ' + r'$mm\ day^{-1}$')
        ax.set_ylabel('Ordered values / ' + r'$mm\ day^{-1}$')
    else:
        print('Normalizaton (option \'norm\') not valid.')
    #
    # Display the plot.
    #plt.show()
    #
    return(fig, ax)


################################################################################
# Data load function.

def get_diffs(basin, var1, var2, nrm):
    #
    # Read all data.
    all_vars = get_budget_vars(basin)
    #
    # Get required variables (in m3 s-1).
    d1 = all_vars[var1]
    d2 = all_vars[var2]
    #
    # Calculate difference.
    diff = d1 - d2
    #
    # Subset to just since 2000.
    diff = diff.sel(time=slice('2000-01-01','2019-12-31'))
    #
    # Apply normalization.
    if nrm == 'total':
        diff.attrs['units'] = 'm3 s-1'
        return(diff)
    elif nrm == 'area':
        # Convert from m3 s-1 to mm day-1
        basin_area = Amazon_catchment_details.areas[basin]*1000000.0
        conversion = (1000.0*24.0*60.0*60.0)/basin_area
        diff = diff*conversion
        diff.attrs['units'] = 'mm day-1'
        return(diff)
    else:
        print('Normalizaton (option \'norm\') not valid.')
        return(np.nan)

################################################################################

###########################################
# Now actually execute the script.
###########################################

if __name__ == '__main__':
    differences = main()
