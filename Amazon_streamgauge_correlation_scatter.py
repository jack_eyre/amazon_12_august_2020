"""
Plot correlation coefficient of Obidos streamflow with estimated streamflow from different combinations of data.

Run it from plotenv conda environment.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################

################################################################################
def main():
    
    ##### Read hydrology data. ###########################################
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Fill gaps in streamflow (inserts nan for missing months)
    ro_ob = ro_ob.resample(time='1M').asfreq()
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    
    # Get factor to correct Obidos streamflow to mouth.
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    ob_to_mouth = ds_DT.ratio_m2s[0].data
    
    # Scale up Obidos streamflow to river mouth.
    ro_DT = ro_ob.copy()
    ro_DT = ro_DT*ob_to_mouth
    
    # Get other water budget variables.
    # All in m3 s-1.
    budget_Amazon = get_budget_vars('Amazon')
    budget_Obidos = get_budget_vars('Obidos')
    #
    # Add streamflow to dictionary.
    budget_Obidos['streamflow'] = ro_ob
    budget_Amazon['streamflow'] = ro_DT
    
    ##### Calculate correlations. ###########################################
    
    print('Processing...')
    
    # Calculate correlations of streamflow estimates with streamgauge.
    corrs_Amazon = corrs_all(budget_Amazon)
    corrs_Obidos = corrs_all(budget_Obidos)
    #
    # Print out max. correlations for each time step for Obidos.
    if 0:
        r_max_m = 0.0 
        r_max_s = 0.0 
        r_max_a = 0.0 
        comb_max_m = ''
        comb_max_s = ''
        comb_max_a = ''
        for comb in corrs_Obidos['monthly'].keys():
            if corrs_Obidos['monthly'][comb]['r_abs_lag0'] > r_max_m:
                r_max_m = corrs_Obidos['monthly'][comb]['r_abs_lag0']
                comb_max_m = comb
            if corrs_Obidos['seasonal'][comb]['r_abs_lag0'] > r_max_s:
                r_max_s = corrs_Obidos['seasonal'][comb]['r_abs_lag0']
                comb_max_s = comb
            if corrs_Obidos['annual'][comb]['r_abs_lag0'] > r_max_a:
                r_max_a = corrs_Obidos['annual'][comb]['r_abs_lag0']
                comb_max_a = comb
        print('---------- Max Obidos correlations ----------')
        print('Monthly:')
        print(comb_max_m)
        print(r_max_m)
        print('Seasonal:')
        print(comb_max_s)
        print(r_max_s)
        print('Annual:')
        print(comb_max_a)
        print(r_max_a)
    
    ##### Plot results. ###########################################
    
    print('Plotting...')
    plot = plot_corrs({'Amazon': corrs_Amazon,
                       'Obidos': corrs_Obidos})
    
    # Return data for debugging.
    return({'Amazon': corrs_Amazon,
            'Obidos': corrs_Obidos})


################################################################################

################################################################################
# Plotting function.

def plot_corrs(data_dict):
    #
    # Define font details.
    plt.rcParams.update({'font.size': 14})
    #
    # Get some info from data.
    basin_list = list(data_dict.keys())
    label_list = basin_list.copy()
    timescale_list = list(data_dict[basin_list[0]].keys())
    special_colors = {'PEG_Ens':'#d33682',
                      'CHG_Ens':'#6c71c4',
                      'PEG_ERA5_ERA5_JPL':'#2aa198',
                      'PEG_CHIRPS_ERA5_JPL':'#859900'}
    stat_type = 'r_abs_lag0'
    
    # Set up over all plot.
    # One panel each for abs and anom.
    fig, axs = plt.subplots(1, len(timescale_list),
                            figsize=(9, 4), sharey=True)
    
    # Loop over timescales (panels).
    for t in range(len(timescale_list)):
        # Initialize variable lists.
        names = []
        values = []
        names_Ens = []
        values_Ens = []
        cols_Ens = []
        # Loop over basins (Amazon, Obidos) and data combinations.
        for b in range(len(basin_list)):
            for comb in data_dict[basin_list[b]][timescale_list[t]]:
                if (comb in special_colors.keys()):
                    # Add highlight values to other lists.
                    names_Ens.append(b + 0.5*np.random.rand(1)[0] - 0.25)
                    values_Ens.append(data_dict[basin_list[b]]\
                                      [timescale_list[t]][comb][stat_type])
                    cols_Ens.append(special_colors[comb])
                else:
                    # Add data to lists.
                    names.append(b + 0.5*np.random.rand(1)[0] - 0.25)
                    values.append(data_dict[basin_list[b]]\
                                  [timescale_list[t]][comb][stat_type])
        # Make the plot panel.        
        axs[t].scatter(names, values, c='#002b36', s=8, marker='.')
        axs[t].scatter(names_Ens, values_Ens, c=cols_Ens,
                       s=15, marker='s', alpha=0.6)
        # Set some details.
        axs[t].title.set_text(timescale_list[t])
        axs[t].set_ylim(0,1)
        axs[0].set_ylabel('correlation coefficient')
        axs[t].set_xlim(-0.5, len(basin_list)-0.5)
        axs[t].set_xticks(range(len(basin_list)))
        axs[t].set_xticklabels(label_list)
        axs[t].xaxis.set_tick_params(which='both', bottom=True, top=True)
        axs[t].yaxis.set_tick_params(which='both', left=True, right=True)
    
    # Add legend.
    legend_entries = []
    for comb in special_colors.keys():
        legend_entries.append(mlines.Line2D([], [],
                                            color=special_colors[comb],
                                            marker='s',
                                            linestyle='None',
                                            label=comb))
    axs[0].legend(handles=legend_entries,
                  loc='lower right',
                  fontsize='small',
                  markerscale=0.7,
                  labelspacing=0.4,
                  handletextpad=0.05)
    
    # Tidy layout.
    fig.tight_layout()
            
    # Get git info for this script.
    sys.path.append(os.path.expanduser('~/Documents/code/'))
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    
    # Add git info to footer.
    plt.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    
    # Show the plot.
    #plt.show()
    # Or save the figure.
    fig.savefig(os.path.expanduser('~') +
                '/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.pdf',
                format='pdf')
    
    return(fig)

################################################################################
### Correlation functions.

def corrs_all(all_vars):
    ###############################################################
    # Returns dictionaries of statistics of correlation coefficient:
    # Monthly, seasonal and annual,
    # each of which has results from different combinations
    # of budget terms, for both absolute values and anomalies.
    # The results are each the output from corr_stats (function
    # below).
    #
    # Input:
    #     all_vars:     a dictionary of variables output by
    #                   get_budget_vars (below).
    #
    # Output:
    #     a dictionary with the following structure of sub-dictionaries:
    #
    #     {'monthly': {<'budget_terms_combination_1'>: corr_stats,
    #                  <'budget_terms_combination_2'>: corr_stats,
    #                  ... },
    #     'seasonal': {<'budget_terms_combination_1'>: corr_stats,
    #                  <'budget_terms_combination_2'>: corr_stats,
    #                  ... },
    #     'annual': {<'budget_terms_combination_1'>: corr_stats,
    #                <'budget_terms_combination_2'>: corr_stats,
    #                  ... }
    #     }
    # The different combinations are named according to their input
    # data and method (e.g., 'PE_GPCP_GLEAM_JPL' [P-E method] or
    # 'conv_ERA5_ERA5_GFZ' [atmospheric convergence method].
    ###############################################################
    
    # Initialize final output dictionary.
    output = {}
    
    # Loop over timesteps.
    freq_list = ['monthly','seasonal','annual']
    for f in freq_list:
        print(f)
        # Initialize sub-dictionary.
        temp = {}
        # Add ensemble means.
        print('PEG_Ens')
        temp['PEG_Ens'] = corr_stats(all_vars['pr_Ens'],
                                     all_vars['evap_Ens'],
                                     all_vars['dSdt_' + f[0] + '_Ens'],
                                     all_vars['streamflow'],
                                     freq=f)
        print('CHG_Ens')
        temp['CHG_Ens'] = corr_stats(all_vars['conv_Ens'],
                                     all_vars['dtcwdt_' + f[0] + '_Ens'],
                                     all_vars['dSdt_' + f[0] + '_Ens'],
                                     all_vars['streamflow'],
                                     freq=f, PE_type="conv")
        # Loop over GRACE methods.
        GRACE_list = ['JPL']
        pr_list = ['GPCP', 'CMAP', 'CHIRPS', 'ERA5', 'MERRA2']
        evap_list = ['GLEAM', 'CLM', 'Noah', 'ERA5', 'MERRA2']
        for g in GRACE_list:
            for p in pr_list:
                for e in evap_list:
                    print('PEG_' + p + '_' + e + '_' + g)
                    temp['PEG_' + p + '_' + e + '_' + g] = corr_stats(
                        all_vars['pr_' + p],
                        all_vars['evap_' + e],
                        all_vars['dSdt_' + f[0] + '_' + g],
                        all_vars['streamflow'],
                        freq=f)
            print('CHG_ERA5_ERA5_' + g)
            temp['CHG_ERA5_ERA5_' + g] = corr_stats(
                all_vars['vimd_ERA5'],
                all_vars['dtcwdt_' + f[0] + '_ERA5'],
                all_vars['dSdt_' + f[0] + '_' + g],
                all_vars['streamflow'], freq=f, PE_type="conv")
            print('CHG_MERRA2_MERRA2_' + g)
            temp['CHG_MERRA2_MERRA2_' + g] = corr_stats(
                all_vars['viwvc_MERRA2'],
                all_vars['dtcwdt_' + f[0] + '_MERRA2'],
                all_vars['dSdt_' + f[0] + '_' + g],
                all_vars['streamflow'], freq=f, PE_type="conv")
        
        # Add all these to the final output.
        output[f] = temp
        
    # Return the output.
    return(output)


def corr_stats(p_m, e_m, dSdt, r_m, freq="monthly", PE_type="PE"):
    ###############################################################
    # Returns dictionary of statistics of correlation statistics.
    # Input:
    #     p_m:   precipitation OR
    #            atmospheric convergence
    #     e_m:   evaporation OR
    #            change in atmospheric precipitable water
    #     dSdt:  change in terrestrial water storage
    #     r_m:   streamflow
    # All variables should be in cubic meters per second.
    #
    # All variables should have a time coordinate with time
    # for each month given at YYYY-MM-01 00:00:00.
    #
    # Optional arguments:
    #     freq: averaging frequency of time series.
    #           allowed values: "monthly" (default), "seasonal", "annual"
    #       !!! NOTE: dSdt variable frequency must match this value;
    #                 other arguments are always passed as monthly.
    #     PE_type: "PE" or "conv": if "PE", does seasonal/annual averaging
    #              for the second argument (evap); if "conv", does not do
    #              seasonal/annual averaging for the second argument
    #              (change in atmospheric precipitable water) as this is
    #              passed at the relevant time scale already.
    #     
    #
    # Output:
    # Dictionary containing correlation coefficients:
    #     r_abs_lag0:    correlation of absolute values at zero lag
    ###############################################################
    
    if PE_type == "PE":
        if freq == "seasonal":
            p = seasonal_mean(p_m)
            e = seasonal_mean(e_m)
            r = seasonal_mean(r_m)
        elif freq == "annual":
            p = annual_mean(p_m)
            e = annual_mean(e_m)
            r = annual_mean(r_m)
            dSdt = dSdt.rename({"year":"time"})
        elif freq == "monthly":
            p = p_m
            e = e_m
            r = r_m
        else:
            print("Invalid option for freq")
            p = np.nan
            e = np.nan
            r = np.nan
    elif PE_type == "conv":
        if freq == "seasonal":
            p = seasonal_mean(p_m)
            e = e_m
            r = seasonal_mean(r_m)
        elif freq == "annual":
            p = annual_mean(p_m)
            r = annual_mean(r_m)
            e = e_m
            e = e.rename({"year":"time"})
            dSdt = dSdt.rename({"year":"time"})
        elif freq == "monthly":
            p = p_m
            e = e_m
            r = r_m
        else:
            print("Invalid option for freq")
            p = np.nan
            e = np.nan
            r = np.nan
    else:
        print("Invalid option for PE_type")
        p = np.nan
        e = np.nan
        r = np.nan        
        #
    # Calculate estimated streamflow.
    q_est = p - e - dSdt
    
    # Subset to common time period.
    st_time = max([min(q_est.time),min(r.time)])
    end_time = min([max(q_est.time),max(r.time)])
    print(st_time.data)
    print(end_time.data)
    v1 = q_est.sel(time=slice(st_time,end_time)).data
    v2 = r.sel(time=slice(st_time,end_time)).data
    
    # Mask out missing data.
    mask = (~np.isnan(v1) & ~np.isnan(v2))
    v1 = v1[mask]
    v2 = v2[mask]
    
    # Calculate answer and put into output dictionary.
    stats = {
        "r_abs_lag0":np.corrcoef(v1,v2)[0,1]}
    print(stats['r_abs_lag0'])
    
    # Return values.
    return(stats)


# Simple function to return seasonal mean with NaN value if not
# all months are present for each season.
# Input is a 1D time series as an xarray data array.
# Output is a 1D time series with one data point per season.
def seasonal_mean(ts):
    ts_s = ts.resample(time='QS-DEC').mean(dim='time')
    ts_count = ts.resample(time='QS-DEC').count(dim='time')
    ts_s[ts_count < 3] = np.nan
    return ts_s

# Simple function to return annual mean with NaN value if not
# all months are present for each year.
# Input is a 1D time series as an xarray data array.
# Output is a 1D time series with one data point per season.
def annual_mean(ts):
    ts_a = ts.groupby('time.year').mean(dim='time')
    ts_count = ts.groupby('time.year').count(dim='time')
    ts_a[ts_count < 12] = np.nan
    ts_a = ts_a.rename({"year":"time"})
    return ts_a


################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    corr_dict = main()

