"""
Plot map of Amazon basin and some relevant details.
"""

###########################################
#### For data analysis:
import numpy as np
import xarray as xr
import datetime
import os
import sys
import subprocess
#### For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.colors as mcolors
from matplotlib import cm, gridspec, rcParams, colors
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import shapefile
import matplotlib.patches as patches
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
#### Routines from other files:
from Amazon_closure import *
################################################################################

# Main script.
def main():
    #
    ############################################
    # Read data.
    print('Reading data...')
    
    # Data locations.
    data_dir = '/Users/jameseyre/Data/satellite/GPCP/'
    shp_dir = '/Users/jameseyre/Data/in-situ/hybam/'
    shp_file_Amazon = 'Amazon_catchment.shp'
    shp_file_Obidos = 'Obidos_catchment.shp'
    GPCP_file_name = 'GPCP.v2.3.precip.mon.mean.nc'
    
    # Read the GRACE file. 
    ds_in = xr.open_dataset(data_dir + GPCP_file_name)
    pr = ds_in.precip
    
    # Sort out the longitude array order in GRACE data. 
    pr = pr.assign_coords(lon=(((pr.lon + 180) % 360) - 180))
    pre = pr.isel(lon=slice(0,72))
    prw = pr.isel(lon=slice(72,144))
    pr = xr.concat([prw,pre],'lon')
    
    # Read the shapefiles - using routine defined below.
    ptchs_amazon = get_sf_patches(shp_dir + shp_file_Amazon)
    ptchs_obidos = get_sf_patches(shp_dir + shp_file_Obidos)
    
    ############################################
    # Average data.
    print('Processing data...')
    
    # Specify averaging period.
    st_yr = 2000
    end_yr = 2018
    
    # Get annual average of this period.
    pr_ann = pr.loc[dict(time=slice(str(st_yr) + '-01-01',
                                    str(end_yr) + '-12-31'))].\
                groupby('time.month').mean('time').mean('month')
    
    ############################################
    # Plot map.
    print('Plotting...')
    #

    # 2d lat and lon arrays for plotting.
    dlatm, dlonm = np.meshgrid(pr.lat,pr.lon,indexing='ij')

    # Define map projection.
    projection = ccrs.PlateCarree()
    
    # Define color map.
    cmap_b = plt.get_cmap('Greens')
    norm_b = colors.BoundaryNorm(np.arange(0.0, 10.01, 1.0),
                                 ncolors=cmap_b.N)
    
    # Open figure.
    fig = plt.figure(figsize=(8, 4.5), frameon=False)
    ax = plt.axes(projection=projection)

    # Plot correlations.
    p = ax.pcolormesh(dlonm, dlatm, pr_ann,
                      vmin=0.0, vmax=10.0, 
                      transform=projection,
                      cmap=cmap_b, norm=norm_b)
    
    # Set map spatial extent.
    ax.set_extent([-90,-19, -21, 26], projection)
    # Set geographic features.
    ax.coastlines()
    # Plot Obidos location.
    plt.plot(-55.0,-1.9,color='red',marker='o', transform=projection)
    # Set gridline and label details.
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=False,
                      linewidth=2, color='gray', alpha=0.5, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.ylocator = mticker.FixedLocator(np.arange(-90,91,15))
    gl.xlocator = mticker.FixedLocator(np.arange(-180,181,15))
    
    # Add the shapefiles.
    ax.add_collection(PatchCollection(ptchs_amazon,
                                      facecolor=mcolors.colorConverter.
                                      to_rgba('white', alpha=0.0),
                                      edgecolor=mcolors.colorConverter.
                                      to_rgba('black', alpha=1.0),
                                      linewidths=1.5))
    ax.add_collection(PatchCollection(ptchs_obidos,
                                      facecolor=mcolors.colorConverter.
                                      to_rgba('white', alpha=0.0),
                                      edgecolor=mcolors.colorConverter.
                                      to_rgba('red', alpha=0.8),
                                      linewidths=.5))
    
    # Add salinity regions.
    plt.plot([-51.,-47.5,-47.5,-50.0],[3.0,3.0,0.0,0.0],
             color='blue', linewidth=1.0, linestyle='solid')
    plt.plot([-60.0,-60.0,-50.0,-50.0,-60.0],[15.0,25.0,25.0,15.0,15.0],
             color='blue', linewidth=1.0, linestyle='solid')
    plt.plot([-50.0,-50.0,-20.0,-20.0,-50.0],[5.0,10.0,10.0,5.0,5.0],
             color='blue', linewidth=1.0, linestyle='solid')
    
    # Add legend.
    cbar = fig.colorbar(p, ax=ax, orientation='vertical',
                        shrink=0.7, pad=0.07, fraction=0.07)
    cbar.ax.set_title(r'$mm\ day^{-1}$')
    
    # Get git info for this script.
    sys.path.append('/Users/jameseyre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to figure.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    
    # Display the plot
    #plt.show()
    
    # Save the figure.
    fig.savefig('/Users/jameseyre/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.pdf',
                format='pdf')

# Function to get shapefile patches.
def get_sf_patches(filename):
    # Routine taken from https://stackoverflow.com/questions/15968762/shapefile-and-matplotlib-plot-polygon-collection-of-shapefile-coordinates
    sf = shapefile.Reader(filename)
    recs    = sf.records()
    shapes  = sf.shapes()
    Nshp    = len(shapes)
    cns     = []
    for nshp in range(Nshp):
        cns.append(recs[nshp][1])
    cns = np.array(cns)
    for nshp in range(Nshp):
        ptchs   = []
        pts     = np.array(shapes[nshp].points)
        prt     = shapes[nshp].parts
        par     = list(prt) + [pts.shape[0]]
        for pij in range(len(prt)):
            ptchs.append(Polygon(pts[par[pij]:par[pij+1]]))
    return ptchs
    
###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    main()
    
    
