"""
Plot ratios of Amazon discharge to Obidos discharge.

Run it from plotenv conda environment.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################



################################################################################
def main():
    
    ##### Read data. ###########################################
    print('Reading data...')
    
    # Get factor to correct Obidos streamflow to mouth.
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    ob_to_mouth = ds_DT.ratio_m2s[0].data
    #
    # Get other water budget variables.
    # All in m3 s-1.
    # (Make global so only need to do this once and don't have
    # to pass through the function.)
    global budget_Amazon, budget_Obidos
    budget_Amazon = get_budget_vars('Amazon')
    budget_Obidos = get_budget_vars('Obidos')
    #
    #
    ##### Calculate ratios. ###########################################
    print('Reading data...')
    #
    # Construct discharge estimate ratios.
    ratio_ts = {}
    for comb in ['PEG_ERA5_ERA5_JPL',
                 'CHG_ERA5_ERA5_JPL',
                 'PEG_GPCP_CLM_JPL']:
        ratio_ts[comb] = get_ratio(comb)
    #
    # Calculate averages.
    ratio_av = {'mean':{},'stdev':{}}
    for comb in ratio_ts.keys():
        ratio_av['mean'][comb] = ratio_ts[comb].groupby('time.month').mean()
        ratio_av['stdev'][comb] = ratio_ts[comb].groupby('time.month').std()
    #
    #
    ##### Plot results. ###########################################
    #
    print('Plotting...')
    plot_ratios(ob_to_mouth, ratio_av)
    #
    return


################################################################################
# Plotting function
def plot_ratios(const, ratio_dict):
    #
    # List colors to use.
    custom_cols = ['#2aa198','#d33682','#6c71c4','#268bd2',
                   '#859900','#dc322f','#cb4b16','#b58900']
    i_col = 0
    #
    # Set up over all plot.
    fig, axs = plt.subplots(1, 1, figsize=(6, 4))
    #
    # Plot constant value from DT02.
    axs.plot([0,13],[const,const],
             c='#002b36', label='Dai et al. (2009)')
    #
    # Loop over combinations.
    for comb in ratio_dict['mean'].keys():
        axs.plot(ratio_dict['mean'][comb].month, ratio_dict['mean'][comb],
                 c=custom_cols[i_col], label=comb)
        axs.fill_between(ratio_dict['mean'][comb].month,
                         ratio_dict['mean'][comb] + ratio_dict['stdev'][comb],
                         ratio_dict['mean'][comb] - ratio_dict['stdev'][comb],
                         color=custom_cols[i_col],
                         alpha=0.1)
        i_col += 1
    #
    # Set axis details.
    axs.set_xlim([0.5, 12.5])
    axs.set_ylim([0.8,1.6])
    axs.set_xticks(range(1,13))
    axs.set_xticklabels(['J','F','M','A','M','J',
                         'J','A','S','O','N','D'])
    #
    # Set titles etc.
    axs.set_title('Amazon:Obidos ratio', loc='left')
    axs.set_title('Discharge', loc='right')
    #
    # Add legend.
    axs.legend(loc='upper right')
    #
    # Get git info for this script.
    sys.path.append(os.path.expanduser('~/Documents/code/'))
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    #
    # Add git info to footer.
    plt.text(0.02,0.001,txtl, transform=fig.transFigure, size=3)
    #
    # Show the plot.
    #plt.show()
    # Or save the figure.
    fig.savefig(os.path.expanduser('~') +
                '/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.pdf',
                format='pdf')


################################################################################

# Function to get Amazon/Obidos ration for combination 'c', specified in
# form PE_P_E_G or conv_C_H_G.
#
def get_ratio(c):
    #
    # Split c into components.
    r_hat_type = c.split('_')[0]
    pr_name = c.split('_')[1]
    evap_name = c.split('_')[2]
    GRACE_name = c.split('_')[3]
    #
    # Get required variables (in m3 s-1).
    if r_hat_type == 'PE' or r_hat_type == 'PEG' :
        pr_a = budget_Amazon['pr_' + pr_name]
        evap_a = budget_Amazon['evap_' + evap_name]
        dSdt_a = budget_Amazon['dSdt_m_' + GRACE_name]
        r_hat_a = pr_a - evap_a - dSdt_a
        pr_o = budget_Obidos['pr_' + pr_name]
        evap_o = budget_Obidos['evap_' + evap_name]
        dSdt_o = budget_Obidos['dSdt_m_' + GRACE_name]
        r_hat_o = pr_o - evap_o - dSdt_o
    elif r_hat_type == 'conv' or r_hat_type == 'CHG' :
        if pr_name == 'ERA5':
            conv_a = budget_Amazon['vimd_ERA5']
            conv_o = budget_Obidos['vimd_ERA5']
        elif pr_name == 'MERRA2':
            conv_a =  budget_Amazon['viwvc_MERRA2']
            conv_o =  budget_Obidos['viwvc_MERRA2']
        else:
            sys.exit('Must use ERA5 or MERRA2 with convergence method.')
        dtcwdt_a = budget_Amazon['dtcwdt_m_' + evap_name]
        dSdt_a = budget_Amazon['dSdt_m_' + GRACE_name]
        r_hat_a = conv_a - dtcwdt_a - dSdt_a
        dtcwdt_o = budget_Obidos['dtcwdt_m_' + evap_name]
        dSdt_o = budget_Obidos['dSdt_m_' + GRACE_name]
        r_hat_o = conv_o - dtcwdt_o - dSdt_o
    else:
        sys.exit('r_hat_type must be one of \'PE\' and \'conv\'.')
    #
    # Get rid of any negative values.
    if any(r_hat_a <= 0.0):
        print('Removing negative values (' + c + ')')
        r_hat_a[r_hat_a <= 0.0] = np.nan
    if any(r_hat_o <= 0.0):
        print('Removing negative values (' + c + ')')
        r_hat_o[r_hat_o <= 0.0] = np.nan
    # Calculate and return ratio
    ratio = r_hat_a/r_hat_o
    if any(ratio <= 1.0):
        print('Values less than 1.0 (' + c + ')')
        times = ratio[ratio <= 1.0].time
        if 0:
            for t in times:                                          
                print(t.data)                                        
                print('pr Amazon: ' + str(pr_a.sel(time=t).data))    
                print('pr Obidos: ' + str(pr_o.sel(time=t).data))    
                print('evap Amazon: ' + str(evap_a.sel(time=t).data))
                print('evap Obidos: ' + str(evap_o.sel(time=t).data))
                print('dSdt Amazon: ' + str(dSdt_a.sel(time=t).data))
                print('dSdt Obidos: ' + str(dSdt_o.sel(time=t).data))
                print('r_hat Amazon: ' + str(r_hat_a.sel(time=t).data))
                print('r_hat Obidos: ' + str(r_hat_o.sel(time=t).data))
    #
    return ratio


################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    main()
