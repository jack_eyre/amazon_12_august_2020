"""
Plot annual cycles and variability of (annual) streamflow and precip.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
###########################################


def main():

    # Specify file locations.
    precip_filename = '~/Data/satellite/GPCP/AmazonBasinAverage_GPCP.v2.3.precip.mon.mean.nc'
    obidos_filename = '~/Data/satellite/GPCP/ObidosBasinAverage_GPCP.v2.3.precip.mon.mean.nc'
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'

    # Specify time period for comparison.
    st_time = np.datetime64('1979-01-01')
    end_time = np.datetime64('2015-12-31')

    # Read data.
    ds_pr = xr.open_dataset(precip_filename)
    ds_pr_ob = xr.open_dataset(obidos_filename)
    ds_hy = xr.open_dataset(hybam_filename)
    pr = ds_pr.precip.loc[dict(time=slice(st_time, end_time))]
    pr_ob = ds_pr_ob.precip.loc[dict(time=slice(st_time, end_time))]
    ro = ds_hy.Q.loc[dict(time=slice(st_time, end_time))]

    # Calculate annual averages.
    pr_ann = pr.groupby('time.year').mean('time')
    pr_ob_ann = pr_ob.groupby('time.year').mean('time')
    ro_ann = ro.groupby('time.year').mean('time')

    # Create plots.
    plot_SC(pr, pr_ob, ro, pr_ann, pr_ob_ann, ro_ann)



###########################################
# Function to plot seasonal cycles and
# distribution of annual means:
def plot_SC(p, p_ob, r, p_a, p_ob_a, r_a):
    
    # Define overall figure.
    plt.rcParams.update({'font.size': 14})
    fig, axes = plt.subplots(3, 2,
                             gridspec_kw={'width_ratios': [7, 1]},
                             figsize=(9, 9))
    
    # Loop over years for annual cycles.
    for y in range(min(p['time.year'].data),
                   max(p['time.year'].data)):
        
        # First panel: annual cycles of precip.
        axes[0,0].plot(range(1,13), p[p['time.year'] == y],
                       'grey', linewidth=0.6)
        axes[0,0].set_xlim(0.5,12.5)
        axes[0,0].set_ylim(0,12)
        axes[0,0].set_ylabel('Precip. rate /\n' + r'$mm\ day^{-1}$')
        axes[0,0].set_xticks(range(1,13))
        axes[0,0].set_xticklabels([], {'fontsize':0.01})
        
        # Third panel: annual cycles of precip.
        axes[1,0].plot(range(1,13), p_ob[p_ob['time.year'] == y],
                       'grey', linewidth=0.6)
        axes[1,0].set_xlim(0.5,12.5)
        axes[1,0].set_ylim(0,12)
        axes[1,0].set_ylabel('Precip. rate /\n' + r'$mm\ day^{-1}$')
        axes[1,0].set_xticks(range(1,13))
        axes[1,0].set_xticklabels([], {'fontsize':0.01})

        # Fifth panel: annual cycles of streamflow.
        axes[2,0].plot(range(1,13), r[r['time.year'] == y]/1.0e5,
                       'grey', linewidth=0.6)
        axes[2,0].set_xlim(0.5,12.5)
        axes[2,0].set_ylim(0,3)
        axes[2,0].set_ylabel('Streamflow /\n' + r'$10^5\ m^3\ s^{-1}$')
        axes[2,0].set_xticks(range(1,13))
        axes[2,0].set_xticklabels(['J','F','M','A','M','J',
                                   'J','A','S','O','N','D'])

    # Add mean annual cycle to these.
    axes[0,0].plot(range(1,13), p.groupby('time.month').mean('time'),
                   'k', linewidth=2.0)
    axes[1,0].plot(range(1,13), p_ob.groupby('time.month').mean('time'),
                   'k', linewidth=2.0)
    axes[2,0].plot(range(1,13), r.groupby('time.month').mean('time')/1.0e5,
                   'k', linewidth=2.0)
    
    # Second panel: distribution of annual mean precip.
    axes[0,1].boxplot(p_a, widths=0.6)
    axes[0,1].set_ylim(0,12)
    axes[0,1].set_xticks([])
    axes[0,1].set_xticklabels([], {'fontsize':0.01})
    axes[0,1].set_yticklabels([], {'fontsize':0.01})
    
    # Fourth panel: distribution of annual mean precip.
    axes[1,1].boxplot(p_ob_a, widths=0.6)
    axes[1,1].set_ylim(0,12)
    axes[1,1].set_xticks([])
    axes[1,1].set_xticklabels([], {'fontsize':0.01})
    axes[1,1].set_yticklabels([], {'fontsize':0.01})
    
    # Sixth panel: distribution of annual mean streamflow.
    axes[2,1].boxplot(r_a/1.0e5, widths=0.6)
    axes[2,1].set_ylim(0,3)
    axes[2,1].set_xticks([])
    axes[2,1].set_xticklabels([], {'fontsize':0.01})
    axes[2,1].set_yticklabels([], {'fontsize':0.01})

    # Add labels.
    plt.text(0.02, 0.9,'a', ha='left', va='center',
             transform=axes[0,0].transAxes)
    plt.text(0.12, 0.9,'b', ha='left', va='center',
             transform=axes[0,1].transAxes)
    plt.text(0.02, 0.9,'c', ha='left', va='center',
             transform=axes[1,0].transAxes)
    plt.text(0.12, 0.9,'d', ha='left', va='center',
             transform=axes[1,1].transAxes)
    plt.text(0.02, 0.9,'e', ha='left', va='center',
             transform=axes[2,0].transAxes)
    plt.text(0.12, 0.9,'f', ha='left', va='center',
             transform=axes[2,1].transAxes)
    #
    plt.text(0.02, 0.1,'Entire Amazon', ha='left', va='center',
             transform=axes[0,0].transAxes)
    plt.text(0.02, 0.1,'Obidos catchment', ha='left', va='center',
             transform=axes[1,0].transAxes)
    plt.text(0.02, 0.1,'Obidos', ha='left', va='center',
             transform=axes[2,0].transAxes)

    # Tidy layout.
    fig.tight_layout()
    
    # Add info to footer.
    [txtl, hash, clean] = get_git_revision_info()
    plt.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)

    # Save to PDF file.
    fig.savefig('/Users/jameseyre/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.pdf',
                format='pdf')
    #plt.show()

###########################################
# Function for git commit hash:
def get_git_revision_info():
    
    import sys
    import subprocess
    import os
    
    # Get info.
    filename = os.path.basename(__file__)
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    clean =  subprocess.check_output(['git', 'diff',
                                      '--name-only',
                                      filename]).strip()
    
    # Change string format if python version 3.x.
    if sys.version_info[0] >= 3:
        #filename = filename.decode('utf-8')
        hash = hash.decode('utf-8')
        clean =  clean.decode('utf-8')
    
    # Put return string together.    
    info = filename + '          '
    if (clean == ''):
        info = info + 'Up to date. Last commit: ' + hash
    else:
        info = info + 'Untracked changes since last commit: ' + hash
    
    return [info, hash, clean]

###########################################
# Now actually execute the script.
if __name__ == '__main__':
    main()
