"""
Plot map of annual mean precip from several data sets over the Amazon basin.
"""

###########################################
#### For data analysis:
import numpy as np
import xarray as xr
import datetime
import os
import sys
import subprocess
#### For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.colors as mcolors
from matplotlib import cm, gridspec, rcParams, colors
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import shapefile
import matplotlib.patches as patches
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
#### Routines from other files:
from Amazon_closure import *
################################################################################

# Main script.
def main():
    #
    ############################################
    # Read data.
    print('Reading data...')
    
    # Shape file locations.
    shp_dir = os.path.expanduser('~/Data/in-situ/hybam/')
    shp_file_Amazon = 'Amazon_catchment.shp'
    shp_file_Obidos = 'Obidos_catchment.shp'
    GPCP_file_name = 'GPCP.v2.3.precip.mon.mean.nc'
    #
    #---- GPCP ----
    # Read precip data using functions below. 
    pr_GPCP = load3D_GPCP()
    pr_CMAP = load3D_CMAP()
    pr_ERA5 = load3D_ERA5()
    pr_MERRA2 = load3D_MERRA2('biascorr')
    pr_MERRA2_raw = load3D_MERRA2('raw')
    pr_CHIRPS = load3D_CHIRPS()
    
    # Read the shapefiles - using routine defined below.
    ptchs_amazon = get_sf_patches(shp_dir + shp_file_Amazon)
    ptchs_obidos = get_sf_patches(shp_dir + shp_file_Obidos)
    
    ############################################
    # Average data.
    print('Processing data...')
    
    # Specify averaging period.
    st_yr = 2000
    end_yr = 2018
    
    # Get annual average of this period.
    pr_ann_GPCP = pr_GPCP.loc[dict(time=slice(str(st_yr) + '-01-01',
                                              str(end_yr) + '-12-31'))].\
                  groupby('time.month').mean('time').mean('month')
    pr_ann_CMAP = pr_CMAP.loc[dict(time=slice(str(st_yr) + '-01-01',
                                              str(end_yr) + '-12-31'))].\
                  groupby('time.month').mean('time').mean('month')
    pr_ann_ERA5 = pr_ERA5.loc[dict(time=slice(str(st_yr) + '-01-01',
                                              str(end_yr) + '-12-31'))].\
                  groupby('time.month').mean('time').mean('month')
    pr_ann_MERRA2 = pr_MERRA2.loc[dict(time=slice(str(st_yr) + '-01-01',
                                                  str(end_yr) + '-12-31'))].\
                    groupby('time.month').mean('time').mean('month')
    pr_ann_MERRA2_raw = pr_MERRA2_raw.loc[dict(time=slice(str(st_yr) + '-01-01',
                                                          str(end_yr) + '-12-31'))].\
                    groupby('time.month').mean('time').mean('month')
    pr_ann_CHIRPS = pr_CHIRPS.loc[dict(time=slice(str(st_yr) + '-01-01',
                                                  str(end_yr) + '-12-31'))].\
                    groupby('time.month').mean('time').mean('month')
    
    
    ############################################
    # Plot map.
    print('Plotting...')
    #
    #
    # Define color map.
    cmap_b = plt.get_cmap('Greens')
    norm_b = colors.BoundaryNorm(np.arange(0.0, 10.01, 1.0),
                                 ncolors=cmap_b.N)
    #
    # Define map projection.
    projection = ccrs.PlateCarree()
    #
    # Set up axes and figure.
    axes_class = (GeoAxes,
                  dict(map_projection=projection))
    fig = plt.figure(figsize=(8, 8))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(3, 2),
                    axes_pad=0.25,
                    cbar_location='bottom',
                    cbar_mode='single',
                    cbar_pad=0.1,
                    cbar_size='2%',
                    label_mode='')  # note the empty label_mode
    #
    # Plot each precip data set.
    #---- GPCP.
    dlatm_GPCP, dlonm_GPCP = np.meshgrid(pr_GPCP.lat,pr_GPCP.lon,indexing='ij')
    p = axgr[0].pcolormesh(dlonm_GPCP, dlatm_GPCP, pr_ann_GPCP,
                           vmin=0.0, vmax=10.0, 
                           transform=projection,
                           cmap=cmap_b, norm=norm_b)
    axgr[0].set_title('GPCP',loc='left')
    #---- CMAP.
    dlatm_CMAP, dlonm_CMAP = np.meshgrid(pr_CMAP.lat,pr_CMAP.lon,indexing='ij')
    p = axgr[1].pcolormesh(dlonm_CMAP, dlatm_CMAP, pr_ann_CMAP,
                           vmin=0.0, vmax=10.0, 
                           transform=projection,
                           cmap=cmap_b, norm=norm_b)
    axgr[1].set_title('CMAP',loc='left')
    #---- CHIRPS.
    dlatm_CHIRPS, dlonm_CHIRPS = np.meshgrid(pr_CHIRPS.latitude,
                                             pr_CHIRPS.longitude,indexing='ij')
    p = axgr[2].pcolormesh(dlonm_CHIRPS, dlatm_CHIRPS, pr_ann_CHIRPS,
                           vmin=0.0, vmax=10.0, 
                           transform=projection,
                           cmap=cmap_b, norm=norm_b)
    axgr[2].set_title('CHIRPS',loc='left')
    #---- ERA5.
    dlatm_ERA5, dlonm_ERA5 = np.meshgrid(pr_ERA5.latitude,\
                                         pr_ERA5.longitude,indexing='ij')
    p = axgr[3].pcolormesh(dlonm_ERA5, dlatm_ERA5, pr_ann_ERA5,
                           vmin=0.0, vmax=10.0, 
                           transform=projection,
                           cmap=cmap_b, norm=norm_b)
    axgr[3].set_title('ERA5',loc='left')
    #---- MERRA2 bias corrected.
    dlatm_MERRA2, dlonm_MERRA2 = np.meshgrid(pr_MERRA2.lat,
                                             pr_MERRA2.lon,indexing='ij')
    p = axgr[4].pcolormesh(dlonm_MERRA2, dlatm_MERRA2, pr_ann_MERRA2,
                           vmin=0.0, vmax=10.0, 
                           transform=projection,
                           cmap=cmap_b, norm=norm_b)
    axgr[4].set_title('MERRA2',loc='left')
    axgr[4].set_title('bias corr.',loc='right')
    #---- MERRA2 raw.
    dlatm_MERRA2_raw, dlonm_MERRA2_raw = np.meshgrid(pr_MERRA2_raw.lat,
                                                     pr_MERRA2_raw.lon,
                                                     indexing='ij')
    p = axgr[5].pcolormesh(dlonm_MERRA2_raw, dlatm_MERRA2_raw,
                           pr_ann_MERRA2_raw,
                           vmin=0.0, vmax=10.0, 
                           transform=projection,
                           cmap=cmap_b, norm=norm_b)
    axgr[5].set_title('MERRA2',loc='left')
    axgr[5].set_title('raw',loc='right')
    #
    # Loop over plots and set common details.
    for i, ax in enumerate(axgr[0:6]):
        # Set map spatial extent.                        
        ax.set_extent([-85,-45, -25, 10], projection)     
        # Set geographic features.                       
        ax.coastlines()                                  
        # Plot Obidos location.                          
        ax.plot(-55.0,-1.9,color='red',marker='o', transform=projection)
        # Set gridline and label details.                
        gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=False,
                          linewidth=2, color='gray', alpha=0.5, linestyle='--')
        if i in [0,2,4]:
            gl.ylabels_left = True
        if i in [4,5]:
            gl.xlabels_bottom = True                         
        gl.ylocator = mticker.FixedLocator(np.arange(-90,91,10))
        gl.xlocator = mticker.FixedLocator(np.arange(-180,181,10))
        #
        # Add the shapefiles.                            
        ax.add_collection(PatchCollection(ptchs_amazon,  
                                          facecolor=mcolors.colorConverter.
                                          to_rgba('white', alpha=0.0),
                                          edgecolor=mcolors.colorConverter.
                                          to_rgba('black', alpha=1.0),
                                          linewidths=1.5))
        ax.add_collection(PatchCollection(ptchs_obidos,  
                                          facecolor=mcolors.colorConverter.
                                          to_rgba('white', alpha=0.0),
                                          edgecolor=mcolors.colorConverter.
                                          to_rgba('red', alpha=0.8),
                                          linewidths=.5)) 
    #
    # Turn off empty 6th frame.
    
    # Add legend.
    cbar = axgr.cbar_axes[0].colorbar(p)
    cbar.set_label_text(r'$mm\ day^{-1}$')
    #
    # Get git info for this script.
    sys.path.append(os.path.expanduser('~/Documents/code/'))
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to figure.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot
    #plt.show()
    #
    # Save the figure.
    fig.savefig(os.path.expanduser('~/Documents/plots/AmazonCongo/') +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.png',
                format='png')
    #
    return


################################################################################
# Function to get shapefile patches.
def get_sf_patches(filename):
    # Routine taken from https://stackoverflow.com/questions/15968762/shapefile-and-matplotlib-plot-polygon-collection-of-shapefile-coordinates
    sf = shapefile.Reader(filename)
    recs    = sf.records()
    shapes  = sf.shapes()
    Nshp    = len(shapes)
    cns     = []
    for nshp in range(Nshp):
        cns.append(recs[nshp][1])
    cns = np.array(cns)
    for nshp in range(Nshp):
        ptchs   = []
        pts     = np.array(shapes[nshp].points)
        prt     = shapes[nshp].parts
        par     = list(prt) + [pts.shape[0]]
        for pij in range(len(prt)):
            ptchs.append(Polygon(pts[par[pij]:par[pij+1]]))
    return ptchs

################################################################################
#### Data loading routines.
def load3D_GPCP():
    ds_in = xr.open_dataset('~/Data/satellite/GPCP/' +
                            'GPCP.v2.3.precip.mon.mean.nc')
    pr = ds_in.precip
    # Sort out the longitude array order in GRACE data. 
    pr = pr.assign_coords(lon=(((pr.lon + 180) % 360) - 180))
    pre = pr.isel(lon=slice(0,72))
    prw = pr.isel(lon=slice(72,144))
    pr = xr.concat([prw,pre],'lon')
    #
    return pr

def load3D_CMAP():
    ds_in = xr.open_dataset('~/Data/satellite/CMAP/' +
                            'precip.mon.mean.std.nc')
    pr = ds_in.precip
    # Sort out the longitude array order in GRACE data. 
    pr = pr.assign_coords(lon=(((pr.lon + 180) % 360) - 180))
    pre = pr.isel(lon=slice(0,72))
    prw = pr.isel(lon=slice(72,144))
    pr = xr.concat([prw,pre],'lon')
    #
    return pr

def load3D_ERA5():
    ds_in = xr.open_mfdataset(os.path.expanduser('~') +
                              '/Data/reanalysis/ERA5/' +
                              'era5_moda_sfc_*.nc')
    # Specify spatial subset.
    lat_min = -25.0
    lat_max = 10.0 
    lon_min = 270.0
    lon_max = 320.0
    # Read data.
    pr = ds_in.tp.loc[dict(latitude=slice(lat_max,lat_min),
                           longitude=slice(lon_min,lon_max))]
    # Change longitude coordinates.                      
    # Data don't wrap round earth so can just subtract 360.
    pr = pr.assign_coords(longitude=(pr.longitude - 360))
    # Change units (from m/day to mm/day)
    pr = 1000.0*pr
    pr.attrs['units'] = 'mm day-1'
    #
    return pr

def load3D_MERRA2(pr_type):
    ####
    # pr_type defines whether to get 'raw' or 'biascorr' precip.
    ####
    ds_in = xr.open_mfdataset(os.path.expanduser('~') +
                              '/Data/reanalysis/MERRA2/Amazon/' +
                              'MERRA2_[234]00.tavgM_2d_flx_Nx.*.nc4.nc')
    # Specify spatial subset.
    lat_min = -25.0
    lat_max = 10.0
    lon_min = -90.0
    lon_max = -40.0
    # Read data.
    if pr_type == 'biascorr':
        pr = ds_in.PRECTOTCORR.loc[dict(lat=slice(lat_min,lat_max),
                                        lon=slice(lon_min,lon_max))]
    elif pr_type == 'raw':
        pr = ds_in.PRECTOT.loc[dict(lat=slice(lat_min,lat_max),
                                    lon=slice(lon_min,lon_max))]
    else:
        sys.exit('MERRA2 pr_type not valid: must be \'raw\' or \'biascorr\'')
    # Convert units
    pr = pr*24.0*60.0*60.0
    pr.attrs['units'] = 'kg m-2 day-1'
    #
    return pr

def load3D_CHIRPS():
    ds_in = xr.open_dataset('~/Data/satellite/CHIRPS/' +
                            'chirps-v2.0.monthly.nc')
    # Specify spatial subset.
    lat_min = -25.0
    lat_max = 10.0
    lon_min = -90.0
    lon_max = -40.0
    # Read data.
    pr = ds_in.precip.sel(latitude=slice(lat_min,lat_max),
                          longitude=slice(lon_min,lon_max))
    # Convert units.
    for m in pr.time:
        pr.loc[dict(time=m)] = pr.loc[dict(time=m)]/\
            calendar.monthrange(m.dt.year.data, m.dt.month.data)[1]
    pr.attrs['units'] = 'mm/day'
    #
    return pr

    
    
    
###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    main()
    
    
