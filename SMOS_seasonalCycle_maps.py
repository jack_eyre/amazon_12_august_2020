"""
Plot maps of SMOS mean salinity for months/seasons to see 
effects of currents and magnitude of variability. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import xesmf as xe
import iris
import datetime
from iris.time import PartialDateTime
import cf_units
import glob
import scipy.stats
import os
import sys
import subprocess
# For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

# Function for git commit hash:
def get_git_revision_info():
    
    import sys
    import subprocess
    import os
    
    # Get info.
    filename = os.path.basename(__file__)
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    clean =  subprocess.check_output(['git', 'diff',
                                      '--name-only',
                                      filename]).strip()
    
    # Change string format if python version 3.x.
    if sys.version_info[0] >= 3:
        #filename = filename.decode('utf-8')
        hash = hash.decode('utf-8')
        clean =  clean.decode('utf-8')
    
    # Put return string together.    
    info = filename + '          '
    if (clean == ''):
        info = info + 'Up to date. Last commit: ' + hash
    else:
        info = info + 'Untracked changes since last commit: ' + hash
    
    return [info, hash, clean]
    
###########################################


###### Load data. #################################################

# Specify file locations.
sss_filename = '~/Data/satellite/SMOS/SMOS_1x1deg_timeseries.nc'

plot_dir = '/Users/jameseyre/Documents/plots/AmazonCongo/'

# Specify time period for comparison.
st_time = np.datetime64('2010-02-01')
end_time = np.datetime64('2017-12-31')


##### Read data. ###########################################

print('Reading data...')

ds_sss = xr.open_dataset(sss_filename)

sss = ds_sss.SSS.load()


###### Calculate climatology. ###############################################

sss_clim = sss.loc[dict(time=slice(st_time, end_time))].groupby('time.month').mean('time')

###### Plot  maps. ###############################################

print('Plotting...')

# 2d lat and lon arrays for plotting.
dlatm, dlonm = np.meshgrid(sss_clim.latitude,sss_clim.longitude,indexing='ij')

# Define map projection.
projection = ccrs.PlateCarree()
axes_class = (GeoAxes,
              dict(map_projection=projection))

# Open figure.
fig = plt.figure(figsize=(12, 6))

# Define number of rows and columns in plot.
n_r_ax = 3
n_c_ax = 4

# Define axes of different panels.
axgr = AxesGrid(fig, 111, axes_class=axes_class,
                nrows_ncols=(n_r_ax, n_c_ax),
                axes_pad=(0.1,0.3),
                cbar_location='bottom',
                cbar_mode='single',
                cbar_pad=0.2,
                cbar_size='3%',
                direction='row',
                label_mode='')  # note the empty label_mode

# Loop over different variables and plot maps.
titles = ['Jan         SMOS', 'Feb', 'Mar',
          'Apr', 'May', 'Jun',
          'Jul', 'Aug', 'Sep',
          'Oct', 'Nov', 'Dec']
max_vals = [37.0]
min_vals = [32.0]
cbar_labels = ['PSU']
for i, ax in enumerate(axgr):
    ax.set_extent([-70, 0, -20, 25], projection)
    ax.coastlines()
    ax.add_feature(cfeature.RIVERS,edgecolor='dodgerblue')
    ax.add_feature(cfeature.LAKES,edgecolor='dodgerblue',
                   facecolor='dodgerblue')
    ax.add_feature(cfeature.LAND, facecolor='lightgray')
    ax.set_title(titles[i], loc='left')
    ax.set_xticks(np.array([-45, -15]), crs=projection)
    ax.set_yticks(np.array([-15, 0, 15]), crs=projection)
    lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                       degree_symbol='')
    lat_formatter = LatitudeFormatter(degree_symbol='')
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    p = ax.pcolormesh(dlonm, dlatm, sss_clim[i,:,:],
                      transform=projection,
                      vmin=min_vals[0], vmax=max_vals[0],
                      cmap='RdBu_r')
    #
    # Add salinity regions.
    ax.plot([-51.,-47.5,-47.5,-50.0],[3.0,3.0,0.0,0.0],
            color='blue', linewidth=1.0, linestyle='solid')
        
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)

# Draw the color bar (shared by all plots).
cbar = axgr.cbar_axes[0].colorbar(p)
cbar.set_label_text(cbar_labels[0])

# Add info to footer.
[txtl, hash, clean] = get_git_revision_info()
anchored_text = AnchoredText(txtl, loc=3,borderpad=0,frameon=False,
                             prop=dict(size=5),
                             bbox_to_anchor=(0., 0.),
                             bbox_transform=fig.transFigure) #ax.transAxes)
axgr[4].add_artist(anchored_text)

# Display the plot
#plt.show()

# Save the figure.
plt.savefig(plot_dir +
            os.path.splitext(os.path.basename(__file__))[0] +
            '.pdf',
            format='pdf')
#           format='png', dpi=900
#####
