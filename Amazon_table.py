"""
Prints text to make a LaTeX table of Amazon water cycle summary stats.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################


################################################################################
def main():
    
    ##### Read hydrology data. ###########################################
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    
    # Get factor to correct Obidos streamflow to mouth.
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    ob_to_mouth = ds_DT.ratio_m2s[0].data
    
    # Scale up Obidos streamflow to river mouth.
    ro_DT = ro_ob.copy()
    ro_DT = ro_DT*ob_to_mouth
    
    # Get water budget variables.
    # All in m3 s-1.
    all_vars = get_budget_vars('Amazon')
    #
    # Get precip and evap.
    pr_GPCP = all_vars['pr_GPCP']
    pr_ERA5 = all_vars['pr_ERA5']
    pr_GPCP = pr_GPCP.sel(time=slice(min(pr_ERA5.time),
                                     max(pr_ERA5.time)))
    evap = all_vars['evap_ERA5']
    
    # Calculate streamflow estimate.
    PE_GPCP_ERA5_JPL = pr_GPCP - \
                       evap - \
                       all_vars['dSdt_m_JPL']
    PE_ERA5_ERA5_JPL = pr_ERA5 - \
                       evap - \
                       all_vars['dSdt_m_JPL']
    
    # Subset Obidos to same period as other streamflow estimates.
    ro_DT = ro_DT.sel(time=slice(min(PE_GPCP_ERA5_JPL.time),
                                 max(PE_GPCP_ERA5_JPL.time)))
    
    
    
    ##### Read salinity data. ###########################################
    
    # Define region to average over.
    sal_region = np.array([0.0,5.0,-52.5,-47.5])
    
    # Open and read file.
    sal_filename_wildcard = '/Users/jameseyre/Data/satellite/SMOS/' + \
                            'SMOS_L3_DEBIAS_LOCEAN_AD_*' + \
                            '_EASE_09d_25km_v03_monthly.nc'
    ds_sal = xr.open_mfdataset(sal_filename_wildcard, concat_dim='time')
    # Don't get the first (incomplete) month.
    sss = ds_sal.SSS.sel(time=slice('2010-02-01','2017-12-31'),
                         lat=slice(sal_region[0],sal_region[1]),
                         lon=slice(sal_region[2],sal_region[3]))\
                         .mean(['lat','lon']).load()

    
    ##### Post-processing. ###########################################
    
    print('Post-processing...')
    
    # Calculate mean annual cycles.
    pr_GPCP_ann_cy = pr_GPCP.groupby('time.month').mean('time')
    pr_ERA5_ann_cy = pr_ERA5.groupby('time.month').mean('time')
    evap_ann_cy = evap.groupby('time.month').mean('time')
    ro_DT_ann_cy = ro_DT.groupby('time.month').mean('time')
    PE_GPCP_ERA5_JPL_ann_cy = PE_GPCP_ERA5_JPL.\
                              groupby('time.month').mean('time')
    PE_ERA5_ERA5_JPL_ann_cy = PE_ERA5_ERA5_JPL.\
                              groupby('time.month').mean('time')
    sss_ann_cy = sss.groupby('time.month').mean('time')
    #
    # Define month names.
    monthnames = ['Jan','Feb','Mar','Apr','May','Jun',
                  'Jul','Aug','Sep','Oct','Nov','Dec']
    # Put data in data dictionary.
    data_dict = {'pr_GPCP':{},'pr_ERA5':{},'evap':{},
                 'PE_GPCP_ERA5_JPL':{},'PE_ERA5_ERA5_JPL':{},'ro_DT':{},
                 'sss':{}}
    data_dict['pr_GPCP']['ann_mean'] = '% .3g' % pr_GPCP_ann_cy.mean()
    data_dict['pr_ERA5']['ann_mean'] = '% .3g' % pr_ERA5_ann_cy.mean()
    data_dict['evap']['ann_mean'] = '% .3g' % evap_ann_cy.mean()
    data_dict['ro_DT']['ann_mean'] = '% .3g' % ro_DT_ann_cy.mean()
    data_dict['PE_GPCP_ERA5_JPL']['ann_mean'] = '% .3g' % \
                                                PE_GPCP_ERA5_JPL_ann_cy.mean()
    data_dict['PE_ERA5_ERA5_JPL']['ann_mean'] = '% .3g' % \
                                                PE_ERA5_ERA5_JPL_ann_cy.mean()
    data_dict['pr_GPCP']['mon_max'] = monthnames[np.argmax(pr_GPCP_ann_cy.data)]
    data_dict['pr_ERA5']['mon_max'] = monthnames[np.argmax(pr_ERA5_ann_cy.data)]
    data_dict['evap']['mon_max'] = monthnames[np.argmax(evap_ann_cy.data)]
    data_dict['ro_DT']['mon_max'] = monthnames[np.argmax(ro_DT_ann_cy.data)]
    data_dict['PE_GPCP_ERA5_JPL']['mon_max'] = monthnames[
        np.argmax(PE_GPCP_ERA5_JPL_ann_cy.data)]
    data_dict['PE_ERA5_ERA5_JPL']['mon_max'] = monthnames[
        np.argmax(PE_ERA5_ERA5_JPL_ann_cy.data)]
    data_dict['pr_GPCP']['mon_min'] = monthnames[np.argmin(pr_GPCP_ann_cy.data)]
    data_dict['pr_ERA5']['mon_min'] = monthnames[np.argmin(pr_ERA5_ann_cy.data)]
    data_dict['evap']['mon_min'] = monthnames[np.argmin(evap_ann_cy.data)]
    data_dict['ro_DT']['mon_min'] = monthnames[np.argmin(ro_DT_ann_cy.data)]
    data_dict['PE_GPCP_ERA5_JPL']['mon_min'] = monthnames[
        np.argmin(PE_GPCP_ERA5_JPL_ann_cy.data)]
    data_dict['PE_ERA5_ERA5_JPL']['mon_min'] = monthnames[
        np.argmin(PE_ERA5_ERA5_JPL_ann_cy.data)]
    data_dict['sss']['mon_max'] = monthnames[np.argmax(sss_ann_cy.data)]
    data_dict['sss']['mon_min'] = monthnames[np.argmin(sss_ann_cy.data)]
    data_dict['sss']['region'] = '%d' % sal_region[0] + '$-$' + \
                                 '%d' % sal_region[1] + '$^{\circ}$N, ' + \
                                 '% .1f' % -sal_region[3] + '$-$' + \
                                 '% .1f' % -sal_region[2] + '$^{\circ}$W'
    
    # Calculate correlations.
    #corr_stats(pr_GPCP, 'pr_GPCP', evap, 'evap_ERA5',
    #           freq='monthly', max_lag=12, anomaly=False)
    #corr_stats(pr_GPCP, 'pr_GPCP',
    #           PE_GPCP_ERA5_JPL, 'PE_GPCP_ERA5_JPL',
    #           freq='monthly', max_lag=12, anomaly=False)
    #corr_stats(pr_GPCP, 'pr_GPCP', sss, 'sss_SMOS',
    #           freq='monthly', max_lag=12, anomaly=False)
    #corr_stats(evap, 'evap_ERA5',
    #           PE_GPCP_ERA5_JPL, 'PE_GPCP_ERA5_JPL',
    #           freq='monthly', max_lag=12, anomaly=False)
    #corr_stats(evap, 'evap_ERA5', sss, 'sss_SMOS',
    #           freq='monthly', max_lag=12, anomaly=False)
    #corr_stats(PE_GPCP_ERA5_JPL, 'PE_GPCP_ERA5_JPL',
    #           sss, 'sss_SMOS',
    #           freq='monthly', max_lag=12, anomaly=False)
    
    ##### Print results. ###########################################
    
    print('Plotting...')
    table_output(data_dict)
    
    return data_dict


################################################################################

################################################################################
# Printing function.
def table_output(data_dict):
    #
    # Print header stuff.
    print('\\begin{table}[ht]')
    print('\\caption{INSERT CAPTION HERE}')
    print('\\centering')
    print('\\begin{tabular}{p{6cm} c c c}')
    #
    # Print water budget means
    #print('\\textbf{Water budget terms} & & & \\\\')
    #print('\\hline')
    print('  & Annual mean & Month of annual  & Month of annual     \\\\')
    print('  & (m$^3$ s$^{-1}$)  &  cycle maximum &  cycle minimum     \\\\')
    print('\\hline')
    print('Precipitation (ERA5) & ' + data_dict['pr_ERA5']['ann_mean'] +
          ' & ' + data_dict['pr_ERA5']['mon_max'] +
          ' & ' + data_dict['pr_ERA5']['mon_min'] + ' \\\\')
    print('Precipitation (GPCP) & ' + data_dict['pr_GPCP']['ann_mean'] +
          ' & ' + data_dict['pr_GPCP']['mon_max'] +
          ' & ' + data_dict['pr_GPCP']['mon_min'] + ' \\\\')
    print('Evaporation (ERA5) & ' + data_dict['evap']['ann_mean'] +
          ' & ' + data_dict['evap']['mon_max'] +
          ' & ' + data_dict['evap']['mon_min'] + ' \\\\')
    print('Discharge (PE\_ERA5\_ERA5\_JPL) & ' +
          data_dict['PE_ERA5_ERA5_JPL']['ann_mean'] +
          ' & ' + data_dict['PE_ERA5_ERA5_JPL']['mon_max'] +
          ' & ' + data_dict['PE_ERA5_ERA5_JPL']['mon_min'] + ' \\\\')
    print('Discharge (PE\_GPCP\_ERA5\_JPL) & ' +
          data_dict['PE_GPCP_ERA5_JPL']['ann_mean'] +
          ' & ' + data_dict['PE_GPCP_ERA5_JPL']['mon_max'] +
          ' & ' + data_dict['PE_GPCP_ERA5_JPL']['mon_min'] + ' \\\\')
    print('Discharge (1.25$\\times$Obidos) & ' +
          data_dict['ro_DT']['ann_mean'] +
          ' & ' + data_dict['ro_DT']['mon_max'] +
          ' & ' + data_dict['ro_DT']['mon_min'] + ' \\\\')
    print('Salinity (SMOS; ' + data_dict['sss']['region'] + ') & ' +
          '-- & ' + data_dict['sss']['mon_max'] +
          ' & ' + data_dict['sss']['mon_min'] + ' \\\\')
    #print('\\hline')
    #
    # Print  correlations
    #print(' \\multicolumn{4}{l}{\\textbf{Maximum ' +
    #      'correlations and lags (months)}} \\\\')
    #print('\\hline')
    #print(' & Precipitation & Evaporation & Discharge \\\\')
    #print(' &  (GPCP) &  (ERA5) &  (PE\_GPCP\_ERA5\_JPL) \\\\')
    #print('\\hline')
    #print(' Precipitation (GPCP) & & & \\\\')
    #print(' Evaporation (ERA5) & & & \\\\')
    #print(' Discharge (PE\_GPCP\_ERA5\_JPL) & & & \\\\')
    #print(' Salinity (SMOS) & & & \\\\')
    print('\\end{tabular}')
    print('\\label{Summary_table}')
    print('\\end{table}')

################################################################################

def corr_stats(x1, x1_name,
               x2, x2_name,
               freq="monthly", max_lag=12, anomaly=False):
    ###############################################################
    # Returns dictionary of statistics of correlation statistics.
    # Input:
    #     streamflow (or could be precipitation for example)
    #     salinity
    # Optional arguments:
    #     freq: averaging frequency of time series.
    #           Currently "monthly" is the default and
    #           only accepted value.
    #     max_lag:  maximum time lag (streamflow leads salinity)
    #               to calculate correlations for. Defaults to 2.
    #
    # Output:
    # Dictionary containing several correlation coefficients:
    #     abs_lag0:    correlation of absolute values at zero lag
    #     anom_lag0:   correlation of anomalies at zero lag
    #     abs_lag1:    correlation of absolute values at 1-month lag
    #     anom_lag1:   correlation of anomalies at 1-month lag
    #     etc....
    # NOTE: the lags are streamflow leading salinity
    # (e.g., pairs of data are (streamflow_Jan, salinity_Feb). 
    ###############################################################
    
    # Initialize output dictionary.
    stats = {}
    
    # Check for valid frequency input.
    if freq != "monthly":
        print("Invalid option for freq")
    
    else:
        
        # Calculate anomalies.
        x1_anom = x1.groupby('time.month') - \
                  x1.groupby('time.month').mean('time')
        x2_anom = x2.groupby('time.month') - \
                  x2.groupby('time.month').mean('time')
        
        # Calculate time overlap.
        st_time = max([min(x1.time), min(x2.time)])
        end_time = min([max(x1.time), max(x2.time)])
        
        # Get subsets of data.
        if anomaly:
            v1 = x1_anom.sel(time=slice(st_time,end_time)).data
            v2 = x2_anom.sel(time=slice(st_time,end_time)).data
        else:
            v1 = x1.sel(time=slice(st_time,end_time)).data
            v2 = x2.sel(time=slice(st_time,end_time)).data
            #
        # Mask out missing values.
        mask = (~np.isnan(v1) & ~np.isnan(v2))
        v1 = v1[mask]
        v2 = v2[mask]
        
        # Calculate correlation coefficients.
        stats[x1_name + '_' + x2_name + 'lag0'] = np.corrcoef(v1, v2)[0,1]
        if max_lag > 0:
            for l in range(1,max_lag+1):     
                stats[x1_name + '_leads_' + x2_name +
                      '_lag' + str(l)] = np.corrcoef(v1[:-l],
                                                     v2[l:])[0,1]
                stats[x2_name + '_leads_' + x1_name +
                      '_lag' + str(l)] = np.corrcoef(v2[:-l],
                                                     v1[l:])[0,1]
        for k in stats.keys():
            print(k + ':   ' + str(stats[k]))
    
    # Return values.
    return(stats)

################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    pr_ann_cy = main()
