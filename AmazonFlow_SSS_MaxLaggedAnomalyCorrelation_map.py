"""
Plot map of maximum correlation (over different time lags) between 
streamflow and salinity anomalies. 
"""

###########################################
#### For data analysis:
import numpy as np
import xarray as xr
import xesmf as xe
import iris
import datetime
from iris.time import PartialDateTime
import cf_units
import glob
import scipy.stats
import os
import sys
import subprocess
#### For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
#### Routines from other files:
from Amazon_closure import *
################################################################################

# Main script.
def main():
    #
    ############################################
    # Read data.
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Fill gaps in streamflow (inserts nan for missing months)
    ro_ob = ro_ob.resample(time='1M').asfreq()
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    
    # Get all other variables.
    budget_Amazon = get_budget_vars('Amazon')
    
    # Optionally print MAR for each Obidos closure combination.
    print_closure_stats = False
    budget_Obidos = get_budget_vars('Obidos')
    resids_Obidos = resids_all(budget_Obidos, ro_ob)
    if print_closure_stats:
        for comb in resids_Obidos['monthly']:
            print(comb + ':     ' + str(resids_Obidos['monthly'][comb]['MAR']))
    
    # Construct alternative streamflow estimate.
    ro_budget = budget_Amazon['pr_GPCP'] - \
                budget_Amazon['evap_ERA5'] - \
                budget_Amazon['dSdt_m_JPL']
    
    # Get salinity data.
    sal_filename_wildcard = '/Users/jameseyre/Data/satellite/SMOS/' + \
                            'SMOS_L3_DEBIAS_LOCEAN_AD_*' + \
                            '_EASE_09d_25km_v03_monthly.nc'
    ds_sal = xr.open_mfdataset(sal_filename_wildcard, concat_dim='time')
    # Don't get the first (incomplete) month.
    sss = ds_sal.SSS.sel(time=slice('2010-02-01','2017-12-31'),
                         lat=slice(-30,30),
                         lon=slice(-90,0)).load()
    sss_anom = sss.groupby('time.month') - \
               sss.groupby('time.month').mean('time')
    
    # Mask everything to a common time.
    st_time = max([min(ro_budget.time),
                   min(ro_ob.time),
                   min(sss.time)])
    end_time = min([max(ro_budget.time),
                    max(ro_ob.time),
                    max(sss.time)])
    
    
    ############################################
    # Calculate correlations.
    print('Calculating correlations...')
    
    # Specify maximum lag to test.
    tau_max = 9
    
    # Define list of variables for which to calculate correlations.
    corr_vars = [ro_ob.groupby('time.month') - \
                 ro_ob.groupby('time.month').mean('time'),
                 ro_budget.groupby('time.month') - \
                 ro_budget.groupby('time.month').mean('time'),
                 budget_Amazon['pr_GPCP'].groupby('time.month') - \
                 budget_Amazon['pr_GPCP'].groupby('time.month').mean('time') ]
    corr_var_names = ['Obidos_streamGauge',
                      'streamflow_waterBudget',
                      'GPCP_precip']
    
    # Define variable to hold answers.
    corr_maps = xr.DataArray(np.empty(sss[0:len(corr_var_names)].shape),
                             coords=[corr_var_names, sss.lat, sss.lon],
                             dims=['variable','lat','lon'])
    corr_maps.data[:] = np.nan
    lag_maps = corr_maps.copy()
    
    # Loop over these variables and calculate correlations for each.
    for i,v in enumerate(corr_vars):
        print(corr_var_names[i])
        [corr_maps[i,:,:],lag_maps[i,:,:]] = \
            max_lagged_corr(v.sel(time=slice(st_time,end_time)),
                            sss_anom.sel(time=slice(st_time,end_time)),
                            tau_max)
    
    
    ############################################
    # Plot maps.
    print('Plotting...')
    
    plot_map(corr_maps, lag_maps)
    
    ############################################


############################################################################### 
### Map plotting routine.
def plot_map(corr, lag):
            
    # Get git info for this script.
    sys.path.append('/Users/jameseyre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    
    # 2d lat and lon arrays for plotting.
    dlatm, dlonm = np.meshgrid(corr.lat,corr.lon,indexing='ij')
    
    # Define color maps.
    cmap_r = plt.get_cmap('RdBu_r')
    cmap_r.set_bad(color='lightgrey')
    norm_r = colors.BoundaryNorm(np.arange(-1.0, 1.01, 0.2),
                                 ncolors=cmap_r.N)
    cmap_t = plt.get_cmap('BuPu')
    cmap_t.set_bad(color='lightgrey')
    norm_t = colors.BoundaryNorm(np.arange(-0.5, 9.51, 1.0),
                                 ncolors=cmap_t.N)
    
    # Define map projection.
    projection = ccrs.PlateCarree()
    axes_class = (GeoAxes, dict(map_projection=projection))
    
    # Open figure.
    fig = plt.figure(figsize=(9, 4.5))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(2, 3),
                    axes_pad=0.25,
                    cbar_location='right',
                    cbar_mode='edge',
                    cbar_pad=0.1,
                    cbar_size='3%',
                    direction='row',
                    label_mode='')  # note the empty label_mode
    
    # Loop over variables and plot maps.
    for i, ax in enumerate(axgr):
        ax.set_extent([-70, 0, -20, 25], projection)
        ax.coastlines()
        ax.add_feature(cfeature.RIVERS,edgecolor='dodgerblue')
        ax.add_feature(cfeature.LAKES,edgecolor='dodgerblue',
                       facecolor='dodgerblue')
        ax.add_feature(cfeature.LAND, facecolor='lightgray')
        ax.set_xticks(np.array([-60, -45, -30, -15]), crs=projection)
        ax.set_yticks(np.array([-15, 0, 15]), crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                           degree_symbol='')
        lat_formatter = LatitudeFormatter(degree_symbol='')
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        ax.tick_params(axis='both', bottom=True, top=True,
                       left=True, right=True)
        # Draw the plots.
        if i < 3:
            ax.set_title(str(corr.coords['variable'].data[i]))
            p_r = ax.pcolormesh(dlonm, dlatm, corr[i,:,:],
                                vmin=-1.0, vmax=1.0, 
                                transform=projection,
                                cmap=cmap_r, norm=norm_r)
        else:
            ax.set_title('')
            p_t = ax.pcolormesh(dlonm, dlatm, lag[i-3,:,:],
                                vmin=-0.5, vmax=9.5, 
                                transform=projection,
                                cmap=cmap_t, norm=norm_t)
    
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelleft=False, labelright=False)
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    
    # Draw the color bars (shared by rows of plots).
    axgr.cbar_axes[0].colorbar(p_r)
    axgr.cbar_axes[1].colorbar(p_t)
    axgr.cbar_axes[1].set_yticks(np.arange(0.0, 9.01, 1.0))
    axgr.cbar_axes[1].set_yticklabels(['0','','','3','','','6','','','9'])
    
    # Add subplot labels.
    subplot_labels = ['a','b','c','d','e','f']
    for i,label in enumerate(subplot_labels):
        axgr[i].text(-8.0, 20.0, label, ha='left', va='center',
                     transform=axgr[i].transData)
    
    # Tidy layout.
    fig.tight_layout(pad=5.0,h_pad=1.0,w_pad=1.0)
    
    # Add git info to figure.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    
    # Display the plot.
    #plt.show()
    # or save as PDF.
    fig.savefig('/Users/jameseyre/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.png',
                format='png')


###############################################################################
### Function to calculate the maximum correlation and lag at which it occurs.
def max_lagged_corr(runoff, sss, tau_max):
    
    # Define function to calculate correlation between one map of time
    # series and one single time series.
    #corr_map = np.vectorize(scipy.stats.pearsonr,signature='(n),(n)->(),()')
    
    # Transpose SSS array to put time coordinate last.
    sss_T = sss.copy().transpose('lat','lon','time')
    sss_mask = np.sum(np.isnan(sss_T.data), axis=2)
    
    # Define data array to hold answer.
    r_maps = xr.DataArray(np.empty([tau_max+1,
                                    sss.lat.shape[0],
                                    sss.lon.shape[0]]),
                          coords=[range(tau_max+1), sss.lat, sss.lon],
                          dims=['lag','lat','lon'])
    r_maps.data[:] = np.nan
    
    # Loop over lags.
    for tau in range(tau_max+1):
        print(str(tau))
        if (tau == 0):
            r_maps[0,:,:] = corr_map(sss_T[:,:,:], runoff[:])
        else:
            r_maps[tau,:,:] = corr_map(sss_T[:,:,tau:], runoff[:-tau])
    #
    # Calculate missing data masks (as nanargmax doesn't 
    # work for grid points with all NaNs).
    r_maps_mask = np.sum(np.isnan(r_maps),axis=0)
    r_maps_filled = np.where(r_maps_mask > 0, 9999.0, r_maps)
    
    # Calculate minimum and lag at which it occurs.
    rMin_filled = np.nanmin(r_maps_filled, axis=0)
    tau_at_rMin_filled = np.nanargmin(r_maps_filled, axis=0)
    
    # Reverse the masking.
    rMin = np.where(r_maps_mask > 0, np.nan, rMin_filled)
    tau_at_rMin = np.where(r_maps_mask > 0, np.nan, tau_at_rMin_filled)
    
    # Remove non-significant correlations.
    # Use most conservative number of degrees of freedom
    # (i.e., highest r_crit).
    dof = len(sss.time) - tau_max
    t_crit = scipy.stats.t.ppf(0.05,dof)
    r_crit = np.sqrt(((t_crit**2)/dof)/(((t_crit**2)/dof) + 1.0))
    r_sig = abs(rMin) < r_crit
    rMin = np.where(r_sig, np.nan, rMin)
    tau_at_rMin = np.where(r_sig, np.nan, tau_at_rMin)
    
    # Return output
    return([rMin, tau_at_rMin])


# Define function to calculate correlation between one map of time
# series and one single time series.
def corr_map(map3d, ts1d):
    # map3d has dimensions (lat, lon, time) in that order.
    # ts1d has dimension (time) only.
    # Both inputs are xarray data arrays.
    
    # Convert to numpy arrays.
    X = map3d.data
    y = ts1d.data
    
    # #####
    # Method below taken from:
    # https://waterprogramming.wordpress.com/2014/06/13/numpy-vectorized-correlation-coefficient/
    # #####
    # Calculate time means.
    Xm = np.reshape(np.nanmean(X, axis=2),
                    (X.shape[0], X.shape[1], 1))
    ym = np.nanmean(y)
    
    # Put together correlation coefficient numerator and denominator.
    r_num = np.nansum((X-Xm)*(y-ym),axis=2)
    r_den = np.sqrt(np.nansum((X-Xm)**2,axis=2)*np.nansum((y-ym)**2))
    
    # Calculate and return correlation coefficient.
    r = r_num/r_den
    return r


###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    main()
