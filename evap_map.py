"""
Plot map of annual mean evaporation from several data sets over the Amazon basin.
"""

###########################################
#### For data analysis:
import numpy as np
import xarray as xr
import datetime
import os
import sys
import subprocess
#### For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.colors as mcolors
from matplotlib import cm, gridspec, rcParams, colors
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import shapefile
import matplotlib.patches as patches
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
#### Routines from other files:
from Amazon_closure import *
################################################################################

# Main script.
def main():
    #
    ############################################
    # Read data.
    print('Reading data...')
    
    # Shape file locations.
    shp_dir = os.path.expanduser('~/Data/in-situ/hybam/')
    shp_file_Amazon = 'Amazon_catchment.shp'
    shp_file_Obidos = 'Obidos_catchment.shp'
    #
    # Read data using functions below. 
    evap_GLEAM = load3D_GLEAM().load()
    evap_Noah = load3D_Noah().load()
    evap_CLM = load3D_CLM().load()
    evap_ERA5 = load3D_ERA5().load()
    evap_MERRA2 = load3D_MERRA2().load()
    #
    # Put into dictionary.
    data_dict = {'GLEAM':{'timeseries':evap_GLEAM},
                 'CLM':{'timeseries':evap_CLM},
                 'Noah':{'timeseries':evap_Noah},
                 'ERA5':{'timeseries':evap_ERA5},
                 'MERRA2':{'timeseries':evap_MERRA2}}
    
    # Read the shapefiles - using routine defined below.
    ptchs_amazon = get_sf_patches(shp_dir + shp_file_Amazon)
    ptchs_obidos = get_sf_patches(shp_dir + shp_file_Obidos)
    
    ############################################
    # Average data.
    print('Processing data...')
    
    # Specify averaging period.
    st_yr = 2000
    end_yr = 2018
    
    # Get annual average of this period.
    for k in data_dict.keys():
        ts = data_dict[k]['timeseries'].\
                 loc[dict(time=slice(str(st_yr) + '-01-01',
                                     str(end_yr) + '-12-31'))]
        data_dict[k]['ann_mean'] = ts.groupby('time.month').\
                                   mean('time').mean('month')
        data_dict[k]['st_time'] = (str(min(ts.time).dt.year.data) + '/' +
                                   str(min(ts.time).dt.month.data))
        data_dict[k]['end_time'] = (str(max(ts.time).dt.year.data) + '/' +
                                    str(max(ts.time).dt.month.data))
    
    ############################################
    # Plot map.
    print('Plotting...')
    #
    #
    # Define color map.
    cmap_b = plt.get_cmap('Greens')
    norm_b = colors.BoundaryNorm(np.arange(0.0, 5.01, 0.5),
                                 ncolors=cmap_b.N)
    #
    # Define map projection.
    projection = ccrs.PlateCarree()
    #
    # Set up axes and figure.
    axes_class = (GeoAxes,
                  dict(map_projection=projection))
    fig = plt.figure(figsize=(8, 8))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(3, 2),ngrids=5,
                    axes_pad=0.25,
                    cbar_location='bottom',
                    cbar_mode='single',
                    cbar_pad=0.1,
                    cbar_size='2%',
                    label_mode='')  # note the empty label_mode
    #
    # Plot each data set.
    for i,k in enumerate(data_dict.keys()):
        print(k)
        dlatm,dlonm = np.meshgrid(data_dict[k]['ann_mean'].lat,
                                  data_dict[k]['ann_mean'].lon,
                                  indexing='ij')
        p = axgr[i].pcolormesh(dlonm, dlatm, data_dict[k]['ann_mean'],
                               vmin=0.0, vmax=5.0, 
                               transform=projection,
                               cmap=cmap_b, norm=norm_b)
        axgr[i].set_title(k,loc='left')
        axgr[i].set_title(str(data_dict[k]['st_time']) + '-' +\
                          str(data_dict[k]['end_time']),
                          loc='right',fontsize=8)
    #
    # Loop over plots and set common details.
    for i, ax in enumerate(axgr[0:5]):
        # Set map spatial extent.                        
        ax.set_extent([-85,-45, -25, 10], projection)     
        # Set geographic features.                       
        ax.coastlines()                                  
        # Plot Obidos location.                          
        ax.plot(-55.0,-1.9,color='red',marker='o', transform=projection)
        # Set gridline and label details.                
        gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=False,
                          linewidth=2, color='gray', alpha=0.5, linestyle='--')
        if i in [0,2,4]:
            gl.ylabels_left = True
        if i in [4,5]:
            gl.xlabels_bottom = True                         
        gl.ylocator = mticker.FixedLocator(np.arange(-90,91,10))
        gl.xlocator = mticker.FixedLocator(np.arange(-180,181,10))
        #
        # Add the shapefiles.                            
        ax.add_collection(PatchCollection(ptchs_amazon,  
                                          facecolor=mcolors.colorConverter.
                                          to_rgba('white', alpha=0.0),
                                          edgecolor=mcolors.colorConverter.
                                          to_rgba('black', alpha=1.0),
                                          linewidths=1.5))
        ax.add_collection(PatchCollection(ptchs_obidos,  
                                          facecolor=mcolors.colorConverter.
                                          to_rgba('white', alpha=0.0),
                                          edgecolor=mcolors.colorConverter.
                                          to_rgba('red', alpha=0.8),
                                          linewidths=.5)) 
    #
    # Add legend.
    cbar = axgr.cbar_axes[0].colorbar(p)
    cbar.set_label_text(r'$mm\ day^{-1}$')
    #
    # Get git info for this script.
    sys.path.append(os.path.expanduser('~/Documents/code/'))
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    # Add git info to figure.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot
    #plt.show()
    #
    # Save the figure.
    fig.savefig(os.path.expanduser('~/Documents/plots/AmazonCongo/') +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.png',
                format='png')
    #
    return


################################################################################
# Function to get shapefile patches.
def get_sf_patches(filename):
    # Routine taken from https://stackoverflow.com/questions/15968762/shapefile-and-matplotlib-plot-polygon-collection-of-shapefile-coordinates
    sf = shapefile.Reader(filename)
    recs    = sf.records()
    shapes  = sf.shapes()
    Nshp    = len(shapes)
    cns     = []
    for nshp in range(Nshp):
        cns.append(recs[nshp][1])
    cns = np.array(cns)
    for nshp in range(Nshp):
        ptchs   = []
        pts     = np.array(shapes[nshp].points)
        prt     = shapes[nshp].parts
        par     = list(prt) + [pts.shape[0]]
        for pij in range(len(prt)):
            ptchs.append(Polygon(pts[par[pij]:par[pij+1]]))
    return ptchs

################################################################################
#### Data loading routines.

def load3D_Noah():
    ds_in = xr.open_mfdataset(os.path.expanduser('~') +
                              '/Data/reanalysis/GLDAS/Noah_v2.1/' +
                              'GLDAS_NOAH10_M.A*.021.nc4.SUB.nc4')
    # Specify spatial subset.
    lat_min = -25.0
    lat_max = 10.0
    lon_min = -90.0
    lon_max = -40.0
    # Read data.
    evap = ds_in.Evap_tavg.loc[dict(lat=slice(lat_min,lat_max),
                                    lon=slice(lon_min,lon_max))]
    # Convert units.
    evap = evap*24.0*60.0*60.0
    evap.attrs['units'] = 'kg m-2 day-1'
    #
    return evap

def load3D_CLM():
    ds_in = xr.open_mfdataset(os.path.expanduser('~') +
                              '/Data/reanalysis/GLDAS/CLM_v2.0/' +
                              'GLDAS_CLM10_M.A*.001.grb.SUB.nc4')
    # Specify spatial subset.
    lat_min = -25.0
    lat_max = 10.0
    lon_min = -90.0
    lon_max = -40.0
    # Read data.
    evap = ds_in.Evap.loc[dict(lat=slice(lat_min,lat_max),
                               lon=slice(lon_min,lon_max))]
    # Convert units.
    evap = evap*24.0*60.0*60.0
    evap.attrs['units'] = 'kg m-2 day-1'
    #
    return evap

def load3D_GLEAM():
    ds_in = xr.open_dataset('~/Data/gridded-obs/GLEAM_v3.3/' +
                            'E_2003_2018_GLEAM_v3.3b_MO.nc')
    # Specify spatial subset.
    lat_min = -25.0
    lat_max = 10.0
    lon_min = -90.0
    lon_max = -40.0
    # Read data.
    evap = ds_in.E.loc[dict(lat=slice(lat_max,lat_min),
                            lon=slice(lon_min,lon_max))].load()
    # Reorder dimensions.
    evap = evap.transpose('time','lat','lon')
    #Convert units.
    for it in range(len(evap.time)):
        d_in_m = calendar.monthrange(evap.time[it].dt.year.data,
                                     evap.time[it].dt.month.data)
        evap[it,:,:] = evap[it,:,:]/d_in_m[1]             
    evap.attrs['units'] = 'mm day-1'
    #
    return evap

def load3D_ERA5():
    ds_in = xr.open_mfdataset(os.path.expanduser('~') +
                              '/Data/reanalysis/ERA5/' +
                              'era5_moda_sfc_*.nc')
    # Specify spatial subset.
    lat_min = -25.0
    lat_max = 10.0 
    lon_min = 270.0
    lon_max = 320.0
    # Read data.
    evap = ds_in.e.loc[dict(latitude=slice(lat_max,lat_min),
                            longitude=slice(lon_min,lon_max))]
    # Change longitude coordinates.                      
    # Data don't wrap round earth so can just subtract 360.
    evap = evap.assign_coords(longitude=(evap.longitude - 360))
    # Change units (from m/day to mm/day)
    evap = -1000.0*evap
    evap.attrs['units'] = 'mm day-1'
    # Change names of coordinates.
    evap = evap.rename({'latitude':'lat','longitude':'lon'})
    #
    return evap

def load3D_MERRA2():
    ds_in = xr.open_mfdataset(os.path.expanduser('~') +
                              '/Data/reanalysis/MERRA2/Amazon/' +
                              'MERRA2_[234]00.tavgM_2d_flx_Nx.*.nc4.nc')
    # Specify spatial subset.
    lat_min = -25.0
    lat_max = 10.0
    lon_min = -90.0
    lon_max = -40.0
    # Read data.
    evap = ds_in.EVAP.loc[dict(lat=slice(lat_min,lat_max),
                               lon=slice(lon_min,lon_max))]
    # Convert units
    evap = evap*24.0*60.0*60.0
    evap.attrs['units'] = 'kg m-2 day-1'
    #
    return evap


    
    
    
###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    main()
    
    
