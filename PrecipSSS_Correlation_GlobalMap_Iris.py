"""
Precipitation vs. salinity correlation
===============

Don't forget to load anaconda environment before running this:
$ source activate plotenv
and you might also need to change environment variables:
$ export DYLD_FALLBACK_LIBRARY_PATH=${HOME}/lib:/usr/local/lib:/lib:/usr/lib

Then it should work....
"""

###########################################
import iris
import numpy as np
import datetime
from iris.time import PartialDateTime
from iris.analysis import stats
import cf_units
import glob
import matplotlib.pyplot as plt
import matplotlib.cm as mpl_cm
import iris.quickplot as qplt
###########################################

###### Load precip. data. #################################################

data_dir = '/Users/jameseyre/Data/'

# Get precip data.
GPCP_filename = data_dir + 'satellite/GPCP.v2.3.precip.mon.mean.nc'
GPCP_period = iris.Constraint(time=lambda tt: datetime.datetime(2011,8,31,12,0) < tt < datetime.datetime(2015,5,31,12,0))
GPCP_var = 'Average Monthly Rate of Precipitation'
precip = iris.load_cube(GPCP_filename, GPCP_var & GPCP_period)

# Add bounds arrays.
lon_bnds = iris.load_cube(GPCP_filename, 'lon_bnds')
precip.coord('longitude').bounds = lon_bnds.data
lat_bnds = iris.load_cube(GPCP_filename, 'lat_bnds')
precip.coord('latitude').bounds = lat_bnds.data
time_bnds = iris.load_cube(GPCP_filename, 'time_bnds' & GPCP_period)
precip.coord('time').bounds = time_bnds.data

###### Load salinity data. #################################################

# Get salinity file list.
aq_fn_list = glob.glob(data_dir + 'satellite/aquarius/' +
                       '*_L3m_MO_SCI_V5.0_SSS_1deg.bz2.nc4')
aq_cubes = iris.load(aq_fn_list, 'Sea Surface Salinity')

# Define lat and lon coordinates.
lats =  iris.coords.DimCoord(points=np.arange(89.5,-90.0,-1.0),
                             standard_name='latitude',
                             long_name='latitude',
                             units='degrees_north')
lats.guess_bounds(bound_position=0.5)
lons = iris.coords.DimCoord(np.arange(-179.5,180.0,1.0),
                            standard_name='longitude',
                            long_name='longitude',
                            units='degrees_east')
lons.guess_bounds(bound_position=0.5)

# Loop over individual monthly cubes and add metadata.
for cube in aq_cubes:
    # Add spatial coordinates.
    cube.add_dim_coord(lats, 0)
    cube.add_dim_coord(lons, 1)
    
    # Get time info from attributes.
    t1 = cube.attributes['H5_GLOBAL.time_coverage_start']
    t2 = cube.attributes['H5_GLOBAL.time_coverage_end']
    
    # Convert to datetime objects.
    dt1 = datetime.datetime(int(t1[0:4]),
                            int(t1[5:7]),
                            int(t1[8:10]),
                            int(t1[11:13]),
                            int(t1[14:16]),
                            round(float(t1[17:23]))%60)
    dt2 = datetime.datetime(int(t2[0:4]),
                            int(t2[5:7]),
                            int(t2[8:10]),
                            int(t2[11:13]),
                            int(t2[14:16]),
                            round(float(t2[17:23]))%60)
    dtmean = dt1 + (dt2-dt1)/2
    
    # Add as scalar coordinates.
    time = iris.coords.AuxCoord(dtmean,
                                standard_name='time',
                                long_name='time_in_datetime_format',
                                bounds=np.array([dt1,dt2]))
    cube.add_aux_coord(time)
    
    # Get rid of mis-matched attributes.
    diff_atts = ['H5_GLOBAL.product_name', 'H5_GLOBAL.date_created', 'H5_GLOBAL.history', 'H5_GLOBAL.time_coverage_start', 'H5_GLOBAL.time_coverage_end', 'H5_GLOBAL.start_orbit_number', 'H5_GLOBAL.end_orbit_number', 'H5_GLOBAL.data_bins', 'H5_GLOBAL.data_minimum', 'H5_GLOBAL.data_maximum', 'H5_GLOBAL._lastModified', 'H5_GLOBAL.id', 'H5_GLOBAL.source']
    for att in diff_atts:
        del cube.attributes[att]

# Merge cubes together to form a single salinity cube.
aq_salinity = aq_cubes.merge_cube()

# Add time dim_coord (and remove time aux_coord).
dt_all = aq_salinity.coord('time')
t_units = 'hours since 2011-08-01 00:00:00'
t_all = cf_units.date2num(dt_all.points, t_units, cf_units.CALENDAR_STANDARD)
time = iris.coords.DimCoord(t_all,standard_name='time',long_name='time',units=t_units)
aq_salinity.remove_coord('time')
aq_salinity.add_dim_coord(time,0)


###### Regrid salinity data #################################################
#      (to precip grid because
#       it is coarser.)

rg_scheme = iris.analysis.AreaWeighted(mdtol=0.5)
aq_precip_grid = aq_salinity.extract(GPCP_period).regrid(precip, rg_scheme)

# Replace time coordinates so that they match in calculations below.
# This is a little dangerous - make sure that they really are the
# same months!!!
aq_precip_grid.remove_coord('time')
aq_precip_grid.add_dim_coord(precip.coord('time'),0)


###### Calculate correlation #################################################

r_map = iris.analysis.stats.pearsonr(aq_precip_grid,
                                     precip,
                                     corr_coords='time',
                                     weights=None,
                                     mdtol=0.0,
                                     common_mask=True)

r_map.long_name = 'Correlation(precip,SSS)'

# Plot quick map.
brewer_cmap = mpl_cm.get_cmap('brewer_RdBu_11')
contour = qplt.contourf(r_map, brewer_cmap.N, cmap=brewer_cmap)
# Add coastlines to the map created by contour.
plt.gca().coastlines()
#plt.show()
plt.savefig('/Users/jameseyre/Documents/plots/AmazonCongo/PrecipSSS_Correlation_GlobalMap.pdf')
plt.savefig('/Users/jameseyre/Documents/plots/AmazonCongo/PrecipSSS_Correlation_GlobalMap.png', dpi=500)


###### Calculate anomalies #################################################

