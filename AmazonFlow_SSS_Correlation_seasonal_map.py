"""
Plot map of seasonal correlation between streamflow and salinity anomalies. 
"""

###########################################
#### For data analysis:
import numpy as np
import xarray as xr
import xesmf as xe
import iris
import datetime
from iris.time import PartialDateTime
import cf_units
import glob
import scipy.stats
import os
import sys
import subprocess
#### For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams, colors
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
import string
#### Routines from other files:
from Amazon_closure import *
################################################################################

# Main script.
def main():
    #
    ############################################
    # Read data.
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Fill gaps in streamflow (inserts nan for missing months)
    ro_ob = ro_ob.resample(time='1M').asfreq()
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    Obidos = seasonal_mean(ro_ob)
    
    # Get all other variables.
    budget_Amazon = get_budget_vars('Amazon')
    budget_Obidos = get_budget_vars('Obidos')
    
    # Optionally print MAR for each Obidos closure combination.
    print_closure_stats = False
    if print_closure_stats:
        resids_Obidos = resids_all(budget_Obidos, ro_ob)
        for comb in resids_Obidos['monthly']:
            print(comb + ':     ' + str(resids_Obidos['monthly'][comb]['MAR']))
    
    # Construct alternative streamflow estimates.
    PE_GPCP_ERA5_JPL = seasonal_mean(budget_Amazon['pr_GPCP']) - \
                       seasonal_mean(budget_Amazon['evap_ERA5']) - \
                       budget_Amazon['dSdt_s_JPL']
    PE_ERA5_ERA5_JPL = seasonal_mean(budget_Amazon['pr_ERA5']) - \
                       seasonal_mean(budget_Amazon['evap_ERA5']) - \
                       budget_Amazon['dSdt_s_JPL']
    UG_GPCP_ERA5_JPL = Obidos + PE_GPCP_ERA5_JPL - \
                       (seasonal_mean(budget_Obidos['pr_GPCP']) - \
                        seasonal_mean(budget_Obidos['evap_ERA5']) - \
                        budget_Obidos['dSdt_s_JPL'])
    pr_GPCP = seasonal_mean(budget_Amazon['pr_GPCP'])
    
    # Get salinity data.
    sal_filename_wildcard = '/Users/jameseyre/Data/satellite/SMOS/' + \
                            'SMOS_L3_DEBIAS_LOCEAN_AD_*' + \
                            '_EASE_09d_25km_v03_monthly.nc'
    ds_sal = xr.open_mfdataset(sal_filename_wildcard, concat_dim='time')
    # Don't get the first (incomplete) month.
    sss = ds_sal.SSS.sel(time=slice('2010-02-01','2017-12-31'),
                         lat=slice(-30,35),
                         lon=slice(-90,0)).load()
    # Take seasonal means.
    sss = seasonal_mean(sss)
    # Transpose SSS array to put time coordinate last.
    sss_T = sss.copy().transpose('lat','lon','time')
    
    
    ############################################
    # Postprocess data.
    print('Postprocessing...')
    
    # Define list of variables for which to calculate correlations.
    corr_vars = [Obidos,
                 PE_GPCP_ERA5_JPL,
                 PE_ERA5_ERA5_JPL,
                 UG_GPCP_ERA5_JPL,
                 pr_GPCP ]
    corr_var_names = ['Obidos_stream_gauge',
                      'PE_GPCP_ERA5_JPL',
                      'PE_ERA5_ERA5_JPL',
                      'UG_GPCP_ERA5_JPL',
                      'GPCP_precip']
    
    # Mask everything to a common time.
    st_time = min(sss_T.time)
    end_time = max(sss_T.time)
    for v in corr_vars:
        st_time = max([st_time, min(v.time)])
        end_time = min([end_time, max(v.time)])
    sss_T = sss_T.sel(time=slice(st_time,end_time))
    for i,v in enumerate(corr_vars):
        corr_vars[i] = v.sel(time=slice(st_time,end_time))
    
    
    ############################################
    # Calculate correlations.
    print('Calculating correlations...')
    
    # Specify maximum lag to test (number of seasons).
    tau_max = 2
    
    # Define variable to hold answers.
    corr_maps = xr.DataArray(np.empty([tau_max+1, len(corr_var_names),
                                       sss.lat.size, sss.lon.size]),
                             coords=[range(tau_max+1),corr_var_names,
                                     sss.lat, sss.lon],
                             dims=['lag','variable','lat','lon'])
    corr_maps.data[:] = np.nan
    
    # Loop over time-lags and variables and calculate correlations for each.
    for i,v in enumerate(corr_vars):
        print(corr_var_names[i])
        for l in range(tau_max + 1):
            if l == 0:
                corr_maps.data[l,i,:,:] = \
                    corr_map(sss_T,v)
            else:
                corr_maps.data[l,i,:,:] = \
                    corr_map(sss_T[:,:,l:],v[:-l])
                
    
    ############################################
    # Plot maps.
    print('Plotting...')
    
    plot_map(corr_maps)
    #
    # For debugging.
    return corr_maps
    
    ############################################


############################################################################### 
### Map plotting routine.
def plot_map(corr):
            
    # Get git info for this script.
    sys.path.append('/Users/jameseyre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    
    # 2d lat and lon arrays for plotting.
    dlatm, dlonm = np.meshgrid(corr.lat,corr.lon,indexing='ij')
    
    # Define color maps.
    cmap_r = plt.get_cmap('RdBu_r')
    cmap_r.set_bad(color='lightgrey')
    norm_r = colors.BoundaryNorm(np.arange(-1.0, 1.01, 0.2),
                                 ncolors=cmap_r.N)
    
    # Define map projection.
    projection = ccrs.PlateCarree()
    axes_class = (GeoAxes, dict(map_projection=projection))
    
    # Open figure.
    fig = plt.figure(figsize=(12, 8))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(corr.shape[0], corr.shape[1]),
                    axes_pad=0.25,
                    cbar_location='right',
                    cbar_mode='single', #'edge',
                    cbar_pad=0.1,
                    cbar_size='3%',
                    direction='row',
                    label_mode='')  # note the empty label_mode
    #
    # Note that for direction='row' the order is:
    #     axgr[0], axgr[1], axgr[2],
    #     axgr[3], axgr[4], axgr[5]
    # I.e., fills up a single row first, before moving to the next row.
    
    # Loop over variables and plot maps.
    for i, ax in enumerate(axgr):
        ax.set_extent([-80, 0, -20, 35], projection)
        ax.coastlines()
        ax.add_feature(cfeature.RIVERS,edgecolor='dodgerblue')
        ax.add_feature(cfeature.LAKES,edgecolor='dodgerblue',
                       facecolor='dodgerblue')
        ax.add_feature(cfeature.LAND, facecolor='lightgray')
        ax.set_xticks(np.array([-75, -60, -45, -30, -15, 0]),
                      crs=projection)
        ax.set_yticks(np.array([-20, -10, 0, 10, 20, 30]),
                      crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                           degree_symbol='')
        lat_formatter = LatitudeFormatter(degree_symbol='')
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        ax.tick_params(axis='both', bottom=True, top=True,
                       left=True, right=True)
        if i < corr.shape[1]:
            ax.set_title(str(corr.coords['variable'].data[i]))
        else:
            ax.set_title('')
        # Draw the plots.
        p_r = ax.pcolormesh(dlonm, dlatm,
                            corr[int((i - i%corr.shape[1])/corr.shape[1]),
                                 i%corr.shape[1],:,:],
                            vmin=-1.0, vmax=1.0, 
                            transform=projection,
                            cmap=cmap_r, norm=norm_r)
        #
        # Add subplot labels.
        ax.text(-8.0, 20.0, list(string.ascii_lowercase)[i],
                ha='left', va='center',
                transform=ax.transData)
        ax.text(-65.0, -10.0, 'lag = ' + \
                str(int((i - i%corr.shape[1])/corr.shape[1])),
                ha='left', va='center',
                transform=ax.transData)
    
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelleft=False, labelright=False)
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    
    # Draw the color bars (shared by rows of plots).
    axgr.cbar_axes[0].colorbar(p_r)
    
    # Tidy layout.
    fig.tight_layout(pad=5.0,h_pad=1.0,w_pad=1.0)
    
    # Add git info to figure.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    
    # Display the plot.
    #plt.show()
    # or save as PDF.
    fig.savefig('/Users/jameseyre/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.png',
                format='png')


###############################################################################
### Correlation functions.

# Simple function to return seasonal mean with NaN value if not
# all months are present for each season.
# Input is an xarray data array with a time coordinate.
# Output is another xarray data array with time averaged to one
# data point per season.
def seasonal_mean(ts):
    ts_s = ts.resample(time='QS-DEC').mean(dim='time')
    ts_count = ts.resample(time='QS-DEC').count(dim='time')
    ts_s.data = np.where(ts_count < 3, np.nan, ts_s)
    return ts_s

# Define function to calculate correlation between one map of time
# series and one single time series.
def corr_map(map3d, ts1d):
    # map3d has dimensions (lat, lon, time) in that order.
    # ts1d has dimension (time) only.
    # Both inputs are xarray data arrays.
    
    # Convert to numpy arrays.
    X = map3d.data
    y = ts1d.data
    
    # #####
    # Method below taken from:
    # https://waterprogramming.wordpress.com/2014/06/13/numpy-vectorized-correlation-coefficient/
    # #####
    # Calculate time means.
    Xm = np.reshape(np.nanmean(X, axis=2),
                    (X.shape[0], X.shape[1], 1))
    ym = np.nanmean(y)
    
    # Put together correlation coefficient numerator and denominator.
    r_num = np.nansum((X-Xm)*(y-ym),axis=2)
    r_den = np.sqrt(np.nansum((X-Xm)**2,axis=2)*np.nansum((y-ym)**2))
    
    # Calculate and return correlation coefficient.
    r = r_num/r_den
    
    # Remove non-significant correlations.
    dof = len(y)
    t_crit = scipy.stats.t.ppf(0.05,dof)
    r_crit = np.sqrt(((t_crit**2)/dof)/(((t_crit**2)/dof) + 1.0))
    r_sig = abs(r) < r_crit
    r = np.where(r_sig, np.nan, r)
    
    return r


###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    corr_maps = main()
