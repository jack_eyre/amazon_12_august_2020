"""
Plot maps of bias, RMS and correlations between salinity datasets 
using anomlies from mean seasonal cycle. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import xesmf as xe
import iris
import datetime
from iris.time import PartialDateTime
import cf_units
import glob
import scipy.stats
import os
import sys
import subprocess
# For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib import cm, gridspec, rcParams
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
###########################################

# Function for git commit hash:
def get_git_revision_info():
    
    import sys
    import subprocess
    import os
    
    # Get info.
    filename = os.path.basename(__file__)
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    clean =  subprocess.check_output(['git', 'diff',
                                      '--name-only',
                                      filename]).strip()
    
    # Change string format if python version 3.x.
    if sys.version_info[0] >= 3:
        #filename = filename.decode('utf-8')
        hash = hash.decode('utf-8')
        clean =  clean.decode('utf-8')
    
    # Put return string together.    
    info = filename + '          '
    if (clean == ''):
        info = info + 'Up to date. Last commit: ' + hash
    else:
        info = info + 'Untracked changes since last commit: ' + hash
    
    return [info, hash, clean]
    
###########################################


###### Load data. #################################################

# Specify file locations.
SMOS_filename = '~/Data/satellite/SMOS/SMOS_1x1deg_timeseries.nc'
SMAP_filename = '~/Data/satellite/SMAP/SMAP_1x1deg_timeseries.nc'
aq_filename = '~/Data/satellite/aquarius/aquarius_1x1deg_timeseries.nc'

plot_dir = '/Users/jameseyre/Documents/plots/AmazonCongo/'

# Specify time period for comparison.
st_time_aq = np.datetime64('2011-09-01')
end_time_aq = np.datetime64('2015-05-31')
st_time_SMAP = np.datetime64('2015-04-01')
end_time_SMAP = np.datetime64('2017-12-31')


##### Read data. ###########################################

print('Reading data...')

ds_SMOS = xr.open_dataset(SMOS_filename)
ds_SMAP = xr.open_dataset(SMAP_filename)
ds_aq = xr.open_dataset(aq_filename)

sss_SMOS = ds_SMOS.SSS.load()
sss_SMAP = ds_SMAP.SSS.load()
sss_aq = ds_aq.SSS.load()


##### Convert to anomalies. ###########################################

# NOTE - SMAP and Aquarius climatologies come from their respective periods
#        noted above, and SMOS climatology is calculated for each of these
#        separately. This means differences (SMAP anomaly minus SMOS anomaly
#        and Aquarius anomaly minus SMOS anomaly) are necessarily zero (not
#        quite, due to round-off errors...).
# Thus bias and de-biased RSMD plots are not necessary.

sss_SMOS_a = sss_SMOS.groupby('time.month') - sss_SMOS.loc[dict(time=slice(st_time_aq, end_time_aq))].groupby('time.month').mean('time')

sss_SMOS_S = sss_SMOS.groupby('time.month') - sss_SMOS.loc[dict(time=slice(st_time_SMAP, end_time_SMAP))].groupby('time.month').mean('time')

sss_SMAP = sss_SMAP.groupby('time.month') - sss_SMAP.loc[dict(time=slice(st_time_SMAP, end_time_SMAP))].groupby('time.month').mean('time')

sss_aq = sss_aq.groupby('time.month') - sss_aq.loc[dict(time=slice(st_time_aq, end_time_aq))].groupby('time.month').mean('time')


###### Calculate bias and RMS. ###############################################

# RMSD (first calculate squared difference time series then take
# the time mean and square root).
SD_SMAP = sss_SMOS_S.loc[dict(time=slice(st_time_SMAP, end_time_SMAP))]
SD_SMAP.data =sss_SMAP.loc[dict(time=slice(st_time_SMAP,
                                           end_time_SMAP))].data - \
               sss_SMOS_S.loc[dict(time=slice(st_time_SMAP,
                                              end_time_SMAP))].data
SD_SMAP.data = np.square(SD_SMAP.data)
RMSD_SMAP = SD_SMAP.mean('time')
RMSD_SMAP.data = np.sqrt(RMSD_SMAP.data)
#
SD_aq = sss_SMOS_a.loc[dict(time=slice(st_time_aq, end_time_aq))]
SD_aq.data = sss_aq.loc[dict(time=slice(st_time_aq, end_time_aq))].data - \
             sss_SMOS_a.loc[dict(time=slice(st_time_aq, end_time_aq))].data
SD_aq.data = np.square(SD_aq.data)
RMSD_aq = SD_aq.mean('time')
RMSD_aq.data = np.sqrt(RMSD_aq.data)

# De-biased RMSD not necessary, as the differences are essentially zero. 


###### Calculate correlations. ###############################################

print('Calculating correlations...')

# Define function.
corr_map = np.vectorize(scipy.stats.pearsonr,signature='(n),(n)->(),()')

# Define dataarrays to hold answer.
r_SMAP = xr.DataArray(np.zeros((len(sss_SMAP.latitude),
                                len(sss_SMAP.longitude))),
                      coords=[('latitude', sss_SMAP.latitude),
                              ('longitude', sss_SMAP.longitude)])
r_aq = xr.DataArray(np.zeros((len(sss_aq.latitude),
                              len(sss_aq.longitude))),
                    coords=[('latitude', sss_aq.latitude),
                            ('longitude', sss_aq.longitude)])

# Do the calculation.
(r_SMAP.data,dummy) = corr_map(
    sss_SMAP.loc[dict(time=slice(st_time_SMAP, end_time_SMAP))]\
    .transpose('latitude','longitude','time').data,
    sss_SMOS_S.loc[dict(time=slice(st_time_SMAP, end_time_SMAP))]\
    .transpose('latitude','longitude','time').data)

(r_aq.data,dummy) = corr_map(
    sss_aq.loc[dict(time=slice(st_time_aq, end_time_aq))]\
    .transpose('latitude','longitude','time').data,
    sss_SMOS_a.loc[dict(time=slice(st_time_aq, end_time_aq))]\
    .transpose('latitude','longitude','time').data)


###### Plot  maps. ###############################################

print('Plotting...')

# 2d lat and lon arrays for plotting.
dlatm, dlonm = np.meshgrid(sss_aq.latitude,sss_aq.longitude,indexing='ij')

# Define map projection.
projection = ccrs.PlateCarree()
axes_class = (GeoAxes,
              dict(map_projection=projection))

# Open figure.
fig = plt.figure(figsize=(12, 6))

# Define number of rows and columns in plot.
n_r_ax = 2
n_c_ax = 2

# Define axes of different panels.
axgr = AxesGrid(fig, 111, axes_class=axes_class,
                nrows_ncols=(n_r_ax, n_c_ax),
                axes_pad=(0.25,0.5),
                cbar_location='bottom',
                cbar_mode='edge',
                cbar_pad=0.4,
                cbar_size='3%',
                direction='row',
                label_mode='')  # note the empty label_mode

# Loop over different variables and plot maps.
var_list = [RMSD_SMAP,r_SMAP,
            RMSD_aq,r_aq]
titles = ['SMAP & SMOS\nAnomaly RMSD', 'Anomaly correlation',
          'Aquarius & SMOS', '',
          '', '']
max_vals = [1.0,  1.0, 1.0,  1.0]
min_vals = [0.0, -1.0, 0.0, -1.0]
cbar_labels = ['PSU','-']
for i, ax in enumerate(axgr):
    ax.set_extent([-70, 0, -20, 25], projection)
    ax.coastlines()
    ax.add_feature(cfeature.RIVERS,edgecolor='dodgerblue')
    ax.add_feature(cfeature.LAKES,edgecolor='dodgerblue',
                   facecolor='dodgerblue')
    ax.add_feature(cfeature.LAND, facecolor='lightgray')
    ax.set_title(titles[i])
    ax.set_xticks(np.array([-60, -45, -30, -15]), crs=projection)
    ax.set_yticks(np.array([-15, 0, 15]), crs=projection)
    lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                       degree_symbol='')
    lat_formatter = LatitudeFormatter(degree_symbol='')
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    p = ax.pcolormesh(dlonm, dlatm, var_list[i],
                      transform=projection,
                      vmin=min_vals[i], vmax=max_vals[i],
                      cmap='RdBu_r')
    if i > (n_c_ax-1):
        cbar = axgr.cbar_axes[i-n_c_ax].colorbar(p)
        cbar.set_label_text(cbar_labels[i-n_c_ax])
        
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)

# Add info to footer.
[txtl, hash, clean] = get_git_revision_info()
anchored_text = AnchoredText(txtl, loc=3,borderpad=0,frameon=False,
                             prop=dict(size=5),
                             bbox_to_anchor=(0., 0.),
                             bbox_transform=fig.transFigure) #ax.transAxes)
axgr[n_c_ax].add_artist(anchored_text)

# Display the plot
#plt.show()

# Save the figure.
plt.savefig(plot_dir +
            os.path.splitext(os.path.basename(__file__))[0] +
            '.pdf',
            format='pdf')
#           format='png', dpi=900
#####
