"""
Calculate errors of water cycle closure.
"""

###########################################
import numpy as np
import xarray as xr
import pandas as pd
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
# My routines for reading in data.
from Amazon_closure import *
###########################################

################################################################################
# This function estimates standard error from estimated percentage errors
# with a given "effective sample size". The estimated errors are rather
# arbtirary (though less so for GPCP and GRACE).
#
# Input:
#     n_eff -- effective sample size (true sample size adjusted for
#              autocorrelation of residuals).
################################################################################
def error_const(n_eff):
    #
    import Amazon_catchment_details
    #
    # Define percentages for each quantity.
    nu_R = 20.0
    nu_E = 20.0
    nu_Q = 10.0
    #
    # Define annual means.
    R_ann = 458000
    E_ann = 245000
    Q_ann = 223000
    #
    # Specify absolute error for Amazon (1.9 cm) or Obidos (2.0 cm) basin
    # (from functions below), including conversion from depth (implicity
    # per month) to cubic meters per second.
    area_catchment = Amazon_catchment_details.areas['Amazon']*1000000.0
    RMS_dSdt = 19.0*area_catchment/(1000.0*30.0*24.0*60.0*60.0)
    #
    # Define which mean to use as the fraction denominator.
    error_denom = Q_ann
    #
    # Calculate RMS error.
    # Don't forget percentage conversion.
    RMS = np.sqrt((0.01*nu_R*R_ann)**2 +
                  (0.01*nu_E*E_ann)**2 +
                  (RMS_dSdt)**2 +
                  (0.01*nu_Q*Q_ann)**2)
    #
    # Calculate relative error.
    RMS_rel = 100.0*RMS/error_denom
    #
    # Calculate standard error.
    SE_rel = RMS_rel/np.sqrt(n_eff)
    #
    # Print answer.
    print(SE_rel)
    #
    return(SE_rel)


################################################################################
# This function estimates standard error from errors taken from differences
# between different data sets, using a 
#
# Input:
#     n_eff -- effective sample size (true sample size adjusted for
#              autocorrelation of residuals).
################################################################################
def error_empirical(n_eff):
    #
    import Amazon_catchment_details
    #
    # Define quantity uncertainties from differences between data sets.
    RMS_R = 108000.0
    RMS_E = 35000
    
    # Define percentage for streamflow.
    nu_Q = 5.0
    Q_ann = 223000
    #
    # GRACE requires some special processing.
    budget_Amazon = get_budget_vars('Amazon')
    RMS_dSdt = np.nanmax(np.array([
        np.nanmean(np.abs(budget_Amazon['dSdt_m_GFZ'].data - \
                          budget_Amazon['dSdt_m_CSR'].data)),
        np.nanmean(np.abs(budget_Amazon['dSdt_m_GFZ'].data - \
                          budget_Amazon['dSdt_m_JPL'].data)),
        np.nanmean(np.abs(budget_Amazon['dSdt_m_JPL'].data - \
                          budget_Amazon['dSdt_m_CSR'].data))]))
    #
    # Define which mean to use as the fraction denominator.
    error_denom = Q_ann
    #
    # Calculate RMS error.
    # Don't forget percentage conversion.
    RMS = np.sqrt((RMS_R)**2 +
                  (RMS_E)**2 +
                  (RMS_dSdt)**2 +
                  (0.01*nu_Q*Q_ann)**2)
    #
    # Calculate relative error.
    RMS_rel = 100.0*RMS/error_denom
    #
    # Calculate standard error.
    SE_rel = RMS_rel/np.sqrt(n_eff)
    #
    # Print answer.
    print(SE_rel)
    #
    return(SE_rel)


################################################################################
# This function estimates standard error from the distribution of errors
# taken from differences between different data sets.
#
# Input:
#     n_eff -- effective sample size (true sample size adjusted for
#              autocorrelation of residuals).
################################################################################
def error_dist(n_eff):
    #
    import Amazon_catchment_details
    #
    # Define quantity uncertainties from differences between data sets.
    RMS_R = 40000/np.sqrt(2.0)
    RMS_E = 30000/np.sqrt(2.0)
    
    # Define percentage for streamflow.
    nu_Q = 5.0
    Q_ann = 223000
    #
    # GRACE requires some special processing.
    RMS_dSdt = 24000/np.sqrt(2.0)
    #
    # Define which mean to use as the fraction denominator.
    error_denom = Q_ann
    #
    # Calculate RMS error.
    # Don't forget percentage conversion.
    RMS = np.sqrt((RMS_R)**2 +
                  (RMS_E)**2 +
                  (RMS_dSdt)**2 +
                  (0.01*nu_Q*Q_ann)**2)
    #
    # Calculate relative error.
    RMS_rel = 100.0*RMS/error_denom
    #
    # Calculate standard error.
    SE_rel = RMS_rel/np.sqrt(n_eff)
    #
    # Print answer.
    print(SE_rel)
    #
    return(SE_rel)

################################################################################
# Function to calculate GRACE basin average error.
#
# Input:
#     basin -- 'Amazon' or 'Obidos'
#     timestep -- integer in [0,162]
# Output:
#     error -- basin mean uncertainty for this timestep.
################################################################################
def GRACE_TWS_error(basin):
    
    import salem
    
    # Read shape file.
    sf = salem.read_shapefile('/Users/jameseyre/Data/in-situ/hybam/' +
                              basin +
                              '_catchment.shp')
    sf.crs = {'init': 'epsg:4326'}
    
    # Read the GRACE data.
    data_dir = '/Users/jameseyre/Data/satellite/GRACE/'
    GRACE_file_name = 'GRCTellus.JPL.200204_201706.GLO.RL06M.MSCNv01CRIv01.nc'
    ds_in = salem.open_xr_dataset(data_dir + GRACE_file_name)
    uc = ds_in.uncertainty
    
    # Sort out the longitude array order in GRACE data. 
    uc = uc.assign_coords(lon=(((uc.lon + 180) % 360) - 180))
    uce = uc.isel(lon=slice(0,360))
    ucw = uc.isel(lon=slice(360,720))
    uc = xr.concat([ucw,uce],'lon')
    
    # Apply the mask.
    uc_sub = uc.salem.roi(shape=sf)
    
    # Calculate or define parameters for error calculation.
    beta = 300.0 # decorrelation length
    n_pix = np.sum(~np.isnan(uc_sub[0,:,:])).data + 0
    n_times = len(uc_sub.time)
    
    # Get uncertainty into 2d array( andlat and lon into 1d arrays)
    # subsetted to rows where uncertainty is non-missing.
    latm, lonm = np.meshgrid(uc_sub.lat, uc_sub.lon, indexing='ij')
    latm_1d = latm.flatten()
    lonm_1d = lonm.flatten()
    uc_2d = np.reshape(uc_sub.data,
                       (uc_sub.data.shape[0],-1) )
    latm_1d = latm_1d[~np.isnan(uc_2d[0,:])]
    lonm_1d = lonm_1d[~np.isnan(uc_2d[0,:])]
    uc_2d_dropna = uc_2d[:, ~np.isnan(uc_2d[0,:])]
    
    # Loop over lat and lon for error calculation.
    # (Pseudo-code taken from https://grace.jpl.nasa.gov/data/get-data/monthly-mass-grids-land/)
    err_sum = np.zeros((n_times))
    for i in range(n_pix):
        for j in range(n_pix):
            dist = dist_haversine(latm_1d[i], lonm_1d[i],
                                  latm_1d[j], lonm_1d[j])
            expdb = np.exp(-(dist**2)/(2.0*beta**2))
            err_sum = err_sum + \
                      (uc_2d_dropna[:,i] * uc_2d_dropna[:,j] * expdb)
    basin_err = np.sqrt(err_sum)/n_pix
    
    return(basin_err)

# Function to calculate distance between lat,lon points.
# LAT AND LON MUST BE IN DEGREES !!!!!
def dist_haversine(lat1, lon1, lat2, lon2):
    #
    # Convert to radians.
    phi1 = np.radians(lat1)
    phi2 = np.radians(lat2)
    lambda1 = np.radians(lon1)
    lambda2 = np.radians(lon2)
    #
    # Calculate angular separations (in radians).
    delta_phi = phi2 - phi1
    delta_lambda = lambda2 - lambda1
    #
    # Calculate haversine.
    h = 0.5*(1 - np.cos(delta_phi)) + \
        np.cos(phi1)*np.cos(phi2)*0.5*(1 - np.cos(delta_lambda))
    #
    # Convert to distance in km.
    rad = 6371.0 # km
    dist = 2.0*rad*np.arcsin(np.sqrt(h))
    #
    return(dist)



################################################################################
# Function to calculate GPCP error ratio over time.
# Basin average error already calculated in separate script
# (GPCP_error_Amazon_timeseries.py).
#
# Input:
#     basin -- 'Amazon' only option currently
#     st_yr -- start of time-averaging period (1979-2018)
#     end_yr -- end of time-averaging period (1979-2018)
# Output:
#     error -- basin mean uncertainty for this period.
################################################################################
def GPCP_error(basin, st_yr, end_yr):
    #
    # Read files.
    data_dir = '/Users/jameseyre/Data/satellite/GPCP/'
    GPCP_file_name = basin + 'BasinAverage_GPCP.v2.3.precip.mon.mean.nc'
    GPCP_error_file_name = basin + \
                           'BasinAverage_GPCP.v2.3.precip.mon.mean.error.nc'
    ds_pr = xr.open_dataset(data_dir + GPCP_file_name)
    ds_err = xr.open_dataset(data_dir + GPCP_error_file_name)
    #
    # Read arrays.
    pr = ds_pr.precip.sel(time=slice(str(st_yr) + '-01-01',
                                     str(end_yr) + '-01-01'))
    err = ds_err.precip.sel(time=slice(str(st_yr) + '-01-01',
                                      str(end_yr) + '-01-01'))
    #
    # Calculate ratios.
    pct_error = 100.0*err/pr
    print('Years: ' + str(st_yr) + '-' + str(end_yr))
    print('===== Percentage errors =====')
    print(pct_error)
    #
    # Calculate averages.
    pct_error_ann_cy = pct_error.groupby('time.month').mean('time')
    print('===== Mean annual cycle =====')
    print(pct_error_ann_cy)
    pct_error_ann_mean = pct_error_ann_cy.mean()
    print('===== Annual mean =====')
    print(pct_error_ann_mean)

    return(pct_error_ann_mean)

