"""
Create a NetCDF file of Amazon basin means from ERA5 data. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import subprocess
import sys
import os
import salem
###########################################

# Get git info for this script.
sys.path.append('/Users/jameseyre/Documents/code/')
from python_git_tools import git_rev_info
[info, last_hash, rel_path, clean] = git_rev_info(os.path.realpath(__file__))


###### Load data. #################################################

print('Reading data...')

# Data locations.
data_dir = '/Users/jameseyre/Data/reanalysis/ERA5/'
shp_dir = '/Users/jameseyre/Data/in-situ/hybam/'
shp_file_name = 'Amazon_catchment.shp'
data_file_name_root = 'era5_moda_sfc_'

# Specify spatial subset.
lat_min = -25.0
lat_max = 10.0
lon_min = 278.0
lon_max = 320.0

# Read the file. 
ds_in = xr.open_mfdataset(data_dir + data_file_name_root + '*.nc')
evap = ds_in.e.loc[dict(latitude=slice(lat_max,lat_min),
                        longitude=slice(lon_min,lon_max))]
pr = ds_in.tp.loc[dict(latitude=slice(lat_max,lat_min),
                       longitude=slice(lon_min,lon_max))]
col_w = ds_in.tcw.loc[dict(latitude=slice(lat_max,lat_min),
                           longitude=slice(lon_min,lon_max))]
col_wv = ds_in.tcwv.loc[dict(latitude=slice(lat_max,lat_min),
                             longitude=slice(lon_min,lon_max))]
wv_div = ds_in.vimd.loc[dict(latitude=slice(lat_max,lat_min),
                             longitude=slice(lon_min,lon_max))]

# Change longitude coordinates.
# Data don't wrap round earth so can just subtract 360.
evap = evap.assign_coords(longitude=(evap.longitude - 360))
pr = pr.assign_coords(longitude=(pr.longitude - 360))
col_w = col_w.assign_coords(longitude=(col_w.longitude - 360))
col_wv = col_wv.assign_coords(longitude=(col_wv.longitude - 360))
wv_div = wv_div.assign_coords(longitude=(wv_div.longitude - 360))

###### Mask and average. #################################################

print('Processing data...')

# Apply the masking.
sf = salem.read_shapefile(shp_dir + shp_file_name)
sf.crs = {'init': 'epsg:4326'}
evap_am = evap.salem.roi(shape=sf)
pr_am = pr.salem.roi(shape=sf)
col_w_am = col_w.salem.roi(shape=sf)
col_wv_am = col_wv.salem.roi(shape=sf)
wv_div_am = wv_div.salem.roi(shape=sf)

# Calcualte cosine weights.
wts = evap_am.copy()
wts.data = np.ones(evap_am.shape)
wts = wts*np.cos(wts.latitude*np.pi/180.0)
wts.data[np.isnan(evap_am)] = np.nan

# Get sum of cosine weights per time step.
sum_wts = np.sum(wts.isel(time=0))

# Calculate area averaged LWE.
evap_wt_mean = (wts*evap_am).sum(dim=['latitude','longitude'])/sum_wts
pr_wt_mean = (wts*pr_am).sum(dim=['latitude','longitude'])/sum_wts
col_w_wt_mean = (wts*col_w_am).sum(dim=['latitude','longitude'])/sum_wts
col_wv_wt_mean = (wts*col_wv_am).sum(dim=['latitude','longitude'])/sum_wts
wv_div_wt_mean = (wts*wv_div_am).sum(dim=['latitude','longitude'])/sum_wts

###### Convert units  #################################################
#      and get rid of zeros (should be NaN).

# Just precipitation and evaporation need changing.
# They already have an implicit "per day" so just
# from m to mm.
evap_wt_mean = 1000.0*evap_wt_mean
pr_wt_mean = 1000.0*pr_wt_mean

# Change units attribute.
evap_wt_mean.attrs['units'] = 'mm day-1'
pr_wt_mean.attrs['units'] = 'mm day-1'

# Make "per day" explicit in water vapor divergence.
wv_div_wt_mean.attrs['units'] = 'kg m-2 day-1'

# Get other units from original variables.
col_w_wt_mean.attrs['units'] = col_w.attrs['units']
col_wv_wt_mean.attrs['units'] = col_wv.attrs['units']

###### Save out file. #################################################

print('Saving data...')

# Create xarray dataset.
ds_out = xr.Dataset({'e': (['time'], evap_wt_mean),
                     'tp': (['time'], pr_wt_mean),
                     'tcw': (['time'], col_w_wt_mean),
                     'tcwv': (['time'], col_wv_wt_mean),
                     'vimd': (['time'], wv_div_wt_mean)},
                    coords={'time':(['time'],evap_wt_mean.coords['time'])})

# Set up encoding.
t_units = 'days since 1900-01-01 00:00:00'
t_cal = 'standard'
fill_val = 9.96921e+36
wr_enc = {'e':{'_FillValue':fill_val},
          'tp':{'_FillValue':fill_val},
          'tcw':{'_FillValue':fill_val},
          'tcwv':{'_FillValue':fill_val},
          'vimd':{'_FillValue':fill_val},
          'time':{'units':t_units,'calendar':t_cal,
                  '_FillValue':fill_val}}

# Add other metadata.
ds_out.e.attrs['units'] = evap_wt_mean.attrs['units']
ds_out.e.attrs['standard_name'] = 'Amazon Basin ' + evap.attrs['standard_name']
ds_out.e.attrs['long_name'] = 'Amazon Basin ' + evap.attrs['long_name']
ds_out.e.attrs['region'] = 'Amazon Basin'
ds_out.e.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'

ds_out.tp.attrs['units'] = pr_wt_mean.attrs['units']
ds_out.tp.attrs['long_name'] = 'Amazon Basin ' + pr.attrs['long_name']
ds_out.tp.attrs['region'] = 'Amazon Basin'
ds_out.tp.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'

ds_out.tcw.attrs['units'] = col_w_wt_mean.attrs['units']
ds_out.tcw.attrs['long_name'] = 'Amazon Basin ' + col_w.attrs['long_name']
ds_out.tcw.attrs['region'] = 'Amazon Basin'
ds_out.tcw.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'

ds_out.tcwv.attrs['units'] = col_wv_wt_mean.attrs['units']
ds_out.tcwv.attrs['standard_name'] = 'Amazon Basin ' + col_wv.attrs['standard_name']
ds_out.tcwv.attrs['long_name'] = 'Amazon Basin ' + col_wv.attrs['long_name']
ds_out.tcwv.attrs['region'] = 'Amazon Basin'
ds_out.tcwv.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'

ds_out.vimd.attrs['units'] = wv_div_wt_mean.attrs['units']
ds_out.vimd.attrs['long_name'] = 'Amazon Basin ' + wv_div.attrs['long_name']
ds_out.vimd.attrs['region'] = 'Amazon Basin'
ds_out.vimd.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'

# Global attributes.
ds_out.attrs = ds_in.attrs
ds_out.attrs['pp_comment'] = 'Post-processing performed by Jack Reeves Eyre (University of Arizona)'
ds_out.attrs['pp_description'] = 'Monthly mean atmospheric water budget terms averaged over Amazon Basin'
ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/amazoncongo/src/master/'
ds_out.attrs['pp_last_commit'] = last_hash
ds_out.attrs['pp_script'] = rel_path
ds_out.attrs['pp_script_status'] = info
ds_out.attrs['pp_conda_env'] = os.environ['CONDA_DEFAULT_ENV']
ds_out.attrs['pp_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
ds_out.attrs['pp_ERA5_source_file'] = data_dir + data_file_name_root + '*.nc'
ds_out.attrs['pp_mask_shapefile'] = shp_dir + shp_file_name
ds_out.attrs['pp_shapefile_source'] = 'http://www.ore-hybam.org/index.php/eng/Data/Cartography/Amazon-basin-hydrography'

# Specify file name.
new_file_name = data_dir + 'AmazonBasinAverage_' + data_file_name_root + \
                str(min(evap_wt_mean.time.dt.year.data)) + '-' + \
                str(max(evap_wt_mean.time.dt.year.data)) + '.nc'

# Save out the file.
ds_out.to_netcdf(path=new_file_name, mode='w',
                 encoding=wr_enc,unlimited_dims=['time'])




