"""
Create a NetCDF file of Amazon basin mean ERA-Interim evaporation
and water vapor convergence. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import subprocess
import sys
import os
import salem
###########################################

# Function for git commit hash:
def get_git_revision_hash():
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    if sys.version_info[0] >= 3:
        return hash.decode('utf-8')
    else:
        return hash


###### Load data. #################################################

print('Reading data...')

# Data locations.
data_dir = '/Users/jameseyre/Data/reanalysis/ERA-int/AmazonCongo/'
shp_dir = '/Users/jameseyre/Data/ancillary/amapoly_ivb/'
shp_file_name = 'amapoly_ivb.shp'
EI_file_name1 = 'era_interim_mdfa_sfc_fc_2010-2018.nc' # fluxes
EI_file_name2 = 'era_interim_moda_sfc_an_2010-2018.nc' # water vapor transport

# Read the ERA-interim files. 
ds_in1 = salem.open_xr_dataset(data_dir + EI_file_name1)
ds_in2 = salem.open_xr_dataset(data_dir + EI_file_name2)

# Note the evaporation is given as a monthly mean of daily total,
# so units are !!!!!!!
#               m/day
#              !!!!!!!
e = ds_in1.e
ivt_e = ds_in2['p71.162']
ivt_n = ds_in2['p72.162']

# Sort out the longitude array order.
e = e.assign_coords(longitude=(((e.longitude + 180) % 360) - 180))
ee = e.isel(longitude=slice(0,180))
ew = e.isel(longitude=slice(180,360))
e = xr.concat([ew,ee],'longitude')
#
ivt_e = ivt_e.assign_coords(longitude=(((ivt_e.longitude + 180) % 360) - 180))
ivt_ee = ivt_e.isel(longitude=slice(0,180))
ivt_ew = ivt_e.isel(longitude=slice(180,360))
ivt_e = xr.concat([ivt_ew,ivt_ee],'longitude')
#
ivt_n = ivt_n.assign_coords(longitude=(((ivt_n.longitude + 180) % 360) - 180))
ivt_ne = ivt_n.isel(longitude=slice(0,180))
ivt_nw = ivt_n.isel(longitude=slice(180,360))
ivt_n = xr.concat([ivt_nw,ivt_ne],'longitude')


###### Mask and average evaporation. ##########################################

print('Processing evaporation data...')

# Apply the masking.
sf = salem.read_shapefile(shp_dir + shp_file_name)
e_am = e.salem.roi(shape=shp_dir + shp_file_name)

# Calcualte cosine weights.
wts = e_am.copy()
wts.data = np.ones(e_am.shape)
wts = wts*np.cos(wts.latitude*np.pi/180.0)
wts.data[np.isnan(e_am)] = np.nan

# Get sum of cosine weights per time step.
sum_wts = np.sum(wts.isel(time=0))

# Calculate area averaged LWE.
e_mean = e_am.mean(dim=['latitude','longitude'])
e_wt_mean = (wts*e_am).sum(dim=['latitude','longitude'])/sum_wts

###### Moisture flux convergence. #######################################

print('Processing water vapor convergence data...')

# Define box.
# These are coordinate values.
box_lats = np.array([3.0, -14.0])
box_lons = np.array([-52.0, -76.0])

# Get flow through box sides.
in_w = ivt_e.loc[dict(longitude=np.min(box_lons),
                      latitude=slice(np.max(box_lats),np.min(box_lats)))]
out_e = ivt_e.loc[dict(longitude=np.max(box_lons),
                       latitude=slice(np.max(box_lats),np.min(box_lats)))]
in_s = ivt_n.loc[dict(latitude=np.min(box_lats),
                      longitude=slice(np.min(box_lons),np.max(box_lons)))]
out_n = ivt_n.loc[dict(latitude=np.max(box_lats),
                       longitude=slice(np.min(box_lons),np.max(box_lons)))]

# Get length of grid cell sides.
r_earth = 6378137
d_theta = 1.0
dy = r_earth*(d_theta*np.pi/180.0)
d_phi = 1.0
dx_n = r_earth*np.cos(np.max(box_lats)*np.pi/180.0)*(d_phi*np.pi/180.0)
dx_s = r_earth*np.cos(np.min(box_lats)*np.pi/180.0)*(d_phi*np.pi/180.0)

# Calculate box total convergence.
box_conv = (in_w*dy).sum(dim=['latitude']) - \
           (out_e*dy).sum(dim=['latitude']) + \
           (in_s*dx_s).sum(dim=['longitude']) - \
           (out_n*dx_n).sum(dim=['longitude'])

# Calculate area of box:
# dy x dx
# sum dx over latitude and multiply by number of longitudes.
# dx = r_earth * cos(lat) * d_phi(radians)
area_box = r_earth*(d_phi*np.pi/180.0)*len(in_s.longitude)*dy*\
           np.cos(in_w.latitude*np.pi/180.0).sum(dim=['latitude']).data

# Convert convergence total mass to kg m-2 day-1.
s_to_day = 24.0*60.0*60.0
conv_rate = box_conv*s_to_day/area_box


###### Save out file. #################################################

print('Saving data...')

# Create xarray dataset.
ds_out = xr.Dataset({'evaporation': (['time'], e_wt_mean),
                     'convergence_rate':(['time'], conv_rate),
                     'convergence_mass':(['time'], box_conv)},
                    coords={'time':(['time'],e_wt_mean.coords['time'])})

# Set up encoding.
t_units = 'days since 1900-01-01 00:00:00'
t_cal = 'standard'
fill_val = 9.96921e+36
wr_enc = {'evaporation':{'_FillValue':fill_val},
          'convergence_rate':{'_FillValue':fill_val},
          'convergence_mass':{'_FillValue':fill_val},
          'time':{'units':t_units,'calendar':t_cal,
                  '_FillValue':fill_val}}

# Add other metadata.
ds_out.evaporation.attrs['units'] = 'm day^{-1}'
ds_out.evaporation.attrs['long_name'] = 'Amazon Basin ' + \
                                   e.attrs['long_name'] + ' rate'
ds_out.evaporation.attrs['standard_name'] = e.attrs['standard_name'] 
ds_out.evaporation.attrs['region'] = 'Amazon Basin'
ds_out.evaporation.attrs['region_area_squareMetres'] = sf['AREAPROJ'].loc[0]
ds_out.evaporation.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'
#
ds_out.convergence_rate.attrs['units'] = 'kg m^{-2} day^{-1}'
ds_out.convergence_rate.attrs['long_name'] = 'Amazon Basin rate of water vapor convergence, per unit area'
ds_out.convergence_rate.attrs['region'] = 'Lat-lon box approximately corresponding to Amazon basin'
ds_out.convergence_rate.attrs['region_bounds'] = 'Lat: ' + \
                                                 str(np.min(box_lats)) + \
                                                 ' to ' + \
                                                 str(np.max(box_lats)) + \
                                                  'Lon: ' + \
                                                 str(np.min(box_lons)) + \
                                                 ' to ' + \
                                                 str(np.max(box_lons))
ds_out.convergence_rate.attrs['region_area_squareMetres'] = area_box
ds_out.convergence_rate.attrs['postprocessing_methods'] = 'Total column atmospheric water vapor mass flux convergence in lat-lon box, divided by area and converted to amount per day (convergence_mass*24*60*60/region_area_squareMetres)'
#
ds_out.convergence_mass.attrs['units'] = 'kg s^{-1}'
ds_out.convergence_mass.attrs['long_name'] = 'Amazon Basin rate of water vapor convergence'
ds_out.convergence_mass.attrs['region'] = 'Lat-lon box approximately corresponding to Amazon basin'
ds_out.convergence_mass.attrs['region_bounds'] = 'Lat: ' + \
                                                 str(np.min(box_lats)) + \
                                                 ' to ' + \
                                                 str(np.max(box_lats)) + \
                                                  'Lon: ' + \
                                                 str(np.min(box_lons)) + \
                                                 ' to ' + \
                                                 str(np.max(box_lons))
ds_out.convergence_mass.attrs['region_area_squareMetres'] = area_box
ds_out.convergence_mass.attrs['postprocessing_methods'] = 'Total column atmospheric water vapor mass flux convergence in lat-lon box'
# Global attributes.
ds_out.attrs['pp_description'] = 'Monthly mean evaporation and convergence ratesaveraged over Amazon Basin, from ERA-Interim reanalysis '
ds_out.attrs['pp_comment'] = 'Post-processing performed by Jack Reeves Eyre (University of Arizona)'
ds_out.attrs['pp_script'] = 'ERA-int_Amazon_timeseries.py'
ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/amazoncongo/src/master/'
ds_out.attrs['pp_script_last_commit'] = get_git_revision_hash()
ds_out.attrs['pp_conda_env'] = os.environ['CONDA_DEFAULT_ENV']
ds_out.attrs['creation_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
ds_out.attrs['ERA-Interim_source_files'] = data_dir + EI_file_name1 + \
                                           '; ' + \
                                           data_dir + EI_file_name2
ds_out.attrs['mask_shapefile'] = shp_dir + shp_file_name
ds_out.attrs['shapefile_source'] = 'http://worldmap.harvard.edu/data/geonode:amapoly_ivb'

# Specify file name.
new_file_name = data_dir + 'AmazonBasinAverage_era_interim_2010-2018.nc'

# Save out the file.
ds_out.to_netcdf(path=new_file_name, mode='w',
                 encoding=wr_enc,unlimited_dims=['time'])




