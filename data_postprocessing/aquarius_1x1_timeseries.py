"""
Create a NetCDF file with all timesteps of Aquarius SSS. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import xesmf as xe
import iris
import datetime
from iris.time import PartialDateTime
import cf_units
import glob
import subprocess
import sys
###########################################

# Function for git commit hash:
def get_git_revision_hash():
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    if sys.version_info[0] >= 3:
        return hash.decode('utf-8')
    else:
        return hash


###### Load salinity data. #################################################

print('Reading data...')

data_dir = '/Users/jameseyre/Data/satellite/aquarius/'

# Get salinity file list.
aq_fn_list = glob.glob(data_dir +
                       '*_L3m_MO_SCI_V5.0_SSS_1deg.bz2.nc4')
aq_cubes = iris.load(aq_fn_list, 'Sea Surface Salinity')

# Define lat and lon coordinates.
lats =  iris.coords.DimCoord(points=np.arange(89.5,-90.0,-1.0),
                             standard_name='latitude',
                             long_name='latitude',
                             var_name='latitude',
                             units='degrees_north')
lats.guess_bounds(bound_position=0.5)
lons = iris.coords.DimCoord(np.arange(-179.5,180.0,1.0),
                            standard_name='longitude',
                            long_name='longitude',
                            var_name='longitude',
                            units='degrees_east')
lons.guess_bounds(bound_position=0.5)

# Loop over individual monthly cubes and add metadata.
for cube in aq_cubes:
    # Add spatial coordinates.
    cube.add_dim_coord(lats, 0)
    cube.add_dim_coord(lons, 1)
    
    # Get time info from attributes.
    t1 = cube.attributes['H5_GLOBAL.time_coverage_start']
    t2 = cube.attributes['H5_GLOBAL.time_coverage_end']
    
    # Convert to datetime objects.
    dt1 = datetime.datetime(int(t1[0:4]),
                            int(t1[5:7]),
                            int(t1[8:10]),
                            int(t1[11:13]),
                            int(t1[14:16]),
                            round(float(t1[17:23]))%60)
    dt2 = datetime.datetime(int(t2[0:4]),
                            int(t2[5:7]),
                            int(t2[8:10]),
                            int(t2[11:13]),
                            int(t2[14:16]),
                            round(float(t2[17:23]))%60)
    dtmean = dt1 + (dt2-dt1)/2
    
    # Add as scalar coordinates.
    time = iris.coords.AuxCoord(dtmean,
                                standard_name='time',
                                var_name='time',
                                long_name='time_in_datetime_format',
                                bounds=np.array([dt1,dt2]))
    cube.add_aux_coord(time)
    
    # Get rid of mis-matched attributes.
    diff_atts = ['H5_GLOBAL.product_name', 'H5_GLOBAL.date_created', 'H5_GLOBAL.history', 'H5_GLOBAL.time_coverage_start', 'H5_GLOBAL.time_coverage_end', 'H5_GLOBAL.start_orbit_number', 'H5_GLOBAL.end_orbit_number', 'H5_GLOBAL.data_bins', 'H5_GLOBAL.data_minimum', 'H5_GLOBAL.data_maximum', 'H5_GLOBAL._lastModified', 'H5_GLOBAL.id', 'H5_GLOBAL.source']
    for att in diff_atts:
        del cube.attributes[att]

# Merge cubes together to form a single salinity cube.
aq_salinity = aq_cubes.merge_cube()

# Add time dim_coord (and remove time aux_coord).
dt_all = aq_salinity.coord('time')
t_units = 'hours since 2011-08-01 00:00:00'
t_all = cf_units.date2num(dt_all.points, t_units, cf_units.CALENDAR_STANDARD)
time = iris.coords.DimCoord(t_all,standard_name='time',long_name='time',var_name='time',units=t_units)
aq_salinity.remove_coord('time')
aq_salinity.add_dim_coord(time,0)

###### Save data. #################################################

print('Saving data...')

# Convert to xarray.
aq_da = xr.DataArray.from_iris(aq_salinity)
aq_da.coords['month'] = ('time', aq_da['time.month'])
aq_da.coords['year'] =  ('time', aq_da['time.year'])

# Create array to indicate which months are complete.
full_months = np.ones([aq_da.shape[0]])
full_months[0] = 0
full_months[-1] = 0

# Create xarray dataset.
ds_out = xr.Dataset({'SSS': (['time', 'latitude', 'longitude'], aq_da),
                     'full_months': (['time'], full_months),
                     'latitude_bounds': (['latitude','n_bounds'],
                                         aq_salinity.coord('latitude').bounds),
                     'longitude_bounds': (['longitude','n_bounds'],
                                         aq_salinity.coord('longitude').bounds)},
                    coords={'time':(['time'],aq_da.coords['time']),
                            'latitude':(['latitude'],aq_da.coords['latitude']),
                            'longitude':(['longitude'],aq_da.coords['longitude']),
                            'n_bounds':(['n_bounds'],np.array([1,2]))})


# Set up encoding.
t_units = 'days since 2010-01-01 00:00:00'
t_cal = 'standard'
fill_val = 9.96921e+36
wr_enc = {'SSS':{'_FillValue':fill_val},
          'full_months':{'_FillValue':fill_val},
          'time':{'units':t_units,'calendar':t_cal,
                  '_FillValue':fill_val},
          'latitude':{'_FillValue':fill_val},
          'longitude':{'_FillValue':fill_val},
          'latitude_bounds':{'_FillValue':fill_val},
          'longitude_bounds':{'_FillValue':fill_val}}

# Add other metadata.
ds_out.SSS.attrs['units'] = 'PSU'
ds_out.SSS.attrs['long_name'] = aq_da.attrs['long_name']
ds_out.full_months.attrs['units'] = ''
ds_out.full_months.attrs['long_name'] = 'flag_for_complete_months_of_data'
ds_out.full_months.attrs['description'] = '1.0 = this time step has a full month of overpasses; 0.0 = this time step is not based on a full month of data'
ds_out.time.attrs['long_name'] = 'time'
ds_out.latitude.attrs['units'] = aq_da.latitude.attrs['units']
ds_out.latitude.attrs['long_name'] = aq_da.latitude.attrs['long_name']
ds_out.longitude.attrs['units'] = aq_da.longitude.attrs['units']
ds_out.longitude.attrs['long_name'] = aq_da.longitude.attrs['long_name']
ds_out.latitude_bounds.attrs['units'] = aq_da.latitude.attrs['units']
ds_out.latitude_bounds.attrs['long_name'] = 'latitude_of_cell_edges'
ds_out.longitude_bounds.attrs['units'] = aq_da.longitude.attrs['units']
ds_out.longitude_bounds.attrs['long_name'] = 'longitude_of_cell_edges'
ds_out.n_bounds.attrs['units'] = ''
ds_out.n_bounds.attrs['long_name'] = 'upper_and_lower_bounds_for_coordinate_arrays'
# Global attributes.
ds_out.attrs['pp_comment'] = 'Files aggregated by Jack Reeves Eyre (University of Arizona)'
ds_out.attrs['pp_script'] = 'aquarius_1x1_timeseries.py'
ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/amazoncongo/src/master/'
ds_out.attrs['pp_script_last_commit'] = get_git_revision_hash()
ds_out.attrs['creation_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

# Specify file name.
new_file_name = data_dir + 'aquarius_1x1deg_timeseries.nc'

# Save out the file.
ds_out.to_netcdf(path=new_file_name, mode='w',
                 encoding=wr_enc,unlimited_dims=['time'])
