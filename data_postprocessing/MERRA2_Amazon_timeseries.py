"""
Create a NetCDF file of Amazon basin means from MERRA2 data. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import subprocess
import sys
import os
import salem
###########################################

# Get git info for this script.
sys.path.append('/Users/jameseyre/Documents/code/')
from python_git_tools import git_rev_info
[info, last_hash, rel_path, clean] = git_rev_info(os.path.realpath(__file__))


###### Load data. #################################################

print('Reading data...')

# Data locations.
data_dir = '/Users/jameseyre/Data/reanalysis/MERRA2/Amazon/'
shp_dir = '/Users/jameseyre/Data/in-situ/hybam/'
shp_file_name = 'Amazon_catchment.shp'
data_file_name_roots = ['MERRA2_[234]00.instM_2d_int_Nx.*.nc4.nc',
                        'MERRA2_[234]00.tavgM_2d_int_Nx.*.nc4.nc',
                        'MERRA2_[234]00.tavgM_2d_flx_Nx.*.nc4.nc']

# Specify spatial subset.
lat_min = -25.0
lat_max = 10.0
lon_min = -82.0
lon_max = 40.0

# Read the files. 
ds_tqv =  xr.open_mfdataset(data_dir + data_file_name_roots[0])
ds_conv =  xr.open_mfdataset(data_dir + data_file_name_roots[1])
ds_pre =  xr.open_mfdataset(data_dir + data_file_name_roots[2])
#
evap = ds_pre.EVAP.loc[dict(lat=slice(lat_min,lat_max),
                            lon=slice(lon_min,lon_max))]
pr = ds_pre.PRECTOT.loc[dict(lat=slice(lat_min,lat_max),
                             lon=slice(lon_min,lon_max))]
prc = ds_pre.PRECTOTCORR.loc[dict(lat=slice(lat_min,lat_max),
                                  lon=slice(lon_min,lon_max))]
col_wv = ds_tqv.TQV.loc[dict(lat=slice(lat_min,lat_max),
                             lon=slice(lon_min,lon_max))]
wv_div = ds_conv.DQVDT_DYN.loc[dict(lat=slice(lat_min,lat_max),
                                    lon=slice(lon_min,lon_max))]

# Change time coordinate of col_wv to match others.
col_wv = col_wv.assign_coords(time=col_wv.time +
                              np.timedelta64(30, 'm'))

###### Mask and average. #################################################

print('Processing data...')

# Apply the masking.
sf = salem.read_shapefile(shp_dir + shp_file_name)
sf.crs = {'init': 'epsg:4326'}
evap_am = evap.salem.roi(shape=sf)
pr_am = pr.salem.roi(shape=sf)
prc_am = prc.salem.roi(shape=sf)
col_wv_am = col_wv.salem.roi(shape=sf)
wv_div_am = wv_div.salem.roi(shape=sf)

# Calcualte cosine weights.
wts = evap_am.copy()
wts.data = np.ones(evap_am.shape)
wts = wts*np.cos(wts.lat*np.pi/180.0)
wts.data[np.isnan(evap_am)] = np.nan

# Get sum of cosine weights per time step.
sum_wts = np.sum(wts.isel(time=0))

# Calculate area averaged LWE.
evap_wt_mean = (wts*evap_am).sum(dim=['lat','lon'])/sum_wts
pr_wt_mean = (wts*pr_am).sum(dim=['lat','lon'])/sum_wts
prc_wt_mean = (wts*prc_am).sum(dim=['lat','lon'])/sum_wts
col_wv_wt_mean = (wts*col_wv_am).sum(dim=['lat','lon'])/sum_wts
wv_div_wt_mean = (wts*wv_div_am).sum(dim=['lat','lon'])/sum_wts

###### Convert units  #################################################

# Need to change time units from per second to per day for
# precip, evaporation and convergence.
evap_wt_mean = 24.0*60.0*60.0*evap_wt_mean
pr_wt_mean = 24.0*60.0*60.0*pr_wt_mean
prc_wt_mean = 24.0*60.0*60.0*prc_wt_mean
wv_div_wt_mean = 24.0*60.0*60.0*wv_div_wt_mean

# Change units attribute.
evap_wt_mean.attrs['units'] = 'kg m-2 day-1'
pr_wt_mean.attrs['units'] = 'kg m-2 day-1'
prc_wt_mean.attrs['units'] = 'kg m-2 day-1'
wv_div_wt_mean.attrs['units'] = 'kg m-2 day-1'

# Get other units from original variables.
col_wv_wt_mean.attrs['units'] = col_wv.attrs['units']

###### Save out file. #################################################

print('Saving data...')

# Create xarray dataset.
ds_out = xr.Dataset({'EVAP': (['time'], evap_wt_mean),
                     'PRECTOT': (['time'], pr_wt_mean),
                     'PRECTOTCORR': (['time'], prc_wt_mean),
                     'TQV': (['time'], col_wv_wt_mean),
                     'DQVDT_DYN': (['time'], wv_div_wt_mean)},
                    coords={'time':(['time'],evap_wt_mean.coords['time'])})

# Set up encoding.
t_units = 'days since 1900-01-01 00:00:00'
t_cal = 'standard'
fill_val = 9.96921e+36
wr_enc = {'EVAP':{'_FillValue':fill_val},
          'PRECTOT':{'_FillValue':fill_val},
          'PRECTOTCORR':{'_FillValue':fill_val},
          'TQV':{'_FillValue':fill_val},
          'DQVDT_DYN':{'_FillValue':fill_val},
          'time':{'units':t_units,'calendar':t_cal,
                  '_FillValue':fill_val}}

# Add other metadata.
ds_out.EVAP.attrs['units'] = evap_wt_mean.attrs['units']
ds_out.EVAP.attrs['long_name'] = 'Amazon_Basin_' + evap.attrs['long_name']
ds_out.EVAP.attrs['region'] = 'Amazon Basin'
ds_out.EVAP.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'
ds_out.EVAP.attrs['collection'] = ds_pre.attrs['ShortName']

ds_out.PRECTOT.attrs['units'] = pr_wt_mean.attrs['units']
ds_out.PRECTOT.attrs['long_name'] = 'Amazon_Basin_' + pr.attrs['long_name']
ds_out.PRECTOT.attrs['region'] = 'Amazon Basin'
ds_out.PRECTOT.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'
ds_out.PRECTOT.attrs['collection'] = ds_pre.attrs['ShortName']

ds_out.PRECTOTCORR.attrs['units'] = prc_wt_mean.attrs['units']
ds_out.PRECTOTCORR.attrs['long_name'] = 'Amazon_Basin_bias_corrected_' + \
                                        prc.attrs['long_name']
ds_out.PRECTOTCORR.attrs['region'] = 'Amazon Basin'
ds_out.PRECTOTCORR.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'
ds_out.PRECTOTCORR.attrs['collection'] = ds_pre.attrs['ShortName']

ds_out.TQV.attrs['units'] = col_wv_wt_mean.attrs['units']
ds_out.TQV.attrs['long_name'] = 'Amazon_Basin_' + col_wv.attrs['long_name']
ds_out.TQV.attrs['region'] = 'Amazon Basin'
ds_out.TQV.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'
ds_out.TQV.attrs['collection'] = ds_tqv.attrs['ShortName']

ds_out.DQVDT_DYN.attrs['units'] = wv_div_wt_mean.attrs['units']
ds_out.DQVDT_DYN.attrs['long_name'] = 'Amazon_Basin_' + wv_div.attrs['long_name']
ds_out.DQVDT_DYN.attrs['region'] = 'Amazon Basin'
ds_out.DQVDT_DYN.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'
ds_out.DQVDT_DYN.attrs['collection'] = ds_conv.attrs['ShortName']

# Global attributes.
ds_out.attrs['Institution'] = ds_tqv.attrs['Institution']
ds_out.attrs['References'] = ds_tqv.attrs['References']
ds_out.attrs['ShortNames'] = ds_tqv.attrs['ShortName'] + '; ' + \
                             ds_pre.attrs['ShortName'] + '; ' + \
                             ds_conv.attrs['ShortName']
ds_out.attrs['LongNames'] = ds_tqv.attrs['LongName'] + '; ' + \
                            ds_pre.attrs['LongName'] + '; ' + \
                            ds_conv.attrs['LongName']
ds_out.attrs['identifier_product_DOIs'] = \
    ds_tqv.attrs['identifier_product_doi'] + '; ' + \
    ds_pre.attrs['identifier_product_doi'] + '; ' + \
    ds_conv.attrs['identifier_product_doi']
ds_out.attrs['pp_comment'] = 'Post-processing performed by Jack Reeves Eyre (University of Arizona)'
ds_out.attrs['pp_description'] = 'Monthly mean atmospheric water budget terms averaged over Amazon Basin'
ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/amazoncongo/src/master/'
ds_out.attrs['pp_last_commit'] = last_hash
ds_out.attrs['pp_script'] = rel_path
ds_out.attrs['pp_script_status'] = info
ds_out.attrs['pp_conda_env'] = os.environ['CONDA_DEFAULT_ENV']
ds_out.attrs['pp_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
ds_out.attrs['pp_MERRA2_source_files'] = ''
for dfnr in data_file_name_roots:
    ds_out.attrs['pp_MERRA2_source_files'] = \
        ds_out.attrs['pp_MERRA2_source_files'] + \
        data_dir + dfnr + ';  '
ds_out.attrs['pp_mask_shapefile'] = shp_dir + shp_file_name
ds_out.attrs['pp_shapefile_source'] = 'http://www.ore-hybam.org/index.php/eng/Data/Cartography/Amazon-basin-hydrography'

# Specify file name.
new_file_name = data_dir + 'AmazonBasinAverage_MERRA2_' + \
                str(min(evap_wt_mean.time.dt.year.data)) + '-' + \
                str(max(evap_wt_mean.time.dt.year.data)) + '.nc'

# Save out the file.
ds_out.to_netcdf(path=new_file_name, mode='w',
                 encoding=wr_enc,unlimited_dims=['time'])




