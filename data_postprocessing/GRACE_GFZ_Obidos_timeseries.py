"""
Create a NetCDF file Amazon basin mean GRACE (GFZ)  gravity anomalies. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import subprocess
import sys
import os
import salem
###########################################

# Get git info for this script.
sys.path.append('/Users/jameseyre/Documents/code/')
from python_git_tools import git_rev_info
[info, last_hash, rel_path, clean] = git_rev_info(os.path.realpath(__file__))


###### Load data. #################################################

print('Reading data...')

# Data locations.
data_dir = '/Users/jameseyre/Data/satellite/GRACE/'
shp_dir = '/Users/jameseyre/Data/in-situ/hybam/'
shp_file_name = 'Obidos_catchment.shp'
GRACE_file_name = 'GRAVIS-3_2002-2019_GFZOP_0600_TWS_GRID_GFZ_0001.nc'

# Read the GRACE file. 
ds_in = salem.open_xr_dataset(data_dir + GRACE_file_name)
lwe = ds_in.tws

# Sort out the longitude array order in GRACE data.
# We need [-180,180] for shape file routine below.
half_nlon = int(len(lwe.lon)/2)
lwe = lwe.assign_coords(lon=(((lwe.lon + 180) % 360) - 180))
lwee = lwe.isel(lon=slice(0,half_nlon))
lwew = lwe.isel(lon=slice(half_nlon,2*half_nlon))
lwe = xr.concat([lwew,lwee],'lon')


###### Mask and average. #################################################

print('Processing data...')

# Apply the masking.
sf = salem.read_shapefile(shp_dir + shp_file_name)
sf.crs = {'init': 'epsg:4326'}
lwe_am = lwe.salem.roi(shape=sf)

# Calcualte cosine weights.
wts = lwe_am.copy()
wts.data = np.ones(lwe_am.shape)
wts = wts*np.cos(wts.lat*np.pi/180.0)
wts.data[np.isnan(lwe_am)] = np.nan

# Get sum of cosine weights per time step.
sum_wts = np.sum(wts.isel(time=0))

# Calculate area averaged LWE.
lwe_mean = lwe_am.mean(dim=['lat','lon'])
lwe_wt_mean = (wts*lwe_am).sum(dim=['lat','lon'])/sum_wts

###### Save out file. #################################################

print('Saving data...')

# Create xarray dataset.
ds_out = xr.Dataset({'lwe_thickness': (['time'], lwe_wt_mean)},
                    coords={'time':(['time'],lwe_wt_mean.coords['time'])})

# Set up encoding.
t_units = 'days since 2000-01-01 00:00:00'
t_cal = 'standard'
fill_val = 9.96921e+36
wr_enc = {'lwe_thickness':{'_FillValue':fill_val},
          'time':{'units':t_units,'calendar':t_cal,
                  '_FillValue':fill_val}}

# Add other metadata.
ds_out.lwe_thickness.attrs['units'] = lwe.attrs['units']
ds_out.lwe_thickness.attrs['long_name'] = 'Obidos catchment ' + lwe.attrs['long_name']
ds_out.lwe_thickness.attrs['standard_name'] = lwe.attrs['standard_name']
ds_out.lwe_thickness.attrs['pyproj_srs'] = lwe.attrs['pyproj_srs']
ds_out.lwe_thickness.attrs['region'] = 'Catchment area of Amazon at Obidos'
ds_out.lwe_thickness.attrs['methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'
# Global attributes.
ds_out.attrs = ds_in.attrs
ds_out.attrs['pp_description'] = 'Monthly mean gravity anomaly averaged over catchment area of Amazon at Obidos, expressed as liquid water equivalent thickness'
ds_out.attrs['pp_comment'] = 'Post-processing performed by Jack Reeves Eyre (University of Arizona)'
ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/amazoncongo/src/master/'
ds_out.attrs['pp_last_commit'] = last_hash
ds_out.attrs['pp_script'] = rel_path
ds_out.attrs['pp_script_status'] = info
ds_out.attrs['pp_conda_env'] = os.environ['CONDA_DEFAULT_ENV']
ds_out.attrs['pp_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
ds_out.attrs['pp_GRACE_source_file'] = data_dir + GRACE_file_name
ds_out.attrs['pp_mask_shapefile'] = shp_dir + shp_file_name
ds_out.attrs['pp_shapefile_source'] = 'http://www.ore-hybam.org/index.php/eng/Data/Cartography/Amazon-basin-hydrography'


# Specify file name.
new_file_name = data_dir + 'ObidosBasinAverage_' + GRACE_file_name

# Save out the file.
ds_out.to_netcdf(path=new_file_name, mode='w',
                 encoding=wr_enc,unlimited_dims=['time'])




