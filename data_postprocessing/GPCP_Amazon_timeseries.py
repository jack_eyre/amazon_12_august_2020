"""
Create a NetCDF file of Amazon basin mean GPCP precipitation. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import subprocess
import sys
import os
import salem
###########################################

# Get git info for this script.
sys.path.append('/Users/jameseyre/Documents/code/')
from python_git_tools import git_rev_info
[info, last_hash, rel_path, clean] = git_rev_info(os.path.realpath(__file__))


###### Load data. #################################################

print('Reading data...')

# Data locations.
data_dir = '/Users/jameseyre/Data/satellite/GPCP/'
shp_dir = '/Users/jameseyre/Data/in-situ/hybam/'
shp_file_name = 'Amazon_catchment.shp'
GPCP_file_name = 'GPCP.v2.3.precip.mon.mean.nc'

# Read the GRACE file. 
ds_in = salem.open_xr_dataset(data_dir + GPCP_file_name)
pr = ds_in.precip

# Sort out the longitude array order in GRACE data. 
pr = pr.assign_coords(lon=(((pr.lon + 180) % 360) - 180))
pre = pr.isel(lon=slice(0,72))
prw = pr.isel(lon=slice(72,144))
pr = xr.concat([prw,pre],'lon')

###### Mask and average. #################################################

print('Processing data...')

# Apply the masking.
sf = salem.read_shapefile(shp_dir + shp_file_name)
sf.crs = {'init': 'epsg:4326'}
pr_am = pr.salem.roi(shape=sf)

# Calcualte cosine weights.
wts = pr_am.copy()
wts.data = np.ones(pr_am.shape)
wts = wts*np.cos(wts.lat*np.pi/180.0)
wts.data[np.isnan(pr_am)] = np.nan

# Get sum of cosine weights per time step.
sum_wts = np.sum(wts.isel(time=0))

# Calculate area averaged LWE.
pr_mean = pr_am.mean(dim=['lat','lon'])
pr_wt_mean = (wts*pr_am).sum(dim=['lat','lon'])/sum_wts

###### Save out file. #################################################

print('Saving data...')

# Create xarray dataset.
ds_out = xr.Dataset({'precip': (['time'], pr_wt_mean)},
                    coords={'time':(['time'],pr_wt_mean.coords['time'])})

# Set up encoding.
t_units = 'days since 1900-01-01 00:00:00'
t_cal = 'standard'
fill_val = 9.96921e+36
wr_enc = {'precip':{'_FillValue':fill_val},
          'time':{'units':t_units,'calendar':t_cal,
                  '_FillValue':fill_val}}

# Add other metadata.
ds_out.precip.attrs['units'] = pr.attrs['units']
ds_out.precip.attrs['long_name'] = 'Amazon Basin ' + pr.attrs['long_name']
ds_out.precip.attrs['var_desc'] = pr.attrs['var_desc']
ds_out.precip.attrs['dataset'] = pr.attrs['dataset'] 
ds_out.precip.attrs['level_desc'] = pr.attrs['level_desc'] 
ds_out.precip.attrs['statistic'] = pr.attrs['statistic'] 
ds_out.precip.attrs['region'] = 'Amazon Basin'
ds_out.precip.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'
# Global attributes.
ds_out.attrs['pp_description'] = 'Monthly mean precipitation rate averaged over Amazon Basin'
ds_out.attrs['pp_comment'] = 'Post-processing performed by Jack Reeves Eyre (University of Arizona)'
ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/amazoncongo/src/master/'
ds_out.attrs['pp_last_commit'] = last_hash
ds_out.attrs['pp_script'] = rel_path
ds_out.attrs['pp_script_status'] = info
ds_out.attrs['pp_conda_env'] = os.environ['CONDA_DEFAULT_ENV']
ds_out.attrs['pp_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
ds_out.attrs['pp_GPCP_source_file'] = data_dir + GPCP_file_name
ds_out.attrs['pp_mask_shapefile'] = shp_dir + shp_file_name
ds_out.attrs['pp_shapefile_source'] = 'http://www.ore-hybam.org/index.php/eng/Data/Cartography/Amazon-basin-hydrography'
ds_out.attrs['title'] = 'GPCP Version 2.3 Combined Precipitation Dataset (Final)'
ds_out.attrs['version'] = 'V2.3'

# Specify file name.
new_file_name = data_dir + 'AmazonBasinAverage_' + GPCP_file_name

# Save out the file.
ds_out.to_netcdf(path=new_file_name, mode='w',
                 encoding=wr_enc,unlimited_dims=['time'])




