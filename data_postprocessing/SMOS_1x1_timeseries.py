"""
Create a NetCDF file with all timesteps of SMOS SSS. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import xesmf as xe
import iris
import datetime
from iris.time import PartialDateTime
import cf_units
import glob
import subprocess
import sys
###########################################

# Function for git commit hash:
def get_git_revision_hash():
    hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    if sys.version_info[0] >= 3:
        return hash.decode('utf-8')
    else:
        return hash


###### Load salinity data. #################################################

print('Reading SMOS data...')

# Data locations.
data_dir = '/Users/jameseyre/Data/satellite/SMOS/'
dgrid_dir = '/Users/jameseyre/Data/satellite/aquarius/'

# Read in all SMAP files.
ds_in = xr.open_mfdataset(data_dir + 'SMOS_L3_DEBIAS_LOCEAN_AD_*_EASE_09d_25km_v03_monthly.nc',
                          concat_dim='time')

# Get lat and lon numpy arrays.
slat1d = ds_in.lat.values
slon1d = ds_in.lon.values

# Create lat and lon bounds arrays.
slatc1d = np.zeros(1 + slat1d.shape[0])
slonc1d = np.zeros(1 + slon1d.shape[0])
#
slatc1d[1:-1] = (slat1d[:-1] + slat1d[1:])/2.0
slatc1d[0] = slat1d[0] - (slat1d[1] - slat1d[0])/2.0
slatc1d[-1] = slat1d[-1] + (slat1d[-1] - slat1d[-2])/2.0
#
slonc1d[1:-1] = (slon1d[:-1] + slon1d[1:])/2.0
slonc1d[0] = slon1d[0] - (slon1d[1] - slon1d[0])/2.0
slonc1d[-1] = slon1d[-1] + (slon1d[-1] - slon1d[-2])/2.0


##### Now get Aquarius data to regrid to.

print('Reading Aquarius data...')

ds_d = xr.open_dataset(dgrid_dir + 'aquarius_1x1deg_timeseries.nc')

# Create lat and lon bounds arrays.
dlat1d = ds_d.latitude.values
dlon1d = ds_d.longitude.values
d_delta = np.absolute(dlat1d[1] - dlat1d[0])

if (d_delta != np.absolute(dlon1d[1] - dlon1d[0])):
    print('ACHTUNG: dest. grid spacing may not be regular')

if ((dlat1d[0] == np.max(dlat1d)) and
    (ds_d.latitude_bounds[0,0] > ds_d.latitude_bounds[0,1])):
    dlatc1d = np.append(ds_d.latitude_bounds[:,0], ds_d.latitude_bounds[-1,1])
else:
    print('ACHTUNG: check arrangement of lat and lat_bound coords in dest.grid')

if ((dlon1d[0] == np.min(dlon1d)) and
    (ds_d.longitude_bounds.data[0,0] < ds_d.longitude_bounds.data[0,1])):
   dlonc1d = np.append(ds_d.longitude_bounds[:,0], ds_d.longitude_bounds[-1,1])
else:
    print('ACHTUNG: check arrangement of lon and lon_bound coords in dest.grid')

    
###### Regrid SMAP. #################################################

print('Regridding SMAP data...')

# Create dictionaries of coordinates and bounds.
grid_in = {'lon': slon1d, 'lat': slat1d,
           'lon_b': slonc1d, 'lat_b': slatc1d}
grid_out = {'lon': dlon1d, 'lat': dlat1d,
            'lon_b': dlonc1d, 'lat_b': dlatc1d}

# Create the regridding tool.
regridder = xe.Regridder(grid_in, grid_out, 'conservative', reuse_weights=True)

# Apply it.
sss_out = regridder(ds_in.SSS)

###### Save data. #################################################

print('Saving data...')

# Create xarray dataset.
ds_out = xr.Dataset({'SSS': (['time', 'latitude', 'longitude'], sss_out)},
                    coords={'time':(['time'],sss_out.coords['time']),
                            'latitude':(['latitude'],sss_out.coords['lat']),
                            'longitude':(['longitude'],sss_out.coords['lon'])})


# Set up encoding.
t_units = 'days since 2010-01-01 00:00:00'
t_cal = 'standard'
fill_val = 9.96921e+36
wr_enc = {'SSS':{'_FillValue':fill_val},
          'time':{'units':t_units,'calendar':t_cal,
                  '_FillValue':fill_val},
          'latitude':{'_FillValue':fill_val},
          'longitude':{'_FillValue':fill_val}}

# Add other metadata.
ds_out.SSS.attrs['units'] = ds_in.SSS.attrs['units']
ds_out.SSS.attrs['long_name'] = ds_in.SSS.attrs['long_name']
ds_out.time.attrs['long_name'] = 'time'
ds_out.latitude.attrs['units'] = ds_d.latitude.attrs['units']
ds_out.latitude.attrs['long_name'] = ds_d.latitude.attrs['long_name']
ds_out.longitude.attrs['units'] = ds_d.longitude.attrs['units']
ds_out.longitude.attrs['long_name'] = ds_d.longitude.attrs['long_name']
# Global attributes.
ds_out.attrs['description'] = 'SMOS monthly sea surface salinity regridded to Aquarius 1x1 degree grid.'
ds_out.attrs['pp_comment'] = 'Files aggregated and regridded by Jack Reeves Eyre (University of Arizona)'
ds_out.attrs['pp_script'] = 'SMOS_1x1_timeseries.py'
ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/amazoncongo/src/master/'
ds_out.attrs['pp_script_last_commit'] = get_git_revision_hash()
ds_out.attrs['creation_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

# Specify file name.
new_file_name = data_dir + 'SMOS_1x1deg_timeseries.nc'

# Save out the file.
ds_out.to_netcdf(path=new_file_name, mode='w',
                 encoding=wr_enc,unlimited_dims=['time'])
