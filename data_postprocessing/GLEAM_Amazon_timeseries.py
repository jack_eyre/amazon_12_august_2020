"""
Create a NetCDF file of Amazon basin mean GLEAM evaporation. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import subprocess
import sys
import os
import salem
from calendar import monthrange
###########################################

# Get git info for this script.
sys.path.append('/Users/jameseyre/Documents/code/')
from python_git_tools import git_rev_info
[info, last_hash, rel_path, clean] = git_rev_info(os.path.realpath(__file__))


###### Load data. #################################################

print('Reading data...')

# Data locations.
data_dir = '/Users/jameseyre/Data/gridded-obs/GLEAM_v3.3/'
shp_dir = '/Users/jameseyre/Data/in-situ/hybam/'
shp_file_name = 'Amazon_catchment.shp'
data_file_name = 'E_2003_2018_GLEAM_v3.3b_MO.nc'

# Read the GRACE file. 
ds_in = salem.open_xr_dataset(data_dir + data_file_name)
evap = ds_in.E

###### Mask and average. #################################################

print('Processing data...')

# Apply the masking.
sf = salem.read_shapefile(shp_dir + shp_file_name)
sf.crs = {'init': 'epsg:4326'}
evap_am = evap.salem.roi(shape=sf)

# Calcualte cosine weights.
wts = evap_am.copy()
wts.data = np.ones(evap_am.shape)
wts = wts*np.cos(wts.lat*np.pi/180.0)
wts.data[np.isnan(evap_am)] = np.nan

# Get sum of cosine weights per time step.
sum_wts = np.sum(wts.isel(time=0))

# Calculate area averaged LWE.
evap_mean = evap_am.mean(dim=['lat','lon'])
evap_wt_mean = (wts*evap_am).sum(dim=['lat','lon'])/sum_wts

###### Convert units  #################################################
#      and get rid of zeros (should be NaN).

for it in range(len(evap_wt_mean.time)):
    d_in_m = monthrange(evap_wt_mean.time[it].dt.year.data,
                        evap_wt_mean.time[it].dt.month.data)
    evap_wt_mean[it] = evap_wt_mean[it]/d_in_m[1]
    if (np.count_nonzero(~np.isnan(evap_am[it,:,:])) == 0):
        evap_wt_mean[it] = np.nan

# Change units attribute.
evap_wt_mean.attrs['units'] = 'mm day-1'

###### Save out file. #################################################

print('Saving data...')

# Create xarray dataset.
ds_out = xr.Dataset({'E': (['time'], evap_wt_mean)},
                    coords={'time':(['time'],evap_wt_mean.coords['time'])})

# Set up encoding.
t_units = 'days since 1900-01-01 00:00:00'
t_cal = 'standard'
fill_val = 9.96921e+36
wr_enc = {'E':{'_FillValue':fill_val},
          'time':{'units':t_units,'calendar':t_cal,
                  '_FillValue':fill_val}}

# Add other metadata.
ds_out.E.attrs['units'] = evap_wt_mean.attrs['units']
ds_out.E.attrs['standard_name'] = 'Amazon Basin ' + evap.attrs['standard_name']
ds_out.E.attrs['long_name'] = 'Amazon Basin ' + evap.attrs['long_name']
ds_out.E.attrs['region'] = 'Amazon Basin'
ds_out.E.attrs['postprocessing_methods'] = 'Masked, cosine weighted, averaged over spatial dimensions'
# Global attributes.
ds_out.attrs = ds_in.attrs
ds_out.attrs['pp_comment'] = 'Post-processing performed by Jack Reeves Eyre (University of Arizona)'
ds_out.attrs['pp_description'] = 'Monthly mean evaporation rate averaged over Amazon Basin'
ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/amazoncongo/src/master/'
ds_out.attrs['pp_last_commit'] = last_hash
ds_out.attrs['pp_script'] = rel_path
ds_out.attrs['pp_script_status'] = info
ds_out.attrs['pp_conda_env'] = os.environ['CONDA_DEFAULT_ENV']
ds_out.attrs['pp_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
ds_out.attrs['pp_GLEAM_source_file'] = data_dir + data_file_name
ds_out.attrs['pp_mask_shapefile'] = shp_dir + shp_file_name
ds_out.attrs['pp_shapefile_source'] = 'http://www.ore-hybam.org/index.php/eng/Data/Cartography/Amazon-basin-hydrography'

# Specify file name.
new_file_name = data_dir + 'AmazonBasinAverage_' + data_file_name

# Save out the file.
ds_out.to_netcdf(path=new_file_name, mode='w',
                 encoding=wr_enc,unlimited_dims=['time'])




