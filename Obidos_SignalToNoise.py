"""
Calculate signal and noise from Obidos streamflow (observed and estimated).

Run it from plotenv conda environment.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
#### Routines from other files: ###########
from GRACE_methods import *
import Amazon_catchment_details
###########################################

################################################################################
def main():
    
    ##### Read data. ###########################################
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Change time coordinate of streamflow.
    r_m = ro_ob.assign_coords(time=ro_ob.time -
                              (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    r_s = seasonal_mean(r_m)
    #
    # Get other water budget variables (custom load functions below).
    # All in m3 s-1.
    budget_Obidos = get_budget_vars('Obidos')
    #
    # Calculate Obidos streamflow estimate from water balance.
    # (Use smallest mean residual combination, PE_ERA5_ERA5_JPL.)
    print('PE_ERA5_ERA5_JPL')
    p_m = budget_Obidos['pr_ERA5']
    p_s = seasonal_mean(p_m)
    e_m = budget_Obidos['evap_ERA5']
    e_s = seasonal_mean(e_m)
    dSdt_m = budget_Obidos['dSdt_m_JPL']
    dSdt_s = budget_Obidos['dSdt_s_JPL']
    rhat_m = p_m - e_m - dSdt_m
    rhat_s = p_s - e_s - dSdt_s
    #
    # Calculate anomalies.
    r_m_a = r_m.groupby('time.month') - r_m.groupby('time.month').mean('time')
    r_s_a = r_s.groupby('time.month') - r_s.groupby('time.month').mean('time')
    rhat_m_a = rhat_m.groupby('time.month') - \
               rhat_m.groupby('time.month').mean('time')
    rhat_s_a = rhat_s.groupby('time.month') - \
               rhat_s.groupby('time.month').mean('time')
    
    
    ##### Calculate stats. ###########################################\
    #
    # Residuals.
    resid_m = rhat_m - r_m
    resid_s = rhat_s - r_s
    resid_m_a = rhat_m_a - r_m_a
    resid_s_a = rhat_s_a - r_s_a
    #
    # Variances.
    var_r_m = r_m.var()
    var_r_s = r_s.var()
    var_resid_m = resid_m.var()
    var_resid_s = resid_s.var()
    var_r_m_a = r_m_a.var()
    var_r_s_a = r_s_a.var()
    var_resid_m_a = resid_m_a.var()
    var_resid_s_a = resid_s_a.var()
    #
    print('===== ABSOLUTE VALUES ==========')
    print('----- Standard deviations: -----')
    print('Obidos monthly:')
    print(np.sqrt(var_r_m.data))
    print('Obidos seasonal:')
    print(np.sqrt(var_r_s.data))
    print('Residual monthly:')
    print(np.sqrt(var_resid_m.data))
    print('Residual seasonal:')
    print(np.sqrt(var_resid_s.data))
    print('----- Variance ratios: -----')
    print('Monthly:')
    print(var_r_m.data/var_resid_m.data)
    print('Seasonal:')
    print(var_r_s.data/var_resid_s.data)
    print('===== ANOMALIES ================')
    print('----- Standard deviations: -----')
    print('Obidos monthly:')
    print(np.sqrt(var_r_m_a.data))
    print('Obidos seasonal:')
    print(np.sqrt(var_r_s_a.data))
    print('Residual monthly:')
    print(np.sqrt(var_resid_m_a.data))
    print('Residual seasonal:')
    print(np.sqrt(var_resid_s_a.data))
    print('----- Variance ratios: -----')
    print('Monthly:')
    print(var_r_m_a.data/var_resid_m_a.data)
    print('Seasonal:')
    print(var_r_s_a.data/var_resid_s_a.data)
    
    
    return()

################################################################################
# Simple function to return seasonal mean with NaN value if not
# all months are present for each season.
# Input is a 1D time series as an xarray data array.
# Output is a 1D time series with one data point per season.
def seasonal_mean(ts):
    ts_s = ts.resample(time='QS-DEC').mean(dim='time')
    ts_count = ts.resample(time='QS-DEC').count(dim='time')
    ts_s[ts_count < 3] = np.nan
    return ts_s
################################################################################


################################################################################
# Functions for reading individual datasets.

#### GPCP precipitation ####
def load_GPCP(catchment):
    filename = '~/Data/satellite/GPCP/' + catchment + \
               'BasinAverage_GPCP.v2.3.precip.mon.mean.nc'
    ds = xr.open_dataset(filename)
    output = ds.precip
    return output

#### CMAP precipitation ####
def load_CMAP(catchment):
    filename = '~/Data/satellite/CMAP/' + catchment + \
               'BasinAverage_precip.mon.mean.std.nc'
    ds = xr.open_dataset(filename)
    output = ds.precip
    return output

#### GLEAM evaporation ####
def load_GLEAM(catchment):
    filename = '~/Data/gridded-obs/GLEAM_v3.3/' + catchment + \
               'BasinAverage_E_2003_2018_GLEAM_v3.3b_MO.nc'
    ds = xr.open_dataset(filename)
    output = ds.E
    output = output.assign_coords(time=output.time -
                                  (output.time.dt.day-1).
                                  astype('timedelta64[D]') -
                                  (output.time.dt.hour).
                                   astype('timedelta64[h]'))
    return output

#### ERA5, all variables. ####
def load_ERA5(catchment):
    filename = '~/Data/reanalysis/ERA5/' + catchment + \
               'BasinAverage_era5_moda_sfc_2000-2018.nc'
    ds = xr.open_dataset(filename)
    e = ds.e
    p = ds.tp
    tcwv = ds.tcwv
    vimd = ds.vimd
    output = (p, e, tcwv, vimd)
    return output

#### MERRA2, all variables. ####
def load_MERRA2(catchment):
    filename = '~/Data/reanalysis/MERRA2/Amazon/' + catchment + \
               'BasinAverage_MERRA2_2000-2018.nc'
    ds = xr.open_dataset(filename)
    ds = ds.assign_coords(time=ds.time - np.timedelta64(30,'m'))
    p = ds.PRECTOTCORR
    e = ds.EVAP
    tcwv = ds.TQV
    viwvc = ds.DQVDT_DYN
    output = (p, e, tcwv, viwvc)
    return output

#### Wrapper for all variables. ####
def get_budget_vars(catchment):
    
    # Get basin area.
    basin_area = Amazon_catchment_details.areas[catchment]*1000000.0
    
    # Load variables using scripts from above.
    pr_GPCP = load_GPCP(catchment)
    pr_CMAP = load_CMAP(catchment)
    evap_GLEAM = load_GLEAM(catchment)
    (pr_ERA5, evap_ERA5, tcw_ERA5, vimd_ERA5) = load_ERA5(catchment)
    (pr_MERRA2, evap_MERRA2, tcw_MERRA2, viwvc_MERRA2) = load_MERRA2(catchment)
    
    # Get GRACE data from subroutine.
    TWS_JPL = load_GRACE_monthly(catchment,'JPL')
    TWS_CSR = load_GRACE_monthly(catchment,'CSR')
    TWS_GFZ = load_GRACE_monthly(catchment,'GFZ')
    
    # GRACE rate of change calculation (and units conversion).
    # Monthly:
    dSdt_m_JPL = deltaS_m_cen4(TWS_JPL)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_m_CSR = deltaS_m_cen4(TWS_CSR)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_m_GFZ = deltaS_m_cen4(TWS_GFZ)*(basin_area/(100.0*24.0*60.0*60.0))
    # Seasonal:
    dSdt_s_JPL = deltaS_s_for2(TWS_JPL)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_s_CSR = deltaS_s_for2(TWS_CSR)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_s_GFZ = deltaS_s_for2(TWS_GFZ)*(basin_area/(100.0*24.0*60.0*60.0))
    # Annual:
    dSdt_a_JPL = deltaS_a_for2(TWS_JPL)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_a_CSR = deltaS_a_for2(TWS_CSR)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_a_GFZ = deltaS_a_for2(TWS_GFZ)*(basin_area/(100.0*24.0*60.0*60.0))
    
    # Atmospheric reanalysis rate of change calculation (and units conversion).
    # Monthly:
    dtcwdt_m_ERA5 = deltaS_m_cen4(tcw_ERA5)*\
                    (basin_area/(1000.0*24.0*60.0*60.0))
    dtcwdt_m_MERRA2 = deltaS_m_cen4(tcw_MERRA2)*\
                      (basin_area/(1000.0*24.0*60.0*60.0))
    # Seasonal:
    dtcwdt_s_ERA5 = deltaS_s_for2(tcw_ERA5)*\
                    (basin_area/(1000.0*24.0*60.0*60.0))
    dtcwdt_s_MERRA2 = deltaS_s_for2(tcw_MERRA2)*\
                      (basin_area/(1000.0*24.0*60.0*60.0))
    # Annual:
    dtcwdt_a_ERA5 = deltaS_a_for2(tcw_ERA5)*\
                    (basin_area/(1000.0*24.0*60.0*60.0))
    dtcwdt_a_MERRA2 = deltaS_a_for2(tcw_MERRA2)*\
                      (basin_area/(1000.0*24.0*60.0*60.0))
    
    # Perform units conversions.
    # (everything to cubic meters per second).
    pr_GPCP = pr_GPCP*(basin_area/(1000.0*24.0*60.0*60.0))
    pr_CMAP = pr_CMAP*(basin_area/(1000.0*24.0*60.0*60.0))
    pr_ERA5 = pr_ERA5*(basin_area/(1000.0*24.0*60.0*60.0))
    pr_MERRA2 = pr_MERRA2*(basin_area/(1000.0*24.0*60.0*60.0))
    evap_GLEAM = evap_GLEAM*(basin_area/(1000.0*24.0*60.0*60.0))
    evap_ERA5 = evap_ERA5*(-1.0*basin_area/(1000.0*24.0*60.0*60.0))
    evap_MERRA2 = evap_MERRA2*(basin_area/(1000.0*24.0*60.0*60.0))
    vimd_ERA5 = vimd_ERA5*(-1.0*basin_area/(1000.0*24.0*60.0*60.0))
    viwvc_MERRA2 = viwvc_MERRA2*(basin_area/(1000.0*24.0*60.0*60.0))
    
    # Construct dictionary of data.
    budget_vars = {}
    budget_vars['pr_GPCP'] = pr_GPCP
    budget_vars['pr_CMAP'] = pr_CMAP
    budget_vars['pr_ERA5'] = pr_ERA5
    budget_vars['pr_MERRA2'] = pr_MERRA2
    budget_vars['evap_GLEAM'] = evap_GLEAM
    budget_vars['evap_ERA5'] = evap_ERA5
    budget_vars['evap_MERRA2'] = evap_MERRA2
    budget_vars['vimd_ERA5'] = vimd_ERA5
    budget_vars['viwvc_MERRA2'] = viwvc_MERRA2
    budget_vars['dSdt_m_JPL'] = dSdt_m_JPL
    budget_vars['dSdt_m_CSR'] = dSdt_m_CSR
    budget_vars['dSdt_m_GFZ'] = dSdt_m_GFZ
    budget_vars['dSdt_s_JPL'] = dSdt_s_JPL
    budget_vars['dSdt_s_CSR'] = dSdt_s_CSR
    budget_vars['dSdt_s_GFZ'] = dSdt_s_GFZ
    budget_vars['dSdt_a_JPL'] = dSdt_a_JPL
    budget_vars['dSdt_a_CSR'] = dSdt_a_CSR
    budget_vars['dSdt_a_GFZ'] = dSdt_a_GFZ
    budget_vars['dtcwdt_m_ERA5'] = dtcwdt_m_ERA5
    budget_vars['dtcwdt_m_MERRA2'] = dtcwdt_m_MERRA2
    budget_vars['dtcwdt_s_ERA5'] = dtcwdt_s_ERA5
    budget_vars['dtcwdt_s_MERRA2'] = dtcwdt_s_MERRA2
    budget_vars['dtcwdt_a_ERA5'] = dtcwdt_a_ERA5
    budget_vars['dtcwdt_a_MERRA2'] = dtcwdt_a_MERRA2
    
    # Construct ensemble means.
    budget_vars['pr_Ens'] = (pr_GPCP + pr_CMAP + pr_ERA5 + pr_MERRA2)/4.0
    budget_vars['evap_Ens'] = (evap_GLEAM + evap_ERA5 + evap_MERRA2)/3.0
    budget_vars['dSdt_m_Ens'] = (dSdt_m_JPL + dSdt_m_CSR + dSdt_m_GFZ)/3.0
    budget_vars['dSdt_s_Ens'] = (dSdt_s_JPL + dSdt_s_CSR + dSdt_s_GFZ)/3.0
    budget_vars['dSdt_a_Ens'] = (dSdt_a_JPL + dSdt_a_CSR + dSdt_a_GFZ)/3.0
    budget_vars['conv_Ens'] = (vimd_ERA5 + viwvc_MERRA2)/2.0
    budget_vars['dtcwdt_m_Ens'] = (dtcwdt_m_ERA5 + dtcwdt_m_MERRA2)/2.0
    budget_vars['dtcwdt_s_Ens'] = (dtcwdt_s_ERA5 + dtcwdt_s_MERRA2)/2.0
    budget_vars['dtcwdt_a_Ens'] = (dtcwdt_a_ERA5 + dtcwdt_a_MERRA2)/2.0
    
    # Return data.
    return(budget_vars)

################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    main()

