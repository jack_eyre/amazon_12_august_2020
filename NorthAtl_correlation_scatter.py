"""
Plot correlation coefficient of streamflow with salinity from different combinations of data.

Run it from plotenv conda environment.
"""

###########################################
import numpy as np
import xarray as xr
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
#### Routines from other files: ###########
from GRACE_methods import *
from Amazon_closure import *
import Amazon_catchment_details
###########################################

################################################################################
def main():
    
    ##### Read hydrology data. ###########################################
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Fill gaps in streamflow (inserts nan for missing months)
    ro_ob = ro_ob.resample(time='1M').asfreq()
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    
    # Get factor to correct Obidos streamflow to mouth.
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    ob_to_mouth = ds_DT.ratio_m2s[0].data
    
    # Scale up Obidos streamflow to river mouth.
    ro_DT = ro_ob.copy()
    ro_DT = ro_DT*ob_to_mouth
    
    # Get other water budget variables (custom load functions below).
    # All in m3 s-1.
    budget_Amazon = get_budget_vars('Amazon')
    #
    # Add streamflow to dictionary.
    budget_Amazon['streamflow'] = ro_ob
    
    
    ##### Read salinity data. ###########################################
    
    # Define region to average over.
    sal_region = np.array([15.0,25.0,-60.0,-50.0])
    sal_st_time = '2010-02-01'
    sal_end_time = '2017-12-31'
    
    # Open and read file.
    sal_filename_wildcard = '/Users/jameseyre/Data/satellite/SMOS/' + \
                            'SMOS_L3_DEBIAS_LOCEAN_AD_*' + \
                            '_EASE_09d_25km_v03_monthly.nc'
    ds_sal = xr.open_mfdataset(sal_filename_wildcard, concat_dim='time')
    # Don't get the first (incomplete) month.
    sss = ds_sal.SSS.sel(time=slice(sal_st_time,sal_end_time),
                         lat=slice(sal_region[0],sal_region[1]),
                         lon=slice(sal_region[2],sal_region[3]))\
                         .mean(['lat','lon']).load()
    
    ##### Read P-E data. ###############################################
    
    # Open files.
    pr_ds = xr.open_dataset('/Users/jameseyre/Data/satellite/GPCP/' +
                            'GPCP.v2.3.precip.mon.mean.nc')
    evap_ds = xr.open_mfdataset('/Users/jameseyre/Data/reanalysis/ERA5/' +
                                'era5_moda_sfc_' + '*.nc')
    
    # Read data (same region and period as salinity).
    # (Have to account for longitude format (0,360) -> (-180,180).)
    pr_local = pr_ds.precip.sel(time=slice(sal_st_time,sal_end_time),
                                lat=slice(sal_region[0],sal_region[1]),
                                lon=slice(sal_region[2]+360.0,
                                          sal_region[3]+360.0)).load()
    evap_local = evap_ds.e.sel(time=slice(sal_st_time,sal_end_time),
                               latitude=slice(sal_region[1],
                                              sal_region[0]),
                               longitude=slice(sal_region[2]+360.0,
                                               sal_region[3]+360.0)).load()
    
    # Just evaporation needs changing.
    # It already has an implicit "per day" so just
    # needs conversion from m to mm.
    evap_local = evap_local*1000.0
    evap_local.attrs['units'] = 'mm/day'
    
    # Calculate area averaged P-E.
    PmE_local = pr_local.mean(['lat','lon']) - \
                evap_local.mean(['latitude','longitude'])
    
    # Add to data dictionary.
    budget_Amazon['P-E_local'] = PmE_local
    
    ##### Calculate correlations. ###########################################
    
    print('Processing...')
    
    # Define maximum time lag for correlations.
    max_lag = 11
    
    # Calculate correlations of streamflow estimates with salinity.
    corrs_Amazon = corrs_all(budget_Amazon, sss, max_lag)
    
    ##### Plot results. ###########################################
    
    print('Plotting...')
    plot = plot_corrs(corrs_Amazon)


################################################################################

################################################################################
# Plotting function.

def plot_corrs(data_dict):
    
    # Specify a few details in advance.
    panel_list = ['abs', 'anom']
    panel_names = ['absolute', 'anomaly']
    time_lags = list(range(0,data_dict['max_lag']+1))
    special_colors = {'Obidos': '#268bd2',
                      'precip_GPCP': '#dc322f',
                      'precip_CMAP': '#b58900',
                      'P-E_local': '#6c71c4'}
    special_colors2 = {'PE_Ens':'#d33682',
                       #'conv_Ens':'#6c71c4',
                       #'all_Ens':'#cb4b16',
                       'PE_ERA5_ERA5_JPL':'#2aa198',
                       'PE_GPCP_ERA5_JPL':'#859900'}
    # Set up over all plot.
    # One panel each for abs and anom.
    fig, axs = plt.subplots(1, len(panel_list),
                            figsize=(9, 4), sharey=True)
    
    # Loop over panels.
    for p in range(len(panel_list)):
        # Initialize variable list.
        names = []
        values = []
        names_2 = []
        values_2 = []
        colors_2 = []
        names_Ens = []
        values_Ens = []
        colors_Ens = []
        # Loop over time lags and data combinations.
        for comb in data_dict['monthly']:
            for t in range(len(time_lags)):
                # Add data to lists.
                if (comb in special_colors.keys()):
                    names_2.append(t + 0.2*np.random.rand(1)[0] - 0.1)
                    values_2.append(-1.0*data_dict['monthly'][comb]\
                                    [panel_list[p] + '_lag' +
                                     str(time_lags[t])])
                    colors_2.append(special_colors[comb])
                elif (comb in special_colors2.keys()):
                    names_Ens.append(t + 0.2*np.random.rand(1)[0] - 0.1)
                    values_Ens.append(-1.0*data_dict['monthly'][comb]\
                                    [panel_list[p] + '_lag' +
                                     str(time_lags[t])])
                    colors_Ens.append(special_colors2[comb])
                    
                else:
                    names.append(t + 0.2*np.random.rand(1)[0] - 0.1)
                    values.append(-1.0*data_dict['monthly'][comb]\
                                  [panel_list[p] + '_lag' +
                                   str(time_lags[t])])
        
        # Make the plot panel.        
        axs[p].scatter(names, values, c='#002b36', s=6, marker='.')
        axs[p].scatter(names_2, values_2, c=colors_2, s=10, marker='o')
        axs[p].scatter(names_Ens, values_Ens, c=colors_Ens, s=10, marker='s')
        # Set some details.
        axs[p].title.set_text(panel_names[p])
        axs[0].set_ylabel('-1 * correlation coefficient')
        axs[p].set_xlabel('time lag / months')
        axs[p].set_ylim(bottom=0, top=1)
        axs[p].set_xlim(-0.75, len(time_lags)-0.25)
        axs[p].set_xticks(range(len(time_lags)))
        axs[p].set_xticklabels(time_lags)
    #
    # Add legends.
    legend_entries = []
    legend_entries2 = []
    for comb in special_colors.keys():
        legend_entries.append(mlines.Line2D([], [],
                                            color=special_colors[comb],
                                            marker='o',
                                            linestyle='None',
                                            label=comb))
    for comb in special_colors2.keys():
        legend_entries2.append(mlines.Line2D([], [],
                                             color=special_colors2[comb],
                                             marker='s',
                                             linestyle='None',
                                             label=comb))
    legend1 = axs[1].legend(handles=legend_entries,
                            loc='upper right',
                            fontsize='small',
                            markerscale=0.7,
                            labelspacing=0.4)
    axs[1].add_artist(legend1)
    legend2 = axs[1].legend(handles=legend_entries2,
                            loc='upper left',
                            fontsize='small',
                            markerscale=0.7,
                            labelspacing=0.4)
    
    # Tidy layout.
    fig.tight_layout()
            
    # Get git info for this script.
    sys.path.append('/Users/jameseyre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    
    # Add git info to footer.
    plt.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    
    # Show the plot.
    #plt.show()
    # Or save the figure.
    fig.savefig('/Users/jameseyre/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.pdf',
                format='pdf')
    
    return(fig)

################################################################################
### Correlation functions.

def corrs_all(all_vars, sal, max_lag=2):
    ###############################################################
    # Returns dictionaries of statistics of correlation coefficient:
    # Monthly, seasonal and annual,
    # each of which has results from different combinations
    # of budget terms, for both absolute values and anomalies.
    # The results are each the output from corr_stats (function
    # below).
    #
    # Input:
    #     all_vars:     a dictionary of variables output by
    #                   get_budget_vars (below).
    #     sal:          an xarray data array of salinity
    #                   averaged over a small ocean region.
    # Optional input
    #     max_lag:      maximum time lag for which correlation
    #                   is calculated. Defaults to 2.
    #
    # Output:
    #     resids_all:   a dictionary with the following
    #                   structure of sub-dictionaries:
    #     {'monthly': {<'budget_terms_combination_1'>: corr_stats,
    #                  <'budget_terms_combination_2'>: corr_stats,
    #                  ... },
    #      'max_lag': max_lag
    #     }
    # The different combinations are named according to their input
    # data and method (e.g., 'PE_GPCP_GLEAM_JPL' [P-E method] or
    # 'conv_ERA5_ERA5_GFZ' [atmospheric convergence method].
    ###############################################################
    
    # Initialize final output dictionary.
    output = {'max_lag': max_lag}
    
    # Loop over timesteps.
    freq_list = ['monthly']
    for f in freq_list:
        # Initialize sub-dictionary.
        temp = {}
        # Start adding residuals to this.
        temp['Obidos'] = corr_stats(all_vars['streamflow'],
                                         sal, freq=f, max_lag=max_lag)
        temp['precip_GPCP'] = corr_stats(all_vars['pr_GPCP'],
                                         sal, freq=f, max_lag=max_lag)
        temp['precip_CMAP'] = corr_stats(all_vars['pr_CMAP'],
                                         sal, freq=f, max_lag=max_lag)
        temp['P-E_local'] = corr_stats(all_vars['P-E_local'],
                                       sal, freq=f, max_lag=max_lag)
        temp['PE_Ens'] = corr_stats(all_vars['pr_Ens'] -
                                    all_vars['evap_Ens'] -
                                    all_vars['dSdt_m_Ens'],
                                    sal, freq=f, max_lag=max_lag)
        temp['conv_Ens'] = corr_stats(all_vars['conv_Ens'] -
                                      all_vars['dtcwdt_m_Ens'] -
                                      all_vars['dSdt_m_Ens'],
                                      sal, freq=f, max_lag=max_lag)
        temp['all_Ens'] = corr_stats(0.4*all_vars['conv_Ens'] +
                                     0.6*all_vars['pr_Ens'] -
                                     0.6*all_vars['evap_Ens'] -
                                     0.4*all_vars['dtcwdt_m_Ens'] -
                                     all_vars['dSdt_m_Ens'],
                                     sal, freq=f, max_lag=max_lag)
        # Loop over GRACE methods.
        GRACE_list = ['JPL','CSR','GFZ']
        for g in GRACE_list:
            temp['PE_GPCP_GLEAM_' + g] = corr_stats(
                all_vars['pr_GPCP'] - all_vars['evap_GLEAM'] - \
                all_vars['dSdt_' + f[0] + '_' + g],
                sal, freq=f, max_lag=max_lag)
            temp['PE_CMAP_GLEAM_' + g] = corr_stats(
                all_vars['pr_CMAP'] - all_vars['evap_GLEAM'] - \
                all_vars['dSdt_' + f[0] + '_' + g],
                sal, freq=f, max_lag=max_lag)
            temp['PE_GPCP_ERA5_' + g] = corr_stats(
                all_vars['pr_GPCP'] - all_vars['evap_ERA5'] - \
                all_vars['dSdt_' + f[0] + '_' + g],
                sal, freq=f, max_lag=max_lag)
            temp['PE_CMAP_ERA5_' + g] = corr_stats(
                all_vars['pr_CMAP'] - all_vars['evap_ERA5'] - \
                all_vars['dSdt_' + f[0] + '_' + g],
                sal, freq=f, max_lag=max_lag)
            temp['PE_GPCP_MERRA2_' + g] = corr_stats(
                all_vars['pr_GPCP'] - all_vars['evap_MERRA2'] - \
                all_vars['dSdt_' + f[0] + '_' + g],
                sal, freq=f, max_lag=max_lag)
            temp['PE_CMAP_MERRA2_' + g] = corr_stats(
                all_vars['pr_CMAP'] - all_vars['evap_MERRA2'] - \
                all_vars['dSdt_' + f[0] + '_' + g],
                sal, freq=f, max_lag=max_lag)
            temp['PE_ERA5_ERA5_' + g] = corr_stats(
                all_vars['pr_ERA5'] - all_vars['evap_ERA5'] - \
                all_vars['dSdt_' + f[0] + '_' + g],
                sal, freq=f, max_lag=max_lag)
            temp['PE_MERRA2_MERRA2_' + g] = corr_stats(
                all_vars['pr_MERRA2'] - all_vars['evap_MERRA2'] - \
                all_vars['dSdt_' + f[0] + '_' + g],
                sal, freq=f, max_lag=max_lag)
            temp['conv_ERA5_ERA5_' + g] = corr_stats(
                all_vars['vimd_ERA5'] - all_vars['dtcwdt_m_ERA5'] - \
                all_vars['dSdt_' + f[0] + '_' + g],
                sal, freq=f, max_lag=max_lag)
            temp['conv_MERRA2_MERRA2_' + g] = corr_stats(
                all_vars['viwvc_MERRA2'] - all_vars['dtcwdt_m_MERRA2'] - \
                all_vars['dSdt_' + f[0] + '_' + g],
                sal, freq=f, max_lag=max_lag)
        
        # Add all these to the final output.
        output[f] = temp
        
    # Return the output.
    return(output)


def corr_stats(streamflow, sal, freq="monthly", max_lag=2):
    ###############################################################
    # Returns dictionary of statistics of correlation statistics.
    # Input:
    #     streamflow (or could be precipitation for example)
    #     salinity
    # Optional arguments:
    #     freq: averaging frequency of time series.
    #           Currently "monthly" is the default and
    #           only accepted value.
    #     max_lag:  maximum time lag (streamflow leads salinity)
    #               to calculate correlations for. Defaults to 2.
    #
    # Output:
    # Dictionary containing several correlation coefficients:
    #     abs_lag0:    correlation of absolute values at zero lag
    #     anom_lag0:   correlation of anomalies at zero lag
    #     abs_lag1:    correlation of absolute values at 1-month lag
    #     anom_lag1:   correlation of anomalies at 1-month lag
    #     etc....
    # NOTE: the lags are streamflow leading salinity
    # (e.g., pairs of data are (streamflow_Jan, salinity_Feb). 
    ###############################################################
    
    # Initialize output dictionary.
    stats = {}
    
    # Check for valid frequency input.
    if freq != "monthly":
        print("Invalid option for freq")
    
    else:
        # HAVE TO DEAL WITH MISSING VALUES!
        # Calcualte anomalies.
        streamflow_anom = streamflow.groupby('time.month') - \
                          streamflow.groupby('time.month').mean('time')
        sal_anom = sal.groupby('time.month') - \
                   sal.groupby('time.month').mean('time')
        
        # Calculate time overlap.
        st_time = max([min(streamflow.time), min(sal.time)])
        end_time = min([max(streamflow.time), max(sal.time)])
        
        # Get subsets of data.
        v1 = streamflow.sel(time=slice(st_time,end_time)).data
        v1a = streamflow_anom.sel(time=slice(st_time,end_time)).data
        v2 = sal.sel(time=slice(st_time,end_time)).data
        v2a = sal_anom.sel(time=slice(st_time,end_time)).data
        #
        # Mask out missing values.
        mask = (~np.isnan(v1) & ~np.isnan(v2))
        v1 = v1[mask]
        v2 = v2[mask]
        v1a = v1a[mask]
        v2a = v2a[mask]
        
        # Calculate correlation coefficients.
        stats['abs_lag0'] = np.corrcoef(v1, v2)[0,1]
        stats['anom_lag0'] = np.corrcoef(v1a, v2a)[0,1]
        if max_lag > 0:
            for l in range(1,max_lag+1):     
                stats['abs_lag' + str(l)] = np.corrcoef(v1[:-l],
                                                        v2[l:])[0,1]
                stats['anom_lag' + str(l)] = np.corrcoef(v1a[:-l],
                                                         v2a[l:])[0,1]
    
    # Return values.
    return(stats)


################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    main()

