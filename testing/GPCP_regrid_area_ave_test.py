"""
Test if re-gridding to a finer grid makes a difference to basin averages. 
"""

###########################################
import xarray as xr
import xesmf as xe
import ESMF
import iris
import numpy as np
import datetime
from iris.time import PartialDateTime
from iris.analysis import stats
import cf_units
import glob
import matplotlib.pyplot as plt
from matplotlib import cm, gridspec, rcParams
import iris.quickplot as qplt
import sys
import os
###########################################

# Get git info for this script.
sys.path.append('/Users/jameseyre/Documents/code/')
from python_git_tools import git_rev_info
[info, last_hash, rel_path, clean] = git_rev_info(os.path.realpath(__file__))


###### Load data. #################################################

print('Reading data...')

# Data locations.
data_dir = '/Users/jameseyre/Data/satellite/GPCP/'
shp_dir = '/Users/jameseyre/Data/in-situ/hybam/'
shp_file_name_a1 = 'Amazon_catchment.shp'
shp_file_name_a2 = 'amazlm_1608.shp'
shp_file_name_o = 'Obidos_catchment.shp'
GPCP_file_name = 'GPCP.v2.3.precip.mon.mean.nc'

# Read the GPCP file. 
ds_in = xr.open_dataset(data_dir + GPCP_file_name)
pr = ds_in.precip[-20:,:,:]

# Sort out the longitude array order in GRACE data. 
pr = pr.assign_coords(lon=(((pr.lon + 180) % 360) - 180))
pre = pr.isel(lon=slice(0,72))
prw = pr.isel(lon=slice(72,144))
pr = xr.concat([prw,pre],'lon')
# This should now have lon from -180 to 180 and lat from -90 to 90.

###### Regrid with xESMF. ######################################################
# Need 2d arrays of lats and lons at grid points centers and edges,
# for both source and destination grids.

print('Regridding....')

# For source grid.
slat1d = pr.coords['lat'].data
slon1d = pr.coords['lon'].data
slatm, slonm = np.meshgrid(slat1d,slon1d,indexing='ij')
s_delta = np.absolute(slat1d[1] - slat1d[0])
if (s_delta != np.absolute(slon1d[1] - slon1d[0])):
    print('ACHTUNG: source grid spacing not regular')
slatc1d = slat1d[0] - (s_delta/2.0) + np.arange(1+len(slat1d))*s_delta
slonc1d = slon1d[0] - (s_delta/2.0) + np.arange(1+len(slon1d))*s_delta
slatcm, sloncm = np.meshgrid(slatc1d,slonc1d,indexing='ij')

# For destination grid.
d_delta = 0.1
dlat1d = np.arange(-90.0+(d_delta/2.0), 90.0, d_delta)
dlon1d = np.arange(-180.0+(d_delta/2.0), 180.0, d_delta)
dlatm, dlonm = np.meshgrid(dlat1d,dlon1d,indexing='ij')
dlatc1d = np.arange(-90.0, 90.0+(d_delta/2.0), d_delta)
dlonc1d = np.arange(-180.0, 180.0+(d_delta/2.0), d_delta)
dlatcm, dloncm = np.meshgrid(dlatc1d,dlonc1d,indexing='ij')

# Now make datasets and define the regridder.
ds = xr.Dataset({'lat': (['x_in', 'y_in'], slatm),
                 'lon': (['x_in', 'y_in'], slonm),
                 'lat_b': (['xb_in', 'yb_in'], slatcm),
                 'lon_b': (['xb_in', 'yb_in'], sloncm),
                }
               )
dd = xr.Dataset({'lat': (['x_out', 'y_out'], dlatm),
                 'lon': (['x_out', 'y_out'], dlonm),
                 'lat_b': (['xb_out', 'yb_out'], dlatcm),
                 'lon_b': (['xb_out', 'yb_out'], dloncm),
                }
               )
regridder = xe.Regridder(ds, dd, 'nearest_s2d', reuse_weights=True)

# Do the regridding.
pr_out = regridder(pr)


###### Save out file. #################################################

print('Saving data...')

# Create xarray dataset.
ds_out = xr.Dataset({'precip':
                     (['time','lat','lon'],
                      pr_out)},
                    coords={'time':(['time'],pr.coords['time']),
                            'lat':(['lat'],dlat1d),
                            'lon':(['lon'],dlon1d)})

# Set up encoding.
t_units = 'days since 1900-01-01 00:00:00'
t_cal = 'standard'
fill_val = 9.96921e+36
wr_enc = {'precip':{'_FillValue':fill_val},
          'time':{'units':t_units,'calendar':t_cal,
                  '_FillValue':fill_val}}

# Add other metadata.
# Global attributes.
ds_out.attrs['pp_description'] = 'Monthly mean precipitation regridded'
ds_out.attrs['pp_comment'] = 'Post-processing performed by Jack Reeves Eyre (University of Arizona)'
ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/amazoncongo/src/master/'
ds_out.attrs['pp_last_commit'] = last_hash
ds_out.attrs['pp_script'] = rel_path
ds_out.attrs['pp_script_status'] = info
ds_out.attrs['pp_conda_env'] = os.environ['CONDA_DEFAULT_ENV']
ds_out.attrs['pp_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
ds_out.attrs['pp_GPCP_source_file'] = data_dir + GPCP_file_name
ds_out.attrs['title'] = 'GPCP Version 2.3 Combined Precipitation Dataset (Final)'
ds_out.attrs['version'] = 'V2.3'

# Specify file name.
new_file_name = data_dir + 'regrid_area_ave_test__' + GPCP_file_name

# Save out the file.
ds_out.to_netcdf(path=new_file_name, mode='w',
                 encoding=wr_enc,unlimited_dims=['time'])
