"""
Create a NetCDF file of Amazon basin mean GPCP precipitation. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import subprocess
import sys
import os
import salem
###########################################

# Get git info for this script.
sys.path.append('/Users/jameseyre/Documents/code/')
from python_git_tools import git_rev_info
[info, last_hash, rel_path, clean] = git_rev_info(os.path.realpath(__file__))


###### Load data. #################################################

print('Reading data...')

# Data locations.
data_dir = '/Users/jameseyre/Data/satellite/GPCP/'
shp_dir = '/Users/jameseyre/Data/in-situ/hybam/'
shp_file_name_a1 = 'Amazon_catchment.shp'
shp_file_name_a2 = 'amazlm_1608.shp'
shp_file_name_o = 'Obidos_catchment.shp'
GPCP_file_name = 'GPCP.v2.3.precip.mon.mean.nc'

# Read the GPCP file. 
ds_in = salem.open_xr_dataset(data_dir + GPCP_file_name)
pr = ds_in.precip

# Sort out the longitude array order in GRACE data. 
pr = pr.assign_coords(lon=(((pr.lon + 180) % 360) - 180))
pre = pr.isel(lon=slice(0,72))
prw = pr.isel(lon=slice(72,144))
pr = xr.concat([prw,pre],'lon')

###### Mask and average. #################################################

print('Processing data...')

# Apply the masking.
sf_a1 = salem.read_shapefile(shp_dir + shp_file_name_a1)
sf_a2 = salem.read_shapefile(shp_dir + shp_file_name_a2)
sf_o = salem.read_shapefile(shp_dir + shp_file_name_o)
sf_a1.crs = {'init': 'epsg:4326'}
sf_a2.crs = {'init': 'epsg:4326'}
sf_o.crs = {'init': 'epsg:4326'}
pr_a1 = pr.salem.roi(shape=sf_a1)
pr_a2 = pr.salem.roi(shape=sf_a2)
pr_o = pr.salem.roi(shape=sf_o)

# Calcualte cosine weights.
wts_a1 = pr_a1.copy()
wts_a1.data = np.ones(pr_a1.shape)
wts_a1 = wts_a1*np.cos(wts_a1.lat*np.pi/180.0)
wts_a1.data[np.isnan(pr_a1)] = np.nan
#
wts_a2 = pr_a2.copy()
wts_a2.data = np.ones(pr_a2.shape)
wts_a2 = wts_a2*np.cos(wts_a2.lat*np.pi/180.0)
wts_a2.data[np.isnan(pr_a2)] = np.nan
#
wts_o = pr_o.copy()
wts_o.data = np.ones(pr_o.shape)
wts_o = wts_o*np.cos(wts_o.lat*np.pi/180.0)
wts_o.data[np.isnan(pr_o)] = np.nan

###### Save out file. #################################################

print('Saving data...')

# Create xarray dataset.
ds_out = xr.Dataset({'precip_Amazon_shapefile':
                     (['time','lat','lon'],
                      pr_a1),
                     'precip_amazlm_1608':
                     (['time','lat','lon'],
                      pr_a2),
                     'precip_Obidos_shapefile':
                     (['time','lat','lon'],
                      pr_o),
                     'wts_Amazon_shapefile':
                     (['time','lat','lon'],
                      wts_a1),
                     'wts_amazlm_1608':
                     (['time','lat','lon'],
                      wts_a2),
                     'wts_Obidos_shapefile':
                     (['time','lat','lon'],
                      wts_o)},
                    coords={'time':(['time'],pr.coords['time']),
                            'lat':(['lat'],pr.coords['lat']),
                            'lon':(['lon'],pr.coords['lon'])})

# Set up encoding.
t_units = 'days since 1900-01-01 00:00:00'
t_cal = 'standard'
fill_val = 9.96921e+36
wr_enc = {'precip_Amazon_shapefile':{'_FillValue':fill_val},
          'precip_amazlm_1608':{'_FillValue':fill_val},
          'precip_Obidos_shapefile':{'_FillValue':fill_val},
          'wts_Amazon_shapefile':{'_FillValue':fill_val},
          'wts_amazlm_1608':{'_FillValue':fill_val},
          'wts_Obidos_shapefile':{'_FillValue':fill_val},
          'time':{'units':t_units,'calendar':t_cal,
                  '_FillValue':fill_val}}

# Add other metadata.
# Global attributes.
ds_out.attrs['pp_description'] = 'Monthly mean precipitation rate masked to Amazon Basin'
ds_out.attrs['pp_comment'] = 'Post-processing performed by Jack Reeves Eyre (University of Arizona)'
ds_out.attrs['pp_script_repo'] = 'https://bitbucket.org/jackreeveseyre/amazoncongo/src/master/'
ds_out.attrs['pp_last_commit'] = last_hash
ds_out.attrs['pp_script'] = rel_path
ds_out.attrs['pp_script_status'] = info
ds_out.attrs['pp_conda_env'] = os.environ['CONDA_DEFAULT_ENV']
ds_out.attrs['pp_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
ds_out.attrs['pp_GPCP_source_file'] = data_dir + GPCP_file_name
ds_out.attrs['pp_mask_shapefile_a1'] = shp_dir + shp_file_name_a1
ds_out.attrs['pp_mask_shapefile_a2'] = shp_dir + shp_file_name_a2
ds_out.attrs['pp_mask_shapefile_o'] = shp_dir + shp_file_name_o
ds_out.attrs['pp_shapefile_source'] = 'http://www.ore-hybam.org/index.php/eng/Data/Cartography/Amazon-basin-hydrography'
ds_out.attrs['title'] = 'GPCP Version 2.3 Combined Precipitation Dataset (Final)'
ds_out.attrs['version'] = 'V2.3'

# Specify file name.
new_file_name = shp_dir + 'Shapefile_mask_test_' + GPCP_file_name

# Save out the file.
ds_out.to_netcdf(path=new_file_name, mode='w',
                 encoding=wr_enc,unlimited_dims=['time'])
