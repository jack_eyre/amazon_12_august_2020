"""
Plot closure residuals for Amazon basin from different combinations of data, using the Dai et al. 2009 streamflow data set instead of HyBAm.

Run it from plotenv conda environment.
"""

###########################################
import numpy as np
import xarray as xr
import pandas as pd
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
#### Routines from other files: ###########
sys.path.append('/Users/jameseyre/Documents/code/AmazonCongo/')
from GRACE_methods import *
import Amazon_catchment_details
###########################################

################################################################################
def main():
    
    ##### Read data. ###########################################
    print('Reading data...')
    
    # Get streamflow dataset. 
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    
    # Convert time coordinate from yyyymm to datetime.
    newtimecoord = pd.to_datetime([str(int(d)) for d in ds_DT.time.data],
                                  format='%Y%m')
    ds_DT.assign_coords(time=newtimecoord)
    ds_DT.time.attrs['long_name'] = 'time at beginning of month (yyyy-mm-01 00:00:00'
    ds_DT.time.attrs['units'] = ''
    ds_DT.time.data = newtimecoord
    
    # Get streamflow.
    ro_ob = ds_DT.FLOW[:,0]
    
    # Get factor to correct Obidos streamflow to mouth.
    ob_to_mouth = ds_DT.ratio_m2s[0].data
    
    # Scale up Obidos streamflow to river mouth.
    ro_DT = ro_ob.copy()
    ro_DT = ro_DT*ob_to_mouth
    #
    # Calculate annual means.
    ro_ob_ann_mean = ro_ob.groupby('time.month').mean().mean().data
    ro_DT_ann_mean = ro_DT.groupby('time.month').mean().mean().data
    
    # Get other water budget variables (custom load functions below).
    # All in m3 s-1.
    budget_Amazon = get_budget_vars('Amazon')
    budget_Obidos = get_budget_vars('Obidos')
    
    ##### Calculate closure. ###########################################
    
    print('Processing')
    
    # Get (dictionaries of) statistics of closure for each combination.
    resids_Amazon = resids_all(budget_Amazon, ro_DT)
    resids_Obidos = resids_all(budget_Obidos, ro_ob)
    
    # Print out results for all combinations.
    if 0:
        print("Amazon--------------------------------------")
        for comb in resids_Amazon['monthly'].keys():
            print(comb + ':   ' +
                  str(resids_Amazon['monthly'][comb]['Lag1_Autocorr']) +
                  '    (' +
                  str(resids_Amazon['monthly'][comb]['N_SampleSize']) +
                  ')')
        print("Obidos--------------------------------------")
        for comb in resids_Obidos['monthly'].keys():
            print(comb + ':   ' +
                  str(resids_Obidos['monthly'][comb]['Lag1_Autocorr']) +
                  '    (' +
                  str(resids_Obidos['monthly'][comb]['N_SampleSize']) +
                  ')')
    
    # Define standard error (taken from output of "closure_error.py").
    standard_error = 5.31
    
    # Print out results for combinations that meet a criterion.
    if 1:
        print("Amazon--------------------------------------")
        for comb in resids_Amazon['monthly'].keys():
            if (abs(resids_Amazon['monthly'][comb]['mean'])*\
                100.0/ro_DT_ann_mean < 2*standard_error) :
                print(comb + ':   ' +
                      str(resids_Amazon['monthly'][comb]['mean']))
        print("Obidos--------------------------------------")
        for comb in resids_Obidos['monthly'].keys():
            if (abs(resids_Obidos['monthly'][comb]['mean'])*\
                100.0/ro_ob_ann_mean < 2*standard_error) :
                print(comb + ':   ' +
                      str(resids_Obidos['monthly'][comb]['mean']))
    
    ##### Plot results. ###########################################
    
    # Plot type must either be 'MAR' or 'mean'.
    
    if 1:
        print('Plotting')
        plot = plot_resids({'Amazon': resids_Amazon,
                            'Obidos': resids_Obidos},
                           {'Amazon': ro_DT_ann_mean,
                            'Obidos': ro_ob_ann_mean},
                           'mean', standard_error)
    
    ##### For testing only.
    return([resids_Amazon, resids_Obidos, ro_DT_ann_mean, ro_ob_ann_mean])

################################################################################

################################################################################
# Plotting function.

def plot_resids(data_dict, ann_means, stat_type, standard_error):
    #
    # Get some info from data.
    basin_list = list(data_dict.keys())
    label_list = basin_list.copy()
    timescale_list = list(data_dict[basin_list[0]].keys())
    special_colors = {'all_Ens': '#cb4b16',
                      'PE_Ens': '#d33682',
                      'conv_Ens': '#6c71c4',
                      'PE_ERA5_ERA5_JPL':'#2aa198',
                      'PE_GPCP_ERA5_JPL':'#859900'}
    # Set up over all plot.
    # One panel for each of monthly, seasonal and annual (if present).
    fig, axs = plt.subplots(1, len(timescale_list),
                            figsize=(9, 4), sharey=True)
    
    # Loop over time scales.
    for t in range(len(timescale_list)):
        # Initialize variable lists.
        names = []
        values = []
        names_Ens = []
        values_Ens = []
        cols_Ens = []
        # Loop over basins (Amazon, Obidos) and data combinations.
        for b in range(len(basin_list)):
            for comb in data_dict[basin_list[b]][timescale_list[t]]:
                if (comb in special_colors.keys()):
                    # Add highlight values (and ensemble means, monthly only)
                    # to other lists.
                    if (timescale_list[t] == 'monthly' or
                        comb[-3:] != 'Ens'):
                        names_Ens.append(b + 0.3*np.random.rand(1)[0] - 0.15)
                        values_Ens.append(100.0*data_dict[basin_list[b]]\
                                          [timescale_list[t]][comb][stat_type]/
                                          ann_means[basin_list[b]])
                        cols_Ens.append(special_colors[comb])
                else:
                    # Add data to lists.
                    names.append(b + 0.3*np.random.rand(1)[0] - 0.15)
                    values.append(100.0*data_dict[basin_list[b]]\
                                  [timescale_list[t]][comb][stat_type]/
                                  ann_means[basin_list[b]])
        # Make the plot panel.        
        axs[t].scatter(names, values, c='#002b36', s=7, marker='.')
        axs[t].scatter(names_Ens, values_Ens, c=cols_Ens,
                       s=10, marker='s')
        # Set some details.
        axs[t].title.set_text(timescale_list[t])
        if stat_type == 'mean':
            axs[t].set_ylim(-100,100)
            axs[0].set_ylabel('Mean residual / % streamflow')
        else:
            axs[t].set_ylim(0,100)
            axs[0].set_ylabel('Mean absolute residual / % streamflow')
        axs[t].set_xlim(-0.75, len(basin_list)-0.25)
        axs[t].set_xticks(range(len(basin_list)))
        axs[t].set_xticklabels(label_list)
    
    # Add legend.
    legend_entries = []
    for comb in special_colors.keys():
        legend_entries.append(mlines.Line2D([], [],
                                            color=special_colors[comb],
                                            marker='s',
                                            linestyle='None',
                                            label=comb))
    axs[0].legend(handles=legend_entries,
                  loc='upper right',
                  fontsize='small',
                  markerscale=0.7,
                  labelspacing=0.4)
    
    # Add error lines to monthly plot.
    if stat_type == 'mean':
        standard_error = 5.31
        axs[0].plot([-0.75, len(basin_list)-0.25],
                    [2*standard_error,2*standard_error],
                    c='#002b36',linestyle='--',linewidth=1.0,
                    alpha=0.5)
        axs[0].plot([-0.75, len(basin_list)-0.25],
                    [-2*standard_error,-2*standard_error],
                    c='#002b36',linestyle='--',linewidth=1.0,
                    alpha=0.5)
        axs[0].text(-0.7, 2*standard_error + 4.0, r'$\pm 2\ SE$',
                    color='#002b36', fontsize='small')
    
    # Tidy layout.
    fig.tight_layout()
            
    # Get git info for this script.
    sys.path.append('/Users/jameseyre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    
    # Add git info to footer.
    plt.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    
    # Show the plot.
    #plt.show()
    # Or save the figure.
    fig.savefig('/Users/jameseyre/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_' + stat_type + 
                '.pdf',
                format='pdf')
    
    return(fig)

    
################################################################################
# Functions for closure residual statistics.


def resids_all(all_vars, streamflow):
    ###############################################################
    # Returns dictionaries of statistics of closure residuals:
    # Monthly, seasonal and annual,
    # each of which has results from different combinations
    # of budget terms (and each of which is output from
    # resid_stats (below)).
    #
    # Input:
    #     all_vars:     a dictionary of variables output by
    #                   get_budget_vars (below).
    #     streamflow:   an xarray data array of streamflow
    #                   for the relevant catchment.
    #
    # Output:
    #     resids_all:   a dictionary with the following
    #                   structure of sub-dictionaries:
    #     {'monthly': {<'budget_terms_combination_1'>: resid_stats,
    #                  <'budget_terms_combination_2'>: resid_stats,
    #                  ... },
    #      'seasonal': {<'budget_terms_combination_1'>: resid_stats,
    #                   <'budget_terms_combination_2'>: resid_stats,
    #                   ... },
    #      'annual': {<'budget_terms_combination_1'>: resid_stats,
    #                 <'budget_terms_combination_2'>: resid_stats,
    #                  ... }
    #     }
    # The different combinations are named according to their input
    # data and method (e.g., 'PE_GPCP_GLEAM_JPL' [P-E method] or
    # 'conv_ERA5_ERA5_GFZ' [atmospheric convergence method].
    ###############################################################
    
    # Initialize final output dictionary.
    output = {}
    
    # Loop over timesteps.
    freq_list = ['monthly','seasonal','annual']
    for f in freq_list:
        # Initialize sub-dictionary.
        temp = {}
        # Start adding residuals to this.
        # Loop over GRACE methods.
        GRACE_list = ['JPL','CSR','GFZ']
        for g in GRACE_list:
            temp['PE_GPCP_GLEAM_' + g] = resid_stats(
                all_vars['pr_GPCP'],
                all_vars['evap_GLEAM'],
                all_vars['dSdt_' + f[0] + '_' + g],
                streamflow, freq=f)
            temp['PE_CMAP_GLEAM_' + g] = resid_stats(
                all_vars['pr_CMAP'],
                all_vars['evap_GLEAM'],
                all_vars['dSdt_' + f[0] + '_' + g],
                streamflow, freq=f)
            temp['PE_GPCP_ERA5_' + g] = resid_stats(
                all_vars['pr_GPCP'],
                all_vars['evap_ERA5'],
                all_vars['dSdt_' + f[0] + '_' + g],
                streamflow, freq=f)
            temp['PE_CMAP_ERA5_' + g] = resid_stats(
                all_vars['pr_CMAP'],
                all_vars['evap_ERA5'],
                all_vars['dSdt_' + f[0] + '_' + g],
                streamflow, freq=f)
            temp['PE_GPCP_MERRA2_' + g] = resid_stats(
                all_vars['pr_GPCP'],
                all_vars['evap_MERRA2'],
                all_vars['dSdt_' + f[0] + '_' + g],
                streamflow, freq=f)
            temp['PE_CMAP_MERRA2_' + g] = resid_stats(
                all_vars['pr_CMAP'],
                all_vars['evap_MERRA2'],
                all_vars['dSdt_' + f[0] + '_' + g],
                streamflow, freq=f)
            temp['PE_ERA5_ERA5_' + g] = resid_stats(
                all_vars['pr_ERA5'],
                all_vars['evap_ERA5'],
                all_vars['dSdt_' + f[0] + '_' + g],
                streamflow, freq=f)
            temp['PE_MERRA2_MERRA2_' + g] = resid_stats(
                all_vars['pr_MERRA2'],
                all_vars['evap_MERRA2'],
                all_vars['dSdt_' + f[0] + '_' + g],
                streamflow, freq=f)
            temp['conv_MERRA2_MERRA2_' + g] = resid_stats(
                all_vars['viwvc_MERRA2'],
                all_vars['dtcwdt_' + f[0] + '_MERRA2'],
                all_vars['dSdt_' + f[0] + '_' + g],
                streamflow, freq=f, type="conv")
            temp['conv_ERA5_ERA5_' + g] = resid_stats(
                all_vars['vimd_ERA5'],
                all_vars['dtcwdt_' + f[0] + '_ERA5'],
                all_vars['dSdt_' + f[0] + '_' + g],
                streamflow, freq=f, type="conv")
        # Add ensemble means for monthly.
        if (f == 'monthly'):
            temp['PE_Ens'] = resid_stats(
                all_vars['pr_Ens'],
                all_vars['evap_Ens'],
                all_vars['dSdt_m_Ens'],
                streamflow, freq=f)
            temp['conv_Ens'] = resid_stats(
                all_vars['conv_Ens'],
                all_vars['dtcwdt_m_Ens'],
                all_vars['dSdt_m_Ens'],
                streamflow, freq=f)
            temp['all_Ens'] = resid_stats(
                (2.0/5.0)*all_vars['conv_Ens'] +
                (3.0/5.0)*all_vars['pr_Ens'],
                (2.0/5.0)*all_vars['dtcwdt_m_Ens'] +
                (3.0/5.0)*all_vars['evap_Ens'],
                all_vars['dSdt_m_Ens'],
                streamflow, freq=f)
        # Add all these to the final output.
        output[f] = temp
        
    # Return the output.
    return(output)
        
        
    
def resid_stats(p_m, e_m, dSdt, r_m, freq="monthly",type="PE"):
    ###############################################################
    # Returns dictionary of statistics of residuals for closure of
    # given combination of water budget terms:
    #     p_m:   precipitation OR
    #            atmospheric convergence
    #     e_m:   evaporation OR
    #            change in atmospheric precipitable water
    #     dSdt:  change in terrestrial water storage
    #     r_m:   streamflow
    # All variables should be in cubic meters per second.
    #
    # All variables should have a time coordinate with time
    # for each month given at YYYY-MM-01 00:00:00.
    #
    # Optional arguments:
    #     freq: averaging frequency of residual time series.
    #           allowed values: "monthly" (default), "seasonal", "annual"
    #       !!! NOTE: dSdt variable frequency must match this value;
    #                 other arguments are always passed as monthly.
    #     type: "PE" or "conv": if "PE", does seasonal/annual averaging
    #           for the second argument (evap); if "conv", does not do
    #           seasonal/annual averaging for the second argument
    #           (change in atmospheric precipitable water) as this is
    #           passed at the relevant time scale already.
    #
    ###################
    # Output variables:
    #     mean:  mean value of residual
    #     MAR:   mean absolute residual
    #     StDev: standard deviation of residuals about the mean
    #     RMS:   root mean square of residuals (about zero)
    ###############################################################
    #
    if type == "PE":
        if freq == "seasonal":
            p = p_m.resample(time='QS-DEC').mean()
            e = e_m.resample(time='QS-DEC').mean()
            r = r_m.resample(time='QS-DEC').mean()
        elif freq == "annual":
            p = p_m.groupby('time.year').mean()
            p = p.rename({"year":"time"})
            e = e_m.groupby('time.year').mean()
            e = e.rename({"year":"time"})
            r = r_m.groupby('time.year').mean()
            r = r.rename({"year":"time"})
            dSdt = dSdt.rename({"year":"time"})
        elif freq == "monthly":
            p = p_m
            e = e_m
            r = r_m
        else:
            print("Invalid option for freq")
            p = np.nan
            e = np.nan
            r = np.nan
    elif type == "conv":
        if freq == "seasonal":
            p = p_m.resample(time='QS-DEC').mean()
            e = e_m
            r = r_m.resample(time='QS-DEC').mean()
        elif freq == "annual":
            p = p_m.groupby('time.year').mean()
            p = p.rename({"year":"time"})
            r = r_m.groupby('time.year').mean()
            r = r.rename({"year":"time"})
            e = e_m
            e = e.rename({"year":"time"})
            dSdt = dSdt.rename({"year":"time"})
        elif freq == "monthly":
            p = p_m
            e = e_m
            r = r_m
        else:
            print("Invalid option for freq")
            p = np.nan
            e = np.nan
            r = np.nan
    else:
        print("Invalid option for type")
        p = np.nan
        e = np.nan
        r = np.nan        
        #
    resid = p - e - dSdt - r
    #
    m = resid.mean('time')
    ma = np.abs(resid).mean('time')
    std = np.std(resid)
    rms = np.sqrt((resid*resid).mean('time'))
    #
    resid_dropna = resid.dropna(dim='time')
    autocorr = np.corrcoef(resid_dropna[:-1],resid_dropna[1:])[0,1]
    N_sample = len(resid_dropna)
    #
    stats = {
        "mean": m.data,
        "MAR": ma.data,
        "StDev": std.data,
        "RMS": rms.data,
        "Lag1_Autocorr": autocorr,
        "N_SampleSize":N_sample
        }
    #
    return(stats)

################################################################################
# Functions for reading individual datasets.

#### GPCP precipitation ####
def load_GPCP(catchment):
    filename = '~/Data/satellite/GPCP/' + catchment + \
               'BasinAverage_GPCP.v2.3.precip.mon.mean.nc'
    ds = xr.open_dataset(filename)
    output = ds.precip
    return output

#### CMAP precipitation ####
def load_CMAP(catchment):
    filename = '~/Data/satellite/CMAP/' + catchment + \
               'BasinAverage_precip.mon.mean.std.nc'
    ds = xr.open_dataset(filename)
    output = ds.precip
    return output

#### GLEAM evaporation ####
def load_GLEAM(catchment):
    filename = '~/Data/gridded-obs/GLEAM_v3.3/' + catchment + \
               'BasinAverage_E_2003_2018_GLEAM_v3.3b_MO.nc'
    ds = xr.open_dataset(filename)
    output = ds.E
    output = output.assign_coords(time=output.time -
                                  (output.time.dt.day-1).
                                  astype('timedelta64[D]') -
                                  (output.time.dt.hour).
                                   astype('timedelta64[h]'))
    return output

#### ERA5, all variables. ####
def load_ERA5(catchment):
    filename = '~/Data/reanalysis/ERA5/' + catchment + \
               'BasinAverage_era5_moda_sfc_2000-2018.nc'
    ds = xr.open_dataset(filename)
    e = ds.e
    p = ds.tp
    tcwv = ds.tcwv
    vimd = ds.vimd
    output = (p, e, tcwv, vimd)
    return output

#### MERRA2, all variables. ####
def load_MERRA2(catchment):
    filename = '~/Data/reanalysis/MERRA2/Amazon/' + catchment + \
               'BasinAverage_MERRA2_2000-2018.nc'
    ds = xr.open_dataset(filename)
    ds = ds.assign_coords(time=ds.time - np.timedelta64(30,'m'))
    p = ds.PRECTOTCORR
    e = ds.EVAP
    tcwv = ds.TQV
    viwvc = ds.DQVDT_DYN
    output = (p, e, tcwv, viwvc)
    return output

#### Wrapper for all variables. ####
def get_budget_vars(catchment):
    
    # Get basin area.
    basin_area = Amazon_catchment_details.areas[catchment]*1000000.0
    
    # Load variables using scripts from above.
    pr_GPCP = load_GPCP(catchment)
    pr_CMAP = load_CMAP(catchment)
    evap_GLEAM = load_GLEAM(catchment)
    (pr_ERA5, evap_ERA5, tcw_ERA5, vimd_ERA5) = load_ERA5(catchment)
    (pr_MERRA2, evap_MERRA2, tcw_MERRA2, viwvc_MERRA2) = load_MERRA2(catchment)
    
    # Get GRACE data from subroutine.
    TWS_JPL = load_GRACE_monthly(catchment,'JPL')
    TWS_CSR = load_GRACE_monthly(catchment,'CSR')
    TWS_GFZ = load_GRACE_monthly(catchment,'GFZ')
    
    # GRACE rate of change calculation (and units conversion).
    # Monthly:
    dSdt_m_JPL = deltaS_m_cen4(TWS_JPL)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_m_CSR = deltaS_m_cen4(TWS_CSR)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_m_GFZ = deltaS_m_cen4(TWS_GFZ)*(basin_area/(100.0*24.0*60.0*60.0))
    # Seasonal:
    dSdt_s_JPL = deltaS_s_for2(TWS_JPL)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_s_CSR = deltaS_s_for2(TWS_CSR)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_s_GFZ = deltaS_s_for2(TWS_GFZ)*(basin_area/(100.0*24.0*60.0*60.0))
    # Annual:
    dSdt_a_JPL = deltaS_a_for2(TWS_JPL)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_a_CSR = deltaS_a_for2(TWS_CSR)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_a_GFZ = deltaS_a_for2(TWS_GFZ)*(basin_area/(100.0*24.0*60.0*60.0))
    
    # Atmospheric reanalysis rate of change calculation (and units conversion).
    # Monthly:
    dtcwdt_m_ERA5 = deltaS_m_cen4(tcw_ERA5)*\
                    (basin_area/(1000.0*24.0*60.0*60.0))
    dtcwdt_m_MERRA2 = deltaS_m_cen4(tcw_MERRA2)*\
                      (basin_area/(1000.0*24.0*60.0*60.0))
    # Seasonal:
    dtcwdt_s_ERA5 = deltaS_s_for2(tcw_ERA5)*\
                    (basin_area/(1000.0*24.0*60.0*60.0))
    dtcwdt_s_MERRA2 = deltaS_s_for2(tcw_MERRA2)*\
                      (basin_area/(1000.0*24.0*60.0*60.0))
    # Annual:
    dtcwdt_a_ERA5 = deltaS_a_for2(tcw_ERA5)*\
                    (basin_area/(1000.0*24.0*60.0*60.0))
    dtcwdt_a_MERRA2 = deltaS_a_for2(tcw_MERRA2)*\
                      (basin_area/(1000.0*24.0*60.0*60.0))
    
    # Perform units conversions.
    # (everything to cubic meters per second).
    pr_GPCP = pr_GPCP*(basin_area/(1000.0*24.0*60.0*60.0))
    pr_CMAP = pr_CMAP*(basin_area/(1000.0*24.0*60.0*60.0))
    pr_ERA5 = pr_ERA5*(basin_area/(1000.0*24.0*60.0*60.0))
    pr_MERRA2 = pr_MERRA2*(basin_area/(1000.0*24.0*60.0*60.0))
    evap_GLEAM = evap_GLEAM*(basin_area/(1000.0*24.0*60.0*60.0))
    evap_ERA5 = evap_ERA5*(-1.0*basin_area/(1000.0*24.0*60.0*60.0))
    evap_MERRA2 = evap_MERRA2*(basin_area/(1000.0*24.0*60.0*60.0))
    vimd_ERA5 = vimd_ERA5*(-1.0*basin_area/(1000.0*24.0*60.0*60.0))
    viwvc_MERRA2 = viwvc_MERRA2*(basin_area/(1000.0*24.0*60.0*60.0))
    
    # Construct dictionary of data.
    budget_vars = {}
    budget_vars['pr_GPCP'] = pr_GPCP
    budget_vars['pr_CMAP'] = pr_CMAP
    budget_vars['pr_ERA5'] = pr_ERA5
    budget_vars['pr_MERRA2'] = pr_MERRA2
    budget_vars['evap_GLEAM'] = evap_GLEAM
    budget_vars['evap_ERA5'] = evap_ERA5
    budget_vars['evap_MERRA2'] = evap_MERRA2
    budget_vars['vimd_ERA5'] = vimd_ERA5
    budget_vars['viwvc_MERRA2'] = viwvc_MERRA2
    budget_vars['dSdt_m_JPL'] = dSdt_m_JPL
    budget_vars['dSdt_m_CSR'] = dSdt_m_CSR
    budget_vars['dSdt_m_GFZ'] = dSdt_m_GFZ
    budget_vars['dSdt_s_JPL'] = dSdt_s_JPL
    budget_vars['dSdt_s_CSR'] = dSdt_s_CSR
    budget_vars['dSdt_s_GFZ'] = dSdt_s_GFZ
    budget_vars['dSdt_a_JPL'] = dSdt_a_JPL
    budget_vars['dSdt_a_CSR'] = dSdt_a_CSR
    budget_vars['dSdt_a_GFZ'] = dSdt_a_GFZ
    budget_vars['dtcwdt_m_ERA5'] = dtcwdt_m_ERA5
    budget_vars['dtcwdt_m_MERRA2'] = dtcwdt_m_MERRA2
    budget_vars['dtcwdt_s_ERA5'] = dtcwdt_s_ERA5
    budget_vars['dtcwdt_s_MERRA2'] = dtcwdt_s_MERRA2
    budget_vars['dtcwdt_a_ERA5'] = dtcwdt_a_ERA5
    budget_vars['dtcwdt_a_MERRA2'] = dtcwdt_a_MERRA2
    
    # Construct ensemble means.
    budget_vars['pr_Ens'] = (pr_GPCP + pr_CMAP + pr_ERA5 + pr_MERRA2)/4.0
    budget_vars['evap_Ens'] = (evap_GLEAM + evap_ERA5 + evap_MERRA2)/3.0
    budget_vars['dSdt_m_Ens'] = (dSdt_m_JPL + dSdt_m_CSR + dSdt_m_GFZ)/3.0
    budget_vars['dSdt_s_Ens'] = (dSdt_s_JPL + dSdt_s_CSR + dSdt_s_GFZ)/3.0
    budget_vars['dSdt_a_Ens'] = (dSdt_a_JPL + dSdt_a_CSR + dSdt_a_GFZ)/3.0
    budget_vars['conv_Ens'] = (vimd_ERA5 + viwvc_MERRA2)/2.0
    budget_vars['dtcwdt_m_Ens'] = (dtcwdt_m_ERA5 + dtcwdt_m_MERRA2)/2.0
    budget_vars['dtcwdt_s_Ens'] = (dtcwdt_s_ERA5 + dtcwdt_s_MERRA2)/2.0
    budget_vars['dtcwdt_a_Ens'] = (dtcwdt_a_ERA5 + dtcwdt_a_MERRA2)/2.0
    
    # Return data.
    return(budget_vars)

################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    [resids_Amazon, resids_Obidos, ro_DT_ann_mean, ro_ob_ann_mean] = main()

