"""
Create a NetCDF file of Amazon basin mean GPCP precipitation. 
"""

###########################################
# For data analysis:
import numpy as np
import xarray as xr
import datetime
import subprocess
import sys
import os
import salem
###########################################

# Get git info for this script.
sys.path.append('/Users/jameseyre/Documents/code/')
from python_git_tools import git_rev_info
[info, last_hash, rel_path, clean] = git_rev_info(os.path.realpath(__file__))


###### Load data. #################################################

print('Reading data...')

# Data locations.
data_dir = '/Users/jameseyre/Data/satellite/GPCP/'
shp_dir = '/Users/jameseyre/Data/in-situ/hybam/'
shp_file_name = 'Amazon_catchment.shp'
GPCP_rg_file_name = 'regrid_area_ave_test__GPCP.v2.3.precip.mon.mean.nc'
GPCP_file_name = 'GPCP.v2.3.precip.mon.mean.nc'

# Read the GRACE file. 
ds_in = salem.open_xr_dataset(data_dir + GPCP_file_name)
pr = ds_in.precip
ds_in_hi = salem.open_xr_dataset(data_dir + GPCP_rg_file_name)
pr_hi = ds_in_hi.precip

# Sort out the longitude array order in GRACE data. 
pr = pr.assign_coords(lon=(((pr.lon + 180) % 360) - 180))
pre = pr.isel(lon=slice(0,72))
prw = pr.isel(lon=slice(72,144))
pr = xr.concat([prw,pre],'lon')

###### Mask and average. #################################################

print('Processing data...')

# Apply the masking.
sf = salem.read_shapefile(shp_dir + shp_file_name)
sf.crs = {'init': 'epsg:4326'}
pr_am = pr.salem.roi(shape=sf)
pr_am_hi = pr_hi.salem.roi(shape=sf)

# Calcualte cosine weights.
wts = pr_am.copy()
wts.data = np.ones(pr_am.shape)
wts = wts*np.cos(wts.lat*np.pi/180.0)
wts2 = wts.copy()
wts.data[np.isnan(pr_am)] = np.nan
wts2.data = np.where(np.isnan(pr_am), np.nan, wts2.data)
print(np.allclose(wts.data, wts2.data, equal_nan=True))
#
wts_hi = pr_am_hi.copy()
wts_hi.data = np.ones(pr_am_hi.shape)
wts_hi = wts_hi*np.cos(wts_hi.lat*np.pi/180.0)
wts_hi.data = np.where(np.isnan(pr_am_hi), np.nan, wts_hi.data)

# Get sum of cosine weights per time step.
sum_wts = np.sum(wts.isel(time=0))
sum_wts_hi = np.sum(wts_hi.isel(time=0))

# Calculate area averaged LWE.
pr_mean = pr_am.mean(dim=['lat','lon'])
pr_wt_mean = (wts*pr_am).sum(dim=['lat','lon'])/sum_wts
pr_hi_wt_mean = (wts_hi*pr_am_hi).sum(dim=['lat','lon'])/sum_wts_hi

print(pr_wt_mean[-20:])
print(pr_hi_wt_mean)




