"""
Plot maps to visualize masking properties of salem with different shapefiles. 
"""

###########################################
#### For data analysis:
import numpy as np
import xarray as xr
import xesmf as xe
import iris
import datetime
from iris.time import PartialDateTime
import cf_units
import glob
import scipy.stats
import os
import sys
import subprocess
#### For plotting:
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.colors as mcolors
from matplotlib import cm, gridspec, rcParams, colors
from mpl_toolkits.axes_grid1 import AxesGrid
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import cartopy.feature as cfeature
import scipy.ndimage as ndimage
import shapefile
import matplotlib.patches as patches
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
################################################################################

# Main script.
def main():
    #
    # Get git info for this script.
    sys.path.append('/Users/jameseyre/Documents/code/')
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    #
    ############################################
    # Read data.
    print('Reading data...')
    #
    data_dir = '~/Data/in-situ/hybam/'
    filename = 'Shapefile_mask_test_GPCP.v2.3.precip.mon.mean.nc'
    ds_in = xr.open_dataset(data_dir + filename)
    #
    shp_dir = '/Users/jameseyre/Data/in-situ/hybam/'
    shp_file_Amazon = 'Amazon_catchment.shp'
    shp_file_Obidos = 'Obidos_catchment.shp'
    ptchs_amazon = get_sf_patches(shp_dir + shp_file_Amazon)
    ptchs_obidos = get_sf_patches(shp_dir + shp_file_Obidos)
    #
    pr_a1 = ds_in.precip_Amazon_shapefile
    pr_a2 = ds_in.precip_amazlm_1608
    pr_o = ds_in.precip_Obidos_shapefile
    wts_a1 = ds_in.wts_Amazon_shapefile
    wts_a2 = ds_in.wts_amazlm_1608
    wts_o = ds_in.wts_Obidos_shapefile
    #
    plot_vars = [pr_a1,pr_a2,pr_o,wts_a1,wts_a2,wts_o]
    plot_names = ['Amazon','amazlm_1608','Obidos']
    #
    # Select which timestep to plot.
    it = 400 # must be in 0 to 462.
    ############################################
    # Plot maps.
    print('Plotting...')
    #
    # 2d lat and lon arrays for plotting.
    dlatm, dlonm = np.meshgrid(pr_a1.lat,pr_a1.lon,indexing='ij')
    #
    # Define color maps.
    cmap_r = plt.get_cmap('Blues')
    norm_r = colors.BoundaryNorm(np.arange(0.0, 10.01, 1.0),
                                 ncolors=cmap_r.N)
    cmap_w = plt.get_cmap('BuPu')
    norm_w = colors.BoundaryNorm(np.arange(0.9, 1.01, 0.01),
                                 ncolors=cmap_w.N)
    #
    # Define map projection.
    projection = ccrs.PlateCarree()
    axes_class = (GeoAxes, dict(map_projection=projection))
    #
    # Open figure.
    fig = plt.figure(figsize=(9, 4.5))
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(2, 3),
                    axes_pad=0.25,
                    cbar_location='right',
                    cbar_mode='edge',
                    cbar_pad=0.1,
                    cbar_size='3%',
                    direction='row',
                    label_mode='')  # note the empty label_mode
    #
    # Loop over variables and plot maps.
    for i, ax in enumerate(axgr):
        ax.set_extent([-90, -35, -25, 15], projection)
        ax.coastlines()
        ax.add_feature(cfeature.LAND, facecolor='lightgray')
        ax.set_xticks(np.array([-60, -45, -30, -15]), crs=projection)
        ax.set_yticks(np.array([-15, 0, 15]), crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=False,
                                           degree_symbol='')
        lat_formatter = LatitudeFormatter(degree_symbol='')
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        ax.tick_params(axis='both', bottom=True, top=True,
                       left=True, right=True)
        # Draw the plots.
        if i < 3:
            ax.set_title(plot_names[i])
            p_r = ax.pcolormesh(dlonm, dlatm, plot_vars[i][it,:,:],
                                vmin=0.0, vmax=10.0, 
                                transform=projection,
                                cmap=cmap_r, norm=norm_r)
        else:
            ax.set_title('')
            p_w = ax.pcolormesh(dlonm, dlatm, plot_vars[i][it,:,:],
                                vmin=0.9, vmax=1.0, 
                                transform=projection,
                                cmap=cmap_w, norm=norm_w)
        #
        # Add the shapefiles.
        if (i == 2) or (i == 5):
            ax.add_collection(PatchCollection(ptchs_obidos,
                                              facecolor=mcolors.colorConverter.
                                              to_rgba('white', alpha=0.0),
                                              edgecolor=mcolors.colorConverter.
                                              to_rgba('black', alpha=1.0),
                                              linewidths=1.0))
        else:
            ax.add_collection(PatchCollection(ptchs_amazon,
                                              facecolor=mcolors.colorConverter.
                                              to_rgba('white', alpha=0.0),
                                              edgecolor=mcolors.colorConverter.
                                              to_rgba('black', alpha=1.0),
                                              linewidths=1.0))
    #
    #
    # Make ticklabels on inner axes invisible
    axes = np.reshape(axgr, axgr.get_geometry())
    for ax in axes[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelleft=False, labelright=False)
    for ax in axes[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    #
    # Draw the color bars (shared by rows of plots).
    axgr.cbar_axes[0].colorbar(p_r)
    axgr.cbar_axes[1].colorbar(p_w)
    #
    # Tidy layout.
    fig.tight_layout(pad=5.0,h_pad=1.0,w_pad=1.0)
    #
    # Add git info to figure.
    fig.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    #
    # Display the plot.
    #plt.show()
    # or save as PDF.
    fig.savefig('/Users/jameseyre/Documents/plots/AmazonCongo/' +
                os.path.splitext(os.path.basename(__file__))[0] +
                '.png',
                format='png')


################################################################################
# Function to get shapefile patches.
################################################################################
def get_sf_patches(filename):
    # Routine taken from https://stackoverflow.com/questions/15968762/shapefile-and-matplotlib-plot-polygon-collection-of-shapefile-coordinates
    sf = shapefile.Reader(filename)
    recs    = sf.records()
    shapes  = sf.shapes()
    Nshp    = len(shapes)
    cns     = []
    for nshp in range(Nshp):
        cns.append(recs[nshp][1])
    cns = np.array(cns)
    for nshp in range(Nshp):
        ptchs   = []
        pts     = np.array(shapes[nshp].points)
        prt     = shapes[nshp].parts
        par     = list(prt) + [pts.shape[0]]
        for pij in range(len(prt)):
            ptchs.append(Polygon(pts[par[pij]:par[pij+1]]))
    return ptchs
    

################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    main()
