"""
Plot closure residuals for Amazon basin from different combinations of data.
Plot residuals month-by-month, with 12 panels in the plot (one per month).

Run it from plotenv conda environment.
"""

###########################################
import numpy as np
import xarray as xr
import scipy.stats
import datetime
import calendar
import os
import sys
import subprocess
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
#### Routines from other files: ###########
from GRACE_methods import *
import Amazon_catchment_details
###########################################

################################################################################
def main():
    
    ##### Read data. ###########################################
    print('Reading data...')
    
    # Get streamflow data. 
    hybam_filename = '~/Data/in-situ/hybam/Amazon_Obidos.nc'
    ds_hy = xr.open_dataset(hybam_filename)
    ro_ob = ds_hy.Q
    
    # Change time coordinate of streamflow.
    ro_ob = ro_ob.assign_coords(time=ro_ob.time -
                                (ro_ob.time.dt.day-1).astype('timedelta64[D]'))
    
    # Get factor to correct Obidos streamflow to mouth.
    DaiTren_filename = '~/Data/in-situ/DaiTrenberth/coastal-stns-Vol-monthly.updated-Aug2014.nc'
    ds_DT = xr.open_dataset(DaiTren_filename,decode_times=False)
    ob_to_mouth = ds_DT.ratio_m2s[0].data
    
    # Scale up Obidos streamflow to river mouth.
    ro_DT = ro_ob.copy()
    ro_DT = ro_DT*ob_to_mouth
    #
    # Calculate annual means.
    ro_ob_ann_mean = ro_ob.groupby('time.month').mean().mean().data
    ro_DT_ann_mean = ro_DT.groupby('time.month').mean().mean().data
    
    # Get other water budget variables (custom load functions below).
    # All in m3 s-1.
    budget_Amazon = get_budget_vars('Amazon')
    budget_Obidos = get_budget_vars('Obidos')
    
    ##### Calculate closure. ###########################################
    
    print('Processing...')
    
    # Get (dictionaries of) statistics of closure for each combination.
    resids_Obidos = resids_all(budget_Obidos, ro_ob, plot_dists=False)
    resids_Amazon = resids_all(budget_Amazon, ro_DT, plot_dists=False)
    
    ##### Plot results. ###########################################
    
    # Plot type must be 'mean'.
    
    if 1:
        print('Plotting...')
        plot = plot_resids({'Amazon': resids_Amazon,
                            'Obidos': resids_Obidos},
                           {'Amazon': ro_DT_ann_mean,
                            'Obidos': ro_ob_ann_mean},
                           'mean')
    
    ##### For testing only.
    return([resids_Amazon, resids_Obidos, ro_DT_ann_mean, ro_ob_ann_mean])

################################################################################

################################################################################
# Plotting function.

def plot_resids(data_dict, ann_means, stat_type):
    #
    # Get some info from data.
    basin_list = list(data_dict.keys())
    label_list = basin_list.copy()
    color_list = ['#2aa198',
                  '#d33682']
    
    # Set up over all plot.
    # One panel for each of monthly, seasonal and annual (if present).
    fig, axs = plt.subplots(3,4,
                            figsize=(9, 6), sharey=True, sharex=True)
    # Sort out ticklabel and axis label appearance.
    for ax in axs[:,:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 left=True, right=True)
        ax.xaxis.set_tick_params(which='both', 
                                 bottom=True, top=True)
        if stat_type == 'mean':
            ax.set_ylim(-100,100)
        else:
            ax.set_ylim(0,100)
        ax.set_xlim(-0.75, len(basin_list)-0.25)
    for ax in axs[:-1, :].flatten():
        ax.xaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    for ax in axs[:, 1:].flatten():
        ax.yaxis.set_tick_params(which='both', 
                                 labelbottom=False, labeltop=False)
    #for ax in axs[:,0].flatten():
    axs[1,0].set_ylabel('Mean residual /\n % streamflow')
    for ax in axs[-1,:].flatten():
        ax.set_xticks(range(len(basin_list)))
        ax.set_xticklabels(label_list)
    
    # Loop over time scales.
    for t in range(1,13):
        # Initialize variable lists.
        names = []
        values = []
        cols = []
        # Loop over basins (Amazon, Obidos) and data combinations.
        for b in range(len(basin_list)):
            for comb in data_dict[basin_list[b]]['monthly']:
                # Add data to lists.
                names.append(b + 0.3*np.random.rand(1)[0] - 0.15)
                values.append(100.0*data_dict[basin_list[b]]\
                              ['monthly'][comb][stat_type].sel(month=t)/
                              ann_means[basin_list[b]])
                cols.append(color_list[data_dict[basin_list[b]]\
                                       ['monthly']\
                                       [comb]\
                                       ['T_test_sig_diff'].sel(month=t).data])
        # Make the plot panel.        
        axs.flatten()[t-1].scatter(names, values, c=cols, s=10, marker='.')
        # Set title.
        axs.flatten()[t-1].set_title(calendar.month_abbr[t], loc='left')
    
    # Tidy layout.
    fig.tight_layout()
            
    # Get git info for this script.
    sys.path.append(os.path.expanduser('~/Documents/code/'))
    from python_git_tools import git_rev_info
    [txtl, last_hash, rel_path, clean] = \
        git_rev_info(os.path.realpath(__file__))
    
    # Add git info to footer.
    plt.text(0.02,0.01,txtl, transform=fig.transFigure, size=4)
    
    # Show the plot.
    #plt.show()
    # Or save the figure.
    fig.savefig(os.path.expanduser('~/Documents/plots/AmazonCongo/') +
                os.path.splitext(os.path.basename(__file__))[0] +
                '_' + stat_type + 
                '.pdf',
                format='pdf')
    
    return(fig)

    
################################################################################
# Functions for closure residual statistics.


def resids_all(all_vars, streamflow, plot_dists=False):
    ###############################################################
    # Returns dictionaries of statistics of closure residuals:
    # Monthly, seasonal and annual,
    # each of which has results from different combinations
    # of budget terms (and each of which is output from
    # resid_stats (below)).
    #
    # Input:
    #     all_vars:     a dictionary of variables output by
    #                   get_budget_vars (below).
    #     streamflow:   an xarray data array of streamflow
    #                   for the relevant catchment.
    #     plot_dists:   flag passed to other functions to
    #                   say whether to plot discharge distributions.
    #
    # Output:
    #     resids_all:   a dictionary with the following
    #                   structure of sub-dictionaries:
    #     {'monthly': {<'budget_terms_combination_1'>: resid_stats,
    #                  <'budget_terms_combination_2'>: resid_stats,
    #                  ... },
    #      }
    # The different combinations are named according to their input
    # data and method (e.g., 'PE_GPCP_GLEAM_JPL' [P-E method] or
    # 'conv_ERA5_ERA5_GFZ' [atmospheric convergence method].
    ###############################################################
    
    # Initialize final output dictionary.
    output = {}
    
    # Loop over timesteps.
    freq_list = ['monthly']
    for f in freq_list:
        # Initialize sub-dictionary.
        temp = {}
        # Start adding residuals to this.
        # Loop over GRACE methods.
        GRACE_list = ['JPL']
        pr_list = ['GPCP', 'CMAP', 'CHIRPS', 'ERA5', 'MERRA2']
        evap_list = ['GLEAM', 'CLM', 'Noah', 'ERA5', 'MERRA2']
        for g in GRACE_list:
            for p in pr_list:
                for e in evap_list:
                    c_name = 'PE_' + p + '_' + e + '_' + g
                    temp[c_name] = resid_stats(
                        all_vars['pr_' + p],
                        all_vars['evap_' + e],
                        all_vars['dSdt_' + f[0] + '_' + g],
                        streamflow, freq=f,
                        plot_dists=plot_dists,
                        comb_name=c_name)
            c_name = 'conv_MERRA2_MERRA2_' + g
            temp[c_name] = resid_stats(
                all_vars['viwvc_MERRA2'],
                all_vars['dtcwdt_' + f[0] + '_MERRA2'],
                all_vars['dSdt_' + f[0] + '_' + g],
                streamflow, freq=f, type="conv",
                plot_dists=plot_dists,
                comb_name=c_name)
            c_name = 'conv_ERA5_ERA5_' + g
            temp[c_name] = resid_stats(
                all_vars['vimd_ERA5'],
                all_vars['dtcwdt_' + f[0] + '_ERA5'],
                all_vars['dSdt_' + f[0] + '_' + g],
                streamflow, freq=f, type="conv",
                plot_dists=plot_dists,
                comb_name=c_name)
        # Add ensemble means.
        c_name = 'PE_Ens'
        temp[c_name] = resid_stats(
            all_vars['pr_Ens'],
            all_vars['evap_Ens'],
            all_vars['dSdt_' + f[0] + '_Ens'],
            streamflow, freq=f,
            plot_dists=plot_dists,
            comb_name=c_name)
        c_name = 'conv_Ens'
        temp[c_name] = resid_stats(
            all_vars['conv_Ens'],
            all_vars['dtcwdt_' + f[0] + '_Ens'],
            all_vars['dSdt_' + f[0] + '_Ens'],
            streamflow, freq=f, type="conv",
            plot_dists=plot_dists,
            comb_name=c_name)
        # Add all these to the final output.
        output[f] = temp
        
    # Return the output.
    return(output)
        
        
    
def resid_stats(p_m, e_m, dSdt, r_m, freq="monthly",type="PE",
                plot_dists=False, comb_name='unknown'):
    ###############################################################
    # Returns dictionary of statistics of residuals for closure of
    # given combination of water budget terms:
    #     p_m:   precipitation OR
    #            atmospheric convergence
    #     e_m:   evaporation OR
    #            change in atmospheric precipitable water
    #     dSdt:  change in terrestrial water storage
    #     r_m:   streamflow
    # All variables should be in cubic meters per second.
    #
    # All variables should have a time coordinate with time
    # for each month given at YYYY-MM-01 00:00:00.
    #
    # Optional arguments:
    #     freq: averaging frequency of residual time series.
    #           allowed values: "monthly" (default), "seasonal", "annual"
    #       !!! NOTE: dSdt variable frequency must match this value;
    #                 other arguments are always passed as monthly.
    #     type: "PE" or "conv": if "PE", does seasonal/annual averaging
    #           for the second argument (evap); if "conv", does not do
    #           seasonal/annual averaging for the second argument
    #           (change in atmospheric precipitable water) as this is
    #           passed at the relevant time scale already.
    #     plot_dists: boolean: whether or not to plot individual monthly/
    #                 seasonal/annual distributions for this combination
    #     comb_name: string: used in plotting and plot file name.
    #                Ignored if plot_dists=False.
    #
    ###################
    # Output variables:
    #     mean:  mean value of residual
    #     MAR:   mean absolute residual
    #     StDev: standard deviation of residuals about the mean
    #     RMS:   root mean square of residuals (about zero)
    ###############################################################
    #
    if type == "PE":
        if freq == "seasonal":
            p = seasonal_mean(p_m)
            e = seasonal_mean(e_m)
            r = seasonal_mean(r_m)
        elif freq == "annual":
            p = annual_mean(p_m)
            e = annual_mean(e_m)
            r = annual_mean(r_m)
            dSdt = dSdt.rename({"year":"time"})
        elif freq == "monthly":
            p = p_m
            e = e_m
            r = r_m
        else:
            print("Invalid option for freq")
            p = np.nan
            e = np.nan
            r = np.nan
    elif type == "conv":
        if freq == "seasonal":
            p = seasonal_mean(p_m)
            e = e_m
            r = seasonal_mean(r_m)
        elif freq == "annual":
            p = annual_mean(p_m)
            r = annual_mean(r_m)
            e = e_m
            e = e.rename({"year":"time"})
            dSdt = dSdt.rename({"year":"time"})
        elif freq == "monthly":
            p = p_m
            e = e_m
            r = r_m
        else:
            print("Invalid option for freq")
            p = np.nan
            e = np.nan
            r = np.nan
    else:
        print("Invalid option for type")
        p = np.nan
        e = np.nan
        r = np.nan        
        #
    resid = p - e - dSdt - r
    #
    m = resid.groupby('time.month').mean('time')
    #
    #
    ttest = T_test_diff(p - e - dSdt, r, freq)
    #
    stats = {
        "mean": m,
        "T_test_sig_diff": ttest
        }
    #
    return(stats)


def T_test_diff(R_hat, R, freq):
    """Counts how many months have significant difference between 
     water-budget-estimated and observed discharge amounts. 
     It test separately for each month. 

    Significance level is hard-coded in this function.

    Input:
        R_hat -- water-budget-estimated discharge (xarray data array).
        R     -- observed discharge (xarray data array).
        freq  -- the frequency of the data inputs.
                 Must be 'monthly' in this version.
    Output:
        sig_diff -- An array saying which months have significant differences
                    between R and R_hat: 0 -> not sig., 1-> sig.
    """
    #
    # Define significance level.
    alpha = 0.05
    #
    # Subset to period of overlap.
    # (This isn't mathematically necessary but probably makes most sense.)
    st_time = np.maximum(np.min(R_hat.time), np.min(R.time))
    end_time = np.minimum(np.max(R_hat.time), np.max(R.time))
    R = R.sel(time=slice(st_time,end_time))
    R_hat = R_hat.sel(time=slice(st_time,end_time))
    #
    # Initialize array to hold results.
    sig_diff = xr.DataArray(np.zeros([12],dtype=int),
                            coords=[range(1,13)],
                            dims=['month'])
    #
    # Loop over periods (depending on freq).
    if freq == 'monthly':
        for m in range(1,13):
            t_stat,p_value = scipy.stats.ttest_ind(\
                R.data[R.time.dt.month == m],
                R_hat.data[R_hat.time.dt.month == m],
                axis=0,
                equal_var=False,
                nan_policy='omit')
            if np.isnan(p_value):
                sig_diff[m-1] = np.nan
            elif p_value < alpha:
               sig_diff[m-1] = 1
    else:
        print("Invalid option for freq")
        sig_diff.data[:] = np.nan
        #
    return sig_diff


# Simple function to return seasonal mean with NaN value if not
# all months are present for each season.
# Input is a 1D time series as an xarray data array.
# Output is a 1D time series with one data point per season.
def seasonal_mean(ts):
    ts_s = ts.resample(time='QS-DEC').mean(dim='time')
    ts_count = ts.resample(time='QS-DEC').count(dim='time')
    ts_s[ts_count < 3] = np.nan
    return ts_s

# Simple function to return annual mean with NaN value if not
# all months are present for each year.
# Input is a 1D time series as an xarray data array.
# Output is a 1D time series with one data point per season.
def annual_mean(ts):
    ts_a = ts.groupby('time.year').mean(dim='time')
    ts_count = ts.groupby('time.year').count(dim='time')
    ts_a[ts_count < 12] = np.nan
    ts_a = ts_a.rename({"year":"time"})
    return ts_a


################################################################################
# Functions for reading individual datasets.

#### GPCP precipitation ####
def load_GPCP(catchment):
    filename = '~/Data/satellite/GPCP/' + catchment + \
               'BasinAverage_GPCP.v2.3.precip.mon.mean.nc'
    ds = xr.open_dataset(filename)
    output = ds.precip
    return output

#### CHIRPS precipitation ####
def load_CHIRPS(catchment):
    filename = '~/Data/satellite/CHIRPS/' + catchment + \
               'BasinAverage_chirps-v2.0.monthly.nc'
    ds = xr.open_dataset(filename)
    output = ds.precip
    for m in output.time:
        output.loc[dict(time=m)] = output.loc[dict(time=m)]/\
            calendar.monthrange(m.dt.year.data, m.dt.month.data)[1]
    output.attrs['units'] = 'mm/day'
    return output

#### CMAP precipitation ####
def load_CMAP(catchment):
    filename = '~/Data/satellite/CMAP/' + catchment + \
               'BasinAverage_precip.mon.mean.std.nc'
    ds = xr.open_dataset(filename)
    output = ds.precip
    return output

#### GLEAM evaporation ####
def load_GLEAM(catchment):
    filename = '~/Data/gridded-obs/GLEAM_v3.3/' + catchment + \
               'BasinAverage_E_2003_2018_GLEAM_v3.3b_MO.nc'
    ds = xr.open_dataset(filename)
    output = ds.E
    output = output.assign_coords(time=output.time -
                                  (output.time.dt.day-1).
                                  astype('timedelta64[D]') -
                                  (output.time.dt.hour).
                                   astype('timedelta64[h]'))
    return output

#### CLM evaporation ####
def load_CLM(catchment):
    filename = '~/Data/reanalysis/GLDAS/CLM_v2.0/' + catchment + \
               'BasinAverage_GLDAS_CLM10_M.A197901-202001.001.nc4'
    ds = xr.open_dataset(filename)
    output = ds.E
    output = output.assign_coords(time=output.time -
                                  (output.time.dt.hour).
                                   astype('timedelta64[h]'))
    return output

#### Noah evaporation ####
def load_Noah(catchment):
    filename = '~/Data/reanalysis/GLDAS/Noah_v2.1/' + catchment + \
               'BasinAverage_GLDAS_NOAH10_M.A200001-202001.021.nc4'
    ds = xr.open_dataset(filename)
    output = ds.E
    return output

#### ERA5, all variables. ####
def load_ERA5(catchment):
    filename = '~/Data/reanalysis/ERA5/' + catchment + \
               'BasinAverage_era5_moda_sfc_2000-2018.nc'
    ds = xr.open_dataset(filename)
    e = ds.e
    p = ds.tp
    tcwv = ds.tcwv
    vimd = ds.vimd
    output = (p, e, tcwv, vimd)
    return output

#### MERRA2, all variables. ####
def load_MERRA2(catchment):
    filename = '~/Data/reanalysis/MERRA2/Amazon/' + catchment + \
               'BasinAverage_MERRA2_2000-2018.nc'
    ds = xr.open_dataset(filename)
    ds = ds.assign_coords(time=ds.time - np.timedelta64(30,'m'))
    p = ds.PRECTOTCORR
    e = ds.EVAP
    tcwv = ds.TQV
    viwvc = ds.DQVDT_DYN
    output = (p, e, tcwv, viwvc)
    return output

#### Wrapper for all variables. ####
def get_budget_vars(catchment):
    
    # Get basin area.
    basin_area = Amazon_catchment_details.areas[catchment]*1000000.0
    
    # Load variables using scripts from above.
    pr_GPCP = load_GPCP(catchment)
    pr_CMAP = load_CMAP(catchment)
    pr_CHIRPS = load_CHIRPS(catchment)
    evap_GLEAM = load_GLEAM(catchment)
    evap_CLM = load_CLM(catchment)
    evap_Noah = load_Noah(catchment)
    (pr_ERA5, evap_ERA5, tcw_ERA5, vimd_ERA5) = load_ERA5(catchment)
    (pr_MERRA2, evap_MERRA2, tcw_MERRA2, viwvc_MERRA2) = load_MERRA2(catchment)
    
    # Get GRACE data from subroutine.
    TWS_JPL = load_GRACE_monthly(catchment,'JPL')
    TWS_CSR = load_GRACE_monthly(catchment,'CSR')
    TWS_GFZ = load_GRACE_monthly(catchment,'GFZ')
    
    # GRACE rate of change calculation (and units conversion).
    # Monthly:
    dSdt_m_JPL = deltaS_m_cen4(TWS_JPL)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_m_CSR = deltaS_m_cen4(TWS_CSR)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_m_GFZ = deltaS_m_cen4(TWS_GFZ)*(basin_area/(100.0*24.0*60.0*60.0))
    # Seasonal:
    dSdt_s_JPL = deltaS_s_for2(TWS_JPL)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_s_CSR = deltaS_s_for2(TWS_CSR)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_s_GFZ = deltaS_s_for2(TWS_GFZ)*(basin_area/(100.0*24.0*60.0*60.0))
    # Annual:
    dSdt_a_JPL = deltaS_a_for2(TWS_JPL)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_a_CSR = deltaS_a_for2(TWS_CSR)*(basin_area/(100.0*24.0*60.0*60.0))
    dSdt_a_GFZ = deltaS_a_for2(TWS_GFZ)*(basin_area/(100.0*24.0*60.0*60.0))
    
    # Atmospheric reanalysis rate of change calculation (and units conversion).
    # Monthly:
    dtcwdt_m_ERA5 = deltaS_m_cen4(tcw_ERA5)*\
                    (basin_area/(1000.0*24.0*60.0*60.0))
    dtcwdt_m_MERRA2 = deltaS_m_cen4(tcw_MERRA2)*\
                      (basin_area/(1000.0*24.0*60.0*60.0))
    # Seasonal:
    dtcwdt_s_ERA5 = deltaS_s_for2(tcw_ERA5)*\
                    (basin_area/(1000.0*24.0*60.0*60.0))
    dtcwdt_s_MERRA2 = deltaS_s_for2(tcw_MERRA2)*\
                      (basin_area/(1000.0*24.0*60.0*60.0))
    # Annual:
    dtcwdt_a_ERA5 = deltaS_a_for2(tcw_ERA5)*\
                    (basin_area/(1000.0*24.0*60.0*60.0))
    dtcwdt_a_MERRA2 = deltaS_a_for2(tcw_MERRA2)*\
                      (basin_area/(1000.0*24.0*60.0*60.0))
    
    # Perform units conversions.
    # (everything to cubic meters per second).
    pr_GPCP = pr_GPCP*(basin_area/(1000.0*24.0*60.0*60.0))
    pr_CMAP = pr_CMAP*(basin_area/(1000.0*24.0*60.0*60.0))
    pr_CHIRPS = pr_CHIRPS*(basin_area/(1000.0*24.0*60.0*60.0))
    pr_ERA5 = pr_ERA5*(basin_area/(1000.0*24.0*60.0*60.0))
    pr_MERRA2 = pr_MERRA2*(basin_area/(1000.0*24.0*60.0*60.0))
    evap_GLEAM = evap_GLEAM*(basin_area/(1000.0*24.0*60.0*60.0))
    evap_CLM = evap_CLM*(basin_area/(1000.0*24.0*60.0*60.0))
    evap_Noah = evap_Noah*(basin_area/(1000.0*24.0*60.0*60.0))
    evap_ERA5 = evap_ERA5*(-1.0*basin_area/(1000.0*24.0*60.0*60.0))
    evap_MERRA2 = evap_MERRA2*(basin_area/(1000.0*24.0*60.0*60.0))
    vimd_ERA5 = vimd_ERA5*(-1.0*basin_area/(1000.0*24.0*60.0*60.0))
    viwvc_MERRA2 = viwvc_MERRA2*(basin_area/(1000.0*24.0*60.0*60.0))
    
    # Construct dictionary of data.
    budget_vars = {}
    budget_vars['pr_GPCP'] = pr_GPCP
    budget_vars['pr_CMAP'] = pr_CMAP
    budget_vars['pr_CHIRPS'] = pr_CHIRPS
    budget_vars['pr_ERA5'] = pr_ERA5
    budget_vars['pr_MERRA2'] = pr_MERRA2
    budget_vars['evap_GLEAM'] = evap_GLEAM
    budget_vars['evap_CLM'] = evap_CLM
    budget_vars['evap_Noah'] = evap_Noah
    budget_vars['evap_ERA5'] = evap_ERA5
    budget_vars['evap_MERRA2'] = evap_MERRA2
    budget_vars['vimd_ERA5'] = vimd_ERA5
    budget_vars['viwvc_MERRA2'] = viwvc_MERRA2
    budget_vars['dSdt_m_JPL'] = dSdt_m_JPL
    budget_vars['dSdt_m_CSR'] = dSdt_m_CSR
    budget_vars['dSdt_m_GFZ'] = dSdt_m_GFZ
    budget_vars['dSdt_s_JPL'] = dSdt_s_JPL
    budget_vars['dSdt_s_CSR'] = dSdt_s_CSR
    budget_vars['dSdt_s_GFZ'] = dSdt_s_GFZ
    budget_vars['dSdt_a_JPL'] = dSdt_a_JPL
    budget_vars['dSdt_a_CSR'] = dSdt_a_CSR
    budget_vars['dSdt_a_GFZ'] = dSdt_a_GFZ
    budget_vars['dtcwdt_m_ERA5'] = dtcwdt_m_ERA5
    budget_vars['dtcwdt_m_MERRA2'] = dtcwdt_m_MERRA2
    budget_vars['dtcwdt_s_ERA5'] = dtcwdt_s_ERA5
    budget_vars['dtcwdt_s_MERRA2'] = dtcwdt_s_MERRA2
    budget_vars['dtcwdt_a_ERA5'] = dtcwdt_a_ERA5
    budget_vars['dtcwdt_a_MERRA2'] = dtcwdt_a_MERRA2
    
    # Construct ensemble means.
    budget_vars['pr_Ens'] = (pr_GPCP + pr_CMAP + pr_CHIRPS +
                             pr_ERA5 + pr_MERRA2)/5.0
    budget_vars['evap_Ens'] = (evap_GLEAM + evap_Noah + evap_CLM +
                               evap_ERA5 + evap_MERRA2)/5.0
    budget_vars['dSdt_m_Ens'] = (dSdt_m_JPL + dSdt_m_CSR + dSdt_m_GFZ)/3.0
    budget_vars['dSdt_s_Ens'] = (dSdt_s_JPL + dSdt_s_CSR + dSdt_s_GFZ)/3.0
    budget_vars['dSdt_a_Ens'] = (dSdt_a_JPL + dSdt_a_CSR + dSdt_a_GFZ)/3.0
    budget_vars['conv_Ens'] = (vimd_ERA5 + viwvc_MERRA2)/2.0
    budget_vars['dtcwdt_m_Ens'] = (dtcwdt_m_ERA5 + dtcwdt_m_MERRA2)/2.0
    budget_vars['dtcwdt_s_Ens'] = (dtcwdt_s_ERA5 + dtcwdt_s_MERRA2)/2.0
    budget_vars['dtcwdt_a_Ens'] = (dtcwdt_a_ERA5 + dtcwdt_a_MERRA2)/2.0
    
    # Return data.
    return(budget_vars)

################################################################################

###########################################
# Now actually execute the script.
###########################################
if __name__ == '__main__':
    [resids_Amazon, resids_Obidos, ro_DT_ann_mean, ro_ob_ann_mean] = main()

